unit uProgress;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxProgressBar;

type
  TfrmProgress = class(TForm)
    Progress: TcxProgressBar;
  private
    class var
      FInstance: TfrmProgress;
  public
    class procedure Start(AMin, AMax: Integer);
    class procedure Step(AStep: Integer = 1);
    class procedure Stop;
    class constructor Create;
    class destructor Destroy;
  end;

implementation

{$R *.dfm}

{ TfrmProgress }

class constructor TfrmProgress.Create;
begin
end;

class destructor TfrmProgress.Destroy;
begin
  if Assigned(FInstance) then
    FInstance.Free;
end;

class procedure TfrmProgress.Stop;
begin
  FInstance.Hide;
  FInstance.Update;
end;

class procedure TfrmProgress.Start(AMin, AMax: Integer);
begin
  if not Assigned(FInstance) then
    FInstance := TfrmProgress.Create(nil);
  FInstance.Progress.Properties.Min := AMin;
  FInstance.Progress.Position := AMin;
  FInstance.Progress.Properties.Max := AMax;
  FInstance.Show;
  FInstance.Update;
end;

class procedure TfrmProgress.Step(AStep: Integer = 1);
begin
  FInstance.Progress.Position := FInstance.Progress.Position + AStep;
  FInstance.Update;
end;

end.
