object fMain: TfMain
  Left = 0
  Top = 0
  Caption = 'API Looper'
  ClientHeight = 823
  ClientWidth = 998
  Color = clBtnFace
  Constraints.MinWidth = 1010
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  TextHeight = 13
  object StatusBar: TdxStatusBar
    Left = 0
    Top = 800
    Width = 998
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Rows'
        Width = 100
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Queue'
        Width = 100
      end
      item
        PanelStyleClassName = 'TdxStatusBarStateIndicatorPanelStyle'
        PanelStyle.Indicators = <
          item
            IndicatorType = sitYellow
          end>
        Text = 'State'
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'PrimaryKey'
        Width = 200
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        PanelStyle.Alignment = taRightJustify
        Text = 'Version'
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitTop = 799
    ExplicitWidth = 994
  end
  object pcApp: TcxPageControl
    Left = 0
    Top = 0
    Width = 998
    Height = 800
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = tsData
    Properties.CustomButtons.Buttons = <>
    Properties.TabSlants.Positions = [spLeft, spRight]
    OnChange = pcAppChange
    OnPageChanging = pcAppPageChanging
    ExplicitWidth = 994
    ExplicitHeight = 799
    ClientRectBottom = 796
    ClientRectLeft = 4
    ClientRectRight = 994
    ClientRectTop = 24
    object tsData: TcxTabSheet
      Caption = '&Data'
      ImageIndex = 0
      ExplicitWidth = 986
      ExplicitHeight = 771
      object acQueryBuilder1: TacQueryBuilder
        Left = 25
        Top = 32
        Width = 518
        Height = 107
        SyntaxProvider = acAdvantageSyntaxProvider1
        MetadataFilter = <>
        LinkPainterClassName = 'TacQueryBuilderLinkPainterAccess'
        LinkPainter.LinkColor = clBlack
        BkImagePos = bkipTopLeft
        BkImageDarkness = bgidWhite
        AddObjectFormOptions.Constraints.MinHeight = 430
        AddObjectFormOptions.Constraints.MinWidth = 430
        TreeOptions.UnionsNodeText = 'UNIONS'
        TreeOptions.FieldsNodeText = 'FIELDS'
        TreeOptions.CTEsNodeText = 'CTE'
        TreeOptions.SubQueriesNodeText = 'Subqueries in Expressions'
        TreeOptions.FromNodeText = 'FROM'
        TreeOptionsMetadata.TablesNodeName = 'Tables'
        TreeOptionsMetadata.ViewsNodeName = 'Views'
        TreeOptionsMetadata.ProceduresNodeName = 'Procedures'
        TreeOptionsMetadata.SynonymsNodeName = 'Synonyms'
        SelectListOptions.ToolbarImageUp.Data = {
          22030000424D22030000000000003600000028000000160000000B0000000100
          180000000000EC020000C10E0000C10E00000000000000000000F18814F18814
          F18814F18814F18814F18814F18814F18814F18814F18814F18814F18814F188
          14F18814F18814F18814F18814F18814F18814F18814F18814F188140000F188
          14F18814919191919191919191919191919191919191919191F18814F18814F1
          8814F18814919191919191919191919191919191919191919191F18814F18814
          0000F18814919191BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF919191
          F18814F18814919191F18814F18814F18814F18814F18814F18814F188149191
          91F188140000F18814919191CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CC919191F18814F18814919191F18814F18814F18814F18814F18814F18814F1
          8814919191F188140000F18814919191D8D8D8404040D8D8D8D8D8D8D8D8D840
          4040D8D8D8919191F18814F18814919191F18814919191F18814F18814F18814
          919191F18814919191F188140000F18814919191E5E5E5E5E5E5404040E5E5E5
          404040E5E5E5E5E5E5919191F18814F18814919191F18814F18814919191F188
          14919191F18814F18814919191F188140000F18814919191E5E5E5E5E5E5E5E5
          E5404040E5E5E5E5E5E5E5E5E5919191F18814F18814919191F18814F18814F1
          8814919191F18814F18814F18814919191F188140000F18814919191E5E5E5E5
          E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5919191F18814F18814919191F18814
          F18814F18814F18814F18814F18814F18814919191F188140000F18814919191
          E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5919191F18814F188149191
          91F18814F18814F18814F18814F18814F18814F18814919191F188140000F188
          14F18814919191919191919191919191919191919191919191F18814F18814F1
          8814F18814919191919191919191919191919191919191919191F18814F18814
          0000F18814F18814F18814F18814F18814F18814F18814F18814F18814F18814
          F18814F18814F18814F18814F18814F18814F18814F18814F18814F18814F188
          14F188140000}
        SelectListOptions.ToolbarImageDown.Data = {
          22030000424D22030000000000003600000028000000160000000B0000000100
          180000000000EC020000C10E0000C10E00000000000000000000F18814F18814
          F18814F18814F18814F18814F18814F18814F18814F18814F18814F18814F188
          14F18814F18814F18814F18814F18814F18814F18814F18814F188140000F188
          14F18814919191919191919191919191919191919191919191F18814F18814F1
          8814F18814919191919191919191919191919191919191919191F18814F18814
          0000F18814919191BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF919191
          F18814F18814919191F18814F18814F18814F18814F18814F18814F188149191
          91F188140000F18814919191CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CC919191F18814F18814919191F18814F18814F18814F18814F18814F18814F1
          8814919191F188140000F18814919191D8D8D8D8D8D8D8D8D8404040D8D8D8D8
          D8D8D8D8D8919191F18814F18814919191F18814F18814F18814919191F18814
          F18814F18814919191F188140000F18814919191E5E5E5E5E5E5404040E5E5E5
          404040E5E5E5E5E5E5919191F18814F18814919191F18814F18814919191F188
          14919191F18814F18814919191F188140000F18814919191E5E5E5404040E5E5
          E5E5E5E5E5E5E5404040E5E5E5919191F18814F18814919191F18814919191F1
          8814F18814F18814919191F18814919191F188140000F18814919191E5E5E5E5
          E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5919191F18814F18814919191F18814
          F18814F18814F18814F18814F18814F18814919191F188140000F18814919191
          E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5919191F18814F188149191
          91F18814F18814F18814F18814F18814F18814F18814919191F188140000F188
          14F18814919191919191919191919191919191919191919191F18814F18814F1
          8814F18814919191919191919191919191919191919191919191F18814F18814
          0000F18814F18814F18814F18814F18814F18814F18814F18814F18814F18814
          F18814F18814F18814F18814F18814F18814F18814F18814F18814F18814F188
          14F188140000}
        SelectListOptions.ToolbarImageInsert.Data = {
          22030000424D22030000000000003600000028000000160000000B0000000100
          180000000000EC020000C20E0000C20E00000000000000000000E8A200E8A200
          E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A2
          00E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A2000000E8A2
          00E8A200919191919191919191919191919191919191919191E8A200E8A200E8
          A200E8A200919191919191919191919191919191919191919191E8A200E8A200
          0000E8A200919191BFFFBFBFFFBFBFFFBFBFFFBFBFFFBFBFFFBFBFFFBF919191
          E8A200E8A200919191E8A200E8A200E8A200E8A200E8A200E8A200E8A2009191
          91E8A2000000E8A200919191CCFFCCCCFFCCCCFFCC008000CCFFCCCCFFCCCCFF
          CC919191E8A200E8A200919191E8A200E8A200E8A200919191E8A200E8A200E8
          A200919191E8A2000000E8A200919191D8FFD8D8FFD8D8FFD8008000D8FFD8D8
          FFD8D8FFD8919191E8A200E8A200919191E8A200E8A200E8A200919191E8A200
          E8A200E8A200919191E8A2000000E8A200919191E5FFE5009700009700009700
          009700009700E5FFE5919191E8A200E8A200919191E8A2009191919191919191
          91919191919191E8A200919191E8A2000000E8A200919191E5FFE5E5FFE5E5FF
          E5008000E5FFE5E5FFE5E5FFE5919191E8A200E8A200919191E8A200E8A200E8
          A200919191E8A200E8A200E8A200919191E8A2000000E8A200919191E5FFE5E5
          FFE5E5FFE5008000E5FFE5E5FFE5E5FFE5919191E8A200E8A200919191E8A200
          E8A200E8A200919191E8A200E8A200E8A200919191E8A2000000E8A200919191
          E5FFE5E5FFE5E5FFE5E5FFE5E5FFE5E5FFE5E5FFE5919191E8A200E8A2009191
          91E8A200E8A200E8A200E8A200E8A200E8A200E8A200919191E8A2000000E8A2
          00E8A200919191919191919191919191919191919191919191E8A200E8A200E8
          A200E8A200919191919191919191919191919191919191919191E8A200E8A200
          0000E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A200
          E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A200E8A2
          00E8A2000000}
        SelectListOptions.ToolbarImageDelete.Data = {
          22030000424D22030000000000003600000028000000160000000B0000000100
          180000000000EC020000C30E0000C30E00000000000000000000F18814F18814
          F18814F18814F18814F18814F18814F18814F18814F18814F18814F18814F188
          14F18814F18814F18814F18814F18814F18814F18814F18814F188140000F188
          14F18814919191919191919191919191919191919191919191F18814F18814F1
          8814F18814919191919191919191919191919191919191919191F18814F18814
          0000F18814919191BFBFFFBFBFFFBFBFFFBFBFFFBFBFFFBFBFFFBFBFFF919191
          F18814F18814919191F18814F18814F18814F18814F18814F18814F188149191
          91F188140000F18814919191CCCCFF0000BF9696FFCCCCFF9696FF0000BFCCCC
          FF919191F18814F18814919191F18814919191F18814F18814F18814919191F1
          8814919191F188140000F18814919191D8D8FF9696FF0000BF9696FF0000BF96
          96FFD8D8FF919191F18814F18814919191F18814F18814919191F18814919191
          F18814F18814919191F188140000F18814919191E5E5FFE5E5FF9696FF0000BF
          9696FFE5E5FFE5E5FF919191F18814F18814919191F18814F18814F188149191
          91F18814F18814F18814919191F188140000F18814919191E5E5FF9696FF0000
          BF9696FF0000BF9696FFE5E5FF919191F18814F18814919191F18814F1881491
          9191F18814919191F18814F18814919191F188140000F18814919191E5E5FF00
          00BF9696FFE5E5FF9696FF0000BFE5E5FF919191F18814F18814919191F18814
          919191F18814F18814F18814919191F18814919191F188140000F18814919191
          E5E5FFE5E5FFE5E5FFE5E5FFE5E5FFE5E5FFE5E5FF919191F18814F188149191
          91F18814F18814F18814F18814F18814F18814F18814919191F188140000F188
          14F18814919191919191919191919191919191919191919191F18814F18814F1
          8814F18814919191919191919191919191919191919191919191F18814F18814
          0000F18814F18814F18814F18814F18814F18814F18814F18814F18814F18814
          F18814F18814F18814F18814F18814F18814F18814F18814F18814F18814F188
          14F188140000}
        SelectListOptions.ToolbarImageCollapse.Data = {
          DE000000424DDE000000000000003600000028000000040000000E0000000100
          180000000000A800000000000000000000000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF616161FF00FFFF00FF7E7E7E62
          6262FF00FF969696999999696969B1B1B1ABABABB1B1B1717171FF00FFA8A8A8
          A8A8A87A7A7AFF00FFFF00FFA6A6A6848484FF00FFFF00FFFF00FF8E8E8EFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FF}
        SelectListOptions.ToolbarImageExpand.Data = {
          DE000000424DDE000000000000003600000028000000040000000E0000000100
          180000000000A800000000000000000000000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FF616161FF00FFFF00FFFF00FF6262627E7E7EFF00FFFF
          00FF696969999999969696FF00FF717171B1B1B1ABABABB1B1B17A7A7AA8A8A8
          A8A8A8FF00FF848484A6A6A6FF00FFFF00FF8E8E8EFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FF}
        FieldsListOptions.MarkColumnOptions.PKIcon.Data = {
          07544269746D617076010000424D760100000000000036000000280000000A00
          00000A0000000100180000000000400100000000000000000000000000000000
          0000FF808055A7D8519AD1FF8080FF8080FF8080FF8080FF8080FF8080FF8080
          00006EBFE886E9F94DD9F54397CEFF8080FF8080FF8080FF8080FF8080FF8080
          00006ABAE6A1E6F838D2F247D6F64298CEFF8080FF8080FF8080FF8080FF8080
          0000FF80805FB0E399E2F653DCF546D9F63F94CE478ED3488FD6FF8080FF8080
          0000FF8080FF80806CBBE766C1E85ED9F24EDBF65BDDF755D8F53483CEFF8080
          0000FF8080FF8080FF808094C9EB89DDF46AE0F673E2F75FDFF655DAF64185CF
          0000FF8080FF8080FF808078BEE7A9EEF97EE6F89AE8F87ED1F080E2F64A9EDB
          0000FF8080FF8080FF8080ABD5EF5EC1EAA3F0FB80D4F07EC7EC56A6DEFF8080
          0000FF8080FF8080FF8080FF8080A1CEED6FC9ECC9F3FB62BEE8FF8080FF8080
          0000FF8080FF8080FF8080FF8080FF808090C8EB6BBCE7FF8080FF8080FF8080
          0000}
        FieldsListOptions.DescriptionColumnOptions.FontColor = clGrayText
        TabOrder = 0
        Visible = False
      end
      object lblSQL: TcxLabel
        Left = 0
        Top = 0
        Align = alTop
        AutoSize = False
        Caption = 'SQL'
        Properties.WordWrap = True
        ExplicitWidth = 986
        Height = 33
        Width = 990
      end
      object cxSplitterpnlMemo: TcxSplitter
        Left = 0
        Top = 527
        Width = 990
        Height = 8
        HotZoneClassName = 'TcxMediaPlayer9Style'
        AlignSplitter = salBottom
        AutoSnap = True
        Control = pnlMemo
        ExplicitTop = 526
        ExplicitWidth = 986
      end
      object cxSplitterpnlBtm: TcxSplitter
        Left = 0
        Top = 519
        Width = 990
        Height = 8
        AlignSplitter = salBottom
        ExplicitTop = 518
        ExplicitWidth = 986
      end
      object pnlData: TPanel
        Left = 0
        Top = 33
        Width = 990
        Height = 486
        Align = alClient
        Caption = 'pnlData'
        TabOrder = 4
        ExplicitWidth = 986
        ExplicitHeight = 485
        object pnlAPIStatus: TPanel
          Left = 608
          Top = 1
          Width = 381
          Height = 442
          Align = alRight
          TabOrder = 0
          ExplicitLeft = 604
          ExplicitHeight = 441
          object grdAPIStatus: TcxGrid
            Left = 1
            Top = 1
            Width = 379
            Height = 377
            Align = alClient
            TabOrder = 0
            ExplicitHeight = 376
            object dbtvAPIStatus: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              Navigator.Visible = True
              FindPanel.DisplayMode = fpdmManual
              ScrollbarAnnotations.CustomAnnotations = <>
              OnFocusedRecordChanged = dbtvAPIStatusFocusedRecordChanged
              DataController.DataSource = dsAPIStatus
              DataController.Filter.OnChanged = dbtvAPIStatusDataControllerFilterChanged
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.BestFitMaxRecordCount = 50
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsSelection.MultiSelect = True
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
            end
            object glAPIStatus: TcxGridLevel
              GridView = dbtvAPIStatus
            end
          end
          object pnlAPIStatusBottom: TPanel
            Left = 1
            Top = 378
            Width = 379
            Height = 63
            Align = alBottom
            TabOrder = 1
            ExplicitTop = 377
            object cbFilterAPIStatusByDataRec: TcxCheckBox
              Left = 5
              Top = 8
              Caption = 'Show Only Statuses of The Selected Data Record '
              Style.TransparentBorder = False
              TabOrder = 0
              Transparent = True
              OnClick = cbFilterAPIStatusByDataRecClick
            end
            object cbFilterAPIStatusByLocation: TcxCheckBox
              Left = 5
              Top = 31
              Caption = 'Show Only Statuses of The Selected Location'
              Style.TransparentBorder = False
              TabOrder = 1
              Transparent = True
              OnClick = cbFilterAPIStatusByLocationClick
            end
          end
        end
        object grdData: TcxGrid
          Left = 1
          Top = 1
          Width = 599
          Height = 442
          Align = alClient
          TabOrder = 1
          ExplicitWidth = 595
          ExplicitHeight = 441
          object dbtvData: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Visible = True
            FindPanel.DisplayMode = fpdmManual
            ScrollbarAnnotations.CustomAnnotations = <>
            OnFocusedRecordChanged = dbtvDataFocusedRecordChanged
            DataController.DataSource = dsData
            DataController.Filter.OnChanged = dbtvDataDataControllerFilterChanged
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.BestFitMaxRecordCount = 50
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
          end
          object glData: TcxGridLevel
            GridView = dbtvData
          end
        end
        object pnlButtons: TPanel
          AlignWithMargins = True
          Left = 7
          Top = 449
          Width = 976
          Height = 30
          Margins.Left = 6
          Margins.Top = 6
          Margins.Right = 6
          Margins.Bottom = 6
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          ExplicitTop = 448
          ExplicitWidth = 972
          DesignSize = (
            976
            30)
          object btnOpen: TButton
            Left = 4
            Top = 2
            Width = 75
            Height = 26
            Margins.Left = 6
            Margins.Right = 6
            Margins.Bottom = 6
            Caption = 'Load Data'
            TabOrder = 0
            OnClick = btnOpenClick
          end
          object btnAPITrans: TButton
            Left = 91
            Top = 2
            Width = 75
            Height = 26
            Margins.Left = 6
            Margins.Right = 6
            Margins.Bottom = 6
            Caption = 'API Translate '
            TabOrder = 1
            OnClick = btnAPITransClick
          end
          object btnAPIAll: TButton
            Left = 259
            Top = 2
            Width = 75
            Height = 26
            Margins.Left = 6
            Margins.Right = 6
            Margins.Bottom = 6
            Caption = 'API All'
            TabOrder = 3
            OnClick = btnAPIAllClick
          end
          object btnAPIselect: TButton
            Left = 178
            Top = 2
            Width = 75
            Height = 26
            Margins.Left = 6
            Margins.Right = 6
            Margins.Bottom = 6
            Caption = 'API Selected'
            TabOrder = 2
            OnClick = btnAPIselectClick
          end
          object seRecno: TSpinEdit
            AlignWithMargins = True
            Left = 496
            Top = 2
            Width = 97
            Height = 22
            Margins.Top = 6
            Margins.Bottom = 6
            MaxValue = 0
            MinValue = 0
            TabOrder = 4
            Value = 0
          end
          object chkRecno: TcxCheckBox
            AlignWithMargins = True
            Left = 608
            Top = 2
            Caption = 'Start From Row'
            Style.TransparentBorder = False
            TabOrder = 5
          end
          object btnRunTests: TButton
            Left = 707
            Top = 2
            Width = 62
            Height = 26
            Caption = 'Test'
            TabOrder = 6
            OnClick = btnRunTestsClick
          end
          object btnClearLog: TButton
            AlignWithMargins = True
            Left = 905
            Top = 2
            Width = 71
            Height = 26
            Margins.Top = 6
            Margins.Bottom = 6
            Anchors = [akTop, akRight]
            Caption = 'ClearLog'
            TabOrder = 7
            OnClick = btnClearLogClick
            ExplicitLeft = 901
          end
          object rbRunForAllLocations: TcxRadioButton
            Left = 347
            Top = -2
            Width = 137
            Height = 17
            Caption = 'Run For All Locations'
            Checked = True
            TabOrder = 8
            TabStop = True
          end
          object rbRunForFocusedLocation: TcxRadioButton
            Left = 347
            Top = 14
            Width = 144
            Height = 17
            Caption = 'Run For Focused Location'
            TabOrder = 9
          end
          object btnAPISelectedStatRecs: TButton
            Left = 780
            Top = 2
            Width = 116
            Height = 26
            Margins.Left = 6
            Margins.Right = 6
            Margins.Bottom = 6
            Anchors = [akTop, akRight]
            Caption = 'API Selected Statuses'
            TabOrder = 10
            OnClick = btnAPISelectedStatRecsClick
            ExplicitLeft = 776
          end
        end
        object cxSplitterpnlAPIStatus: TcxSplitter
          Left = 600
          Top = 1
          Width = 8
          Height = 442
          AlignSplitter = salRight
          ExplicitLeft = 596
          ExplicitHeight = 441
        end
      end
      object pnlMemo: TPanel
        AlignWithMargins = True
        Left = 6
        Top = 538
        Width = 978
        Height = 86
        Margins.Left = 6
        Margins.Right = 6
        Margins.Bottom = 6
        Align = alBottom
        TabOrder = 5
        ExplicitTop = 537
        ExplicitWidth = 974
        object mmoTransl: TMemo
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 434
          Height = 78
          Align = alLeft
          ScrollBars = ssVertical
          TabOrder = 0
        end
        object mmoSuccess: TMemo
          AlignWithMargins = True
          Left = 452
          Top = 4
          Width = 245
          Height = 78
          Align = alClient
          ScrollBars = ssVertical
          TabOrder = 1
          WordWrap = False
          ExplicitWidth = 241
        end
        object mmoError: TMemo
          AlignWithMargins = True
          Left = 711
          Top = 4
          Width = 263
          Height = 78
          Align = alRight
          ScrollBars = ssVertical
          TabOrder = 2
          WordWrap = False
          ExplicitLeft = 707
        end
        object cxSplitter1: TcxSplitter
          Left = 441
          Top = 1
          Width = 8
          Height = 84
        end
        object cxSplitter2: TcxSplitter
          Left = 700
          Top = 1
          Width = 8
          Height = 84
          AlignSplitter = salRight
          ExplicitLeft = 696
        end
      end
      object pgcQueues: TcxPageControl
        Left = 0
        Top = 630
        Width = 990
        Height = 142
        Align = alBottom
        TabOrder = 6
        Properties.ActivePage = tsHostQueue
        Properties.CustomButtons.Buttons = <>
        Properties.HideTabs = True
        Properties.TabPosition = tpLeft
        ExplicitTop = 629
        ExplicitWidth = 986
        ClientRectBottom = 138
        ClientRectLeft = 4
        ClientRectRight = 986
        ClientRectTop = 4
        object tsHostQueues: TcxTabSheet
          Caption = 'tsHostQueues'
          ImageIndex = 0
          DesignSize = (
            982
            134)
          object btnClearAllQue: TButton
            Left = 887
            Top = 9
            Width = 89
            Height = 26
            Margins.Top = 6
            Margins.Bottom = 6
            Anchors = [akTop, akRight]
            Caption = 'Clear All Queues'
            TabOrder = 0
            OnClick = btnClearAllQueClick
          end
          object btnPauseAll: TButton
            Left = 887
            Top = 42
            Width = 88
            Height = 26
            Margins.Top = 6
            Margins.Bottom = 6
            Anchors = [akTop, akRight]
            Caption = 'Pause All'
            TabOrder = 1
            OnClick = btnPauseAllClick
          end
          object btnResumeAll: TButton
            Left = 887
            Top = 78
            Width = 88
            Height = 26
            Margins.Top = 6
            Margins.Bottom = 6
            Anchors = [akTop, akRight]
            Caption = 'Resume All'
            TabOrder = 2
            OnClick = btnResumeAllClick
          end
          object grdHostQueues: TcxGrid
            Left = 10
            Top = 0
            Width = 864
            Height = 126
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 3
            LookAndFeel.ScrollbarMode = sbmClassic
            object dbtvHostQueues: TcxGridDBTableView
              OnMouseLeave = dbtvHostQueuesMouseLeave
              OnMouseMove = dbtvHostQueuesMouseMove
              Navigator.Buttons.CustomButtons = <>
              Navigator.Buttons.Insert.Enabled = False
              Navigator.Buttons.Insert.Visible = False
              Navigator.Buttons.Append.Enabled = False
              Navigator.Buttons.Append.Visible = False
              Navigator.Buttons.Delete.Enabled = False
              Navigator.Buttons.Delete.Visible = False
              Navigator.Buttons.Edit.Enabled = False
              Navigator.Buttons.Edit.Visible = False
              Navigator.Buttons.Post.Enabled = False
              Navigator.Buttons.Post.Visible = False
              Navigator.Buttons.Cancel.Enabled = False
              Navigator.Buttons.Cancel.Visible = False
              Navigator.Buttons.SaveBookmark.Visible = True
              Navigator.Buttons.GotoBookmark.Visible = True
              ScrollbarAnnotations.CustomAnnotations = <>
              OnCellClick = dbtvHostQueuesCellClick
              DataController.DataSource = dsHostQueues
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <
                item
                  Links = <
                    item
                    end>
                  SummaryItems = <
                    item
                      Kind = skCount
                    end>
                end>
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsSelection.InvertSelect = False
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              object dbtvHostQueuesname: TcxGridDBColumn
                DataBinding.FieldName = 'name'
                Width = 90
              end
              object dbtvHostQueuesenterprise: TcxGridDBColumn
                Caption = 'enterprise'
                DataBinding.FieldName = 'Lenterprise'
                Visible = False
                GroupIndex = 0
                SortIndex = 0
                SortOrder = soAscending
              end
              object dbtvHostQueuesListCount: TcxGridDBColumn
                Caption = 'Queue'
                DataBinding.FieldName = 'ListCount'
                PropertiesClassName = 'TcxButtonEditProperties'
                Properties.Buttons = <
                  item
                    Default = True
                    Glyph.SourceDPI = 96
                    Glyph.Data = {
                      89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
                      6100000029744558745469746C650052656D6F76653B44656C6574653B426172
                      733B526962626F6E3B5374616E646172643B635648300000026449444154785E
                      A551494C5351146568194A2B583746627F458902118892AAB811369246043462
                      8556D032A82D831664486D8C3FB40B8128E3C68998A289625720314469022591
                      26566BC484681C22C6012B5A1556C7F73EBF507FDCF193F3DFBBE7DC7BDE7BF7
                      86005811B820E80B2310F16B6880E4F7E10462AAF3F1B2014F889E961FBB307D
                      AAE2EBC8FEDC137C72280FB1AB546BA0DAE4F11296C491940F36089F2CD5593F
                      DFE8C682771453E71B31909D6D207C044D76166B8C339D1789E6C4A76B5D18D7
                      15D9A869B04184A744E7FBED72C03FD08EF9B13BF09CADC1F50C55CDE08182DA
                      B7ED562CB807E1BFDB06FF701F3C47753E52131D6C20B2EFCA343C31EAF1D3D1
                      89B93E167F1EF5E37155255ED92C989F70E0C74D96E3C734F9B89498749A1E2A
                      EC41644F4A6AF5B8F6107C7D2D98ED6D847FE80A7EDDBF8A6FBD4D98ED69C088
                      3A0BD678A589E44A843D089844DB141B4D0FF3D5F8D251878FAC9EA08C5B87F6
                      EC44B37C6D23C991F2530AF99F81E472BAAA7E42AFC307B612EFEA0E2FC1A52D
                      446B5A4633C991090D968A5B93D24D4EAD06336DF5982E5363BA9C07D9BF674F
                      62B4F020AC9B53976EF1CF145A36249B1EE4E5E28DA5022F8AB23045706B1383
                      DB498974CFE1F599620CAB736061B63409A720ED5624CFBD341E81B760379EE5
                      65A23F410183487ACE208EB1F42728E1CDCFE4F0BC641FBA9894395213176C10
                      655EA3EC706CDF06778E0A76C57A5447C8E87B63298C91B166BB92817BEF0EDC
                      4BDB8A0639D34DF8986083309A58278BEFB0C631BEAAC5E255940F6886A8D566
                      A27DAF95ADEB22B15CD883808984208E209A8F859A9C6F6078F0145684BF98E8
                      BFC080A205F60000000049454E44AE426082}
                    Hint = 'Clears host queue.'
                    Kind = bkGlyph
                  end>
                Width = 41
              end
              object dbtvHostQueuesRunAll: TcxGridDBColumn
                DataBinding.FieldName = 'RunAll'
                PropertiesClassName = 'TcxCheckBoxProperties'
                Properties.Glyph.SourceDPI = 96
                Properties.Glyph.Data = {
                  89504E470D0A1A0A0000000D494844520000002000000010080600000077007D
                  59000000017352474200AECE1CE90000000467414D410000B18F0BFC61050000
                  00097048597300000EC300000EC301C76FA8640000056F49444154484B8D5509
                  545347148D0B5611ABB507EBD66A15B75A15F71AB7AAED81B29DAA144404598B
                  06840814040AA81501C525228B91352C454059CA113544896151A8B80414A54A
                  40100C2A882D10106E67FE878A566DEF39F7FCFF67E6BEB96F66DE7CCEFFC400
                  C281846ADEA2127844CBD22DBDE326F4B6BD0F6ABE196BE099B22AD772EFBCD9
                  F49B90C67A0D83080713D2CEB751C32EE094B17398A4D4254CDAB9235C861CB9
                  12AEC282675B76A79A93FEFEE833DA174FC3F3D42A5C5508119069D4C18B5C1A
                  ACAD3B56B3B79F35122E92E5472616213CA10061A24284C6CB702C4E06418C14
                  39927224E7CAC18F2C40D62D256EB4BC84BCB507E2C73D287CD48E754E096082
                  B018B8C97FFE78A7A8A53C7EDCB2D3AE226E355FC4EDDA29E242D11283FAE739
                  482874865334B7C674EF9CCD643C35C8E11C1715E07DD829904070AE0A1BFD33
                  C1B53B89953FC6C2EF6C1516599F78364DC7CB81094226B7172C59E3285C921D
                  2EB6C505F901DC6E4841C38B0BB85597880A65308A6A3C21B9FF13F2EEECC6FA
                  8019D4B83AA33C1A2D454F4F0FF2EE59E0DC5D53E4129EA5AC34C1B5FA1018B8
                  A7638D432462F38EA2B6AE1E0B2C2230CDC0FFE2C7738C6610F9078403B6046A
                  CF731472F353AF78E3D29D83D89F62028BC085F8D66D3278115C88EFF1107B45
                  1FBC706D2CB61C533FD370D456A21B42E7E71C3C7111DDDD3D28BEEF88ECDBC6
                  C8AED880AC8A8DC82A5F8FAB3507F0835738A22F1CC21545103ABBBA9194514A
                  DD0F63C42C065984CC0D129C3547B26C07CCF72D542E779828586C364E67B4D6
                  B08986FED3B12B691156D87FA29AAE3F52A039497D2CD1D0C9D93310785C8CAE
                  AE1E543EF44596DC18E9378D9076D31069378C20FDC38FD017D22A5F64CB2DD1
                  D6F1129E0119AF968F85DAE6A02FCB620ACC6173581B5FD98EF7236D4309E9E1
                  56D7F3D1C294EF465C1A337FE832F24D0FE04784237AFB399CBD825CA83ABB51
                  52938BEB8A7D48293340F2EFBA48224C2C252CD1214F1D88C8F3AFB62EB8EE49
                  A7068633621643F47CA6AA08A1E7AD8539FA63A69036BA429434536A7638D768
                  DB0C971385702607DACC2F5B45DAA8090EC7372407ED24B3C4EB8D64C925B859
                  ED81EC1BA64828D1454CF13788295A87E8A2B50C9FBFE884B3EF296A408311B3
                  A0654733A6A6461AD8EF5BE0189A27E60B8BFBC6D192D330F349D81F78FA16A2
                  2595F87CB56B051D4BC8E178056531991DC9574054D688224501E40F4FA2AA6E
                  0F2A6BFD5076DF1DC5F79C90596682E65615785EC93430EB9E05DDCBC1DA2B8C
                  35ED82324376C515AB4E973580172A85E6D42FB4F41D438C4CBD93846EC2CB4F
                  04E7EEC0C42F0D1396DA47100DBB8DEEBF9C41EB9F9D8C01CAD8D247C8B8DD84
                  F30F9A217EA0447EF55DC814D770BD36054F9B3BE0E0C1D4FE878C98C5E04D7E
                  09663C81A436425C0549F50B2497B7C03DAA083607C5CC1D127C468E939715B0
                  3A908BA96BDD4A47CDD25B45758C9AEF9F8A96D65706FA5370B90611C50F1175
                  B51EA9E4F6533E6D879D5B3C35C02E1F8BA1768724F8ADFC09D22B9F23ACA409
                  478A94E4DC34E07C552B626435708F2E848E4B3C3E5DEE7471F4EC8D1BA88690
                  AD02A79F7FC5B3661542C9C0B799E8CFC6A63658EF8C7BD380DA3ADBC35BBFF7
                  48ABDF11558AE0FC46044B1BA1EB9282B9EB835FCED2F5AF9BB4DAF5FCF8C5D6
                  3E232673FFB93B1825C5F65D49687ADAC164A77CD28EC77D2493D1091B946D78
                  44F9B80DF58D6DB0E2C7BE6980061BF2D9CC95E3565B1D3B6EE899A1E2275760
                  EDF6243A8E56445FD9F595E6EBB072124AEDDC45B07523748D878D6B1C6C4896
                  D6FC3866B237B9795B988CC8FADF037DA0D5A03EFD6BDBE5CBB684C956DA315B
                  358AE9F90FD0603423EAF45D1CDD4BFA4ECBEDDF99B0A0AB417F3234631A93FD
                  E1BC131CCEDF889C665B8D06B52B0000000049454E44AE426082}
                Properties.GlyphCount = 2
                Width = 40
              end
              object dbtvHostQueuesRunSelected: TcxGridDBColumn
                DataBinding.FieldName = 'RunSelected'
                PropertiesClassName = 'TcxCheckBoxProperties'
                Properties.Glyph.SourceDPI = 96
                Properties.Glyph.Data = {
                  89504E470D0A1A0A0000000D494844520000002000000010080600000077007D
                  59000000017352474200AECE1CE90000000467414D410000B18F0BFC61050000
                  00097048597300000EC300000EC301C76FA8640000045549444154484BBD550B
                  4C9357180504863C2273830973BE20A0381551AC1484C1C260E591B92D224FD1
                  4206E555290A01A5868950658E2208AB147948411803D922F212A13C65900541
                  9CC614080441541E59B6A2C9D9BD1436669CB844779293F6BBF79EFF7BDDEFFF
                  95DE30D48E97DB23BAD8B6CA2F61DB666A132ACFEDBC21D087AB10AA125267DA
                  D1976DD13120426285FB1F9C2C86C0DC7995DEFCFE6B0F44653F7FBB61683683
                  C3CDB52A8BCC67CAB8F9CCA787F3991898CCC1C8D44F28680947A89839E891B0
                  C58B9CA7012E09E51345DD624E7A83F6BCFD6F500914EEB20F11EDAA3C5FCB46
                  CDADD3B83D5A8CD1991AF40C5F42DFB800AD83D1A8BF7F0475FD27B037D11444
                  A3A990BE1CAAC7F37F46CC85F6BB11E9D719F36BCF43D927C97C5B888879A3A4
                  3D160DFD6770AA781F7C9376C091B70E9C4C266AEF7270B1DD059CF3E6B0F4D3
                  1FD9E8A67B80E8D415F297439D276A43C7E024C2D39A66FD13AB8FD9F1F9B47F
                  8BB1CC37656BB2F0AA3724D230789FDC316EFDD56AA1A5A781D34AE3E5ABDDF8
                  268829DC099BC0F7E4262E2B847A6B3557110D75FEF71D389ADDDE7C8C641A5F
                  D085B8BC4E448B3B10451C4764342328B511BF4E3F43F5FD2970D29AE01E5321
                  750CCD5F3F2FA550F34AFEB03BA7D91B87CE9A6337DB309EAC69102E23D464C5
                  1963C3A73A0DFADB35AC884D2FE0DB843AF3FB0A70529BD0F76416BD8F67D1F3
                  488EAEB1DFD136FC1BA4433368904D93DE4D21B7FB21C49D63084E6F815D50C9
                  940D5BE243A4340B75569C919C10AC58636C71D1DF40D696CF93664A7BADC574
                  0F328DF8AE05E159CDF08CAF9493351A84023E5FD7E0F2EDC728E97B82A2DE47
                  C8FB6502A28E31A44A4790543784846B030A56C9E099D400EB00C9F46EEF9C00
                  22A523474933D6225CE11A78D222E45C5D2D975490D8F4F2D296697BC6159C4A
                  2AEB81B8FE0ED6DB45F6D1B3840A7C125ED2C63A5C0117DE15B8465D99FB75E6
                  96C331AC0CF6C1A5E05F95E148493F9C789560F8E6DD3472E05A1019754A9DD3
                  2AA89ADB7CA917905C911293DB262FEB1E05E75C23F48CCC8C5D4252DC3D620B
                  453C51D384F05A3FF6C597E27D466026D1FC630A6839DE7901F598078B1024EA
                  02935DF4D4C2234BA0AD6DF22E59A7E5A53D9C73BE3FBEC09323AC1FCAACBD87
                  7AD90C24BD9388CA6EC5A133B5E092920B7EB8850B4D03F03F5D0523075EA7EE
                  26962DD511FE056AD07EBDF51CB5187E85D8E95D203363F19D894D03A5E768E6
                  0BD008F8A61E3FF64EE0FB3B53C8B8F910DFB68EE352E728AAEF4D23473A8828
                  710B9C22F2F08175E8F5959BBFF89C6A085FE94DA86EE975516260E6BA86FCA7
                  257BD12B54ED63F6D9039F1D2D1D09CBEE84E0C603081A1FC039A2185BF70A9E
                  6D72E60FAFB58BAC36B43C18A7B38E694ACED3C45EC939057548CB4D458BB35E
                  0CFA30F5351BF718D8F9A7A5BB4597CBB9923E380417D24B48276261EC1646F3
                  3F813E7CE1A22D057A4ED3E423B6B5954F86744F401E0D40776EE77F040D947E
                  6468C674CC96F8E02829FD09CE54EFA7D88E3E860000000049454E44AE426082}
                Properties.GlyphCount = 2
                Width = 71
              end
              object dbtvHostQueuesPaused: TcxGridDBColumn
                Caption = 'Pause / Resume'
                DataBinding.FieldName = 'Paused'
                PropertiesClassName = 'TcxCheckBoxProperties'
                Properties.Glyph.SourceDPI = 96
                Properties.Glyph.Data = {
                  89504E470D0A1A0A0000000D494844520000002000000010080600000077007D
                  59000000017352474200AECE1CE90000000467414D410000B18F0BFC61050000
                  00097048597300000EC300000EC301C76FA8640000033249444154484BCD957B
                  48935118C6EDE2A40B95A083FA279082A85562AC4232345B2D73C2BA98E6BAD0
                  45A49BA699A5D6D2D4B159EA7464167611E7345790359750E40D9B5D5C373143
                  43A51B99144419513D9D777CB37D695FD65F3DF0C0B3DFFB9E9D77673BDFDCFE
                  278D28B42ACA8B6AC25E9DBC16D67BD2AAA82446A64C8C6AD4C3719EF4D5728F
                  82AA503FEEE53F69D489EA50F47E6A435F7F3B28137372625473E13C698C8B3C
                  0D96D0EF064B88C9600E99CAE1BF927B7E5508DE7F7D8AE66E0D281313E03CC5
                  ABE779E55F51C07A5F87BCCB21FD7997E5C735C6959E5C795812E55C92A3F74B
                  0B9ABA3240999800E7499530477C8CD57ADED5E2C1CB125C6C4E45B679795F76
                  852C71B75EEEC1B5094AA4AB90E1C5E73A3474A681323101CE130DA061B5C7AF
                  CAD849E5E24E8F1E8D1D0618EBF721CBB4B433D314BC5EAD761BC9B50F295166
                  6930BA3E5950DB7108948909709E6880A3A54B70BBDB80AB8FA3D1F84C035B57
                  8EC3D6875938752D1AE9E7826C69E71607714B064974E46C20DA3F54E0467B32
                  281313E03C2963E6880F9F094463A71695F60898ED91A86EDD83BA8E4CC6740E
                  57DC4A46DEC528A414064471CB7812A59C0AC0A377C5A8694B026562AEBCBA35
                  CE95F344031C280AC0F527A930DD5DCDB3D9AE4271ED366495AF4262817FE3B6
                  74E92CB664D05516ED37F8A3E5AD1EE5F7D6813231576EBCA374E53C29637CC4
                  0905FEECF8E350D21C36E0C29B6B916E5220F6D8FC8E883D3337B1D649CC748B
                  060FB03777016CAFB370DEA6006562029C2799CA471C9BB310E696AD38D3B402
                  A7EB953852B6123BB5D237910992242FAFB19359DB38E62137278976E9A46878
                  9E8AE22639281313E03CD1003BB45294D8C271C8B80C31997E1F558992DCE9B3
                  27FAB0F27866DA58F816B045B8D1158FA20619BDC1C000BFE13CD100D1197ED8
                  AE9EFB55B54F5226954DF165780233F5D2C6437E6A5789B6A6F97EBB707733B2
                  AD81A04CCCC9CDF62DD05A789C27F99A69DEAA2449CDE25553E99A4D64A6870F
                  3DB2FFB8B153EEE1B1330E6E4891BCD9982A791F1E37534DCCC159264635EAE1
                  F8AF1ACD3C96790C9787BDB153744CF406DE9CE907E3F833E2B29353CF50DF25
                  6D38ACA3FE2937B71FA4DA0863DC4F72280000000049454E44AE426082}
                Properties.GlyphCount = 2
                Width = 85
              end
              object dbtvHostQueuesClearQueue: TcxGridDBColumn
                DataBinding.FieldName = 'ClearQueue'
                PropertiesClassName = 'TcxCheckBoxProperties'
                Properties.Glyph.SourceDPI = 96
                Properties.Glyph.Data = {
                  89504E470D0A1A0A0000000D494844520000002000000010080600000077007D
                  59000000017352474200AECE1CE90000000467414D410000B18F0BFC61050000
                  00097048597300000EC300000EC301C76FA864000004F149444154484BBD9479
                  4C545714C6A9C230B2444A84205A514416953A80A8336C2A1A645023A5194544
                  A848A83315102D58A68051CBA2483B6E286511060854D12A22E838550645294A
                  2B4B41A9550C74949A94C5693BC5FAF51E04E2428CFDA75FF2CB7BEFDEF3DD73
                  EE7BF73C9DB7D43B8CB10C5DC6181A784BE9259C5A84D8128FCAF53BE7CCA267
                  06ADF59F4406BD1BDB43329AA41B6E9647042C64CF54C868A2582A90E6299951
                  5CA927EAEE6761F7A9957F898FCC4FE32DB3301B9A7FAB42288853171394FBAC
                  BF1D60B4EE96688A827D96B0F1577733664D9293E527D9F3C5D1C7F865310582
                  7B51F9FC812D0502DCEFC94557EF59C8AF464292C3EF58BDD3712D8B27FF1B45
                  8BEBD7448AF2FEE96983A63287918D67BDB7D1981CA54970B6F361F39CC14896
                  3C5C366F91E4E8BC33871561B8D0B4073FA94BA0EEBF80C6CE42B474A7A1B623
                  0ECABB9FE262EB0EF87F6107E631786E1D5D949CAB8CF03FF6F47133346732F1
                  84A129CFC440E70DDC0911A2D4DBF98F2D0E56428A5D97C29B23C9125CFEE67A
                  3C2EB5EE45728908C1292E58BA752AC4990228EE889177DD0FE2C33CB8069B77
                  D9AF300961BEE1E25F13251F5719BA3C7F40FD03FACBF6E349998CB11F7FFF52
                  8BF6F5BEF839D817AD6B7D70C069460F8B1D1B9CFE7EAAEC5C108A6B36236897
                  4BB75BC464996BE0441F539B71935724D9627BD15CB8879B6B6DFDC6CBCCAC0C
                  2C988792BFF8F94644830627D72C2DD03EA8437F493A63DFE055DB7619B7572F
                  C16D9137630954BE7C24595BAA58BCDEDAD4D90DB95782B0218387056196896C
                  8CCBA0AE31104A6D30CDD7F892B91397CF9EE900BECB301E9A7F4994DC302443
                  216FEC78843E790A7A0B5306AF7FFE781E2DAB3C87F0826AA92B764CB56874D4
                  D7B7661E8E6FBCB556289D0E61BC0D1CFDCC696CDC10B453FAD68682951FDB45
                  1DBD8AC8235710987046CBC6A8881151EB1806A55616563FD020E1EC23549556
                  A0376F273475A7718BEDF69650C0AE0228BD78F87CD28426070E6706F3D04926
                  2FEDD890317E79F86E67F1818B8AE8AC6B74D08C18D47246815279724A592372
                  946D98E6B5A585621983A29D1B8976951729EEF60D261FA6A3E1161ABCE7E226
                  83AE17F8B3116731BED99EC3B1659EE136247479EE1F9A6D4CFD363D2EAF565B
                  D6A086F84035CCA6CFB4F193A4AF5C1D5F941593A57A2CAB6A8528F13826CD0F
                  CF649E912EE0FA279EC8296FFDFDA5E48537BBE129AD40F2BAADB8E1E5842A57
                  076C9B60DC62C7E1D831CF8BFF00DD3589F240B14CF92053D10EE5BD7E1437F7
                  605B762D36EC55209ABDF2B4934DF85A751FA17B2A317DF1D67A1307A127F99E
                  DBD9B7F0DF55D51F7FFAD791E439D7D4F0883FC7A818441AB009D126062D561C
                  1D7B16FFEA09E66EDCA74479F3639C68EBC5A1EF7FC397B5DD28AC57E37C7B1F
                  726B3AB02DE72A7CA2F2F19E9BE43BD359011F908731B206D723ECABED817BAB
                  9FC59EE8C0615527DC3EAB80FB10FC983238AD3FD86661643A93C58ED63E7ADE
                  611921AB628F776DCEAE47DAE58748AB7E88655125705C95FAD4C127A9738A57
                  CC794BD78FA4C65305F4F6F4192FAD4187C898274A127B46CABB0492222CD824
                  C7BC887CB884E50DCC16A5551A59B98CB6F361D118678ABDC744AFD0FD0797C7
                  9ED24617B760F1A6223A84D411C36D37DC9AA38A8AA0B63165D08FC27208BA37
                  61BCFADF1F4DB48681EDC230377ED0A11A8F8DF9540079FF575191542CED98DA
                  8CEEDF201D9D7F01AF1B5D203E6179D80000000049454E44AE426082}
                Properties.GlyphCount = 2
                Width = 65
              end
              object dbtvHostQueuesStartedCount: TcxGridDBColumn
                DataBinding.FieldName = 'StartedAmount'
                Width = 46
              end
              object dbtvHostQueuesCDoneCount: TcxGridDBColumn
                DataBinding.FieldName = 'CDoneAmount'
                Width = 46
              end
              object dbtvHostQueuesSuccessCount: TcxGridDBColumn
                DataBinding.FieldName = 'SuccessAmount'
                Width = 46
              end
              object dbtvHostQueuesErrorCount: TcxGridDBColumn
                DataBinding.FieldName = 'ErrorAmount'
                Width = 46
              end
              object dbtvHostQueuesLastMessage: TcxGridDBColumn
                DataBinding.FieldName = 'LastMessage'
                Width = 600
              end
            end
            object glHostQueues: TcxGridLevel
              GridView = dbtvHostQueues
            end
          end
        end
        object tsHostQueue: TcxTabSheet
          Caption = 'Queue'
          ImageIndex = 1
          ExplicitLeft = 3
          ExplicitTop = 6
          DesignSize = (
            982
            134)
          object dcbHQPaused: TcxDBCheckBox
            Left = 262
            Top = 5
            Caption = 'Paused'
            DataBinding.DataField = 'Paused'
            DataBinding.DataSource = dsHostQueues
            Properties.ReadOnly = True
            Style.TransparentBorder = False
            TabOrder = 3
            Transparent = True
          end
          object lblStartedCount: TcxLabel
            Left = 1
            Top = 56
            Caption = 'Started Count :'
            FocusControl = dlbHQStartedCount
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 80
          end
          object lblLocation: TcxLabel
            Left = 29
            Top = 6
            Caption = 'Location :'
            FocusControl = dlbHQLocation
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 80
          end
          object lblListCount: TcxLabel
            Left = 21
            Top = 34
            Caption = 'List Count :'
            FocusControl = dlbHQListCount
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 80
          end
          object lblSuccessCount: TcxLabel
            Left = 180
            Top = 56
            Caption = 'Success Count :'
            FocusControl = dlbHQSuccessCount
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 261
          end
          object lblErrorCount: TcxLabel
            Left = 194
            Top = 80
            Caption = 'Error Count :'
            FocusControl = dlbHQErrorCount
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 261
          end
          object lblDoneCount: TcxLabel
            Left = 12
            Top = 80
            Caption = 'Done Count :'
            FocusControl = dlbHQDoneCount
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 80
          end
          object Label7: TcxLabel
            Left = 387
            Top = 6
            Caption = 'Last Message :'
            FocusControl = cxDBTextEdit2
            Transparent = True
          end
          object cxDBTextEdit2: TcxMemo
            Left = 469
            Top = 5
            Lines.Strings = (
              '')
            Properties.ReadOnly = True
            TabOrder = 6
            Height = 126
            Width = 500
          end
          object btnClearHostQueue: TcxButton
            Left = 12
            Top = 107
            Width = 93
            Height = 24
            Margins.Top = 6
            Margins.Bottom = 6
            Anchors = [akTop, akRight]
            Caption = 'Clear Queue'
            OptionsImage.Glyph.SourceDPI = 96
            OptionsImage.Glyph.Data = {
              89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
              6100000025744558745469746C6500436C6561723B45726173653B52656D6F76
              653B426172733B526962626F6E3B878083730000027C49444154785EA5916D48
              535118C76F3ADDCC46567E308922CB4C9170A691961A4C918DA052D05E2C5748
              362DCD8C9A9A56BE52A894647EC9966D830A254CD7B4146A443170E6CB6C335F
              6A628911A4CE5B98B57FE75EBCB2B420E8811FCFE11C7EFF07CE43FD632D2138
              137804A74582FEF81EEA7DD949EA5D493A355C92460D15CBA9C1223935507882
              935D8C0A59656FDE3163B33C7E171364CA394AF52A64145B6DC9520AC04238D9
              D5909D74DB6E1B0008E6E2745A9324896642D977A6AA0B2AFE26F35F64242A7F
              4E5840EB6A09B7609FEC474F69269D1FEC17CB84B301D99A377F9205EDA97177
              7E7C36817E54836902DD5483D9D10EBC4D96E28178EBD7AC8075522E60A1ECA6
              93EDAE9B1D7B0D5B4315A61BAE13AAF07DF825068E4830785802F3C158DC08F6
              9D6003B254264779E9C3FD317767460CB0DD2B2754B07DC6F20CFD89D1E84F10
              13A2A19784E1928FB79E0D38A5ECE2647759659BAAC73A8E29551926D5656CFF
              D6D58ABEBD917344411F138ACBEBBD7AB6B8F37DB84D3A31F2A12B3AF5F3111A
              F9CDE368B9AFC5A4B210B4A111DD645AB7349CF470B44705E1C21ACF5E7F81AB
              2FB3096EF2B284E226CDD3A12956E6B07676A3531C022381E94FC20271DECBC3
              E4C7E76D725CA3605F417D6D93F9CB6FB2DAF80991795A942665A3234A849650
              7F9CF514F66D74E1F97132B9A79812C615B5DA721B3FCECBB5AFC61091FB98A0
              65C98B97E3F40AF7BE0D3CE7CDCCEE39990B1044A45C531C28D7DBCFD55B7153
              3F8A1D395AEC9C23EC4C0344C9D596D5C255018E3207F7814251C2C5B4C80CF5
              87F0740DB6CB55D8965A879014E56C60E255DDF2B541F39333DD5C28471CB7E0
              465849F0227833CC9D3D1C3F6C1100FE8B5F5E8AB24DCA40F5DB000000004945
              4E44AE426082}
            TabOrder = 0
            OnClick = btnClearHostQueueClick
            ExplicitLeft = 8
          end
          object btnPauseHostQueue: TcxButton
            Left = 126
            Top = 107
            Width = 95
            Height = 24
            Margins.Top = 6
            Margins.Bottom = 6
            Anchors = [akTop, akRight]
            Caption = 'Pause Queue'
            OptionsImage.Glyph.SourceDPI = 96
            OptionsImage.Glyph.Data = {
              89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
              610000001974455874536F6674776172650041646F626520496D616765526561
              647971C9653C00000011744558745469746C650050617573653B53746F703B3B
              8A7A3C0000012249444154785EAD91C14AC4301086A7D68AE89328EC9BE81EF5
              A6AFA04751F02D647D0341104FB23E8908227A12B6AE2D6B366B9B99D4FE2565
              1743C8C51F063EBE0993494BFF996434DEBFB97E1C7EB4958FC6C35B3814180E
              3D9C71DE4B7AF5B0D7E4FAA9992E9E1B309CF370E82DBDCB1AA067664BD9464A
              2F933B02C3F53ECD127ACDEF97DE657DF509681AAB481A2170BF6AC07B1B101B
              A1DA7E93888063DE1F604C7F1383C33E3C40A812859BC0111F1850B322B602F6
              7CC53A32A01632A2A8D06F1DFBFE1D1CDB604E95D160CFB33038F21159135B03
              8EF8D06FC4411170D4FB03D8DAE9E717CD94E978D5174549E5CCF940B283939D
              B3A3F3C1E4F862501E9EEE5EC2A1C070E8E14CE75D923FDB6CB6B5ED846EEB07
              E0FC96E3B9F3168D5F3C1F3F11AF0B16D00000000049454E44AE426082}
            TabOrder = 1
            OnClick = btnPauseHostQueueClick
            ExplicitLeft = 122
          end
          object btnResumeHostQueue: TcxButton
            Left = 239
            Top = 107
            Width = 105
            Height = 24
            Margins.Top = 6
            Margins.Bottom = 6
            Anchors = [akTop, akRight]
            Caption = 'Resume Queue'
            OptionsImage.Glyph.SourceDPI = 96
            OptionsImage.Glyph.Data = {
              89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
              610000001974455874536F6674776172650041646F626520496D616765526561
              647971C9653C00000011744558745469746C6500506C61793B53746172743B5D
              61407A000001F049444154785EAD904B6B13511886DF938C4E5A31C560A5174C
              68E3056C91D07A5D98B6D616BC54105CC4DB527F801050BA511411A550D14271
              2314ACC5955D14A90B770A820B15A9422B0DB69149C64E9C2493CB3973267E84
              5884566DC0071E38B3380FEF1CFC3746A74FAA2353273A50252E54D035AB160C
              6F1F4C1D7F32FCEC58A0EA407231EB9692B160735784B9F07978F2E8D0ED89FE
              4D6B0E64CCA28B0B81B6C03E74EF8978B636745E56143677F7697FF4EAC821F5
              9F01291D0821912E7C43412CC1DF588F03A1015F53C3EE3B5E9F3A736BBCF7EC
              91D3ADAE3F066CDB814D811C37A0673EA168E7A0281C417F13426D875B37FB82
              8FC30381D7D71F75F7ACBE409420B8036E5BC88B3434F3030C2B46A13C366E50
              B06BFB36ECDC71707F9DB7F9E5E068F81C2A28CB0B245D1692E667E82C40C0CC
              C7CBAE73D7C0343D48680652BAF62A9DE2EF0030B2A4FCFE0B8253C0CE4250E0
              17564E85AE99588ACF7E492C583726EE7D9C0460AD5820CB6FE0A0C0CB0BE8DB
              83F822C7F785989ED20B43CFC7E6C68C643E0D809336595A1110E547CC607E5E
              428F7DCD657FF0876F5EC4EFCFBE3792008AA4241DAC465FA465CBA59B1DA58B
              D742F6F968FBF8DEDEC610002FB9BEF2D80C7FA3EF4C4BFD852BEDD35DA7FC3D
              00EA489574930C6B44216BC99ACA99A14AD8F2D42AF809500AE0796F04355100
              00000049454E44AE426082}
            TabOrder = 2
            OnClick = btnResumeHostQueueClick
            ExplicitLeft = 235
          end
          object dlbHQListCount: TcxDBLabel
            Left = 82
            Top = 34
            DataBinding.DataField = 'ListCount'
            DataBinding.DataSource = dsHostQueues
            ParentColor = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 17
            Width = 43
            AnchorX = 125
          end
          object dlbHQStartedCount: TcxDBLabel
            Left = 82
            Top = 56
            DataBinding.DataField = 'StartedAmount'
            DataBinding.DataSource = dsHostQueues
            ParentColor = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 17
            Width = 43
            AnchorX = 125
          end
          object dlbHQSuccessCount: TcxDBLabel
            Left = 262
            Top = 56
            DataBinding.DataField = 'SuccessAmount'
            DataBinding.DataSource = dsHostQueues
            ParentColor = False
            Transparent = True
            Height = 17
            Width = 43
          end
          object dlbHQErrorCount: TcxDBLabel
            Left = 262
            Top = 80
            DataBinding.DataField = 'ErrorAmount'
            DataBinding.DataSource = dsHostQueues
            ParentColor = False
            Transparent = True
            Height = 17
            Width = 43
          end
          object dlbHQDoneCount: TcxDBLabel
            Left = 82
            Top = 80
            DataBinding.DataField = 'CDoneAmount'
            DataBinding.DataSource = dsHostQueues
            ParentColor = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 17
            Width = 43
            AnchorX = 125
          end
          object dlbHQLocation: TcxDBLabel
            Left = 83
            Top = 5
            AutoSize = True
            DataBinding.DataField = 'name'
            DataBinding.DataSource = dsHostQueues
            ParentColor = False
            Transparent = True
          end
        end
      end
    end
    object tsConfig: TcxTabSheet
      Caption = '&Config'
      ImageIndex = 1
      object pnlSQL: TcxGroupBox
        Left = 0
        Top = 79
        Align = alTop
        Caption = 'SQL string - Use this to get all the surce rows'
        TabOrder = 1
        DesignSize = (
          990
          164)
        Height = 164
        Width = 990
        object btnTestSQL: TButton
          AlignWithMargins = True
          Left = 905
          Top = 12
          Width = 80
          Height = 26
          Anchors = [akTop, akRight]
          Caption = 'Load Data'
          TabOrder = 0
          OnClick = btnTestSQLClick
        end
        object edtSql: TacSqlTextEditor
          AlignWithMargins = True
          Left = 5
          Top = 41
          Width = 893
          Height = 93
          Cursor = crIBeam
          Text = ''
          OnDocumentChanged = edtSqlDocumentChanged
          TextEditorOptions.Font.Charset = DEFAULT_CHARSET
          TextEditorOptions.Font.Color = clWindowText
          TextEditorOptions.Font.Height = -13
          TextEditorOptions.Font.Name = 'Courier New'
          TextEditorOptions.Font.Style = []
          TextEditorOptions.GutterProperties.Lanes = <>
          QueryBuilder = acQueryBuilder1
          SqlTextEditorOptions.CompleteOnKeys = [ckEnter, ckTab, ckContextKeys]
          SqlTextEditorOptions.FiltrationAlgorythms = [faInclusion, faCamelCase]
        end
        object btnCanned: TcxButton
          Left = 905
          Top = 52
          Width = 80
          Height = 26
          Anchors = [akTop, akRight]
          Caption = 'Select'
          TabOrder = 2
          OnClick = btnCannedClick
        end
        object edPrimaryKey: TcxTextEdit
          Left = 174
          Top = 138
          Properties.OnChange = edPrimaryKeyPropertiesChange
          Properties.OnValidate = edPrimaryKeyPropertiesValidate
          TabOrder = 3
          Width = 233
        end
        object cxLabel9: TcxLabel
          Left = 8
          Top = 139
          AutoSize = False
          Caption = 'Primary key (empty=auto) :'
          Style.Shadow = False
          Style.TransparentBorder = True
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 17
          Width = 160
          AnchorX = 168
          AnchorY = 148
        end
        object edSrcURI: TcxTextEdit
          AlignWithMargins = True
          Left = 176
          Top = 14
          Margins.Left = 6
          Margins.Right = 6
          Anchors = [akLeft, akTop, akRight]
          Properties.OnChange = edSrcURIChange
          TabOrder = 5
          Width = 723
        end
        object cxLabel10: TcxLabel
          Left = 75
          Top = 15
          Caption = 'Connection string :'
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          AnchorX = 170
          AnchorY = 24
        end
      end
      object cxGroupBox1: TcxGroupBox
        Left = 0
        Top = 243
        Align = alClient
        PanelStyle.Active = True
        TabOrder = 2
        Height = 529
        Width = 990
        object pnlBaseURL: TcxGroupBox
          Left = 2
          Top = 2
          Align = alTop
          Caption = 'API URLs'
          TabOrder = 0
          DesignSize = (
            986
            163)
          Height = 163
          Width = 986
          object edtUrlPath: TcxTextEdit
            AlignWithMargins = True
            Left = 568
            Top = 96
            Margins.Left = 6
            Margins.Right = 6
            Anchors = [akLeft, akTop, akRight]
            Properties.OnChange = edtUrlPathChange
            TabOrder = 1
            Width = 326
          end
          object btnTestURL: TButton
            AlignWithMargins = True
            Left = 903
            Top = 107
            Width = 80
            Height = 26
            Anchors = [akTop, akRight]
            Caption = 'Test URL'
            TabOrder = 0
            OnClick = btnTestURLClick
          end
          object btnAPIHostEdit: TButton
            AlignWithMargins = True
            Left = 568
            Top = 45
            Width = 83
            Height = 26
            Caption = 'Edit Locations'
            TabOrder = 2
            OnClick = btnAPIHostEditClick
          end
          object lblBaseUrl: TcxLabel
            Left = 568
            Top = 128
            Caption = 'URL'
            Transparent = True
          end
          object grdHosts: TcxGrid
            Left = 3
            Top = 48
            Width = 542
            Height = 113
            TabOrder = 4
            object dbtvHosts: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              Navigator.Buttons.Insert.Enabled = False
              Navigator.Buttons.Insert.Visible = False
              Navigator.Buttons.Append.Enabled = False
              Navigator.Buttons.Append.Visible = False
              Navigator.Buttons.Delete.Enabled = False
              Navigator.Buttons.Delete.Visible = False
              Navigator.Buttons.Edit.Enabled = False
              Navigator.Buttons.Edit.Visible = False
              Navigator.Buttons.Post.Enabled = False
              Navigator.Buttons.Post.Visible = False
              Navigator.Buttons.Cancel.Enabled = False
              Navigator.Buttons.Cancel.Visible = False
              Navigator.Buttons.SaveBookmark.Visible = True
              Navigator.Buttons.GotoBookmark.Visible = True
              Navigator.Visible = True
              ScrollbarAnnotations.CustomAnnotations = <>
              OnSelectionChanged = dbtvHostsSelectionChanged
              DataController.DataSource = dsHosts
              DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoMultiSelectionSyncGroupWithChildren]
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <
                item
                  Links = <
                    item
                      Column = dbtvHostsenterprise
                    end>
                  SummaryItems = <
                    item
                      Kind = skCount
                      Column = dbtvHostsname
                    end>
                end>
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsSelection.MultiSelect = True
              OptionsSelection.CheckBoxVisibility = [cbvDataRow, cbvGroupRow]
              OptionsSelection.MultiSelectMode = msmPersistent
              OptionsSelection.ShowCheckBoxesDynamically = True
              OptionsView.ColumnAutoWidth = True
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              object dbtvHostsname: TcxGridDBColumn
                Caption = 'Name'
                DataBinding.FieldName = 'name'
                SortIndex = 1
                SortOrder = soAscending
                Width = 139
              end
              object dbtvHostshost: TcxGridDBColumn
                Caption = 'URL Host'
                DataBinding.FieldName = 'host'
                Width = 297
              end
              object dbtvHostsenterprise: TcxGridDBColumn
                DataBinding.FieldName = 'enterprise'
                Visible = False
                VisibleForExpressionEditor = bTrue
                GroupIndex = 0
                SortIndex = 0
                SortOrder = soDescending
                Width = 139
              end
            end
            object grdHostsLevel1: TcxGridLevel
              GridView = dbtvHosts
            end
          end
          object cbEnterprise: TcxImageComboBox
            Left = 103
            Top = 21
            Properties.ClearKey = 46
            Properties.Items = <>
            Properties.OnChange = cbEnterprisePropertiesChange
            Properties.OnEditValueChanged = cbEnterprisePropertiesEditValueChanged
            TabOrder = 5
            Width = 346
          end
          object cxLabel11: TcxLabel
            Left = 3
            Top = 19
            AutoSize = False
            Caption = 'Enterprise :'
            Properties.Alignment.Horz = taRightJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 17
            Width = 94
            AnchorX = 97
            AnchorY = 28
          end
          object lblURLPath: TcxLabel
            Left = 568
            Top = 77
            Caption = 'URL Path:'
            Transparent = True
          end
        end
        object cxGroupBox2: TcxGroupBox
          Left = 2
          Top = 165
          Align = alTop
          Caption = 'API Resource'
          TabOrder = 1
          DesignSize = (
            986
            50)
          Height = 50
          Width = 986
          object edtResource: TcxTextEdit
            AlignWithMargins = True
            Left = 8
            Top = 18
            Margins.Left = 6
            Margins.Right = 6
            Anchors = [akLeft, akTop, akRight]
            Properties.OnChange = edtResourceChange
            TabOrder = 1
            Width = 889
          end
          object btnSelectResAndPar: TButton
            AlignWithMargins = True
            Left = 903
            Top = 16
            Width = 80
            Height = 26
            Anchors = [akTop, akRight]
            Caption = 'Select'
            TabOrder = 0
            OnClick = btnSelectResAndParClick
          end
        end
        object cxGroupBox3: TcxGroupBox
          Left = 2
          Top = 215
          Align = alTop
          Caption = 'API Parameters'
          TabOrder = 2
          DesignSize = (
            986
            116)
          Height = 116
          Width = 986
          object edtParameters: TcxMemo
            AlignWithMargins = True
            Left = 8
            Top = 18
            Margins.Left = 6
            Margins.Right = 6
            Anchors = [akLeft, akTop, akRight]
            Properties.OnChange = edtParametersPropertiesChange
            TabOrder = 1
            Height = 90
            Width = 889
          end
          object btnAddAll: TButton
            AlignWithMargins = True
            Left = 903
            Top = 16
            Width = 80
            Height = 26
            Margins.Left = 6
            Margins.Top = 6
            Margins.Right = 6
            Margins.Bottom = 6
            Anchors = [akTop, akRight]
            Caption = 'Add fields'
            TabOrder = 0
            OnClick = btnAddAllClick
          end
          object btnAddGroups: TButton
            AlignWithMargins = True
            Left = 903
            Top = 47
            Width = 80
            Height = 26
            Margins.Left = 6
            Margins.Top = 6
            Margins.Right = 6
            Margins.Bottom = 6
            Anchors = [akTop, akRight]
            Caption = 'Groups'
            TabOrder = 2
            OnClick = btnAddGroupsClick
          end
        end
        object Settings: TcxGroupBox
          Left = 2
          Top = 331
          Align = alTop
          Caption = 'Settings'
          TabOrder = 3
          Transparent = True
          Height = 168
          Width = 986
          object lblRetries: TcxLabel
            Left = 8
            Top = 22
            AutoSize = False
            Caption = 'Retries :'
            Properties.Alignment.Horz = taRightJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 17
            Width = 160
            AnchorX = 168
            AnchorY = 31
          end
          object seRetries: TcxSpinEdit
            AlignWithMargins = True
            Left = 174
            Top = 21
            Margins.Top = 9
            Margins.Bottom = 9
            Properties.AssignedValues.MaxValue = True
            Properties.AssignedValues.MinValue = True
            Properties.OnChange = seRetriesChange
            TabOrder = 0
            Value = 3
            Width = 100
          end
          object cxLabel1: TcxLabel
            Left = 8
            Top = 49
            AutoSize = False
            Caption = 'Tasks :'
            Properties.Alignment.Horz = taRightJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 17
            Width = 160
            AnchorX = 168
            AnchorY = 58
          end
          object seTasks: TcxSpinEdit
            AlignWithMargins = True
            Left = 174
            Top = 48
            Margins.Top = 9
            Margins.Bottom = 9
            Properties.MaxValue = 32.000000000000000000
            Properties.MinValue = -8.000000000000000000
            Properties.OnChange = seTasksChange
            TabOrder = 2
            Width = 100
          end
          object chkLogging: TcxCheckBox
            AlignWithMargins = True
            Left = 614
            Top = 23
            Style.TransparentBorder = False
            TabOrder = 7
            OnClick = chkLoggingClick
          end
          object btnEditScript: TButton
            AlignWithMargins = True
            Left = 902
            Top = 21
            Width = 80
            Height = 26
            Margins.Left = 6
            Margins.Top = 6
            Margins.Right = 6
            Margins.Bottom = 6
            Caption = 'Edit Script'
            TabOrder = 16
            OnClick = btnEditScriptClick
          end
          object edSentryDSN: TcxTextEdit
            Left = 614
            Top = 74
            Properties.OnChange = edSentryDSNPropertiesChange
            TabOrder = 9
            Width = 233
          end
          object cxLabel3: TcxLabel
            Left = 448
            Top = 76
            AutoSize = False
            Caption = 'Sentry DSN :'
            Properties.Alignment.Horz = taRightJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 17
            Width = 160
            AnchorX = 608
            AnchorY = 85
          end
          object cxLabel4: TcxLabel
            Left = 448
            Top = 101
            AutoSize = False
            Caption = 'Sentry tags :'
            Style.Shadow = False
            Style.TransparentBorder = True
            Properties.Alignment.Horz = taRightJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 17
            Width = 160
            AnchorX = 608
            AnchorY = 110
          end
          object edSentryTags: TcxTextEdit
            Left = 614
            Top = 100
            Properties.OnChange = edSentryDSNPropertiesChange
            TabOrder = 10
            Width = 233
          end
          object cxLabel6: TcxLabel
            Left = 448
            Top = 25
            AutoSize = False
            Caption = 'Logging :'
            Properties.Alignment.Horz = taRightJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 17
            Width = 160
            AnchorX = 608
            AnchorY = 34
          end
          object cxLabel2: TcxLabel
            Left = 8
            Top = 129
            AutoSize = False
            Caption = 'Exit on done :'
            Style.Shadow = False
            Style.TransparentBorder = True
            Properties.Alignment.Horz = taRightJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 17
            Width = 160
            AnchorX = 168
            AnchorY = 138
          end
          object cbExitOnDone: TcxComboBox
            Left = 174
            Top = 128
            Properties.DropDownListStyle = lsEditFixedList
            Properties.Items.Strings = (
              'Never'
              'Always'
              'Success')
            Properties.OnChange = cbExitOnDonePropertiesChange
            TabOrder = 6
            Text = 'Never'
            Width = 233
          end
          object cbRunMode: TcxComboBox
            Left = 174
            Top = 101
            Properties.DropDownListStyle = lsEditFixedList
            Properties.Items.Strings = (
              'DESIGN'
              'RUN'
              'RUNMIN'
              'RUNSHOW'
              'RUNHIDE')
            Properties.OnChange = cbRunModePropertiesChange
            TabOrder = 5
            Text = 'DESIGN'
            Width = 233
          end
          object cxLabel7: TcxLabel
            Left = 8
            Top = 102
            AutoSize = False
            Caption = 'Run mode :'
            Style.Shadow = False
            Style.TransparentBorder = True
            Properties.Alignment.Horz = taRightJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 17
            Width = 160
            AnchorX = 168
            AnchorY = 111
          end
          object seSleep: TcxSpinEdit
            AlignWithMargins = True
            Left = 174
            Top = 74
            Margins.Top = 9
            Margins.Bottom = 9
            Properties.AssignedValues.MinValue = True
            Properties.Increment = 20.000000000000000000
            Properties.MaxValue = 999999.000000000000000000
            Properties.OnChange = seSleepPropertiesChange
            TabOrder = 4
            Width = 100
          end
          object cxLabel8: TcxLabel
            Left = 8
            Top = 75
            AutoSize = False
            Caption = 'Sleep :'
            Properties.Alignment.Horz = taRightJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 17
            Width = 160
            AnchorX = 168
            AnchorY = 84
          end
          object cxLabel12: TcxLabel
            Left = 440
            Top = 49
            AutoSize = False
            Caption = 'Separate Logging File For Hosts :'
            Properties.Alignment.Horz = taRightJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 17
            Width = 168
            AnchorX = 608
            AnchorY = 58
          end
          object chkSeparateLoggingFiles: TcxCheckBox
            AlignWithMargins = True
            Left = 614
            Top = 47
            Enabled = False
            Style.TransparentBorder = False
            TabOrder = 8
            OnClick = chkSeparateLoggingFilesClick
          end
        end
      end
      object gbProfile: TcxGroupBox
        Left = 0
        Top = 0
        Align = alTop
        Caption = 'Profile'
        TabOrder = 0
        Height = 79
        Width = 990
        object cxLabel5: TcxLabel
          Left = 8
          Top = 19
          AutoSize = False
          Caption = 'Profile :'
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 17
          Width = 162
          AnchorX = 170
          AnchorY = 28
        end
        object btnLoadIni: TButton
          AlignWithMargins = True
          Left = 176
          Top = 41
          Width = 100
          Height = 26
          Margins.Top = 6
          Margins.Bottom = 6
          Action = aLoadIni
          TabOrder = 1
        end
        object btnSaveIni: TButton
          AlignWithMargins = True
          Left = 282
          Top = 41
          Width = 100
          Height = 26
          Margins.Left = 6
          Margins.Top = 6
          Margins.Bottom = 6
          Action = aSaveIni
          TabOrder = 2
        end
        object edtIniFileName: TcxButtonEdit
          Left = 176
          Top = 15
          Properties.Buttons = <>
          Properties.ReadOnly = True
          Properties.OnChange = edtIniFileNamePropertiesChange
          Style.Color = clInfoBk
          TabOrder = 3
          Width = 722
        end
        object Button1: TButton
          AlignWithMargins = True
          Left = 386
          Top = 41
          Width = 100
          Height = 26
          Margins.Left = 6
          Margins.Top = 6
          Margins.Bottom = 6
          Action = aSaveIniAs
          TabOrder = 4
        end
      end
    end
  end
  object connData: TFDConnection
    Params.Strings = (
      'DriverID=ADS'
      'Alias=TisWin3DD'
      'Password=lezaM123$51'
      'User_Name=User1'
      'Port=0')
    LoginPrompt = False
    Left = 816
    Top = 112
  end
  object qData: TFDQuery
    Connection = connData
    FetchOptions.AssignedValues = [evMode, evRowsetSize, evUnidirectional, evCursorKind]
    FetchOptions.CursorKind = ckForwardOnly
    FetchOptions.Unidirectional = True
    FetchOptions.RowsetSize = 1000
    SQL.Strings = (
      '')
    Left = 888
    Top = 96
  end
  object dsData: TDataSource
    DataSet = mtData
    Left = 936
    Top = 144
  end
  object tmrEndApp: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = tmrEndAppTimer
    Left = 688
    Top = 40
  end
  object acAdvantageSyntaxProvider1: TacAdvantageSyntaxProvider
    Left = 360
    Top = 85
  end
  object mtData: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 936
    Top = 96
  end
  object tmrAPIStatusUpd: TTimer
    Enabled = False
    Interval = 200
    OnTimer = tmrAPIStatusUpdTimer
    Left = 672
    Top = 96
  end
  object gpmData: TcxGridPopupMenu
    PopupMenus = <>
    Left = 568
    Top = 96
  end
  object Scripter: TatPascalScripter
    SourceCode.Strings = (
      '')
    SaveCompiledCode = False
    ShortBooleanEval = True
    LibOptions.SearchPath.Strings = (
      '$(CURDIR)'
      '$(APPDIR)')
    LibOptions.SourceFileExt = '.psc'
    LibOptions.CompiledFileExt = '.pcu'
    LibOptions.UseScriptFiles = True
    CallExecHookEvent = False
    Left = 456
    Top = 88
  end
  object dlgSave: TSaveDialog
    DefaultExt = 'src'
    Filter = 'API Looper Profile(*.src)|*.src'
    Left = 652
    Top = 162
  end
  object AL: TActionList
    Left = 518
    Top = 280
    object aSaveIni: TAction
      Caption = 'Save'
      OnExecute = aSaveIniExecute
    end
    object aLoadIni: TAction
      Caption = 'Load'
      OnExecute = aLoadIniExecute
    end
    object aSaveIniAs: TAction
      Caption = 'Save As'
      OnExecute = aSaveIniAsExecute
    end
  end
  object dlgOpen: TOpenDialog
    DefaultExt = 'src'
    Filter = 'API Looper Profile(*.src)|*.src'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 588
    Top = 162
  end
  object dsHosts: TDataSource
    DataSet = mtHosts
    Left = 136
    Top = 336
  end
  object mtHosts: TFDMemTable
    AfterScroll = mtHostsAfterScroll
    OnCalcFields = mtHostsCalcFields
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 64
    Top = 337
    object mtHostsname: TStringField
      FieldName = 'name'
      Size = 50
    end
    object mtHostshost: TStringField
      FieldName = 'host'
      Size = 200
    end
    object mtHostsenterprise: TStringField
      FieldName = 'enterprise'
      Size = 50
    end
    object mtHostsFullName: TStringField
      FieldKind = fkCalculated
      FieldName = 'FullName'
      Size = 250
      Calculated = True
    end
  end
  object dsHostQueues: TDataSource
    DataSet = mtHostQueues
    Left = 632
    Top = 536
  end
  object mtHostQueues: TFDMemTable
    AfterOpen = mtHostQueuesAfterOpen
    AfterPost = mtHostQueuesAfterPost
    AfterDelete = mtHostQueuesAfterDelete
    OnCalcFields = mtHostQueuesCalcFields
    FieldDefs = <>
    IndexDefs = <>
    Aggregates = <
      item
        Name = 'QueueCount'
        Expression = 'SUM(ListCount)'
        Active = True
      end>
    AggregatesActive = True
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 472
    Top = 504
    object mtHostQueuesname: TStringField
      DisplayLabel = 'Location'
      FieldName = 'name'
      Size = 50
    end
    object mtHostQueueshost: TStringField
      DisplayLabel = 'Host'
      FieldName = 'host'
      Size = 200
    end
    object mtHostQueuesLenterprise: TStringField
      DisplayLabel = 'Enterprise'
      FieldKind = fkLookup
      FieldName = 'Lenterprise'
      LookupDataSet = mtHosts
      LookupKeyFields = 'host'
      LookupResultField = 'enterprise'
      KeyFields = 'host'
      Size = 50
      Lookup = True
    end
    object mtHostQueuesListCount: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'List Count'
      FieldName = 'ListCount'
      DisplayFormat = '#,##0'
    end
    object mtHostQueuesPaused: TBooleanField
      DefaultExpression = 'false'
      FieldName = 'Paused'
    end
    object mtHostQueuesRunAll: TBooleanField
      DefaultExpression = 'false'
      DisplayLabel = 'Run All'
      FieldName = 'RunAll'
    end
    object mtHostQueuesRunSelected: TBooleanField
      DefaultExpression = 'false'
      DisplayLabel = 'Run Selected'
      FieldName = 'RunSelected'
    end
    object mtHostQueuesClearQueue: TBooleanField
      DefaultExpression = 'false'
      DisplayLabel = 'Clear Queue'
      FieldName = 'ClearQueue'
    end
    object mtHostQueuesCFullLocationName: TStringField
      FieldKind = fkCalculated
      FieldName = 'CFullLocationName'
      Size = 101
      Calculated = True
    end
    object mtHostQueuesStartedAmount: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'Started'
      FieldName = 'StartedAmount'
      DisplayFormat = '#,##0'
    end
    object mtHostQueuesSuccessAmount: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'Success '
      FieldName = 'SuccessAmount'
      DisplayFormat = '#,##0'
    end
    object mtHostQueuesErrorAmount: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'Error'
      FieldName = 'ErrorAmount'
      DisplayFormat = '#,##0'
    end
    object mtHostQueuesCDoneAmount: TIntegerField
      DisplayLabel = 'Done'
      FieldKind = fkCalculated
      FieldName = 'CDoneAmount'
      DisplayFormat = '#,##0'
      Calculated = True
    end
    object mtHostQueuesLastMessage: TWideStringField
      DisplayLabel = 'Last Message'
      FieldName = 'LastMessage'
      Size = 200
    end
  end
  object mtAPIStatus: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 888
    Top = 272
  end
  object dsAPIStatus: TDataSource
    DataSet = mtAPIStatus
    Left = 768
    Top = 288
  end
  object HintStyleControllerHostQuees: TcxHintStyleController
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    Left = 368
    Top = 376
  end
  object tmrHostQueuesHint: TTimer
    Enabled = False
    OnTimer = tmrHostQueuesHintTimer
    Left = 672
    Top = 336
  end
end
