unit uHosts;

interface

uses
  Spring, Spring.Collections,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, FireDAC.Comp.DataSet, FireDAC.Comp.Client, dxLayoutContainer,
  cxGridInplaceEditForm;

type
  TfrmHosts = class(TForm)
    Button1: TButton;
    Panel1: TPanel;
    mtHosts: TFDMemTable;
    dsHosts: TDataSource;
    mtHostsname: TStringField;
    mtHostshost: TStringField;
    dbtvHosts: TcxGridDBTableView;
    grdHostsLevel1: TcxGridLevel;
    grdHosts: TcxGrid;
    dbtvHostsname: TcxGridDBColumn;
    dbtvHostshost: TcxGridDBColumn;
    mtHostsenterprise: TStringField;
    dbtvHostsenterprise: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
    class procedure Edit;
    class function List: IList<Tuple<string, string>>;
  end;

implementation

uses
  System.IOUtils, System.JSON, System.UITypes, uRecords, uMain;


{$R *.dfm}


procedure TfrmHosts.FormDestroy(Sender: TObject);
var
  js: TJSONObject;
begin
  js := MT2JS(mtHosts);
  try
    TFile.WriteAllText(HOSTS_FILE_NAME, js.ToString);
  finally
    js.Free;
  end;
end;

class function TfrmHosts.List: IList<Tuple<string, string>>;
var
  form: TfrmHosts;
begin
  Result := TCollections.CreateList<Tuple<string, string>>;
  form := TfrmHosts.Create(nil);
  try
    form.mtHosts.First;
    while not form.mtHosts.Eof do
    begin
      Result.Add(Tuple.Create(form.mtHostsname.AsString, form.mtHostshost.AsString));
      form.mtHosts.Next;
    end;

  finally
    form.Free;
  end;
end;

class procedure TfrmHosts.Edit;
var
  form: TfrmHosts;
begin
  form := TfrmHosts.Create(nil);
  try
    form.ShowModal;
  finally
    form.Free;
  end;
end;

procedure TfrmHosts.FormClose(Sender: TObject; var Action: TCloseAction);
var
  B: TBookmark;
  U: ISet<string>;
begin
  Action := caFree;
  mtHosts.DisableControls;
  B := mtHosts.Bookmark;
  try
    mtHosts.First;
    U := TCollections.CreateSet<string>;
    while not mtHosts.Eof do
    begin
      if U.Contains(mtHostshost.AsString) then
      begin
        Action := caNone;
        MessageDlg(Format('Url host "%s" with name "%s" is not unique, please correct it.',
          [mtHostshost.AsString, mtHostsname.AsString]), mtError, [mbOK], 0);
        Exit;
      end;
      U.Add(mtHostshost.AsString);
      mtHosts.Next;
    end;
  finally
    mtHosts.Bookmark := B;
    mtHosts.EnableControls;
  end;
end;

procedure TfrmHosts.FormCreate(Sender: TObject);
var
  js: TJSONObject;
begin
  if TFile.Exists(HOSTS_FILE_NAME) then
    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(HOSTS_FILE_NAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtHosts);
      finally
        js.Free;
      end;
    except
    end;
  mtHosts.Active := True;
end;

end.
