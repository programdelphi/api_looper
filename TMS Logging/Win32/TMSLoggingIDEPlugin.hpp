﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingIDEPlugin.pas' rev: 31.00 (Windows)

#ifndef TmsloggingidepluginHPP
#define TmsloggingidepluginHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <ToolsAPI.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <DesignIntf.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Graphics.hpp>
#include <System.Generics.Collections.hpp>
#include <System.Generics.Defaults.hpp>
#include <System.Types.hpp>
#include <System.SysUtils.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingideplugin
{
//-- forward type declarations -----------------------------------------------
struct TTMSLoggingItemPosition;
struct TTMSLoggingUsesData;
class DELPHICLASS TTMSLoggingIDEPlugin;
//-- type declarations -------------------------------------------------------
struct DECLSPEC_DRECORD TTMSLoggingItemPosition
{
public:
	bool IsUnit;
	System::UnicodeString Text;
	short Line;
	short Column;
};


struct DECLSPEC_DRECORD TTMSLoggingUsesData
{
public:
	System::UnicodeString AText;
	Toolsapi::TOTACharPos APosition;
};


enum DECLSPEC_DENUM TTMSLoggingSourceCodeMode : unsigned char { scmComment, scmUncomment };

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggingIDEPlugin : public Toolsapi::TNotifierObject
{
	typedef Toolsapi::TNotifierObject inherited;
	
private:
	Vcl::Extctrls::TTimer* FTimer;
	Vcl::Menus::TMenuItem* FMenu;
	bool FProcessComment;
	System::StaticArray<Vcl::Graphics::TBitmap*, 5> FBitmaps;
	System::StaticArray<Vcl::Actnlist::TAction*, 5> FActions;
	System::StaticArray<Vcl::Actnlist::TAction*, 8> FSubActions;
	System::StaticArray<Vcl::Graphics::TBitmap*, 8> FSubBitmaps;
	
protected:
	System::UnicodeString __fastcall GetVerb(int Index);
	System::Word __fastcall GetVerbState(int Index);
	System::UnicodeString __fastcall GetSubVerb(int Index);
	System::Word __fastcall GetSubVerbState(int Index);
	bool __fastcall IsMethod(System::UnicodeString strLine);
	bool __fastcall IsUnit(System::UnicodeString strLine);
	System::UnicodeString __fastcall EditorAsString(Toolsapi::_di_IOTASourceEditor SourceEditor);
	Toolsapi::_di_IOTASourceEditor __fastcall ActiveSourceEditor(void);
	Toolsapi::_di_IOTASourceEditor __fastcall SourceEditor(Toolsapi::_di_IOTAModule Module);
	Vcl::Menus::TMenu* __fastcall FindMenu(System::Classes::TComponent* Item);
	System::UnicodeString __fastcall GetBitmapName(int Index);
	System::UnicodeString __fastcall GetSubBitmapName(int Index);
	Toolsapi::TOTAEditPos __fastcall HandleSourceCode(System::Generics::Collections::TList__1<TTMSLoggingItemPosition>* slItems, int iIndex, TTMSLoggingSourceCodeMode AMode, bool ARemove);
	void __fastcall Tick(System::TObject* Sender);
	void __fastcall ExecuteVerb(int Index);
	void __fastcall ExecuteSubVerb(int Index);
	void __fastcall UpdateAction(System::TObject* Sender);
	void __fastcall ExecuteAction(System::TObject* Sender);
	void __fastcall UpdateSubAction(System::TObject* Sender);
	void __fastcall ExecuteSubAction(System::TObject* Sender);
	void __fastcall GetMethods(System::Generics::Collections::TList__1<TTMSLoggingItemPosition>* slItems);
	void __fastcall OutputText(Toolsapi::_di_IOTAEditWriter Writer, int iIndent, System::UnicodeString strText);
	void __fastcall Uncomment(void);
	void __fastcall Comment(void);
	void __fastcall AddMissingUnits(void);
	void __fastcall RemoveLogging(void);
	void __fastcall InsertTextAtCursor(System::UnicodeString AText);
	void __fastcall InsertTextAtPosition(System::UnicodeString AText, const Toolsapi::TOTACharPos &APosition);
	void __fastcall InsertCode(Toolsapi::_di_IOTAEditBuffer EditBuffer, const Toolsapi::TOTAEditPos &APosition, System::UnicodeString Snippet);
	void __fastcall TMSLoggingMenuClick(System::TObject* Sender);
	void __fastcall CreateMenu(void)/* overload */;
	void __fastcall CreateMenu(Vcl::Menus::TMenuItem* Parent, const System::UnicodeString Ident = System::UnicodeString())/* overload */;
	
public:
	__fastcall TTMSLoggingIDEPlugin(void);
	__fastcall virtual ~TTMSLoggingIDEPlugin(void);
	System::UnicodeString __fastcall GetIDString(void);
	System::UnicodeString __fastcall GetName(void);
	Toolsapi::TWizardState __fastcall GetState(void);
	void __fastcall Execute(void);
private:
	void *__IOTAWizard;	// Toolsapi::IOTAWizard 
	
public:
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {B75C0CE0-EEA6-11D1-9504-00608CCBF153}
	operator Toolsapi::_di_IOTAWizard()
	{
		Toolsapi::_di_IOTAWizard intf;
		this->GetInterface(intf);
		return intf;
	}
	#else
	operator Toolsapi::IOTAWizard*(void) { return (Toolsapi::IOTAWizard*)&__IOTAWizard; }
	#endif
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {F17A7BCF-E07D-11D1-AB0B-00C04FB16FB3}
	operator Toolsapi::_di_IOTANotifier()
	{
		Toolsapi::_di_IOTANotifier intf;
		this->GetInterface(intf);
		return intf;
	}
	#else
	operator Toolsapi::IOTANotifier*(void) { return (Toolsapi::IOTANotifier*)&__IOTAWizard; }
	#endif
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
#define sVersion L"1.0.0.0"
static const System::Int8 TMSLogging_AddUnits = System::Int8(0x0);
static const System::Int8 TMSLogging_Register = System::Int8(0x1);
static const System::Int8 TMSLogging_Comment = System::Int8(0x2);
static const System::Int8 TMSLogging_Uncomment = System::Int8(0x3);
static const System::Int8 TMSLogging_Remove = System::Int8(0x4);
static const System::Int8 TMSLoggingItemCount = System::Int8(0x5);
static const System::Int8 TMSLoggingSubItemCount = System::Int8(0x8);
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggingAddUnits;
#define Tmsloggingideplugin_sTMSLoggingAddUnits System::LoadResourceString(&Tmsloggingideplugin::_sTMSLoggingAddUnits)
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggingRegister;
#define Tmsloggingideplugin_sTMSLoggingRegister System::LoadResourceString(&Tmsloggingideplugin::_sTMSLoggingRegister)
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggingUncomment;
#define Tmsloggingideplugin_sTMSLoggingUncomment System::LoadResourceString(&Tmsloggingideplugin::_sTMSLoggingUncomment)
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggingComment;
#define Tmsloggingideplugin_sTMSLoggingComment System::LoadResourceString(&Tmsloggingideplugin::_sTMSLoggingComment)
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggingRemove;
#define Tmsloggingideplugin_sTMSLoggingRemove System::LoadResourceString(&Tmsloggingideplugin::_sTMSLoggingRemove)
extern DELPHI_PACKAGE void __fastcall Register(void);
}	/* namespace Tmsloggingideplugin */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGIDEPLUGIN)
using namespace Tmsloggingideplugin;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TmsloggingidepluginHPP
