﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingCorePkgDXE10.dpk' rev: 31.00 (Windows)

#ifndef Tmsloggingcorepkgdxe10HPP
#define Tmsloggingcorepkgdxe10HPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// (rtl)
#include <SysInit.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <TMSLoggingBrowserOutputHandler.hpp>
#include <TMSLoggingEventLogOutputHandler.hpp>
#include <TMSLoggingHelpers.hpp>
#include <TMSLoggingHTMLOutputHandler.hpp>
#include <TMSLoggingDataSourceOutputHandler.hpp>
#include <TMSLoggingTCPOutputHandler.hpp>
#include <TMSLoggingTextOutputHandler.hpp>
#include <TMSLoggingCSVOutputHandler.hpp>
#include <TMSLoggingReg.hpp>
#include <Winapi.Windows.hpp>	// (rtl)
#include <Winapi.PsAPI.hpp>	// (rtl)
#include <System.Character.hpp>	// (rtl)
#include <System.Internal.ExcUtils.hpp>	// (rtl)
#include <System.SysUtils.hpp>	// (rtl)
#include <System.VarUtils.hpp>	// (rtl)
#include <System.Variants.hpp>	// (rtl)
#include <System.Rtti.hpp>	// (rtl)
#include <System.TypInfo.hpp>	// (rtl)
#include <System.Math.hpp>	// (rtl)
#include <System.Generics.Defaults.hpp>	// (rtl)
#include <System.Classes.hpp>	// (rtl)
#include <System.TimeSpan.hpp>	// (rtl)
#include <System.DateUtils.hpp>	// (rtl)
#include <System.IOUtils.hpp>	// (rtl)
#include <System.Win.Registry.hpp>	// (rtl)
#include <System.SyncObjs.hpp>	// (rtl)
#include <IdGlobal.hpp>	// (IndySystem)
#include <System.AnsiStrings.hpp>	// (rtl)
#include <IdWinsock2.hpp>	// (IndySystem)
#include <IdWship6.hpp>	// (IndySystem)
#include <IdIDN.hpp>	// (IndySystem)
#include <IdStackWindows.hpp>	// (IndySystem)
#include <IdStack.hpp>	// (IndySystem)
#include <IdGlobalProtocols.hpp>	// (IndyProtocols)
#include <IdCoderMIME.hpp>	// (IndyProtocols)
#include <IdComponent.hpp>	// (IndySystem)
#include <IdIOHandler.hpp>	// (IndyCore)
#include <IdIOHandlerStack.hpp>	// (IndyCore)
#include <IdThread.hpp>	// (IndyCore)
#include <IdAuthentication.hpp>	// (IndyProtocols)
#include <IdHTTPHeaderInfo.hpp>	// (IndyProtocols)
#include <IdSSL.hpp>	// (IndyProtocols)
#include <IdFIPS.hpp>	// (IndyProtocols)
#include <Data.SqlTimSt.hpp>	// (dbrtl)
#include <Data.FmtBcd.hpp>	// (dbrtl)
#include <Data.DB.hpp>	// (dbrtl)
#include <System.Threading.hpp>	// (rtl)

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingcorepkgdxe10
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tmsloggingcorepkgdxe10 */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGCOREPKGDXE10)
using namespace Tmsloggingcorepkgdxe10;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Tmsloggingcorepkgdxe10HPP
