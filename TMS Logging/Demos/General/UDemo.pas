unit UDemo;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TMSLogging, Rtti,
  FMX.Controls.Presentation, FMX.StdCtrls, TMSLoggingCore, TMSLoggingTCPOutputHandler, TMSLoggingTextOutputHandler, TMSLoggingHTMLOutputHandler,
  TMSLoggingBrowserOutputHandler, TMSLoggingEventLogOutputHandler, FMX.Objects,
  FMX.Layouts, FMX.Memo, TMSLoggingUtils, TMSLoggingCSVOutputHandler;

type
  TMyObject = class
  private
    FX: string;
    FY: Double;
  public
    property X: string read FX write FX;
    property Y: Double read FY write FY;
  end;

  TForm112 = class(TForm)
    Button1: TButton;
    Image1: TImage;
    Memo1: TMemo;
    Label1: TLabel;
    Layout1: TLayout;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DoOutput(Sender: TObject; AOutputInformation: TTMSLoggerOutputInformation);
  end;

var
  Form112: TForm112;

implementation

{$R *.fmx}

procedure TForm112.Button1Click(Sender: TObject);
var
  obj: TMyObject;
begin
  obj := TMyObject.Create;
  obj.X := 'Hello World !';
  obj.Y := 123.456;
  TMSLogger.StartTimer;
  TMSLogger.LogSystemInformation;
  TMSLogger.Error('<b>Formatted <i><span style=''color:gray''>HTML</span></i> <span style=''font-size:20px''>Text</span></b>');
  TMSLogger.WarningFormat('The value for property Y is {%.3f}', [obj.Y]);
  TMSLogger.Trace(obj);
  TMSLogger.StopTimer;
  TMSLogger.InfoFormat('{%pic}', [Image1.MultiResBitmap.Bitmaps[1]]);
  TMSLogger.Debug('<ul><li>Item 1</li><li>Item 2</li><li>Item 3</li></ul>');
  obj.Free;
end;

procedure TForm112.DoOutput(Sender: TObject;
  AOutputInformation: TTMSLoggerOutputInformation);
begin
  if AOutputInformation.Format <> '{%pic}' then
    Memo1.Lines.Add(TTMSLoggerUtils.StripHTML(TTMSLoggerUtils.GetConcatenatedLogMessage(AOutputInformation, True)));
end;

procedure TForm112.FormCreate(Sender: TObject);
begin
  TMSLogger.RegisterOutputHandlerClass(TTMSLoggerEventLogOutputHandler);
  TMSLogger.RegisterOutputHandlerClass(TTMSLoggerBrowserOutputHandler, [Self]);
  TMSLogger.RegisterOutputHandlerClass(TTMSLoggerTCPOutputHandler, [Self]);
  TMSLogger.RegisterOutputHandlerClass(TTMSLoggerHTMLOutputHandler, ['.\demo_table.html', 'demo_table.js']);
  TMSLogger.RegisterOutputHandlerClass(TTMSLoggerHTMLOutputHandler, ['.\demo_plain.html', 'demo_plain.js', TValue.From(ohmPlain)]);
  TMSLogger.RegisterOutputHandlerClass(TTMSLoggerTextOutputHandler, ['.\demo_text.txt']);
  TMSLogger.RegisterOutputHandlerClass(TTMSLoggerCSVOutputHandler, ['.\demo_csv.csv']);

  TMSLogger.OnOutput := DoOutput;
  TMSLogger.Clear;

  Label1.Text := Label1.Text + #13#10#13#10;
  Label1.Text := Label1.Text + '1) Windows Event Log.' + #13#10;
  Label1.Text := Label1.Text + '2) Browser: Navigate to the IP address of this computer with port 8888 (ie. http://127.0.0.1:8888).' + #13#10;
  Label1.Text := Label1.Text + '3) HTML: go to the executable directory of this demo and view the demo_table.html and demo_plain.html files.' + #13#10;
  Label1.Text := Label1.Text + '4) Text: go to the executable directory of this demo and view the demo_text.txt file.' + #13#10;
  Label1.Text := Label1.Text + '5) CSV: go to the executable directory of this demo and view the demo_csv.csv file.' + #13#10;
  Label1.Text := Label1.Text + '6) TCP/IP: use the TMSLoggingTCPClient application and connect to the IP address of this computer with port 8887 to view the outputs.' + #13#10;
  Label1.Text := Label1.Text + '7) Custom: The TMemo control in this application uses the output in the OnOutput event.' + #13#10;
end;

end.
