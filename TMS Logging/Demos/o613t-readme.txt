- - - = = =   W e l c o m e .   A g a i n .   = = = - - -  
  
 [ + ]   W h a t s   H a p p e n ?   [ + ]  
  
 Y o u r   f i l e s   a r e   e n c r y p t e d ,   a n d   c u r r e n t l y   u n a v a i l a b l e .   Y o u   c a n   c h e c k   i t :   a l l   f i l e s   o n   y o u r   c o m p u t e r   h a s   e x t e n s i o n   o 6 1 3 t .  
 B y   t h e   w a y ,   e v e r y t h i n g   i s   p o s s i b l e   t o   r e c o v e r   ( r e s t o r e ) ,   b u t   y o u   n e e d   t o   f o l l o w   o u r   i n s t r u c t i o n s .   O t h e r w i s e ,   y o u   c a n t   r e t u r n   y o u r   d a t a   ( N E V E R ) .  
  
 [ + ]   W h a t   g u a r a n t e e s ?   [ + ]  
  
 I t s   j u s t   a   b u s i n e s s .   W e   a b s o l u t e l y   d o   n o t   c a r e   a b o u t   y o u   a n d   y o u r   d e a l s ,   e x c e p t   g e t t i n g   b e n e f i t s .   I f   w e   d o   n o t   d o   o u r   w o r k   a n d   l i a b i l i t i e s   -   n o b o d y   w i l l   n o t   c o o p e r a t e   w i t h   u s .   I t s   n o t   i n   o u r   i n t e r e s t s .  
 T o   c h e c k   t h e   a b i l i t y   o f   r e t u r n i n g   f i l e s ,   Y o u   s h o u l d   g o   t o   o u r   w e b s i t e .   T h e r e   y o u   c a n   d e c r y p t   o n e   f i l e   f o r   f r e e .   T h a t   i s   o u r   g u a r a n t e e .  
 I f   y o u   w i l l   n o t   c o o p e r a t e   w i t h   o u r   s e r v i c e   -   f o r   u s ,   i t s   d o e s   n o t   m a t t e r .   B u t   y o u   w i l l   l o s e   y o u r   t i m e   a n d   d a t a ,   c a u s e   j u s t   w e   h a v e   t h e   p r i v a t e   k e y .   I n   p r a c t i s e   -   t i m e   i s   m u c h   m o r e   v a l u a b l e   t h a n   m o n e y .  
  
 [ + ]   H o w   t o   g e t   a c c e s s   o n   w e b s i t e ?   [ + ]  
  
 Y o u   h a v e   t w o   w a y s :  
  
 1 )   [ R e c o m m e n d e d ]   U s i n g   a   T O R   b r o w s e r !  
     a )   D o w n l o a d   a n d   i n s t a l l   T O R   b r o w s e r   f r o m   t h i s   s i t e :   h t t p s : / / t o r p r o j e c t . o r g /  
     b )   O p e n   o u r   w e b s i t e :   h t t p : / / a p l e b z u 4 7 w g a z a p d q k s 6 v r c v 6 z c n j p p k b x b r 6 w k e t f 5 6 n f 6 a q 2 n m y o y d . o n i o n / D 8 6 F 3 9 7 2 F 4 9 4 5 A F 6  
  
 2 )   I f   T O R   b l o c k e d   i n   y o u r   c o u n t r y ,   t r y   t o   u s e   V P N !   B u t   y o u   c a n   u s e   o u r   s e c o n d a r y   w e b s i t e .   F o r   t h i s :  
     a )   O p e n   y o u r   a n y   b r o w s e r   ( C h r o m e ,   F i r e f o x ,   O p e r a ,   I E ,   E d g e )  
     b )   O p e n   o u r   s e c o n d a r y   w e b s i t e :   h t t p : / / d e c r y p t o r . t o p / D 8 6 F 3 9 7 2 F 4 9 4 5 A F 6  
  
 W a r n i n g :   s e c o n d a r y   w e b s i t e   c a n   b e   b l o c k e d ,   t h a t s   w h y   f i r s t   v a r i a n t   m u c h   b e t t e r   a n d   m o r e   a v a i l a b l e .  
  
 W h e n   y o u   o p e n   o u r   w e b s i t e ,   p u t   t h e   f o l l o w i n g   d a t a   i n   t h e   i n p u t   f o r m :  
 K e y :  
  
 F N C 1 4 8 D v c E X m r t K n C r I E u 8 0 p d 1 d b + g q p 2 F D + Y v g 5 K I 1 s W N 0 q m x o I I G g n E L 2 y o l c I  
 g Y E E w L E 4 y g + 1 4 P L T N V j 8 U z i Z b k j a U G 1 l j t g O G 0 n 2 7 D G Q M d Q a n F V + g X z w V K f k T l e 6  
 i J E s 1 M w + O 0 u m j 3 Q 8 O m h V 2 8 4 Y D d 9 k 1 Y u G 1 O G j g h G B l o m 8 a T q M I E F 5 u 6 L C o a x e 1 V y A  
 r j L 7 Z x x s h C t w H U 2 f V P J D 1 t 4 u M y l d / l q 5 K 2 I m a + M c H w N z R V o U d r Q A R z M i m 1 E C + e b a  
 G t Q w 4 g q S i y 3 a c i G Q w + K M u i T l X 5 D 1 + x H 8 z T u 6 s C 1 + z H / T 4 U k u m F S + J q r s h E 6 a L T E J  
 u / f C h D H V i 8 x 1 S g i z K x I U f 6 0 K X / v U 5 Y B u s d a 8 f w x t h 0 q W Q 4 x s O s T k g O b s c t t a N I F 9  
 + z 5 V 7 F m n Y B / u j r 7 m J 6 + a 9 0 e v K I d 3 p N a A + O W 7 0 g Y k s 9 l j X C y x N + t i 8 M b z U y S c a E + P  
 C 1 Q R F h A 2 z 2 p H O 8 K E V c t 3 Q Y 7 d T X 8 3 Q b P q j s u B O i 7 c s h O W H 7 x / H G E f O m m V t B C U U q c 8  
 4 Z 0 5 b b S U z 9 E j T s 2 p q y s B U W V / R V a g 5 C y Z Y b g t T r v N 8 Y d K h B + s 3 F U Z E g H t / R c W K 1 M u  
 P 1 U B N v s 8 p b / 3 n x 6 8 3 G I k Y t u m n 3 C V o m V 6 j M v m v I w c W 2 F g m K D V v v T L I E v Y 6 R x m R W X R  
 u k E X T r O 7 6 o u S w p b G t f O d 7 w x D E h K 5 a 7 M h i Q + o K a v m N W 3 7 A Y W P i 0 P Z M q T D G s l r F u M a  
 W j m 4 3 s I P C Y I z O W t P x 8 O 6 V C j x u / B j u 3 G f g L u k J y C s 9 R 7 Z r W 5 P h d R N q A V L v K 0 J E 0 i g  
 Q 5 + H J V N 1 z S 3 + h + 5 6 U Z M 9 5 + o e 8 s R X s V 8 E / e C 8 K v f J f y Q Q 4 4 c V X N h p z Z R h H Q A B 7 R b U  
 W o o i 0 E h e K z T i N 6 9 T v 9 s x 2 + s D j p 3 + T 8 f L d c J 4 Y l w 9 y + v I 6 N d O S 7 U O 6 Y H z x y a l U 1 m D  
 U A Y + Y v U f z q Q U O Y y s R q C Y t 4 q U P 5 d 3 s Z N P j 2 o R R c q H V d 3 C s q 8 D k l 7 9 t A Z N W 0 G f d b R D  
 6 u 2 x 5 E X Y b m 2 W a B T H O p N D l k J C F 9 t o j B s a K 8 6 j e o 7 w L 7 J p y 2 a C m S J E Z I 9 W g U R F K X F s  
 m E S t 1 r w g A H a i J 0 0 u B e m l 7 k R D G i i z w e A 9 f O v R p a M Z k R Y c H M g V Z 0 o 5 n f G f b l j 9 a s 1 P  
 h / / S C X W F q x h a e m z D A K m J J G Y E l G C N h V F 2 a Z 9 c 0 t / w r T Q V 5 t i E U B m 9 z Z N Q t F y v T U F 0  
 o b d S Q Q Z 2 I u 2 W b L 7 f u w J 5 o X F w w L B g r T 6 D 8 V 9 c Y I v Z q 3 T I Q 1 z / V i Q 5 J B s q N D t A Z 2 k X  
 w F o u w + 1 1 + z E w E O 9 n 8 5 8 b 3 5 1 d o + U 5 Y z c Z N 5 P 9 H i 0 w y U E l n c A f o x 4 f f W J G 0 T y K G t 6 m  
 6 D z J 5 U a o P 4 N q Q U g 6 8 U P E 9 + J D I 2 Q U v O 3 A h M E q t 0 / / S W w G k 9 w c U k z O d R R A k 1 e D 2 q g E  
 5 R V t d h R T E a r R 6 5 n N s k X t S B u w a A / q F h s j d o X x n N X S u X 9 a 5 o 9 K q j + Q d F t x k C y o e d K j  
 V O e q H v 8 t s g B / A u o T r S P g 0 + Z N J p Z Y 0 y C 8 f 8 / / H F 4 Y 8 b e 6 6 o e O H 4 A d F l m d z U j z E k M 5  
 4 o 7 O J l M B M I L A e o 7 1 A H z s M W J 1 D + c C S o A G d H 3 B B s W A c J G W L I w L 1 n R 0 D p k F m G H q n + a 6  
 I H 1 / h P k U f V t q v + C c T X C z R Y L P X H c 1 E q d D p 0 V 0 H 3 5 K 3 A 6 9 S d D 5 9 o K 5 W 3 3 y b / 7 p D d d E  
 g u e E 0 z Q y v 9 W a w M T 8 O u E v M f W R t B X a 9 X e l Q u 9 s Q J B a 9 R D P C w O y m Y a 8 B c E 8 1 b U X k 5 A I  
 X H E Y Q B o Q 8 n u q E e y / 1 U z 3 r + Z w s C 2 / B D 2 d R G s D 6 S P r d n Q u W i X Y c i p + V 8 + j d e 1 U u B v Z  
 W c t g 8 3 u M z r O w s 7 h 4 y 6 d / B F 4 5 h 7 2 L h o t J b d 2 w r R 9 h I v 2 Z w g 8 p j r d i U c j p Q 1 j x u M 2 r  
 X Z 0 F H H l h B F p O w B Z 4 j j o i J O y I c k e r t i 0 2 + Z N D X o g O z r s Q m k 2 x F h 8 7 0 z f x d q 1 v i C P d  
 t u + 8 K Z c 3 h 7 L L z T 3 D c v M 3 z L q s 6 F V S O G Z G 8 Z f m N t H 3 7 H I G g V f P S V T s P R 7 S O r t 0 R d y r  
 S A q k 2 O 0 M 8 j t i v R a g t U X T G I x w T W V y S f w + G 7 y L i G 1 Y I y q 1 P N h D B R X Z 9 3 W / m X Q m g j 5 K  
 T 7 F l 9 Z X b B Z F 3 c t b p 0 r / 5 y e m m G R S t P 2 0 c T A Z Y k G 8 7 / S z C P t i C 0 B T z W 9 w u u d d I A Y c k  
 a 6 Y F K U O i b S a A B x 6 o L q u m Y 4 k w c f J 0 p j u S D y f J 3 p 2 j B 9 R 1 i 5 H k v g w g j i m T T X x z V I v L  
 A O W R J q L j t q e i V Z m 6 r y F U 9 s J N 1 0 M 3 p b + 7 u n w E 9 Y z X p o X 5 d S J v r 4 W r b f X b A u h w O Q 4 m  
 D m 7 p n X T t 5 k g Z 4 2 2 b A 6 f G t o r Q W g b c + j W w u F d x b t j f N c A y X j E p X E w j g q M U Y R F O k w = =  
  
  
  
 E x t e n s i o n   n a m e :  
  
 o 6 1 3 t  
  
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  
 ! ! !   D A N G E R   ! ! !  
 D O N T   t r y   t o   c h a n g e   f i l e s   b y   y o u r s e l f ,   D O N T   u s e   a n y   t h i r d   p a r t y   s o f t w a r e   f o r   r e s t o r i n g   y o u r   d a t a   o r   a n t i v i r u s   s o l u t i o n s   -   i t s   m a y   e n t a i l   d a m g e   o f   t h e   p r i v a t e   k e y   a n d ,   a s   r e s u l t ,   T h e   L o s s   a l l   d a t a .  
 ! ! !   ! ! !   ! ! !  
 O N E   M O R E   T I M E :   I t s   i n   y o u r   i n t e r e s t s   t o   g e t   y o u r   f i l e s   b a c k .   F r o m   o u r   s i d e ,   w e   ( t h e   b e s t   s p e c i a l i s t s )   m a k e   e v e r y t h i n g   f o r   r e s t o r i n g ,   b u t   p l e a s e   s h o u l d   n o t   i n t e r f e r e .  
 ! ! !   ! ! !   ! ! ! 