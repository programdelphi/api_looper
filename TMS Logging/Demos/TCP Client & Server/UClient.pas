unit UClient;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Edit, FMX.TMSLogging, TMSLoggingTCPOutputHandler;

type
  TMyObject = class
  private
    FX: string;
    FY: Double;
  public
    property X: string read FX write FX;
    property Y: Double read FY write FY;
  end;

  TForm133 = class(TForm)
    Label2: TLabel;
    Line1: TLine;
    Edit1: TEdit;
    Label1: TLabel;
    Label3: TLabel;
    Edit2: TEdit;
    Button1: TButton;
    Panel1: TPanel;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Image1: TImage;
    Button5: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
    FConnected: Boolean;
    cl: TTMSLoggerTCPOutputHandler;
  public
    { Public declarations }
    procedure Connect;
    procedure Disconnect;
  end;

var
  Form133: TForm133;

implementation

{$R *.fmx}

procedure TForm133.Button1Click(Sender: TObject);
begin
  if not FConnected then
    Connect
  else
    Disconnect;
end;

procedure TForm133.Button2Click(Sender: TObject);
var
  a, b, c: Integer;
begin
  a := 10;
  b := 0;
  c := a div b;
end;

procedure TForm133.Button3Click(Sender: TObject);
begin
  TMSLogger.LogSystemInformation;
end;

procedure TForm133.Button4Click(Sender: TObject);
begin
  TMSLogger.ErrorFormat('{%pichex}', [Image1.Bitmap]);
end;

procedure TForm133.Button5Click(Sender: TObject);
var
  obj: TMyObject;
begin
  obj := TMyObject.Create;
  obj.X := 'Hello World !';
  obj.Y := 123.456;
  TMSLogger.StartTimer;
  TMSLogger.LogSystemInformation;
  TMSLogger.Error('<b>Formatted <i><span style=''color:gray''>HTML</span></i> <span style=''font-size:20px''>Text</span></b>');
  TMSLogger.WarningFormat('The value for property Y is {%.3f}', [obj.Y]);
  TMSLogger.Trace(obj);
  TMSLogger.StopTimer;
  TMSLogger.InfoFormat('{%pichex}', [Image1.MultiResBitmap.Bitmaps[1]]);
  TMSLogger.Debug('<ul><li>Item 1</li><li>Item 2</li><li>Item 3</li></ul>');
  obj.Free;
end;

procedure TForm133.Connect;
begin
  try
    cl := TTMSLoggerTCPOutputHandler(TMSLogger.RegisterOutputHandlerClass(TTMSLoggerTCPClientOutputHandler, [Self, Edit1.Text, StrToInt(Edit2.Text)]));
  finally
    if Assigned(cl) and Assigned(cl.Client) and cl.Client.Connected then
    begin
      Panel1.Enabled := True;
      Edit1.Enabled := False;
      Edit2.Enabled := False;
      Button1.Text := 'Disconnect';
      FConnected := True;
    end;
  end;
end;

procedure TForm133.Disconnect;
begin
  if Assigned(cl) and Assigned(cl.Client) then
    cl.Client.Disconnect;

  if Assigned(cl.Client) and not cl.Client.Connected then
  begin
    TMSLogger.UnregisterAllOutputHandlers;
    cl := nil;
    Panel1.Enabled := False;
    Edit1.Enabled := True;
    Edit2.Enabled := True;
    Button1.Text := 'Connect';
    FConnected := False;
  end;
end;

procedure TForm133.FormCreate(Sender: TObject);
begin
  TMSLogger.UnregisterAllOutputHandlers;
  TMSLogger.ExceptionHandling := True;
  TMSLogger.OutputFormats.ValueFormat := '{%s}';
  TMSLogger.OutputFormats.TimeStampFormat := '{%dt}';
  TMSLogger.OutputFormats.NameFormat := '{%s}';
  TMSLogger.OutputFormats.TypeFormat := '{%s}';
end;

end.
