unit UServer;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  IdBaseComponent, IdComponent, IdCustomTCPServer, IdTCPServer,
  TMSLoggingTCPOutputHandler, TMSLoggingUtils, IdContext,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.ListBox, FMX.Objects,
  FMX.ListView.Types, FMX.ListView;

type
  TForm132 = class(TForm)
    TMSLoggerTCPServer1: TTMSLoggerTCPServer;
    ListBox1: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    Line1: TLine;
    ListView1: TListView;
    Label3: TLabel;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure TMSLoggerTCPServer1Execute(AContext: TIdContext);
    procedure TMSLoggerTCPServer1ReceivedOutputInformation(Sender: TObject;
      AOutputInformation: TTMSLoggerOutputInformation);
    procedure TMSLoggerTCPServer1Connect(AContext: TIdContext);
    procedure TMSLoggerTCPServer1Disconnect(AContext: TIdContext);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form132: TForm132;

implementation

{$R *.fmx}

procedure TForm132.Button1Click(Sender: TObject);
begin
  ListView1.Items.Clear;
end;

procedure TForm132.FormCreate(Sender: TObject);
begin
  TMSLoggerTCPServer1.DefaultPort := 8887;
  TMSLoggerTCPServer1.Active := True;
end;

procedure TForm132.TMSLoggerTCPServer1Connect(AContext: TIdContext);
begin
  TThread.Queue(TThread.CurrentThread,
  procedure
  begin
    ListBox1.Items.Add(AContext.Connection.Socket.Binding.PeerIP);
  end
  );
end;

procedure TForm132.TMSLoggerTCPServer1Disconnect(AContext: TIdContext);
begin
  if csDestroying in ComponentState then
    Exit;

  TThread.Queue(TThread.CurrentThread,
  procedure
  var
    I: Integer;
  begin
    for I := ListBox1.Items.Count - 1 downto 0 do
      if ListBox1.Items[I] = AContext.Connection.Socket.Binding.PeerIP then
        ListBox1.Items.Delete(I);
  end
  );
end;

procedure TForm132.TMSLoggerTCPServer1Execute(AContext: TIdContext);
begin
//
end;

procedure TForm132.TMSLoggerTCPServer1ReceivedOutputInformation(Sender: TObject;
  AOutputInformation: TTMSLoggerOutputInformation);
begin
  TThread.Queue(TThread.CurrentThread,
  procedure
  var
    lv: TListViewItem;
    picstr: string;
    bst: TBytesStream;
    sst: TStringStream;
    s: TStream;
    msg: string;
  begin
    lv := ListView1.Items.Add;
    msg := AOutputInformation.ValueOutput;
    picstr := TTMSLoggerUtils.ExtractPicture(AOutputInformation.ValueOutput);
    msg := msg.Replace(BeginPictureTag + picstr + EndPictureTag, 'IMAGE');

    if picstr <> '' then
    begin
      sst := nil;
      bst := nil;
      if AOutputInformation.Format.ToUpper.Contains('HEX') then
      begin
        bst := TTMSLoggerUtils.HexStringToByteStream(picstr);
        s := bst;
      end
      else
      begin
        sst := TStringStream.Create;
        sst.WriteString(picstr);
        s := sst;
      end;

      try
        lv.Bitmap.LoadFromStream(s);
      finally
        if Assigned(bst) then
          bst.Free;

        if Assigned(sst) then
           sst.Free;
      end;
    end;


    lv.Text := AOutputInformation.TimeStampOutput + ' | ' + AOutputInformation.Host + ' | ' + msg;
    lv.Detail := AOutputInformation.LogLevelOutput + ' | ' + AOutputInformation.NameOutput + ' | ' + AOutputInformation.TypeOutput;
  end
  );
end;

end.
