﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingUtils.pas' rev: 31.00 (iOS)

#ifndef TmsloggingutilsHPP
#define TmsloggingutilsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <IdGlobal.hpp>
#include <System.SysUtils.hpp>
#include <System.Rtti.hpp>
#include <System.Generics.Collections.hpp>
#include <System.UITypes.hpp>
#include <System.Diagnostics.hpp>
#include <System.Types.hpp>
#include <System.Generics.Defaults.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingutils
{
//-- forward type declarations -----------------------------------------------
struct TTMSLoggerOutputParameters;
struct TTMSLoggerOutputInformation;
class DELPHICLASS TTMSLoggerUtils;
/*Helper*/typedef void *TTMSLoggerLogLevelHelper;

//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TTMSLoggerLogLevel : unsigned char { Trace, Debug, Info, Warning, Error, Exception, All };

enum DECLSPEC_DENUM TTMSLoggerTimeStampOutputMode : unsigned char { tsomDateTime, tsomMilliseconds, tsomMicroseconds, tsomTicks, tsomMillisecondsDelta, tsomMicrosecondsDelta, tsomTicksDelta };

enum DECLSPEC_DENUM TTMSLoggerHTMLOutputHandlerMode : unsigned char { ohmPlain, ohmTable, ohmTableFilter };

typedef System::DynamicArray<System::Rtti::TValue> TTMSLoggerValueArray;

typedef System::Generics::Collections::TList__1<System::UnicodeString> * TTMSLoggerStringList;

typedef System::DynamicArray<System::UnicodeString> TTMSLoggerStringArray;

enum DECLSPEC_DENUM TTMSLoggerFilter : unsigned char { lfPublic, lfPublished, lfPublicWithAttributes, lfPublishedWithAttributes };

typedef System::Set<TTMSLoggerFilter, TTMSLoggerFilter::lfPublic, TTMSLoggerFilter::lfPublishedWithAttributes> TTMSLoggerFilters;

typedef System::Set<TTMSLoggerLogLevel, TTMSLoggerLogLevel::Trace, TTMSLoggerLogLevel::All> TTMSLoggerLogLevelFilters;

enum DECLSPEC_DENUM TTMSLoggerOutput : unsigned char { loTimeStamp, loProcessID, loThreadID, loMemoryUsage, loLogLevel, loName, loValue, loType };

typedef System::Set<TTMSLoggerOutput, TTMSLoggerOutput::loTimeStamp, TTMSLoggerOutput::loType> TTMSLoggerOutputs;

struct DECLSPEC_DRECORD TTMSLoggerOutputParameters
{
public:
	System::Uitypes::TAlphaColor Color;
	static TTMSLoggerOutputParameters __fastcall Default();
	static TTMSLoggerOutputParameters __fastcall Error();
	static TTMSLoggerOutputParameters __fastcall Trace();
	static TTMSLoggerOutputParameters __fastcall Debug();
	static TTMSLoggerOutputParameters __fastcall Info();
	static TTMSLoggerOutputParameters __fastcall Warning();
	static TTMSLoggerOutputParameters __fastcall Exception();
	__fastcall TTMSLoggerOutputParameters(const System::Uitypes::TAlphaColor AColor);
	TTMSLoggerOutputParameters() {}
};


struct DECLSPEC_DRECORD TTMSLoggerOutputInformation
{
public:
	TTMSLoggerValueArray Values;
	TTMSLoggerLogLevel LogLevel;
	int Indent;
	TTMSLoggerStringArray ValueNames;
	TTMSLoggerOutputParameters OutputParameters;
	System::UnicodeString TimeStampOutput;
	System::UnicodeString ProcessIDOutput;
	System::UnicodeString MemoryUsageOutput;
	System::UnicodeString ThreadIDOutput;
	System::UnicodeString ValueOutput;
	System::UnicodeString TypeOutput;
	System::UnicodeString NameOutput;
	System::UnicodeString LogLevelOutput;
	System::TDateTime TimeStamp;
	unsigned ProcessID;
	unsigned MemoryUsage;
	unsigned ThreadID;
	System::UnicodeString Format;
	TTMSLoggerOutputs Outputs;
	System::UnicodeString ID;
	System::UnicodeString Host;
	static TTMSLoggerOutputInformation __fastcall Default();
	__fastcall TTMSLoggerOutputInformation(const TTMSLoggerValueArray AValues, const TTMSLoggerStringArray AValueNames, const TTMSLoggerOutputParameters &AOutputParameters, const System::UnicodeString AFormat, const TTMSLoggerOutputs AOutputs, const int AIndent, const System::TDateTime ATimeStamp, const unsigned AProcessID, const unsigned AThreadID, const unsigned AMemoryUsage, const System::UnicodeString AID, const System::UnicodeString AHost, const TTMSLoggerLogLevel ALogLevel);
	TTMSLoggerOutputInformation() {}
};


enum DECLSPEC_DENUM TTMSLoggerTimerMode : unsigned char { lsmTicks, lsmMicroseconds, lsmMilliseconds };

class PASCALIMPLEMENTATION TTMSLoggerUtils : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	static System::Diagnostics::TStopwatch FStopwatch;
	
public:
	__classmethod void __fastcall StartTimer();
	__classmethod bool __fastcall IsTimerRunning();
	__classmethod System::UnicodeString __fastcall GetSystemInformation();
	__classmethod __int64 __fastcall GetTimer(const TTMSLoggerTimerMode AMode = (TTMSLoggerTimerMode)(0x2));
	__classmethod __int64 __fastcall StopTimer(const TTMSLoggerTimerMode AMode = (TTMSLoggerTimerMode)(0x2));
	__classmethod unsigned __fastcall GetThreadID();
	__classmethod System::UnicodeString __fastcall GetIPAddress();
	__classmethod unsigned __fastcall GetProcessID();
	__classmethod System::UnicodeString __fastcall GetCurrentLangID();
	__classmethod unsigned __fastcall GetMemoryUsage();
	__classmethod System::UnicodeString __fastcall Encode64String(const System::UnicodeString AValue);
	__classmethod System::UnicodeString __fastcall Encode64Bytes(const System::TArray__1<System::Byte> AValue);
	__classmethod System::UnicodeString __fastcall Decode64String(const System::UnicodeString AValue);
	__classmethod System::TArray__1<System::Byte> __fastcall Decode64Bytes(const System::UnicodeString AValue);
	__classmethod System::UnicodeString __fastcall IntToBinByte(const System::Byte AValue, const int ADecimals);
	__classmethod System::UnicodeString __fastcall GetIndent(const int AIndent);
	__classmethod System::UnicodeString __fastcall GetConcatenatedLogMessage(const TTMSLoggerOutputInformation &AOutputInformation, const bool AIndent = false, const System::WideChar ADelimiter = (System::WideChar)(0x0));
	__classmethod System::UnicodeString __fastcall AddBackslash(const System::UnicodeString AValue);
	__classmethod System::UnicodeString __fastcall ColorToHTML(const System::Uitypes::TAlphaColor AValue);
	__classmethod System::UnicodeString __fastcall StripHTML(const System::UnicodeString AValue);
	__classmethod System::Classes::TResourceStream* __fastcall GetResourceStream(const System::UnicodeString AResourceName);
	__classmethod System::UnicodeString __fastcall ExtractPicture(const System::UnicodeString AValue);
	__classmethod System::Classes::TBytesStream* __fastcall HexStringToByteStream(const System::UnicodeString AValue, const int ADigits = 0x2);
	__classmethod System::TArray__1<System::Byte> __fastcall HexStrToBytes(const System::UnicodeString AValue, const int ADigits = 0x2);
	__classmethod System::UnicodeString __fastcall GetHTMLFormattedMessage(const TTMSLoggerOutputInformation &AOutputInformation, const TTMSLoggerHTMLOutputHandlerMode AMode, const bool AApplyOutputParameters, const bool ADocumentWrite, const bool AEven);
	__classmethod System::UnicodeString __fastcall GetDefaultOutputFileName();
	__classmethod void __fastcall CreateFileFromResource(const System::UnicodeString AFileName, const System::UnicodeString AResourceName);
	__classmethod void __fastcall ReplaceTextInFile(const System::UnicodeString AFileName, const System::UnicodeString AText, const System::UnicodeString AReplaceText);
	__classmethod void __fastcall AppendStream(const System::UnicodeString AFileName, System::Classes::TStringStream* const AStream);
	__classmethod void __fastcall LogToConsole(const System::UnicodeString AValue);
public:
	/* TObject.Create */ inline __fastcall TTMSLoggerUtils(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TTMSLoggerUtils(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggerTimeStamp;
#define Tmsloggingutils_sTMSLoggerTimeStamp System::LoadResourceString(&Tmsloggingutils::_sTMSLoggerTimeStamp)
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggerMemoryUsage;
#define Tmsloggingutils_sTMSLoggerMemoryUsage System::LoadResourceString(&Tmsloggingutils::_sTMSLoggerMemoryUsage)
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggerProcessID;
#define Tmsloggingutils_sTMSLoggerProcessID System::LoadResourceString(&Tmsloggingutils::_sTMSLoggerProcessID)
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggerThreadID;
#define Tmsloggingutils_sTMSLoggerThreadID System::LoadResourceString(&Tmsloggingutils::_sTMSLoggerThreadID)
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggerLogLevel;
#define Tmsloggingutils_sTMSLoggerLogLevel System::LoadResourceString(&Tmsloggingutils::_sTMSLoggerLogLevel)
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggerName;
#define Tmsloggingutils_sTMSLoggerName System::LoadResourceString(&Tmsloggingutils::_sTMSLoggerName)
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggerValue;
#define Tmsloggingutils_sTMSLoggerValue System::LoadResourceString(&Tmsloggingutils::_sTMSLoggerValue)
extern DELPHI_PACKAGE System::ResourceString _sTMSLoggerType;
#define Tmsloggingutils_sTMSLoggerType System::LoadResourceString(&Tmsloggingutils::_sTMSLoggerType)
static constexpr System::Int8 TMSLoggerLessThanValue = System::Int8(-1);
static constexpr System::Int8 TMSLoggerEqualsValue = System::Int8(0x0);
static constexpr System::Int8 TMSLoggerGreaterThanValue = System::Int8(0x1);
#define AllOutputs (System::Set<TTMSLoggerOutput, TTMSLoggerOutput::loTimeStamp, TTMSLoggerOutput::loType>() << TTMSLoggerOutput::loTimeStamp << TTMSLoggerOutput::loProcessID << TTMSLoggerOutput::loThreadID << TTMSLoggerOutput::loMemoryUsage << TTMSLoggerOutput::loLogLevel << TTMSLoggerOutput::loName << TTMSLoggerOutput::loValue << TTMSLoggerOutput::loType )
#define BeginPictureTag u"#BEGINPIC#"
#define EndPictureTag u"#ENDPIC#"
}	/* namespace Tmsloggingutils */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGUTILS)
using namespace Tmsloggingutils;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TmsloggingutilsHPP
