﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingCore.pas' rev: 31.00 (iOS)

#ifndef TmsloggingcoreHPP
#define TmsloggingcoreHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SyncObjs.hpp>
#include <System.Generics.Collections.hpp>
#include <System.Rtti.hpp>
#include <TMSLoggingUtils.hpp>
#include <System.IniFiles.hpp>
#include <System.TypInfo.hpp>
#include <System.Diagnostics.hpp>
#include <System.SysUtils.hpp>
#include <System.Generics.Defaults.hpp>
#include <System.Types.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingcore
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TTMSLoggerBaseOutputHandler;
class DELPHICLASS TTMSLoggerConsoleOutputHandler;
class DELPHICLASS TTMSLoggerBaseOutputHandlerSaver;
class DELPHICLASS TTMSLoggerBaseOutputHandlerClassSaver;
class DELPHICLASS TTMSLoggerCustomFileOutputHandler;
class DELPHICLASS TMSLoggerBaseValidation;
class DELPHICLASS TMSLoggerValidation;
class DELPHICLASS TMSLoggerFormattingAttribute;
class DELPHICLASS TMSLoggerClassFilter;
class DELPHICLASS TMSLoggerPropertyFilter;
class DELPHICLASS TMSLoggerRangeValidation;
class DELPHICLASS TMSLoggerDateTimeValidation;
class DELPHICLASS TMSLoggerStringValidation;
class DELPHICLASS TMSLoggerStringLengthValidation;
class DELPHICLASS TMSLoggerRegularExpressionValidation;
class DELPHICLASS TTMSLoggerOutputFormats;
class DELPHICLASS TTMSCustomLogger;
class DELPHICLASS TTMSLoggerSaver;
template<typename T> class DELPHICLASS TTMSSingletonHelper__1;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TTMSLoggerBaseOutputHandler : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::Syncobjs::TCriticalSection* FCriticalSection;
	bool FActive;
	System::UnicodeString FName;
	bool FApplyOutputParameters;
	
protected:
	virtual void __fastcall Clear(void);
	virtual void __fastcall SetActive(const bool Value);
	void __fastcall Log(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual void __fastcall LogOutput(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation) = 0 ;
	virtual void __fastcall WriteValuesToFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	virtual void __fastcall ReadValuesFromFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	
public:
	__fastcall virtual TTMSLoggerBaseOutputHandler(void);
	__fastcall virtual ~TTMSLoggerBaseOutputHandler(void);
	virtual void __fastcall SaveConfigurationToStream(System::Classes::TStream* const AStream);
	virtual void __fastcall LoadConfigurationFromStream(System::Classes::TStream* const AStream);
	virtual void __fastcall SaveConfigurationToFile(const System::UnicodeString AFile, const System::UnicodeString ASection = System::UnicodeString());
	virtual void __fastcall LoadConfigurationFromFile(const System::UnicodeString AFile, const System::UnicodeString ASection = System::UnicodeString());
	virtual void __fastcall SaveConfigurationToFileStream(const System::UnicodeString AFile);
	virtual void __fastcall LoadConfigurationFromFileStream(const System::UnicodeString AFile);
	
__published:
	__property bool Active = {read=FActive, write=SetActive, nodefault};
	__property bool ApplyOutputParameters = {read=FApplyOutputParameters, write=FApplyOutputParameters, nodefault};
	__property System::UnicodeString Name = {read=FName, write=FName};
};


typedef System::Generics::Collections::TList__1<TTMSLoggerBaseOutputHandler*> * TTMSLoggerBaseOutputHandlerList;

typedef System::Generics::Collections::TObjectList__1<TTMSLoggerBaseOutputHandler*> * TTMSLoggerBaseOutputHandlerObjectList;

typedef System::TArray__1<System::TCustomAttribute*> TTMSLoggerCustomAttributeArray;

typedef System::TArray__1<TTMSLoggerBaseOutputHandler*> TTMSLoggerBaseOutputHandlerArray;

class PASCALIMPLEMENTATION TTMSLoggerConsoleOutputHandler : public TTMSLoggerBaseOutputHandler
{
	typedef TTMSLoggerBaseOutputHandler inherited;
	
protected:
	virtual void __fastcall LogOutput(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	
public:
	virtual void __fastcall LogString(const System::UnicodeString AValue);
	__fastcall virtual TTMSLoggerConsoleOutputHandler(void);
public:
	/* TTMSLoggerBaseOutputHandler.Destroy */ inline __fastcall virtual ~TTMSLoggerConsoleOutputHandler(void) { }
	
};


class PASCALIMPLEMENTATION TTMSLoggerBaseOutputHandlerSaver : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	TTMSLoggerBaseOutputHandler* FOutputHandler;
	
__published:
	__property TTMSLoggerBaseOutputHandler* OutputHandler = {read=FOutputHandler, write=FOutputHandler};
public:
	/* TComponent.Create */ inline __fastcall virtual TTMSLoggerBaseOutputHandlerSaver(System::Classes::TComponent* AOwner) : System::Classes::TComponent(AOwner) { }
	/* TComponent.Destroy */ inline __fastcall virtual ~TTMSLoggerBaseOutputHandlerSaver(void) { }
	
};


class PASCALIMPLEMENTATION TTMSLoggerBaseOutputHandlerClassSaver : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	System::UnicodeString FOutputHandlerClassName;
	bool FManaged;
	System::UnicodeString FOutputHandlerName;
	
__published:
	__property System::UnicodeString OutputHandlerClassName = {read=FOutputHandlerClassName, write=FOutputHandlerClassName};
	__property System::UnicodeString OutputHandlerName = {read=FOutputHandlerName, write=FOutputHandlerName};
	__property bool Managed = {read=FManaged, write=FManaged, nodefault};
public:
	/* TComponent.Create */ inline __fastcall virtual TTMSLoggerBaseOutputHandlerClassSaver(System::Classes::TComponent* AOwner) : System::Classes::TComponent(AOwner) { }
	/* TComponent.Destroy */ inline __fastcall virtual ~TTMSLoggerBaseOutputHandlerClassSaver(void) { }
	
};


class PASCALIMPLEMENTATION TTMSLoggerCustomFileOutputHandler : public TTMSLoggerBaseOutputHandler
{
	typedef TTMSLoggerBaseOutputHandler inherited;
	
private:
	System::UnicodeString FFileName;
	
protected:
	virtual System::UnicodeString __fastcall GetOutputText(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation) = 0 ;
	virtual System::UnicodeString __fastcall GetOutputExtension(void);
	virtual System::UnicodeString __fastcall GetOutputFile(void);
	virtual void __fastcall Clear(void);
	virtual void __fastcall LogOutput(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual void __fastcall WriteValuesToFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	virtual void __fastcall ReadValuesFromFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	
public:
	__fastcall virtual TTMSLoggerCustomFileOutputHandler(void)/* overload */;
	__fastcall virtual TTMSLoggerCustomFileOutputHandler(const System::UnicodeString AFileName)/* overload */;
	
__published:
	__property System::UnicodeString FileName = {read=FFileName, write=FFileName};
public:
	/* TTMSLoggerBaseOutputHandler.Destroy */ inline __fastcall virtual ~TTMSLoggerCustomFileOutputHandler(void) { }
	
};


_DECLARE_METACLASS(System::TMetaClass, TTMSLoggerBaseOutputHandlerClass);

class PASCALIMPLEMENTATION TMSLoggerBaseValidation : public System::TCustomAttribute
{
	typedef System::TCustomAttribute inherited;
	
private:
	Tmsloggingutils::TTMSLoggerLogLevel FLogLevel;
	bool FReverseCondition;
	System::UnicodeString FFormat;
	
protected:
	virtual bool __fastcall Validate(const System::Rtti::TValue &AValue);
	virtual System::UnicodeString __fastcall GetFormat(const System::Rtti::TValue &AValue);
	__property Tmsloggingutils::TTMSLoggerLogLevel LogLevel = {read=FLogLevel, write=FLogLevel, nodefault};
	__property System::UnicodeString Format = {read=FFormat, write=FFormat};
	__property bool ReverseCondition = {read=FReverseCondition, write=FReverseCondition, nodefault};
	
public:
	__fastcall TMSLoggerBaseValidation(const bool AReverseCondition, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const System::UnicodeString AFormat);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TMSLoggerBaseValidation(void) { }
	
};


typedef System::DynamicArray<TMSLoggerBaseValidation*> TTMSLoggerBaseValidationArray;

class PASCALIMPLEMENTATION TMSLoggerValidation : public TMSLoggerBaseValidation
{
	typedef TMSLoggerBaseValidation inherited;
	
public:
	__property LogLevel;
	__property Format = {default=0};
	__property ReverseCondition;
public:
	/* TMSLoggerBaseValidation.Create */ inline __fastcall TMSLoggerValidation(const bool AReverseCondition, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const System::UnicodeString AFormat) : TMSLoggerBaseValidation(AReverseCondition, ALogLevel, AFormat) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TMSLoggerValidation(void) { }
	
};


class PASCALIMPLEMENTATION TMSLoggerFormattingAttribute : public TMSLoggerBaseValidation
{
	typedef TMSLoggerBaseValidation inherited;
	
private:
	bool FLogProperties;
	
public:
	__fastcall TMSLoggerFormattingAttribute(const System::UnicodeString AFormat, const bool ALogProperties, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel)/* overload */;
	__property bool LogProperties = {read=FLogProperties, write=FLogProperties, nodefault};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TMSLoggerFormattingAttribute(void) { }
	
};


class PASCALIMPLEMENTATION TMSLoggerClassFilter : public TMSLoggerBaseValidation
{
	typedef TMSLoggerBaseValidation inherited;
	
private:
	Tmsloggingutils::TTMSLoggerFilters FFilter;
	
public:
	__fastcall TMSLoggerClassFilter(const Tmsloggingutils::TTMSLoggerFilters AFilter, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const System::UnicodeString AFormat)/* overload */;
	__property Tmsloggingutils::TTMSLoggerFilters Filter = {read=FFilter, write=FFilter, nodefault};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TMSLoggerClassFilter(void) { }
	
};


class PASCALIMPLEMENTATION TMSLoggerPropertyFilter : public TMSLoggerClassFilter
{
	typedef TMSLoggerClassFilter inherited;
	
public:
	/* TMSLoggerClassFilter.Create */ inline __fastcall TMSLoggerPropertyFilter(const Tmsloggingutils::TTMSLoggerFilters AFilter, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const System::UnicodeString AFormat)/* overload */ : TMSLoggerClassFilter(AFilter, ALogLevel, AFormat) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TMSLoggerPropertyFilter(void) { }
	
};


class PASCALIMPLEMENTATION TMSLoggerRangeValidation : public TMSLoggerValidation
{
	typedef TMSLoggerValidation inherited;
	
private:
	System::Extended FMin;
	System::Extended FMax;
	
protected:
	virtual bool __fastcall Validate(const System::Rtti::TValue &AValue);
	
public:
	__fastcall TMSLoggerRangeValidation(const System::Extended AMin, const System::Extended AMax, const bool AReverseCondition, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const System::UnicodeString AFormat)/* overload */;
	__property System::Extended Min = {read=FMin, write=FMin};
	__property System::Extended Max = {read=FMax, write=FMax};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TMSLoggerRangeValidation(void) { }
	
};


class PASCALIMPLEMENTATION TMSLoggerDateTimeValidation : public TMSLoggerValidation
{
	typedef TMSLoggerValidation inherited;
	
private:
	System::UnicodeString FMax;
	System::UnicodeString FMin;
	
protected:
	virtual bool __fastcall Validate(const System::Rtti::TValue &AValue);
	
public:
	__fastcall TMSLoggerDateTimeValidation(const System::UnicodeString AMin, const System::UnicodeString AMax, const bool AReverseCondition, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const System::UnicodeString AFormat)/* overload */;
	__property System::UnicodeString Min = {read=FMin, write=FMin};
	__property System::UnicodeString Max = {read=FMax, write=FMax};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TMSLoggerDateTimeValidation(void) { }
	
};


enum DECLSPEC_DENUM TTMSLoggerStringValidationType : unsigned char { svtContains, svtEquals };

class PASCALIMPLEMENTATION TMSLoggerStringValidation : public TMSLoggerValidation
{
	typedef TMSLoggerValidation inherited;
	
private:
	System::UnicodeString FValue;
	TTMSLoggerStringValidationType FValidationType;
	
protected:
	virtual bool __fastcall Validate(const System::Rtti::TValue &AValue);
	
public:
	__fastcall TMSLoggerStringValidation(const System::UnicodeString AValue, const TTMSLoggerStringValidationType AValidationType, const bool AReverseCondition, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const System::UnicodeString AFormat)/* overload */;
	__property System::UnicodeString Value = {read=FValue, write=FValue};
	__property TTMSLoggerStringValidationType ValidationType = {read=FValidationType, write=FValidationType, nodefault};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TMSLoggerStringValidation(void) { }
	
};


enum DECLSPEC_DENUM TTMSLoggerStringLengthValidationValueRelationShip : unsigned char { vrsLessThanValue, vrsEqualsValue, vrsGreaterThanValue };

typedef System::Set<TTMSLoggerStringLengthValidationValueRelationShip, TTMSLoggerStringLengthValidationValueRelationShip::vrsLessThanValue, TTMSLoggerStringLengthValidationValueRelationShip::vrsGreaterThanValue> TTMSLoggerStringLengthValidationValueRelationShips;

class PASCALIMPLEMENTATION TMSLoggerStringLengthValidation : public TMSLoggerValidation
{
	typedef TMSLoggerValidation inherited;
	
private:
	int FLength;
	TTMSLoggerStringLengthValidationValueRelationShips FValueRelationShips;
	
protected:
	virtual bool __fastcall Validate(const System::Rtti::TValue &AValue);
	
public:
	__fastcall TMSLoggerStringLengthValidation(const int ALength, const TTMSLoggerStringLengthValidationValueRelationShips AValueRelationShips, const bool AReverseCondition, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const System::UnicodeString AFormat)/* overload */;
	__property int Length = {read=FLength, write=FLength, nodefault};
	__property TTMSLoggerStringLengthValidationValueRelationShips ValueRelationShips = {read=FValueRelationShips, write=FValueRelationShips, nodefault};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TMSLoggerStringLengthValidation(void) { }
	
};


class PASCALIMPLEMENTATION TMSLoggerRegularExpressionValidation : public TMSLoggerValidation
{
	typedef TMSLoggerValidation inherited;
	
private:
	System::UnicodeString FRegularExpression;
	
protected:
	virtual bool __fastcall Validate(const System::Rtti::TValue &AValue);
	
public:
	__fastcall TMSLoggerRegularExpressionValidation(const System::UnicodeString AExpression, const bool AReverseCondition, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const System::UnicodeString AFormat)/* overload */;
	__property System::UnicodeString RegularExpression = {read=FRegularExpression, write=FRegularExpression};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TMSLoggerRegularExpressionValidation(void) { }
	
};


class PASCALIMPLEMENTATION TTMSLoggerOutputFormats : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::UnicodeString FTimeStampFormat;
	System::UnicodeString FTypeFormat;
	System::UnicodeString FValueFormat;
	System::UnicodeString FLogLevelFormat;
	System::UnicodeString FNameFormat;
	System::UnicodeString FThreadIDFormat;
	System::UnicodeString FProcessIDFormat;
	System::UnicodeString FMemoryUsageFormat;
	
public:
	__fastcall TTMSLoggerOutputFormats(void);
	
__published:
	__property System::UnicodeString TimeStampFormat = {read=FTimeStampFormat, write=FTimeStampFormat};
	__property System::UnicodeString ProcessIDFormat = {read=FProcessIDFormat, write=FProcessIDFormat};
	__property System::UnicodeString ThreadIDFormat = {read=FThreadIDFormat, write=FThreadIDFormat};
	__property System::UnicodeString MemoryUsageFormat = {read=FMemoryUsageFormat, write=FMemoryUsageFormat};
	__property System::UnicodeString LogLevelFormat = {read=FLogLevelFormat, write=FLogLevelFormat};
	__property System::UnicodeString NameFormat = {read=FNameFormat, write=FNameFormat};
	__property System::UnicodeString ValueFormat = {read=FValueFormat, write=FValueFormat};
	__property System::UnicodeString TypeFormat = {read=FTypeFormat, write=FTypeFormat};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TTMSLoggerOutputFormats(void) { }
	
};


typedef void __fastcall (__closure *TTMSLoggerCustomFormatEvent)(System::TObject* Sender, const System::Rtti::TValue &AValue, System::UnicodeString AFormat, System::UnicodeString &AResult);

typedef void __fastcall (__closure *TTMSLoggerOutputEvent)(System::TObject* Sender, const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);

typedef void __fastcall (__closure *TTMSLoggerHandleExceptionEvent)(System::TObject* Sender, System::Sysutils::Exception* AException, bool &AOutput);

class PASCALIMPLEMENTATION TTMSCustomLogger : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	__int64 FTime;
	__int64 FTimeCall;
	System::Diagnostics::TStopwatch FStopWatch;
	int FIndent;
	unsigned FMemoryUsage;
	int FRootID;
	System::Generics::Collections::TList__1<System::UnicodeString> * FPropList;
	System::Generics::Collections::TList__1<TTMSLoggerBaseOutputHandler*> * FOutputHandlers;
	System::Generics::Collections::TObjectList__1<TTMSLoggerBaseOutputHandler*> * FManagedOutputHandlers;
	bool FActive;
	Tmsloggingutils::TTMSLoggerOutputs FOutputs;
	TTMSLoggerOutputFormats* FOutputFormats;
	Tmsloggingutils::TTMSLoggerFilters FFilters;
	TTMSLoggerCustomFormatEvent FOnCustomFormat;
	TTMSLoggerOutputEvent FOnOutput;
	Tmsloggingutils::TTMSLoggerTimeStampOutputMode FTimeStampOutputMode;
	Tmsloggingutils::TTMSLoggerLogLevelFilters FLogLevelFilters;
	bool FExceptionHandling;
	TTMSLoggerHandleExceptionEvent FOnHandleException;
	void __fastcall SetOutputFormats(TTMSLoggerOutputFormats* const Value);
	void __fastcall SetTimeStampOutputMode(const Tmsloggingutils::TTMSLoggerTimeStampOutputMode Value);
	System::UnicodeString __fastcall GetVersion(void);
	void __fastcall SetExceptionHandling(const bool Value);
	
protected:
	virtual System::UnicodeString __fastcall GetPictureAsString(const System::Rtti::TValue &AValue) = 0 ;
	virtual System::Classes::TStream* __fastcall GetPictureAsStream(const System::Rtti::TValue &AValue) = 0 ;
	virtual bool __fastcall IsPicture(const System::Rtti::TValue &AValue) = 0 ;
	virtual bool __fastcall IsUnsupportedPropertyName(const System::UnicodeString APropertyName);
	virtual bool __fastcall CanLogField(System::Rtti::TRttiField* const AField, const bool AUseFilters = false, const Tmsloggingutils::TTMSLoggerFilters AFilters = Tmsloggingutils::TTMSLoggerFilters() );
	virtual bool __fastcall CanLogProperty(System::Rtti::TRttiProperty* const AProperty, const bool AUseFilters = false, const Tmsloggingutils::TTMSLoggerFilters AFilters = Tmsloggingutils::TTMSLoggerFilters() );
	virtual bool __fastcall ValidateValue(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, TMSLoggerBaseValidation* const AValidation);
	virtual System::UnicodeString __fastcall ParseOutputFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues);
	virtual bool __fastcall LogFormatter(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const TTMSLoggerBaseValidationArray AValidations, const System::UnicodeString AID, const System::TArray__1<System::TCustomAttribute*> AAttributes, bool &ALogValue, bool &AUseFilters, Tmsloggingutils::TTMSLoggerFilters &AFilters);
	virtual __int64 __fastcall GetInternalTime(const Tmsloggingutils::TTMSLoggerTimerMode AMode = (Tmsloggingutils::TTMSLoggerTimerMode)(0x2));
	virtual void __fastcall LogInternalValue(const System::Rtti::TValue &AValue, const System::UnicodeString AValueName, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames, const TTMSLoggerBaseValidationArray AValidations, const System::TArray__1<System::TCustomAttribute*> AAttributes, const System::UnicodeString AID);
	virtual void __fastcall LogObject(System::TObject* const AObject, const System::UnicodeString AValueName, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames, const TTMSLoggerBaseValidationArray AValidations, const System::UnicodeString AID);
	virtual void __fastcall LogRecord(const System::Rtti::TValue &AValue, const System::UnicodeString AValueName, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames, const TTMSLoggerBaseValidationArray AValidations, const System::UnicodeString AID);
	virtual void __fastcall LogArray(const System::Rtti::TValue &AValue, const System::UnicodeString AValueName, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames, const TTMSLoggerBaseValidationArray AValidations, const System::UnicodeString AID);
	virtual void __fastcall LogValue(const System::Rtti::TValue &AValue, const System::UnicodeString AValueName, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const TTMSLoggerBaseValidationArray AValidations, const System::UnicodeString AID);
	void __fastcall LogInternal(const System::Rtti::TValue &AValue, const System::UnicodeString AValueName, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames, const TTMSLoggerBaseValidationArray AValidations, const System::UnicodeString AID);
	virtual void __fastcall LogInternalFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const TTMSLoggerBaseValidationArray AValidations, const System::UnicodeString AID);
	virtual void __fastcall ParseOutputResult(Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual void __fastcall LogOutputInformation(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual void __fastcall SaveTime(void);
	virtual void __fastcall SaveTimeCall(void);
	virtual void __fastcall StartInternalTimer(void);
	virtual void __fastcall StopInternalTimer(void);
	virtual void __fastcall CreateApplicationEvents(void);
	virtual void __fastcall DestroyApplicationEvents(void);
	virtual void __fastcall DoApplicationException(System::TObject* Sender, System::Sysutils::Exception* E);
	__property int RootID = {read=FRootID, write=FRootID, nodefault};
	
public:
	__fastcall virtual TTMSCustomLogger(void);
	__fastcall virtual ~TTMSCustomLogger(void);
	virtual void __fastcall Indent(void);
	virtual void __fastcall Unindent(void);
	virtual void __fastcall Clear(void);
	virtual void __fastcall LogCurrentDateTime(const System::UnicodeString AFormat = System::UnicodeString());
	virtual void __fastcall LogSystemInformation(const System::UnicodeString AFormat = System::UnicodeString());
	virtual void __fastcall LogCurrentLocale(const System::UnicodeString AFormat = System::UnicodeString());
	virtual void __fastcall LogMemoryUsage(const System::UnicodeString AFormat = System::UnicodeString());
	virtual void __fastcall LogThreadID(const System::UnicodeString AFormat = System::UnicodeString());
	virtual void __fastcall LogProcessID(const System::UnicodeString AFormat = System::UnicodeString());
	virtual void __fastcall LogSeparator(void);
	virtual void __fastcall StartTimer(void);
	virtual void __fastcall Info(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall Error(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall Warning(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall Trace(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall Debug(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall Exception(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall Info(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall Error(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall Warning(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall Trace(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall Debug(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall Exception(const System::Rtti::TValue &AValue, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall InfoValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall ErrorValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall WarningValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall TraceValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall DebugValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall ExceptionValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall InfoValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall ErrorValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall WarningValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall TraceValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall DebugValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall ExceptionValues(const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerStringArray APropertyNames = Tmsloggingutils::TTMSLoggerStringArray(), const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall InfoFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall ErrorFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall WarningFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall TraceFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall DebugFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall ExceptionFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const Tmsloggingutils::TTMSLoggerOutputParameters &AOutputParameters, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall InfoFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall ErrorFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall WarningFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall TraceFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall DebugFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall ExceptionFormat(const System::UnicodeString AFormat, const Tmsloggingutils::TTMSLoggerValueArray AValues, const TTMSLoggerBaseValidationArray AValidations = TTMSLoggerBaseValidationArray())/* overload */;
	virtual void __fastcall UnregisterAllOutputHandlers(void);
	virtual void __fastcall UnregisterOutputHandler(TTMSLoggerBaseOutputHandler* const AOutputHandler)/* overload */;
	virtual void __fastcall UnregisterOutputHandlers(const System::TArray__1<TTMSLoggerBaseOutputHandler*> AOutputHandlers)/* overload */;
	virtual void __fastcall RegisterOutputHandler(TTMSLoggerBaseOutputHandler* const AOutputHandler)/* overload */;
	virtual void __fastcall RegisterOutputHandlers(const System::TArray__1<TTMSLoggerBaseOutputHandler*> AOutputHandlers)/* overload */;
	virtual void __fastcall UnregisterOutputHandler(const TTMSLoggerBaseOutputHandlerClass AOutputHandlerClass)/* overload */;
	virtual void __fastcall SaveConfigurationToStream(System::Classes::TStream* const AStream);
	virtual void __fastcall LoadConfigurationFromStream(System::Classes::TComponent* const AOwner, System::Classes::TStream* const AStream);
	virtual void __fastcall SaveConfigurationToFile(const System::UnicodeString AFile = System::UnicodeString());
	virtual void __fastcall LoadConfigurationFromFile(System::Classes::TComponent* const AOwner, const System::UnicodeString AFile = System::UnicodeString());
	virtual void __fastcall SaveConfigurationToFileStream(const System::UnicodeString AFile = System::UnicodeString());
	virtual void __fastcall LoadConfigurationFromFileStream(System::Classes::TComponent* const AOwner, const System::UnicodeString AFile = System::UnicodeString());
	virtual void __fastcall DeactivateAllOutputHandlers(void);
	virtual void __fastcall ActivateAllOutputHandlers(void);
	virtual TTMSLoggerBaseOutputHandler* __fastcall RegisterOutputHandlerClass(const TTMSLoggerBaseOutputHandlerClass AOutputHandlerClass, const Tmsloggingutils::TTMSLoggerValueArray AParameters = Tmsloggingutils::TTMSLoggerValueArray())/* overload */;
	virtual __int64 __fastcall GetTimer(const bool ALogResult = false, const Tmsloggingutils::TTMSLoggerTimerMode AMode = (Tmsloggingutils::TTMSLoggerTimerMode)(0x2), const System::UnicodeString AFormat = System::UnicodeString());
	virtual __int64 __fastcall StopTimer(const bool ALogResult = false, const Tmsloggingutils::TTMSLoggerTimerMode AMode = (Tmsloggingutils::TTMSLoggerTimerMode)(0x2), const System::UnicodeString AFormat = System::UnicodeString());
	virtual unsigned __fastcall LogMemoryUsageDifference(const bool ALogResult = true, const System::UnicodeString AFormat = System::UnicodeString());
	bool __fastcall IsTimerRunning(void);
	__property System::Generics::Collections::TList__1<TTMSLoggerBaseOutputHandler*> * OutputHandlers = {read=FOutputHandlers, write=FOutputHandlers};
	__property System::Generics::Collections::TObjectList__1<TTMSLoggerBaseOutputHandler*> * ManagedOutputHandlers = {read=FManagedOutputHandlers, write=FManagedOutputHandlers};
	__property TTMSLoggerCustomFormatEvent OnCustomFormat = {read=FOnCustomFormat, write=FOnCustomFormat};
	__property TTMSLoggerOutputEvent OnOutput = {read=FOnOutput, write=FOnOutput};
	__property TTMSLoggerHandleExceptionEvent OnHandleException = {read=FOnHandleException, write=FOnHandleException};
	__property System::UnicodeString Version = {read=GetVersion};
	
__published:
	__property bool Active = {read=FActive, write=FActive, nodefault};
	__property Tmsloggingutils::TTMSLoggerOutputs Outputs = {read=FOutputs, write=FOutputs, nodefault};
	__property Tmsloggingutils::TTMSLoggerFilters Filters = {read=FFilters, write=FFilters, nodefault};
	__property Tmsloggingutils::TTMSLoggerLogLevelFilters LogLevelFilters = {read=FLogLevelFilters, write=FLogLevelFilters, nodefault};
	__property TTMSLoggerOutputFormats* OutputFormats = {read=FOutputFormats, write=SetOutputFormats};
	__property Tmsloggingutils::TTMSLoggerTimeStampOutputMode TimeStampOutputMode = {read=FTimeStampOutputMode, write=SetTimeStampOutputMode, nodefault};
	__property bool ExceptionHandling = {read=FExceptionHandling, write=SetExceptionHandling, nodefault};
};


class PASCALIMPLEMENTATION TTMSLoggerSaver : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	TTMSCustomLogger* FLogger;
	
__published:
	__property TTMSCustomLogger* Logger = {read=FLogger, write=FLogger};
public:
	/* TComponent.Create */ inline __fastcall virtual TTMSLoggerSaver(System::Classes::TComponent* AOwner) : System::Classes::TComponent(AOwner) { }
	/* TComponent.Destroy */ inline __fastcall virtual ~TTMSLoggerSaver(void) { }
	
};


// Template declaration generated by Delphi parameterized types is
// used only for accessing Delphi variables and fields.
// Don't instantiate with new type parameters in user code.
template<typename T> class PASCALIMPLEMENTATION TTMSSingletonHelper__1 : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	static T FInstance;
	
public:
	__classmethod T __fastcall GetInstance();
	__classmethod void __fastcall ReleaseInstance();
	
private:
	// __classmethod void __fastcall Create@();
	// __classmethod void __fastcall Destroy@();
public:
	/* TObject.Create */ inline __fastcall TTMSSingletonHelper__1(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TTMSSingletonHelper__1(void) { }
	
};


typedef TTMSSingletonHelper__1<TTMSCustomLogger*> * TTMSLoggerSingleton;

typedef System::Set<System::Typinfo::TMemberVisibility, System::Typinfo::TMemberVisibility::mvPrivate, System::Typinfo::TMemberVisibility::mvPublished> TTMSLoggerUnsupportedVisibilityMembers;

//-- var, const, procedure ---------------------------------------------------
static constexpr System::Int8 MAJ_VER = System::Int8(0x1);
static constexpr System::Int8 MIN_VER = System::Int8(0x1);
static constexpr System::Int8 REL_VER = System::Int8(0x0);
static constexpr System::Int8 BLD_VER = System::Int8(0x0);
#define UnsupportedPropertyTypes (System::Set<System::TTypeKind, System::TTypeKind::tkUnknown, System::TTypeKind::tkProcedure>() << System::TTypeKind::tkUnknown << System::TTypeKind::tkMethod << System::TTypeKind::tkInterface << System::TTypeKind::tkClassRef << System::TTypeKind::tkProcedure )
extern DELPHI_PACKAGE TTMSLoggerUnsupportedVisibilityMembers UnsupportedVisibilityMembers;
extern DELPHI_PACKAGE void __fastcall TMSLog(const System::Rtti::TValue &AValue, const System::UnicodeString AFormat = System::UnicodeString(), const Tmsloggingutils::TTMSLoggerLogLevel ALogLevel = (Tmsloggingutils::TTMSLoggerLogLevel)(0x1));
extern DELPHI_PACKAGE TTMSCustomLogger* __fastcall TMSDefaultLogger(void);
}	/* namespace Tmsloggingcore */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGCORE)
using namespace Tmsloggingcore;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TmsloggingcoreHPP
