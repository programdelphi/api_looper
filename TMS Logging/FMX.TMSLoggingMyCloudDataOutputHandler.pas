{********************************************************************}
{                                                                    }
{ written by TMS Software                                            }
{            copyright � 2016                                        }
{            Email : info@tmssoftware.com                            }
{            Web : http://www.tmssoftware.com                        }
{                                                                    }
{ The source code is given as is. The author is not responsible      }
{ for any possible damage done due to the use of this code.          }
{ The complete source code remains property of the author and may    }
{ not be distributed, published, given or sold in any form as such.  }
{ No parts of the source code can be included in any other component }
{ or application without written authorization of the author.        }
{********************************************************************}

unit FMX.TMSLoggingMyCloudDataOutputHandler;

interface

uses
  TMSLoggingMyCloudDataBaseOutputHandler, TMSLoggingUtils,
  FMX.TMSCloudCustomMyCloudData, Data.DB;

type
  TTMSLoggerMyCloudDataOutputHandler = class(TTMSLoggerMyCloudDataBaseOutputHandler)
  private
    FTable: TMyCloudDataTable;
    FMyCloudDataClient: TTMSFMXCloudCustomMyCloudData;
  protected
    procedure CreateTable; virtual;
    procedure LogOutput(const AOutputInformation: TTMSLoggerOutputInformation); override;
  public
    constructor Create(const AMyCloudDataClient: TTMSFMXCloudCustomMyCloudData); reintroduce; overload; virtual;
    constructor Create(const AMyCloudDataClient: TTMSFMXCloudCustomMyCloudData; const ATableName: string); reintroduce; overload; virtual;
    property MyCloudDataClient: TTMSFMXCloudCustomMyCloudData read FMyCloudDataClient write FMyCloudDataClient;
  end;

implementation

uses
  Classes, SysUtils;

{ TTMSLoggerMyCloudDataOutputHandler }

constructor TTMSLoggerMyCloudDataOutputHandler.Create(
  const AMyCloudDataClient: TTMSFMXCloudCustomMyCloudData);
begin
  inherited Create;
  FMyCloudDataClient := AMyCloudDataClient;
  CreateTable;
end;

constructor TTMSLoggerMyCloudDataOutputHandler.Create(
  const AMyCloudDataClient: TTMSFMXCloudCustomMyCloudData; const ATableName: string);
begin
  inherited Create;
  FMyCloudDataClient := AMyCloudDataClient;
  TableName := ATableName;
  CreateTable;
end;

procedure TTMSLoggerMyCloudDataOutputHandler.CreateTable;
var
  tbln, mn: string;
  iCount: Integer;
const
  sz = 9999;
begin
  if not Assigned(FTable) then
  begin
    tbln := TableName;
    FTable := MyCloudDataClient.TableByName(tbln);
    if not Assigned(FTable) then
      FTable := MyCloudDataClient.CreateTable(tbln);

    if Assigned(FTable) then
    begin
      iCount := 0;

      mn := MetaData.TimeStamp;
      if not Assigned(FTable.MetaData.GetMetaDataItemByName(mn)) then
      begin
        FTable.MetaData.Add(mn, ftString, sz);
        Inc(iCount);
      end;

      mn := MetaData.ProcessID;
      if not Assigned(FTable.MetaData.GetMetaDataItemByName(mn)) then
      begin
        FTable.MetaData.Add(mn, ftString, sz);
        Inc(iCount);
      end;

      mn := MetaData.ThreadID;
      if not Assigned(FTable.MetaData.GetMetaDataItemByName(mn)) then
      begin
        FTable.MetaData.Add(mn, ftString, sz);
        Inc(iCount);
      end;

      mn := MetaData.MemoryUsage;
      if not Assigned(FTable.MetaData.GetMetaDataItemByName(mn)) then
      begin
        FTable.MetaData.Add(mn, ftString, sz);
        Inc(iCount);
      end;

      mn := MetaData.LogLevel;
      if not Assigned(FTable.MetaData.GetMetaDataItemByName(mn)) then
      begin
        FTable.MetaData.Add(mn, ftString, sz);
        Inc(iCount);
      end;

      mn := MetaData.Name;
      if not Assigned(FTable.MetaData.GetMetaDataItemByName(mn)) then
      begin
        FTable.MetaData.Add(mn, ftString, sz);
        Inc(iCount);
      end;

      mn := MetaData.Value;
      if not Assigned(FTable.MetaData.GetMetaDataItemByName(mn)) then
      begin
        FTable.MetaData.Add(mn, ftString, sz);
        Inc(iCount);
      end;

      mn := MetaData.&Type;
      if not Assigned(FTable.MetaData.GetMetaDataItemByName(mn)) then
      begin
        FTable.MetaData.Add(mn, ftString, sz);
        Inc(iCount);
      end;

      if iCount > 0 then
        FTable.SetMetaData;
    end;
  end;
end;

procedure TTMSLoggerMyCloudDataOutputHandler.LogOutput(
  const AOutputInformation: TTMSLoggerOutputInformation);
var
  e: TMyCloudDataEntity;
begin
  inherited;
  if Assigned(MyCloudDataClient) and Assigned(FTable) then
  begin
    FTable.Entities.Clear;
    e := FTable.Entities.Add;
    if loTimeStamp in AOutputInformation.Outputs then
      e.Value[MetaData.TimeStamp] := AOutputInformation.TimeStampOutput;
    if loProcessID in AOutputInformation.Outputs then
      e.Value[MetaData.ProcessID] := AOutputInformation.ProcessIDOutput;
    if loThreadID in AOutputInformation.Outputs then
      e.Value[MetaData.ThreadID] := AOutputInformation.ThreadIDOutput;
    if loMemoryUsage in AOutputInformation.Outputs then
      e.Value[MetaData.MemoryUsage] := AOutputInformation.MemoryUsageOutput;
    if loLogLevel in AOutputInformation.Outputs then
      e.Value[MetaData.LogLevel] := AOutputInformation.LogLevelOutput;
    if loName in AOutputInformation.Outputs then
      e.Value[MetaData.Name] := AOutputInformation.NameOutput;
    if loValue in AOutputInformation.Outputs then
      e.Value[MetaData.Value] := AOutputInformation.ValueOutput;
    if loType in AOutputInformation.Outputs then
      e.Value[MetaData.&Type] := AOutputInformation.TypeOutput;
  end;
end;

initialization
  RegisterClasses([TTMSLoggerMyCloudDataOutputHandler]);

end.
