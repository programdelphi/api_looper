{********************************************************************}
{                                                                    }
{ written by TMS Software                                            }
{            copyright � 2016                                        }
{            Email : info@tmssoftware.com                            }
{            Web : http://www.tmssoftware.com                        }
{                                                                    }
{ The source code is given as is. The author is not responsible      }
{ for any possible damage done due to the use of this code.          }
{ The complete source code remains property of the author and may    }
{ not be distributed, published, given or sold in any form as such.  }
{ No parts of the source code can be included in any other component }
{ or application without written authorization of the author.        }
{********************************************************************}

unit VCL.TMSLoggingExceptionlessOutputHandler;

interface

uses
  TMSLoggingExceptionlessBaseOutputHandler, TMSLoggingUtils,
  CloudCustomExceptionless;

type
  TTMSLoggerExceptionlessOutputHandler = class(TTMSLoggerExceptionlessBaseOutputHandler)
  private
    FExceptionlessClient: TAdvCustomExceptionLess;
  protected
    procedure LogOutput(const AOutputInformation: TTMSLoggerOutputInformation); override;
  public
    constructor Create(const AExceptionlessClient: TAdvCustomExceptionless); reintroduce; overload; virtual;
    constructor Create(const AExceptionlessClient: TAdvCustomExceptionless; const AProjectID: string); reintroduce; overload; virtual;
    property ExceptionlessClient: TAdvCustomExceptionless read FExceptionlessClient write FExceptionlessClient;
  end;

implementation

uses
  Classes, SysUtils;

{ TTMSLoggerExceptionlessOutputHandler }

constructor TTMSLoggerExceptionlessOutputHandler.Create(
  const AExceptionlessClient: TAdvCustomExceptionless);
begin
  inherited Create;
  FExceptionlessClient := AExceptionlessClient;
end;

constructor TTMSLoggerExceptionlessOutputHandler.Create(
  const AExceptionlessClient: TAdvCustomExceptionless; const AProjectID: string);
begin
  inherited Create;
  FExceptionlessClient := AExceptionlessClient;
  ProjectID := AProjectID;
end;

procedure TTMSLoggerExceptionlessOutputHandler.LogOutput(
  const AOutputInformation: TTMSLoggerOutputInformation);
var
  msg: string;
  l: TExceptionlessLogLevel;
begin
  inherited;
  if Assigned(ExceptionlessClient) then
  begin
    msg := TTMSLoggerUtils.StripHTML(TTMSLoggerUtils.GetConcatenatedLogMessage(AOutputInformation));
    l := ellInfo;
    if AOutputInformation.LogLevel = TTMSLoggerLogLevel.Exception then
      ExceptionlessClient.LogException(AOutputInformation.ValueOutput, AOutputInformation.TypeOutput, '', ProjectID)
    else
    begin
      case AOutputInformation.LogLevel of
        TTMSLoggerLogLevel.Trace: l := ellTrace;
        TTMSLoggerLogLevel.Debug: l := ellDebug;
        TTMSLoggerLogLevel.Info: l := ellInfo;
        TTMSLoggerLogLevel.Warning: l := ellWarning;
        TTMSLoggerLogLevel.Error: l := ellError;
        TTMSLoggerLogLevel.All: l := ellUnknown;
      end;

      ExceptionlessClient.LogMessage(msg, ExtractFilePath(ParamStr(0)), ProjectID, l);
    end;
  end;
end;

initialization
  RegisterClasses([TTMSLoggerExceptionlessOutputHandler]);

end.
