﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingHelpers.pas' rev: 31.00 (Windows)

#ifndef TmslogginghelpersHPP
#define TmslogginghelpersHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <System.Rtti.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmslogginghelpers
{
//-- forward type declarations -----------------------------------------------
/*Helper*/typedef void *TTMSLoggerObjectHelper;

/*Helper*/typedef void *TTMSLoggerStringHelper;

/*Helper*/typedef void *TTMSLoggerSingleHelper;

/*Helper*/typedef void *TTMSLoggerDoubleHelper;

/*Helper*/typedef void *TTMSLoggerTDateTimeHelper;

/*Helper*/typedef void *TTMSLoggerExtendedHelper;

/*Helper*/typedef void *TTMSLoggerByteHelper;

/*Helper*/typedef void *TTMSLoggerShortIntHelper;

/*Helper*/typedef void *TTMSLoggerSmallIntHelper;

/*Helper*/typedef void *TTMSLoggerWordHelper;

/*Helper*/typedef void *TTMSLoggerCardinalHelper;

/*Helper*/typedef void *TTMSLoggerIntegerHelper;

/*Helper*/typedef void *TTMSLoggerInt64Helper;

/*Helper*/typedef void *TTMSLoggerUInt64Helper;

/*Helper*/typedef void *TTMSLoggerNativeIntHelper;

/*Helper*/typedef void *TTMSLoggerNativeUIntHelper;

/*Helper*/typedef void *TTMSLoggerBooleanHelper;

/*Helper*/typedef void *TTMSLoggerByteBoolHelper;

/*Helper*/typedef void *TTMSLoggerWordBoolHelper;

/*Helper*/typedef void *TTMSLoggerLongBoolHelper;

/*Helper*/typedef void *TTMSLoggerTValueHelper;

//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tmslogginghelpers */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGHELPERS)
using namespace Tmslogginghelpers;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TmslogginghelpersHPP
