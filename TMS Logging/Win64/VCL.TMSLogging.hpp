﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'VCL.TMSLogging.pas' rev: 31.00 (Windows)

#ifndef Vcl_TmsloggingHPP
#define Vcl_TmsloggingHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <System.Rtti.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.AppEvnts.hpp>

//-- user supplied -----------------------------------------------------------

namespace Vcl
{
namespace Tmslogging
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TTMSVCLLogger;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TTMSVCLLogger : public Tmsloggingcore::TTMSCustomLogger
{
	typedef Tmsloggingcore::TTMSCustomLogger inherited;
	
private:
	Vcl::Appevnts::TApplicationEvents* FAppEvents;
	
protected:
	virtual System::UnicodeString __fastcall GetPictureAsString(const System::Rtti::TValue &AValue);
	virtual System::Classes::TStream* __fastcall GetPictureAsStream(const System::Rtti::TValue &AValue);
	virtual bool __fastcall IsPicture(const System::Rtti::TValue &AValue);
	virtual void __fastcall CreateApplicationEvents(void);
	virtual void __fastcall DestroyApplicationEvents(void);
	
public:
	virtual void __fastcall LogScreenShot(void);
	virtual void __fastcall LogControl(Vcl::Controls::TWinControl* const AControl);
public:
	/* TTMSCustomLogger.Create */ inline __fastcall virtual TTMSVCLLogger(void) : Tmsloggingcore::TTMSCustomLogger() { }
	/* TTMSCustomLogger.Destroy */ inline __fastcall virtual ~TTMSVCLLogger(void) { }
	
};


typedef Tmsloggingcore::TTMSSingletonHelper__1<TTMSVCLLogger*>* TTMSVCLLoggerSingleton;

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TTMSVCLLogger* __fastcall TMSLogger(void);
}	/* namespace Tmslogging */
}	/* namespace Vcl */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_VCL_TMSLOGGING)
using namespace Vcl::Tmslogging;
#endif
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_VCL)
using namespace Vcl;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Vcl_TmsloggingHPP
