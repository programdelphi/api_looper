﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingCorePkgDXE10.dpk' rev: 31.00 (Windows)

#ifndef Tmsloggingcorepkgdxe10HPP
#define Tmsloggingcorepkgdxe10HPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// (rtl)
#include <SysInit.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <TMSLoggingBrowserOutputHandler.hpp>
#include <TMSLoggingEventLogOutputHandler.hpp>
#include <TMSLoggingHelpers.hpp>
#include <TMSLoggingHTMLOutputHandler.hpp>
#include <TMSLoggingDataSourceOutputHandler.hpp>
#include <TMSLoggingTCPOutputHandler.hpp>
#include <TMSLoggingTextOutputHandler.hpp>
#include <TMSLoggingCSVOutputHandler.hpp>
#include <TMSLoggingReg.hpp>
#include <System.Types.hpp>	// (rtl)
#include <System.UITypes.hpp>	// (rtl)
#include <Winapi.Windows.hpp>	// (rtl)
#include <Winapi.Messages.hpp>	// (rtl)
#include <System.SysConst.hpp>	// (rtl)
#include <Winapi.ImageHlp.hpp>	// (rtl)
#include <Winapi.SHFolder.hpp>	// (rtl)
#include <Winapi.PsAPI.hpp>	// (rtl)
#include <System.RTLConsts.hpp>	// (rtl)
#include <System.Character.hpp>	// (rtl)
#include <System.Internal.ExcUtils.hpp>	// (rtl)
#include <System.SysUtils.hpp>	// (rtl)
#include <System.VarUtils.hpp>	// (rtl)
#include <System.Variants.hpp>	// (rtl)
#include <Winapi.ActiveX.hpp>	// (rtl)
#include <System.Generics.Collections.hpp>	// (rtl)
#include <System.Hash.hpp>	// (rtl)
#include <System.Rtti.hpp>	// (rtl)
#include <System.TypInfo.hpp>	// (rtl)
#include <System.Math.hpp>	// (rtl)
#include <System.Generics.Defaults.hpp>	// (rtl)
#include <System.Classes.hpp>	// (rtl)
#include <System.Masks.hpp>	// (rtl)
#include <System.StrUtils.hpp>	// (rtl)
#include <System.TimeSpan.hpp>	// (rtl)
#include <System.DateUtils.hpp>	// (rtl)
#include <System.IOUtils.hpp>	// (rtl)
#include <System.IniFiles.hpp>	// (rtl)
#include <System.Win.Registry.hpp>	// (rtl)
#include <System.Diagnostics.hpp>	// (rtl)
#include <System.SyncObjs.hpp>	// (rtl)
#include <IdException.hpp>	// (IndySystem)
#include <IdResourceStrings.hpp>	// (IndySystem)
#include <IdStreamVCL.hpp>	// (IndySystem)
#include <IdStream.hpp>	// (IndySystem)
#include <IdGlobal.hpp>	// (IndySystem)
#include <IdBaseComponent.hpp>	// (IndySystem)
#include <IdCharsets.hpp>	// (IndyProtocols)
#include <System.AnsiStrings.hpp>	// (rtl)
#include <IdWinsock2.hpp>	// (IndySystem)
#include <IdWship6.hpp>	// (IndySystem)
#include <IdStackConsts.hpp>	// (IndySystem)
#include <IdStackBSDBase.hpp>	// (IndySystem)
#include <IdIDN.hpp>	// (IndySystem)
#include <Winapi.WinSock.hpp>	// (rtl)
#include <Winapi.IpTypes.hpp>	// (rtl)
#include <Winapi.IpRtrMib.hpp>	// (rtl)
#include <Winapi.IpExport.hpp>	// (rtl)
#include <Winapi.IpHlpApi.hpp>	// (rtl)
#include <IdStackWindows.hpp>	// (IndySystem)
#include <IdStack.hpp>	// (IndySystem)
#include <IdIPAddress.hpp>	// (IndyCore)
#include <IdAssignedNumbers.hpp>	// (IndyCore)
#include <IdResourceStringsCore.hpp>	// (IndyCore)
#include <IdResourceStringsProtocols.hpp>	// (IndyProtocols)
#include <IdGlobalProtocols.hpp>	// (IndyProtocols)
#include <IdCoder.hpp>	// (IndyProtocols)
#include <IdCoder3to4.hpp>	// (IndyProtocols)
#include <IdCoderMIME.hpp>	// (IndyProtocols)
#include <System.UIConsts.hpp>	// (rtl)
#include <System.Win.Crtl.hpp>	// (rtl)
#include <System.RegularExpressionsAPI.hpp>	// (rtl)
#include <System.RegularExpressionsConsts.hpp>	// (rtl)
#include <System.RegularExpressionsCore.hpp>	// (rtl)
#include <System.RegularExpressions.hpp>	// (rtl)
#include <IdAntiFreezeBase.hpp>	// (IndySystem)
#include <IdComponent.hpp>	// (IndySystem)
#include <IdSocketHandle.hpp>	// (IndyCore)
#include <IdExceptionCore.hpp>	// (IndyCore)
#include <IdBuffer.hpp>	// (IndyCore)
#include <IdIntercept.hpp>	// (IndyCore)
#include <IdIOHandler.hpp>	// (IndyCore)
#include <IdCustomTransparentProxy.hpp>	// (IndyCore)
#include <IdTCPClient.hpp>	// (IndyCore)
#include <IdIOHandlerStack.hpp>	// (IndyCore)
#include <IdSocks.hpp>	// (IndyCore)
#include <IdIOHandlerSocket.hpp>	// (IndyCore)
#include <IdReply.hpp>	// (IndyCore)
#include <IdReplyRFC.hpp>	// (IndyCore)
#include <IdTCPConnection.hpp>	// (IndyCore)
#include <IdYarn.hpp>	// (IndyCore)
#include <IdTask.hpp>	// (IndyCore)
#include <IdThreadSafe.hpp>	// (IndyCore)
#include <IdContext.hpp>	// (IndyCore)
#include <IdHeaderList.hpp>	// (IndyProtocols)
#include <IdThread.hpp>	// (IndyCore)
#include <IdScheduler.hpp>	// (IndyCore)
#include <IdTCPServer.hpp>	// (IndyCore)
#include <IdSchedulerOfThread.hpp>	// (IndyCore)
#include <IdServerIOHandler.hpp>	// (IndyCore)
#include <IdServerIOHandlerSocket.hpp>	// (IndyCore)
#include <IdServerIOHandlerStack.hpp>	// (IndyCore)
#include <IdGlobalCore.hpp>	// (IndyCore)
#include <IdSchedulerOfThreadDefault.hpp>	// (IndyCore)
#include <IdCustomTCPServer.hpp>	// (IndyCore)
#include <IdUriUtils.hpp>	// (IndyProtocols)
#include <IdURI.hpp>	// (IndyProtocols)
#include <IdCookie.hpp>	// (IndyProtocols)
#include <IdAuthentication.hpp>	// (IndyProtocols)
#include <IdHTTPHeaderInfo.hpp>	// (IndyProtocols)
#include <IdSSL.hpp>	// (IndyProtocols)
#include <IdCustomHTTPServer.hpp>	// (IndyProtocols)
#include <IdHTTPServer.hpp>	// (IndyProtocols)
#include <IdFIPS.hpp>	// (IndyProtocols)
#include <IdHash.hpp>	// (IndyProtocols)
#include <IdHashSHA.hpp>	// (IndyProtocols)
#include <System.MaskUtils.hpp>	// (rtl)
#include <Data.DBConsts.hpp>	// (dbrtl)
#include <Data.SqlTimSt.hpp>	// (dbrtl)
#include <Data.FmtBcd.hpp>	// (dbrtl)
#include <Data.DBCommonTypes.hpp>	// (dbrtl)
#include <Data.DB.hpp>	// (dbrtl)
#include <System.Threading.hpp>	// (rtl)

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingcorepkgdxe10
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tmsloggingcorepkgdxe10 */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGCOREPKGDXE10)
using namespace Tmsloggingcorepkgdxe10;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Tmsloggingcorepkgdxe10HPP
