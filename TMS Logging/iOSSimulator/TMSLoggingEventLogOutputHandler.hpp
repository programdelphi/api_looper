﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingEventLogOutputHandler.pas' rev: 31.00 (iOSSIM)

#ifndef TmsloggingeventlogoutputhandlerHPP
#define TmsloggingeventlogoutputhandlerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <System.IniFiles.hpp>
#include <System.Classes.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingeventlogoutputhandler
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TTMSLoggerEventLogOutputHandler;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerEventLogOutputHandler : public Tmsloggingcore::TTMSLoggerBaseOutputHandler
{
	typedef Tmsloggingcore::TTMSLoggerBaseOutputHandler inherited;
	
private:
	System::UnicodeString FSource;
	System::UnicodeString FServer;
	
protected:
	virtual void __fastcall LogOutput(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual void __fastcall WriteValuesToFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	virtual void __fastcall ReadValuesFromFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	
public:
	__fastcall virtual TTMSLoggerEventLogOutputHandler(void)/* overload */;
	__fastcall virtual TTMSLoggerEventLogOutputHandler(const System::UnicodeString ASource)/* overload */;
	__fastcall virtual TTMSLoggerEventLogOutputHandler(const System::UnicodeString ASource, const System::UnicodeString AServer)/* overload */;
	__fastcall virtual ~TTMSLoggerEventLogOutputHandler(void);
	
__published:
	__property System::UnicodeString Server = {read=FServer, write=FServer};
	__property System::UnicodeString Source = {read=FSource, write=FSource};
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tmsloggingeventlogoutputhandler */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGEVENTLOGOUTPUTHANDLER)
using namespace Tmsloggingeventlogoutputhandler;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TmsloggingeventlogoutputhandlerHPP
