﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'FMX.TMSLogging.pas' rev: 31.00 (iOSSIM)

#ifndef Fmx_TmsloggingHPP
#define Fmx_TmsloggingHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <FMX.Graphics.hpp>
#include <System.Rtti.hpp>
#include <FMX.Controls.hpp>

//-- user supplied -----------------------------------------------------------

namespace Fmx
{
namespace Tmslogging
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TTMSFMXLogger;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TTMSFMXLogger : public Tmsloggingcore::TTMSCustomLogger
{
	typedef Tmsloggingcore::TTMSCustomLogger inherited;
	
protected:
	virtual System::UnicodeString __fastcall GetPictureAsString(const System::Rtti::TValue &AValue);
	virtual System::Classes::TStream* __fastcall GetPictureAsStream(const System::Rtti::TValue &AValue);
	virtual bool __fastcall IsPicture(const System::Rtti::TValue &AValue);
	virtual void __fastcall CreateApplicationEvents(void);
	virtual void __fastcall DestroyApplicationEvents(void);
	
public:
	virtual void __fastcall LogScreenShot(void);
	virtual void __fastcall LogControl(Fmx::Controls::TControl* const AControl);
public:
	/* TTMSCustomLogger.Create */ inline __fastcall virtual TTMSFMXLogger(void) : Tmsloggingcore::TTMSCustomLogger() { }
	/* TTMSCustomLogger.Destroy */ inline __fastcall virtual ~TTMSFMXLogger(void) { }
	
};


typedef Tmsloggingcore::TTMSSingletonHelper__1<TTMSFMXLogger*>* TTMSFMXLoggerSingleton;

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TTMSFMXLogger* __fastcall TMSLogger(void);
}	/* namespace Tmslogging */
}	/* namespace Fmx */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_FMX_TMSLOGGING)
using namespace Fmx::Tmslogging;
#endif
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_FMX)
using namespace Fmx;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Fmx_TmsloggingHPP
