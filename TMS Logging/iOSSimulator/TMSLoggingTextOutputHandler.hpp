﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingTextOutputHandler.pas' rev: 31.00 (iOSSIM)

#ifndef TmsloggingtextoutputhandlerHPP
#define TmsloggingtextoutputhandlerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <System.Classes.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingtextoutputhandler
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TTMSLoggerTextOutputHandler;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerTextOutputHandler : public Tmsloggingcore::TTMSLoggerCustomFileOutputHandler
{
	typedef Tmsloggingcore::TTMSLoggerCustomFileOutputHandler inherited;
	
protected:
	virtual System::UnicodeString __fastcall GetOutputText(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
public:
	/* TTMSLoggerCustomFileOutputHandler.Create */ inline __fastcall virtual TTMSLoggerTextOutputHandler(void)/* overload */ : Tmsloggingcore::TTMSLoggerCustomFileOutputHandler() { }
	/* TTMSLoggerCustomFileOutputHandler.Create */ inline __fastcall virtual TTMSLoggerTextOutputHandler(const System::UnicodeString AFileName)/* overload */ : Tmsloggingcore::TTMSLoggerCustomFileOutputHandler(AFileName) { }
	
public:
	/* TTMSLoggerBaseOutputHandler.Destroy */ inline __fastcall virtual ~TTMSLoggerTextOutputHandler(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tmsloggingtextoutputhandler */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGTEXTOUTPUTHANDLER)
using namespace Tmsloggingtextoutputhandler;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TmsloggingtextoutputhandlerHPP
