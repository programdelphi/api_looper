﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingCorePkgDXE10.dpk' rev: 31.00 (iOSSIM)

#ifndef Tmsloggingcorepkgdxe10HPP
#define Tmsloggingcorepkgdxe10HPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// (rtl)
#include <SysInit.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <TMSLoggingBrowserOutputHandler.hpp>
#include <TMSLoggingEventLogOutputHandler.hpp>
#include <TMSLoggingHelpers.hpp>
#include <TMSLoggingHTMLOutputHandler.hpp>
#include <TMSLoggingDataSourceOutputHandler.hpp>
#include <TMSLoggingTCPOutputHandler.hpp>
#include <TMSLoggingTextOutputHandler.hpp>
#include <TMSLoggingCSVOutputHandler.hpp>
#include <TMSLoggingReg.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingcorepkgdxe10
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tmsloggingcorepkgdxe10 */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGCOREPKGDXE10)
using namespace Tmsloggingcorepkgdxe10;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Tmsloggingcorepkgdxe10HPP
