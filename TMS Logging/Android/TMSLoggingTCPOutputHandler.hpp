﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingTCPOutputHandler.pas' rev: 31.00 (Android)

#ifndef TmsloggingtcpoutputhandlerHPP
#define TmsloggingtcpoutputhandlerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <System.IniFiles.hpp>
#include <IdTCPServer.hpp>
#include <IdTCPClient.hpp>
#include <IdIOHandler.hpp>
#include <IdContext.hpp>
#include <System.Threading.hpp>
#include <IdCustomTCPServer.hpp>
#include <IdComponent.hpp>
#include <IdBaseComponent.hpp>
#include <IdTCPConnection.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingtcpoutputhandler
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TTMSLoggerTCPOutputInformationHandler;
class DELPHICLASS TTMSLoggerTCPServer;
class DELPHICLASS TTMSLoggerTCPClient;
class DELPHICLASS TTMSLoggerTCPOutputHandler;
class DELPHICLASS TTMSLoggerTCPServerOutputHandler;
class DELPHICLASS TTMSLoggerTCPClientOutputHandler;
//-- type declarations -------------------------------------------------------
typedef void __fastcall (__closure *TTMSLoggerTCPClientReceivedOutputInformation)(System::TObject* Sender, const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerTCPOutputInformationHandler : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	__classmethod System::UnicodeString __fastcall ReadString(Idiohandler::TIdIOHandler* const AHandler);
	__classmethod double __fastcall ReadDouble(Idiohandler::TIdIOHandler* const AHandler);
	__classmethod System::TDateTime __fastcall ReadDateTime(Idiohandler::TIdIOHandler* const AHandler);
	__classmethod int __fastcall ReadInteger(Idiohandler::TIdIOHandler* const AHandler);
	__classmethod void __fastcall ReadOutputInformation(Idiohandler::TIdIOHandler* const AHandler, Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	__classmethod void __fastcall WriteString(Idiohandler::TIdIOHandler* const AHandler, const System::UnicodeString AValue);
	__classmethod void __fastcall WriteDouble(Idiohandler::TIdIOHandler* const AHandler, const double AValue);
	__classmethod void __fastcall WriteInteger(Idiohandler::TIdIOHandler* const AHandler, const int AValue);
	__classmethod void __fastcall WriteDatetime(Idiohandler::TIdIOHandler* const AHandler, const double AValue);
	__classmethod void __fastcall WriteOutputInformation(Idiohandler::TIdIOHandler* const AHandler, const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
public:
	/* TObject.Create */ inline __fastcall TTMSLoggerTCPOutputInformationHandler(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TTMSLoggerTCPOutputInformationHandler(void) { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TTMSLoggerTCPMode : unsigned char { tcpReceive, tcpSend };

class PASCALIMPLEMENTATION TTMSLoggerTCPServer : public Idtcpserver::TIdTCPServer
{
	typedef Idtcpserver::TIdTCPServer inherited;
	
private:
	TTMSLoggerTCPClientReceivedOutputInformation FOnReceivedOutputInformation;
	System::Classes::TNotifyEvent FOnDestroyNotification;
	
protected:
	virtual void __fastcall DoReceivedOutputInformation(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	
public:
	virtual bool __fastcall DoExecute(Idcontext::TIdContext* AContext);
	virtual void __fastcall LogOutput(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	__fastcall virtual ~TTMSLoggerTCPServer(void);
	__property System::Classes::TNotifyEvent OnDestroyNotification = {read=FOnDestroyNotification, write=FOnDestroyNotification};
	
__published:
	__property TTMSLoggerTCPClientReceivedOutputInformation OnReceivedOutputInformation = {read=FOnReceivedOutputInformation, write=FOnReceivedOutputInformation};
public:
	/* TIdBaseComponent.Create */ inline __fastcall TTMSLoggerTCPServer(System::Classes::TComponent* AOwner)/* overload */ : Idtcpserver::TIdTCPServer(AOwner) { }
	
public:
	/* TIdInitializerComponent.Create */ inline __fastcall TTMSLoggerTCPServer(void)/* overload */ : Idtcpserver::TIdTCPServer() { }
	
};


class PASCALIMPLEMENTATION TTMSLoggerTCPClient : public Idtcpclient::TIdTCPClient
{
	typedef Idtcpclient::TIdTCPClient inherited;
	
private:
	TTMSLoggerTCPClientReceivedOutputInformation FOnReceivedOutputInformation;
	System::Classes::TNotifyEvent FOnDestroyNotification;
	TTMSLoggerTCPMode FMode;
	
protected:
	virtual void __fastcall DoReceivedOutputInformation(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual void __fastcall DoOnConnected(void);
	
public:
	virtual void __fastcall LogOutput(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	__fastcall virtual ~TTMSLoggerTCPClient(void);
	__property System::Classes::TNotifyEvent OnDestroyNotification = {read=FOnDestroyNotification, write=FOnDestroyNotification};
	
__published:
	__property TTMSLoggerTCPClientReceivedOutputInformation OnReceivedOutputInformation = {read=FOnReceivedOutputInformation, write=FOnReceivedOutputInformation};
	__property TTMSLoggerTCPMode Mode = {read=FMode, write=FMode, default=0};
public:
	/* TIdBaseComponent.Create */ inline __fastcall TTMSLoggerTCPClient(System::Classes::TComponent* AOwner)/* overload */ : Idtcpclient::TIdTCPClient(AOwner) { }
	
public:
	/* TIdInitializerComponent.Create */ inline __fastcall TTMSLoggerTCPClient(void)/* overload */ : Idtcpclient::TIdTCPClient() { }
	
};


enum DECLSPEC_DENUM TTMSLoggerTCPOutputHandlerMode : unsigned char { tcpohmServer, tcpohmClient };

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerTCPOutputHandler : public Tmsloggingcore::TTMSLoggerBaseOutputHandler
{
	typedef Tmsloggingcore::TTMSLoggerBaseOutputHandler inherited;
	
private:
	System::Classes::TComponent* FOwner;
	int FPort;
	TTMSLoggerTCPServer* FTCPServer;
	TTMSLoggerTCPClient* FTCPClient;
	TTMSLoggerTCPOutputHandlerMode FMode;
	System::UnicodeString FHost;
	void __fastcall SetPort(const int Value);
	TTMSLoggerTCPServer* __fastcall GetServer(void);
	void __fastcall SetMode(const TTMSLoggerTCPOutputHandlerMode Value);
	void __fastcall SetHost(const System::UnicodeString Value);
	TTMSLoggerTCPClient* __fastcall GetClient(void);
	
protected:
	virtual void __fastcall LogOutput(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual void __fastcall SetActive(const bool Value);
	virtual void __fastcall WriteValuesToFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	virtual void __fastcall ReadValuesFromFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	virtual void __fastcall DestroyInstances(void);
	virtual void __fastcall ConfigureServer(System::Classes::TComponent* AOwner);
	virtual void __fastcall DestroyInstance(System::TObject* Sender);
	virtual void __fastcall TCPServerExecute(Idcontext::TIdContext* AContext);
	
public:
	__fastcall virtual TTMSLoggerTCPOutputHandler(void)/* overload */;
	__fastcall virtual TTMSLoggerTCPOutputHandler(System::Classes::TComponent* const AOwner)/* overload */;
	__fastcall virtual TTMSLoggerTCPOutputHandler(System::Classes::TComponent* const AOwner, const int APort)/* overload */;
	__fastcall virtual TTMSLoggerTCPOutputHandler(System::Classes::TComponent* const AOwner, const System::UnicodeString AHost, const int APort)/* overload */;
	__fastcall virtual ~TTMSLoggerTCPOutputHandler(void);
	__property TTMSLoggerTCPServer* Server = {read=GetServer};
	__property TTMSLoggerTCPClient* Client = {read=GetClient};
	
__published:
	__property int Port = {read=FPort, write=SetPort, nodefault};
	__property System::UnicodeString Host = {read=FHost, write=SetHost};
	__property TTMSLoggerTCPOutputHandlerMode Mode = {read=FMode, write=SetMode, default=0};
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerTCPServerOutputHandler : public TTMSLoggerTCPOutputHandler
{
	typedef TTMSLoggerTCPOutputHandler inherited;
	
public:
	/* TTMSLoggerTCPOutputHandler.Create */ inline __fastcall virtual TTMSLoggerTCPServerOutputHandler(void)/* overload */ : TTMSLoggerTCPOutputHandler() { }
	/* TTMSLoggerTCPOutputHandler.Create */ inline __fastcall virtual TTMSLoggerTCPServerOutputHandler(System::Classes::TComponent* const AOwner)/* overload */ : TTMSLoggerTCPOutputHandler(AOwner) { }
	/* TTMSLoggerTCPOutputHandler.Create */ inline __fastcall virtual TTMSLoggerTCPServerOutputHandler(System::Classes::TComponent* const AOwner, const int APort)/* overload */ : TTMSLoggerTCPOutputHandler(AOwner, APort) { }
	/* TTMSLoggerTCPOutputHandler.Create */ inline __fastcall virtual TTMSLoggerTCPServerOutputHandler(System::Classes::TComponent* const AOwner, const System::UnicodeString AHost, const int APort)/* overload */ : TTMSLoggerTCPOutputHandler(AOwner, AHost, APort) { }
	/* TTMSLoggerTCPOutputHandler.Destroy */ inline __fastcall virtual ~TTMSLoggerTCPServerOutputHandler(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerTCPClientOutputHandler : public TTMSLoggerTCPOutputHandler
{
	typedef TTMSLoggerTCPOutputHandler inherited;
	
public:
	/* TTMSLoggerTCPOutputHandler.Create */ inline __fastcall virtual TTMSLoggerTCPClientOutputHandler(void)/* overload */ : TTMSLoggerTCPOutputHandler() { }
	/* TTMSLoggerTCPOutputHandler.Create */ inline __fastcall virtual TTMSLoggerTCPClientOutputHandler(System::Classes::TComponent* const AOwner)/* overload */ : TTMSLoggerTCPOutputHandler(AOwner) { }
	/* TTMSLoggerTCPOutputHandler.Create */ inline __fastcall virtual TTMSLoggerTCPClientOutputHandler(System::Classes::TComponent* const AOwner, const int APort)/* overload */ : TTMSLoggerTCPOutputHandler(AOwner, APort) { }
	/* TTMSLoggerTCPOutputHandler.Create */ inline __fastcall virtual TTMSLoggerTCPClientOutputHandler(System::Classes::TComponent* const AOwner, const System::UnicodeString AHost, const int APort)/* overload */ : TTMSLoggerTCPOutputHandler(AOwner, AHost, APort) { }
	/* TTMSLoggerTCPOutputHandler.Destroy */ inline __fastcall virtual ~TTMSLoggerTCPClientOutputHandler(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tmsloggingtcpoutputhandler */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGTCPOUTPUTHANDLER)
using namespace Tmsloggingtcpoutputhandler;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TmsloggingtcpoutputhandlerHPP
