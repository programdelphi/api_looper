﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingBrowserOutputHandler.pas' rev: 31.00 (Android)

#ifndef TmsloggingbrowseroutputhandlerHPP
#define TmsloggingbrowseroutputhandlerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <System.IniFiles.hpp>
#include <IdContext.hpp>
#include <IdTCPConnection.hpp>
#include <IdCustomHTTPServer.hpp>
#include <System.Generics.Collections.hpp>
#include <IdHTTPServer.hpp>
#include <IdScheduler.hpp>
#include <IdSchedulerOfThreadDefault.hpp>
#include <IdHeaderList.hpp>
#include <System.Generics.Defaults.hpp>
#include <System.Types.hpp>
#include <IdCustomTCPServer.hpp>
#include <IdComponent.hpp>
#include <IdBaseComponent.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingbrowseroutputhandler
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TTMSLoggerBitConverter;
class DELPHICLASS TTMSLoggerBrowserException;
class DELPHICLASS TTMSLoggerBrowserHandshakeException;
class DELPHICLASS TTMSLoggerBrowserRequest;
class DELPHICLASS TTMSLoggerBrowserFrame;
class DELPHICLASS TTMSLoggerBrowserConnection;
class DELPHICLASS TTMSLoggerBrowserConnections;
class DELPHICLASS TTMSLoggerHTTPServer;
class DELPHICLASS TTMSLoggerBrowserServer;
class DELPHICLASS TTMSLoggerBrowserOutputHandler;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerBitConverter : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	template<typename T> __classmethod T __fastcall InTo(System::TArray__1<System::Byte> Buffer, int Index);
	template<typename T> __classmethod void __fastcall From(T Value, System::TArray__1<System::Byte> &Buffer);
public:
	/* TObject.Create */ inline __fastcall TTMSLoggerBitConverter(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TTMSLoggerBitConverter(void) { }
	
};

#pragma pack(pop)

typedef void __fastcall (__closure *TTMSLoggerBrowserMessageEvent)(TTMSLoggerBrowserConnection* AConnection, const System::UnicodeString AMessage);

typedef void __fastcall (__closure *TTMSLoggerBrowserConnectEvent)(TTMSLoggerBrowserConnection* AConnection);

typedef void __fastcall (__closure *TTMSLoggerBrowserDisconnectEvent)(TTMSLoggerBrowserConnection* AConnection);

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerBrowserException : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall TTMSLoggerBrowserException(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall TTMSLoggerBrowserException(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High) : System::Sysutils::Exception(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall TTMSLoggerBrowserException(System::PResStringRec ResStringRec) : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall TTMSLoggerBrowserException(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High) : System::Sysutils::Exception(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall TTMSLoggerBrowserException(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall TTMSLoggerBrowserException(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall TTMSLoggerBrowserException(System::PResStringRec ResStringRec, int AHelpContext) : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall TTMSLoggerBrowserException(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~TTMSLoggerBrowserException(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerBrowserHandshakeException : public TTMSLoggerBrowserException
{
	typedef TTMSLoggerBrowserException inherited;
	
public:
	/* Exception.Create */ inline __fastcall TTMSLoggerBrowserHandshakeException(const System::UnicodeString Msg) : TTMSLoggerBrowserException(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall TTMSLoggerBrowserHandshakeException(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High) : TTMSLoggerBrowserException(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall TTMSLoggerBrowserHandshakeException(System::PResStringRec ResStringRec) : TTMSLoggerBrowserException(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall TTMSLoggerBrowserHandshakeException(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High) : TTMSLoggerBrowserException(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall TTMSLoggerBrowserHandshakeException(const System::UnicodeString Msg, int AHelpContext) : TTMSLoggerBrowserException(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall TTMSLoggerBrowserHandshakeException(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High, int AHelpContext) : TTMSLoggerBrowserException(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall TTMSLoggerBrowserHandshakeException(System::PResStringRec ResStringRec, int AHelpContext) : TTMSLoggerBrowserException(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall TTMSLoggerBrowserHandshakeException(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High, int AHelpContext) : TTMSLoggerBrowserException(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~TTMSLoggerBrowserHandshakeException(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerBrowserRequest : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::UnicodeString FHost;
	System::UnicodeString FOrigin;
	System::UnicodeString FKey;
	System::UnicodeString FProtocol;
	System::UnicodeString FVersion;
	
public:
	__fastcall TTMSLoggerBrowserRequest(Idheaderlist::TIdHeaderList* const AHeaderList);
	__property System::UnicodeString Host = {read=FHost};
	__property System::UnicodeString Origin = {read=FOrigin};
	__property System::UnicodeString Protocol = {read=FProtocol};
	__property System::UnicodeString Version = {read=FVersion};
	__property System::UnicodeString Key = {read=FKey};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TTMSLoggerBrowserRequest(void) { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TTMSLoggerBrowserFrameOpcode : unsigned char { focContinuation, focText, focBinary, focClose = 8, focPing, focPong };

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerBrowserFrame : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TTMSLoggerBrowserFrameOpcode FOpcode;
	bool FIsFin;
	unsigned FPayloadLength;
	System::TArray__1<System::Byte> FUnmaskedPayload;
	int FMaskingKey;
	bool FIsMasked;
	
public:
	__fastcall TTMSLoggerBrowserFrame(TTMSLoggerBrowserFrameOpcode AOpcode, System::TArray__1<System::Byte> APayload, bool AIsFin)/* overload */;
	__fastcall TTMSLoggerBrowserFrame(void)/* overload */;
	System::TArray__1<System::Byte> __fastcall ToBuffer(void);
	__classmethod TTMSLoggerBrowserFrame* __fastcall FromBuffer(System::TArray__1<System::Byte> buffer);
	__classmethod System::TArray__1<System::Byte> __fastcall UnMask(System::TArray__1<System::Byte> &payload, int Key);
	__property bool IsFin = {read=FIsFin, write=FIsFin, nodefault};
	__property bool IsMasked = {read=FIsMasked, write=FIsMasked, nodefault};
	__property unsigned PayloadLength = {read=FPayloadLength, write=FPayloadLength, nodefault};
	__property int MaskingKey = {read=FMaskingKey, write=FMaskingKey, nodefault};
	__property System::TArray__1<System::Byte> UnmaskedPayload = {read=FUnmaskedPayload, write=FUnmaskedPayload};
	__property TTMSLoggerBrowserFrameOpcode Opcode = {read=FOpcode, write=FOpcode, nodefault};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TTMSLoggerBrowserFrame(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerBrowserConnection : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TTMSLoggerBrowserOutputHandler* FLoggerOutputHandler;
	Idcontext::TIdContext* FPeerThread;
	TTMSLoggerBrowserRequest* FHandshakeRequest;
	bool FHandshakeResponseSent;
	TTMSLoggerBrowserMessageEvent FOnMessageReceived;
	bool __fastcall GetHandshakeCompleted(void);
	Idtcpconnection::TIdTCPConnection* __fastcall GetServerConnection(void);
	System::UnicodeString __fastcall GetPeerIP(void);
	
protected:
	static constexpr System::WideChar FRAME_START = (System::WideChar)(0x0);
	
	static constexpr char FRAME_SIZE_START = '\x80';
	
	static constexpr char FRAME_END = '\xff';
	
	virtual void __fastcall Handshake(Idcustomhttpserver::TIdHTTPRequestInfo* const ARequestInfo, Idcustomhttpserver::TIdHTTPResponseInfo* const AResponseInfo);
	virtual void __fastcall SendFrame(const System::UnicodeString AData);
	__property Idcontext::TIdContext* PeerThread = {read=FPeerThread, write=FPeerThread};
	__property Idtcpconnection::TIdTCPConnection* ServerConnection = {read=GetServerConnection};
	__property TTMSLoggerBrowserRequest* HandshakeRequest = {read=FHandshakeRequest, write=FHandshakeRequest};
	__property bool HandshakeCompleted = {read=GetHandshakeCompleted, nodefault};
	__property bool HandshakeResponseSent = {read=FHandshakeResponseSent, write=FHandshakeResponseSent, nodefault};
	
public:
	__fastcall TTMSLoggerBrowserConnection(TTMSLoggerBrowserOutputHandler* ALoggerOutputHandler, Idcontext::TIdContext* APeerThread);
	__fastcall virtual ~TTMSLoggerBrowserConnection(void);
	void __fastcall Receive(Idcustomhttpserver::TIdHTTPRequestInfo* const ARequestInfo, Idcustomhttpserver::TIdHTTPResponseInfo* const AResponseInfo);
	void __fastcall Send(const System::UnicodeString AMessage);
	void __fastcall SendBytes(const System::TArray__1<System::Byte> ABytes);
	__property TTMSLoggerBrowserMessageEvent OnMessageReceived = {read=FOnMessageReceived, write=FOnMessageReceived};
	__property System::UnicodeString PeerIP = {read=GetPeerIP};
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerBrowserConnections : public System::Generics::Collections::TList__1<TTMSLoggerBrowserConnection*> 
{
	typedef System::Generics::Collections::TList__1<TTMSLoggerBrowserConnection*>  inherited;
	
public:
	/* {System_Generics_Collections}TList<TMSLoggingBrowserOutputHandler_TTMSLoggerBrowserConnection>.Create */ inline __fastcall TTMSLoggerBrowserConnections(void)/* overload */ : System::Generics::Collections::TList__1<TTMSLoggerBrowserConnection*> () { }
	/* {System_Generics_Collections}TList<TMSLoggingBrowserOutputHandler_TTMSLoggerBrowserConnection>.Create */ inline __fastcall TTMSLoggerBrowserConnections(const System::DelphiInterface<System::Generics::Defaults::IComparer__1<TTMSLoggerBrowserConnection*> > AComparer)/* overload */ : System::Generics::Collections::TList__1<TTMSLoggerBrowserConnection*> (AComparer) { }
	/* {System_Generics_Collections}TList<TMSLoggingBrowserOutputHandler_TTMSLoggerBrowserConnection>.Create */ inline __fastcall TTMSLoggerBrowserConnections(System::Generics::Collections::TEnumerable__1<TTMSLoggerBrowserConnection*> * const Collection)/* overload */ : System::Generics::Collections::TList__1<TTMSLoggerBrowserConnection*> (Collection) { }
	/* {System_Generics_Collections}TList<TMSLoggingBrowserOutputHandler_TTMSLoggerBrowserConnection>.Destroy */ inline __fastcall virtual ~TTMSLoggerBrowserConnections(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TTMSLoggerHTTPServer : public Idhttpserver::TIdHTTPServer
{
	typedef Idhttpserver::TIdHTTPServer inherited;
	
private:
	System::Classes::TNotifyEvent FOnDestroyNotification;
	
public:
	__fastcall virtual ~TTMSLoggerHTTPServer(void);
	__property System::Classes::TNotifyEvent OnDestroyNotification = {read=FOnDestroyNotification, write=FOnDestroyNotification};
public:
	/* TIdBaseComponent.Create */ inline __fastcall TTMSLoggerHTTPServer(System::Classes::TComponent* AOwner)/* overload */ : Idhttpserver::TIdHTTPServer(AOwner) { }
	
public:
	/* TIdInitializerComponent.Create */ inline __fastcall TTMSLoggerHTTPServer(void)/* overload */ : Idhttpserver::TIdHTTPServer() { }
	
};


#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerBrowserServer : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TTMSLoggerBrowserOutputHandler* FLoggerOutputHandler;
	int FDefaultPort;
	System::Classes::TComponent* FOwner;
	TTMSLoggerHTTPServer* FHTTPServer;
	Idscheduler::TIdScheduler* FThreadManager;
	TTMSLoggerBrowserConnections* FConnections;
	TTMSLoggerBrowserConnectEvent FOnConnect;
	TTMSLoggerBrowserMessageEvent FOnMessageReceived;
	TTMSLoggerBrowserDisconnectEvent FOnDisconnect;
	TTMSLoggerHTTPServer* __fastcall GetHTTPServer(void);
	Idscheduler::TIdScheduler* __fastcall GetThreadManager(void);
	TTMSLoggerBrowserConnections* __fastcall GetConnections(void);
	bool __fastcall GetActive(void);
	void __fastcall SetActive(const bool Value);
	
protected:
	virtual void __fastcall DestroyHTTPServer(System::TObject* Sender);
	virtual void __fastcall HTTPServerConnect(Idcontext::TIdContext* AThread);
	virtual void __fastcall HTTPServerDisconnect(Idcontext::TIdContext* AThread);
	virtual void __fastcall HTTPServerCommandGet(Idcontext::TIdContext* AContext, Idcustomhttpserver::TIdHTTPRequestInfo* ARequestInfo, Idcustomhttpserver::TIdHTTPResponseInfo* AResponseInfo);
	virtual void __fastcall MessageReceived(TTMSLoggerBrowserConnection* AConnection, const System::UnicodeString AMessage);
	__property int DefaultPort = {read=FDefaultPort, write=FDefaultPort, nodefault};
	__property TTMSLoggerHTTPServer* HTTPServer = {read=GetHTTPServer};
	__property Idscheduler::TIdScheduler* ThreadManager = {read=GetThreadManager};
	__property TTMSLoggerBrowserConnections* Connections = {read=GetConnections};
	
public:
	__fastcall virtual TTMSLoggerBrowserServer(System::Classes::TComponent* const AOwner, TTMSLoggerBrowserOutputHandler* const ALoggerOutputHandler, const int ADefaultPort);
	__fastcall virtual ~TTMSLoggerBrowserServer(void);
	virtual void __fastcall BroadcastMessage(System::UnicodeString AMessage);
	virtual void __fastcall BroadcastData(System::TArray__1<System::Byte> AData);
	__property bool Active = {read=GetActive, write=SetActive, nodefault};
	__property TTMSLoggerBrowserConnectEvent OnConnect = {read=FOnConnect, write=FOnConnect};
	__property TTMSLoggerBrowserMessageEvent OnMessageReceived = {read=FOnMessageReceived, write=FOnMessageReceived};
	__property TTMSLoggerBrowserDisconnectEvent OnDisconnect = {read=FOnDisconnect, write=FOnDisconnect};
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerBrowserOutputHandler : public Tmsloggingcore::TTMSLoggerBaseOutputHandler
{
	typedef Tmsloggingcore::TTMSLoggerBaseOutputHandler inherited;
	
private:
	bool FEven;
	int FPort;
	TTMSLoggerBrowserServer* FBrowserServer;
	Tmsloggingutils::TTMSLoggerHTMLOutputHandlerMode FMode;
	bool FCreateTable;
	void __fastcall SetPort(const int Value);
	TTMSLoggerHTTPServer* __fastcall GetServer(void);
	
protected:
	virtual void __fastcall Clear(void);
	virtual void __fastcall CreateTable(void);
	virtual void __fastcall SetActive(const bool Value);
	virtual void __fastcall DoGetMessageReceived(TTMSLoggerBrowserConnection* AConnection, const System::UnicodeString AMessage);
	virtual void __fastcall ConfigureServer(void);
	virtual void __fastcall LogOutput(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual void __fastcall WriteValuesToFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	virtual void __fastcall ReadValuesFromFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	
public:
	__fastcall virtual TTMSLoggerBrowserOutputHandler(void)/* overload */;
	__fastcall virtual TTMSLoggerBrowserOutputHandler(System::Classes::TComponent* const AOwner)/* overload */;
	__fastcall virtual TTMSLoggerBrowserOutputHandler(System::Classes::TComponent* const AOwner, const int APort)/* overload */;
	__fastcall virtual TTMSLoggerBrowserOutputHandler(System::Classes::TComponent* const AOwner, const int APort, const Tmsloggingutils::TTMSLoggerHTMLOutputHandlerMode AMode)/* overload */;
	__fastcall virtual ~TTMSLoggerBrowserOutputHandler(void);
	__property TTMSLoggerHTTPServer* Server = {read=GetServer};
	
__published:
	__property int Port = {read=FPort, write=SetPort, nodefault};
	__property Tmsloggingutils::TTMSLoggerHTMLOutputHandlerMode Mode = {read=FMode, write=FMode, nodefault};
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::Byte Fin;
static constexpr System::Int8 TwoBytesLengthCode = System::Int8(0x7e);
static constexpr System::Int8 EightBytesLengthCode = System::Int8(0x7f);
}	/* namespace Tmsloggingbrowseroutputhandler */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGBROWSEROUTPUTHANDLER)
using namespace Tmsloggingbrowseroutputhandler;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TmsloggingbrowseroutputhandlerHPP
