﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingHTMLOutputHandler.pas' rev: 31.00 (iOS)

#ifndef TmslogginghtmloutputhandlerHPP
#define TmslogginghtmloutputhandlerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <System.IniFiles.hpp>
#include <System.Classes.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmslogginghtmloutputhandler
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TTMSLoggerHTMLOutputHandler;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerHTMLOutputHandler : public Tmsloggingcore::TTMSLoggerCustomFileOutputHandler
{
	typedef Tmsloggingcore::TTMSLoggerCustomFileOutputHandler inherited;
	
private:
	bool FEven;
	System::UnicodeString FDataName;
	Tmsloggingutils::TTMSLoggerHTMLOutputHandlerMode FMode;
	void __fastcall SetMode(const Tmsloggingutils::TTMSLoggerHTMLOutputHandlerMode Value);
	
protected:
	virtual System::UnicodeString __fastcall GetOutputText(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual System::UnicodeString __fastcall GetOutputExtension(void);
	virtual System::UnicodeString __fastcall GetOutputFile(void);
	virtual void __fastcall LogOutput(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual void __fastcall WriteValuesToFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	virtual void __fastcall ReadValuesFromFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	void __fastcall CreateViewer(void);
	
public:
	__fastcall virtual TTMSLoggerHTMLOutputHandler(void)/* overload */;
	__fastcall virtual TTMSLoggerHTMLOutputHandler(const System::UnicodeString AFileName)/* overload */;
	__fastcall virtual TTMSLoggerHTMLOutputHandler(const System::UnicodeString AFileName, System::UnicodeString ADataName)/* overload */;
	__fastcall virtual TTMSLoggerHTMLOutputHandler(const System::UnicodeString AFileName, System::UnicodeString ADataName, Tmsloggingutils::TTMSLoggerHTMLOutputHandlerMode AMode)/* overload */;
	
__published:
	__property System::UnicodeString DataName = {read=FDataName, write=FDataName};
	__property Tmsloggingutils::TTMSLoggerHTMLOutputHandlerMode Mode = {read=FMode, write=SetMode, nodefault};
public:
	/* TTMSLoggerBaseOutputHandler.Destroy */ inline __fastcall virtual ~TTMSLoggerHTMLOutputHandler(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tmslogginghtmloutputhandler */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGHTMLOUTPUTHANDLER)
using namespace Tmslogginghtmloutputhandler;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TmslogginghtmloutputhandlerHPP
