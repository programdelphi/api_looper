﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingCSVOutputHandler.pas' rev: 31.00 (iOS)

#ifndef TmsloggingcsvoutputhandlerHPP
#define TmsloggingcsvoutputhandlerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <System.IniFiles.hpp>
#include <System.Classes.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingcsvoutputhandler
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TTMSLoggerCSVOutputHandler;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerCSVOutputHandler : public Tmsloggingcore::TTMSLoggerCustomFileOutputHandler
{
	typedef Tmsloggingcore::TTMSLoggerCustomFileOutputHandler inherited;
	
private:
	System::WideChar FDelimiter;
	bool FCreateHeader;
	
protected:
	virtual System::UnicodeString __fastcall GetOutputText(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual System::UnicodeString __fastcall GetOutputExtension(void);
	virtual void __fastcall WriteValuesToFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	virtual void __fastcall ReadValuesFromFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	virtual void __fastcall Clear(void);
	
public:
	__fastcall virtual TTMSLoggerCSVOutputHandler(void)/* overload */;
	__fastcall virtual TTMSLoggerCSVOutputHandler(const System::UnicodeString AFileName)/* overload */;
	__fastcall virtual TTMSLoggerCSVOutputHandler(const System::UnicodeString AFileName, const System::WideChar ADelimiter)/* overload */;
	
__published:
	__property System::WideChar Delimiter = {read=FDelimiter, write=FDelimiter, nodefault};
public:
	/* TTMSLoggerBaseOutputHandler.Destroy */ inline __fastcall virtual ~TTMSLoggerCSVOutputHandler(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tmsloggingcsvoutputhandler */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGCSVOUTPUTHANDLER)
using namespace Tmsloggingcsvoutputhandler;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TmsloggingcsvoutputhandlerHPP
