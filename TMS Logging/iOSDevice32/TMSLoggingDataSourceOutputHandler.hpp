﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'TMSLoggingDataSourceOutputHandler.pas' rev: 31.00 (iOS)

#ifndef TmsloggingdatasourceoutputhandlerHPP
#define TmsloggingdatasourceoutputhandlerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <TMSLoggingCore.hpp>
#include <TMSLoggingUtils.hpp>
#include <System.IniFiles.hpp>
#include <Data.DB.hpp>

//-- user supplied -----------------------------------------------------------

namespace Tmsloggingdatasourceoutputhandler
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TTMSLoggerDataSourceOutputHandlerFields;
class DELPHICLASS TTMSLoggerDataSourceOutputHandler;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerDataSourceOutputHandlerFields : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::UnicodeString FName;
	System::UnicodeString FMemoryUsage;
	System::UnicodeString FThreadID;
	System::UnicodeString FProcessID;
	System::UnicodeString FTimeStamp;
	System::UnicodeString FType;
	System::UnicodeString FValue;
	System::UnicodeString FLogLevel;
	
public:
	__fastcall TTMSLoggerDataSourceOutputHandlerFields(void);
	
__published:
	__property System::UnicodeString TimeStamp = {read=FTimeStamp, write=FTimeStamp};
	__property System::UnicodeString ProcessID = {read=FProcessID, write=FProcessID};
	__property System::UnicodeString ThreadID = {read=FThreadID, write=FThreadID};
	__property System::UnicodeString MemoryUsage = {read=FMemoryUsage, write=FMemoryUsage};
	__property System::UnicodeString LogLevel = {read=FLogLevel, write=FLogLevel};
	__property System::UnicodeString Name = {read=FName, write=FName};
	__property System::UnicodeString Value = {read=FValue, write=FValue};
	__property System::UnicodeString Type = {read=FType, write=FType};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TTMSLoggerDataSourceOutputHandlerFields(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTMSLoggerDataSourceOutputHandler : public Tmsloggingcore::TTMSLoggerBaseOutputHandler
{
	typedef Tmsloggingcore::TTMSLoggerBaseOutputHandler inherited;
	
private:
	Data::Db::TDataSource* FDataSource;
	TTMSLoggerDataSourceOutputHandlerFields* FFields;
	void __fastcall SetFields(TTMSLoggerDataSourceOutputHandlerFields* const Value);
	
protected:
	bool __fastcall CheckDataSet(void);
	virtual void __fastcall LogOutput(const Tmsloggingutils::TTMSLoggerOutputInformation &AOutputInformation);
	virtual void __fastcall WriteValuesToFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	virtual void __fastcall ReadValuesFromFile(System::Inifiles::TIniFile* const AIniFile, const System::UnicodeString ASection);
	
public:
	__fastcall virtual TTMSLoggerDataSourceOutputHandler(void)/* overload */;
	__fastcall virtual TTMSLoggerDataSourceOutputHandler(Data::Db::TDataSource* const ADataSource)/* overload */;
	__fastcall virtual ~TTMSLoggerDataSourceOutputHandler(void);
	
__published:
	__property Data::Db::TDataSource* DataSource = {read=FDataSource, write=FDataSource};
	__property TTMSLoggerDataSourceOutputHandlerFields* Fields = {read=FFields, write=SetFields};
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Tmsloggingdatasourceoutputhandler */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_TMSLOGGINGDATASOURCEOUTPUTHANDLER)
using namespace Tmsloggingdatasourceoutputhandler;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TmsloggingdatasourceoutputhandlerHPP
