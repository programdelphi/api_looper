{********************************************************************}
{                                                                    }
{ written by TMS Software                                            }
{            copyright � 2016                                        }
{            Email : info@tmssoftware.com                            }
{            Web : http://www.tmssoftware.com                        }
{                                                                    }
{ The source code is given as is. The author is not responsible      }
{ for any possible damage done due to the use of this code.          }
{ The complete source code remains property of the author and may    }
{ not be distributed, published, given or sold in any form as such.  }
{ No parts of the source code can be included in any other component }
{ or application without written authorization of the author.        }
{********************************************************************}

unit TMSLoggingMyCloudDataBaseOutputHandler;

interface

uses
  Classes, TMSLoggingCore, TMSLoggingUtils
  {$IFDEF MSWINDOWS}
  ,Windows, Registry
  {$ENDIF}
  ,IniFiles
  ;

type
  TTMSLoggerMyCloudDataBaseOutputHandlerMetaData = class(TPersistent)
  private
    FName: string;
    FMemoryUsage: string;
    FThreadID: string;
    FProcessID: string;
    FTimeStamp: string;
    FType: string;
    FValue: string;
    FLogLevel: string;
  public
    constructor Create;
  published
    property TimeStamp: string read FTimeStamp write FTimeStamp;
    property ProcessID: string read FProcessID write FProcessID;
    property ThreadID: string read FThreadID write FThreadID;
    property MemoryUsage: string read FMemoryUsage write FMemoryUsage;
    property LogLevel: string read FLogLevel write FLogLevel;
    property Name: string read FName write FName;
    property Value: string read FValue write FValue;
    property &Type: string read FType write FType;
  end;

  TTMSLoggerMyCloudDataBaseOutputHandler = class(TTMSLoggerBaseOutputHandler)
  private
    FTableName: string;
    FMetaData: TTMSLoggerMyCloudDataBaseOutputHandlerMetaData;
    procedure SetMetaData(
      const Value: TTMSLoggerMyCloudDataBaseOutputHandlerMetaData);
  protected
    procedure WriteValuesToFile(const AIniFile: TIniFile; const ASection: string); override;
    procedure ReadValuesFromFile(const AIniFile: TIniFile; const ASection: string); override;
    {$IFDEF MSWINDOWS}
    procedure WriteValuesToRegistry(const ARegistry: TRegistry); override;
    procedure ReadValuesFromRegistry(const ARegistry: TRegistry); override;
    {$ENDIF}
  public
    constructor Create; override;
    destructor Destroy; override;
  published
    property TableName: string read FTableName write FTableName;
    property MetaData: TTMSLoggerMyCloudDataBaseOutputHandlerMetaData read FMetaData write SetMetaData;
  end;

implementation

uses
  SysUtils;

{ TTMSLoggerMyCloudDataBaseOutputHandler }

constructor TTMSLoggerMyCloudDataBaseOutputHandler.Create;
begin
  inherited Create;
  FTableName := 'MyCloudDataLogging';
  FMetaData := TTMSLoggerMyCloudDataBaseOutputHandlerMetaData.Create;
end;

destructor TTMSLoggerMyCloudDataBaseOutputHandler.Destroy;
begin
  FMetaData.Free;
  inherited;
end;

procedure TTMSLoggerMyCloudDataBaseOutputHandler.ReadValuesFromFile(
  const AIniFile: TIniFile; const ASection: string);
begin
  inherited;
  TableName := AIniFile.ReadString(ASection, 'TableName', TableName);
  MetaData.Name := AIniFile.ReadString(ASection, 'MetaData.Name', MetaData.Name);
  MetaData.MemoryUsage := AIniFile.ReadString(ASection, 'MetaData.MemoryUsage', MetaData.MemoryUsage);
  MetaData.ThreadID := AIniFile.ReadString(ASection, 'MetaData.ThreadID', MetaData.ThreadID);
  MetaData.ProcessID := AIniFile.ReadString(ASection, 'MetaData.ProcessID', MetaData.ProcessID);
  MetaData.FTimeStamp := AIniFile.ReadString(ASection, 'MetaData.TimeStamp', MetaData.TimeStamp);
  MetaData.&Type := AIniFile.ReadString(ASection, 'MetaData.Type', MetaData.&Type);
  MetaData.Value := AIniFile.ReadString(ASection, 'MetaData.Value', MetaData.Value);
  MetaData.LogLevel := AIniFile.ReadString(ASection, 'MetaData.LogLevel', MetaData.LogLevel);
end;

procedure TTMSLoggerMyCloudDataBaseOutputHandler.WriteValuesToFile(
  const AIniFile: TIniFile; const ASection: string);
begin
  inherited;
  AIniFile.WriteString(ASection, 'TableName', TableName);
  AIniFile.WriteString(ASection, 'MetaData.Name', MetaData.Name);
  AIniFile.WriteString(ASection, 'MetaData.MemoryUsage', MetaData.MemoryUsage);
  AIniFile.WriteString(ASection, 'MetaData.ThreadID', MetaData.ThreadID);
  AIniFile.WriteString(ASection, 'MetaData.ProcessID', MetaData.ProcessID);
  AIniFile.WriteString(ASection, 'MetaData.TimeStamp', MetaData.TimeStamp);
  AIniFile.WriteString(ASection, 'MetaData.Type', MetaData.&Type);
  AIniFile.WriteString(ASection, 'MetaData.Value', MetaData.Value);
  AIniFile.WriteString(ASection, 'MetaData.LogLevel', MetaData.LogLevel);
end;

{$IFDEF MSWINDOWS}
procedure TTMSLoggerMyCloudDataBaseOutputHandler.ReadValuesFromRegistry(
  const ARegistry: TRegistry);
begin
  inherited;
  TableName := ARegistry.ReadString('TableName');
  MetaData.Name := ARegistry.ReadString('MetaData.Name');
  MetaData.MemoryUsage := ARegistry.ReadString('MetaData.MemoryUsage');
  MetaData.ThreadID := ARegistry.ReadString('MetaData.ThreadID');
  MetaData.ProcessID := ARegistry.ReadString('MetaData.ProcessID');
  MetaData.FTimeStamp := ARegistry.ReadString('MetaData.TimeStamp');
  MetaData.&Type := ARegistry.ReadString('MetaData.Type');
  MetaData.Value := ARegistry.ReadString('MetaData.Value');
  MetaData.LogLevel := ARegistry.ReadString('MetaData.LogLevel');
end;

procedure TTMSLoggerMyCloudDataBaseOutputHandler.SetMetaData(
  const Value: TTMSLoggerMyCloudDataBaseOutputHandlerMetaData);
begin
  FMetaData.Assign(Value);
end;

procedure TTMSLoggerMyCloudDataBaseOutputHandler.WriteValuesToRegistry(
  const ARegistry: TRegistry);
begin
  inherited;
  ARegistry.WriteString('TableName', TableName);
  ARegistry.WriteString('MetaData.Name', MetaData.Name);
  ARegistry.WriteString('MetaData.MemoryUsage', MetaData.MemoryUsage);
  ARegistry.WriteString('MetaData.ThreadID', MetaData.ThreadID);
  ARegistry.WriteString('MetaData.ProcessID', MetaData.ProcessID);
  ARegistry.WriteString('MetaData.TimeStamp', MetaData.TimeStamp);
  ARegistry.WriteString('MetaData.Type', MetaData.&Type);
  ARegistry.WriteString('MetaData.Value', MetaData.Value);
  ARegistry.WriteString('MetaData.LogLevel', MetaData.LogLevel);
end;
{$ENDIF}

{ TTMSLoggerMyCloudDataBaseOutputHandlerMetaData }

constructor TTMSLoggerMyCloudDataBaseOutputHandlerMetaData.Create;
begin
  FThreadID := 'ThreadID';
  FMemoryUsage := 'MemoryUsage';
  FName := 'Name';
  FProcessID := 'ProcessID';
  FTimeStamp := 'TimeStamp';
  FType := 'Type';
  FLogLevel := 'LogLevel';
  FValue := 'Value';
end;

initialization
  RegisterClasses([TTMSLoggerMyCloudDataBaseOutputHandler]);

end.
