{********************************************************************}
{                                                                    }
{ written by TMS Software                                            }
{            copyright � 2016                                        }
{            Email : info@tmssoftware.com                            }
{            Web : http://www.tmssoftware.com                        }
{                                                                    }
{ The source code is given as is. The author is not responsible      }
{ for any possible damage done due to the use of this code.          }
{ The complete source code remains property of the author and may    }
{ not be distributed, published, given or sold in any form as such.  }
{ No parts of the source code can be included in any other component }
{ or application without written authorization of the author.        }
{********************************************************************}

unit TMSLoggingExceptionlessBaseOutputHandler;

interface

uses
  TMSLoggingCore, TMSLoggingUtils
  {$IFDEF MSWINDOWS}
  ,Windows, Registry
  {$ENDIF}
  ,IniFiles
  ;

type
  TTMSLoggerExceptionlessBaseOutputHandler = class(TTMSLoggerBaseOutputHandler)
  private
    FProjectID: string;
  protected
    procedure WriteValuesToFile(const AIniFile: TIniFile; const ASection: string); override;
    procedure ReadValuesFromFile(const AIniFile: TIniFile; const ASection: string); override;
    {$IFDEF MSWINDOWS}
    procedure WriteValuesToRegistry(const ARegistry: TRegistry); override;
    procedure ReadValuesFromRegistry(const ARegistry: TRegistry); override;
    {$ENDIF}
  published
    property ProjectID: string read FProjectID write FProjectID;
  end;

implementation

uses
  Classes, SysUtils;

{ TTMSLoggerExceptionlessBaseOutputHandler }

procedure TTMSLoggerExceptionlessBaseOutputHandler.ReadValuesFromFile(
  const AIniFile: TIniFile; const ASection: string);
begin
  inherited;
  ProjectID := AIniFile.ReadString(ASection, 'ProjectID', ProjectID);
end;

procedure TTMSLoggerExceptionlessBaseOutputHandler.WriteValuesToFile(
  const AIniFile: TIniFile; const ASection: string);
begin
  inherited;
  AIniFile.WriteString(ASection, 'ProjectID', ProjectID);
end;

{$IFDEF MSWINDOWS}
procedure TTMSLoggerExceptionlessBaseOutputHandler.ReadValuesFromRegistry(
  const ARegistry: TRegistry);
begin
  inherited;
  ProjectID := ARegistry.ReadString('ProjectID');
end;

procedure TTMSLoggerExceptionlessBaseOutputHandler.WriteValuesToRegistry(
  const ARegistry: TRegistry);
begin
  inherited;
  ARegistry.WriteString('ProjectID', ProjectID);
end;
{$ENDIF}

initialization
  RegisterClasses([TTMSLoggerExceptionlessBaseOutputHandler]);

end.
