unit uTestRecords;

interface

uses
  DUnitX.TestFramework;

type
  [TestFixture]
    TTestRecords = class(TObject)
  public
    [TestCase]
    procedure TestFlatKV;
  end;

implementation

uses
  uRecords;

{ TTestRecords }

procedure TTestRecords.TestFlatKV;
var
  RC: TRestCall;
  D: IDictionaryStringString;
begin
  //  RestCall.ResponseText := '{"k":"v", "b":"1111"}';
  //  Assert.IsTrue(RestCall.FlatKV.ContainsKey('k'));
  RC.ResponseText := '{"b":{"c":"1111"},"d":[{"d1":11, "d2":2}, {"e":1}, {"x":null}, {"y":false}]}';
  D := RC.FlatKV();
  Assert.IsTrue(D.ContainsKey('e'));
  Assert.AreEqual('1', D['e']);
  Assert.AreEqual('null', D['x']);
  Assert.AreEqual('false', D['y']);
end;

initialization
  TDUnitX.RegisterTestFixture(TTestRecords);
end.

