object fCS: TfCS
  Left = 0
  Top = 0
  Caption = 'Canned SQL'
  ClientHeight = 411
  ClientWidth = 852
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 374
    Width = 852
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      852
      37)
    object btnCancel: TButton
      Left = 764
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Cancel'
      Default = True
      ModalResult = 2
      TabOrder = 0
    end
    object btnOK: TButton
      Left = 683
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 1
    end
  end
  object grdCS: TcxGrid
    Left = 0
    Top = 0
    Width = 852
    Height = 374
    Align = alClient
    TabOrder = 1
    object dbtvCS: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Append.Visible = True
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      OnCellDblClick = dbtvCSCellDblClick
      DataController.DataSource = dsCS
      DataController.KeyFieldNames = 'name'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Appending = True
      OptionsView.CellAutoHeight = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.DataRowHeight = 60
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object dbtvCSname: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 200
      end
      object dbtvCSsql: TcxGridDBColumn
        DataBinding.FieldName = 'sql'
        Width = 600
      end
    end
    object grdCSLevel1: TcxGridLevel
      GridView = dbtvCS
    end
  end
  object mtCS: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'name'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'sql'
        DataType = ftWideMemo
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 336
    Top = 24
    Content = {
      414442530F006734F8000000FF00010001FF02FF030400080000006D00740043
      00530005000A0000005400610062006C00650006000000000007000008003200
      0000090000FF0AFF0B0400080000006E0061006D0065000500080000006E0061
      006D0065000C00010000000E000D000F00640000001000011100011200011300
      011400011500011600080000006E0061006D006500170064000000FEFF0B0400
      06000000730071006C00050006000000730071006C000C00020000000E001800
      100001110001190001120001130001140001150001160006000000730071006C
      00FEFEFF1AFEFF1BFEFF1CFEFEFEFF1DFEFF1EFF1FFEFEFE0E004D0061006E00
      61006700650072001E0055007000640061007400650073005200650067006900
      730074007200790012005400610062006C0065004C006900730074000A005400
      610062006C00650008004E0061006D006500140053006F007500720063006500
      4E0061006D0065000A0054006100620049004400240045006E0066006F007200
      6300650043006F006E00730074007200610069006E00740073001E004D006900
      6E0069006D0075006D0043006100700061006300690074007900180043006800
      650063006B004E006F0074004E0075006C006C00140043006F006C0075006D00
      6E004C006900730074000C0043006F006C0075006D006E00100053006F007500
      7200630065004900440018006400740041006E00730069005300740072006900
      6E0067001000440061007400610054007900700065000800530069007A006500
      1400530065006100720063006800610062006C006500120041006C006C006F00
      77004E0075006C006C000800420061007300650014004F0041006C006C006F00
      77004E0075006C006C0012004F0049006E005500700064006100740065001000
      4F0049006E00570068006500720065001A004F0072006900670069006E004300
      6F006C004E0061006D006500140053006F007500720063006500530069007A00
      650014006400740057006900640065004D0065006D006F00100042006C006F00
      620044006100740061001C0043006F006E00730074007200610069006E007400
      4C00690073007400100056006900650077004C006900730074000E0052006F00
      77004C006900730074001800520065006C006100740069006F006E004C006900
      730074001C0055007000640061007400650073004A006F00750072006E006100
      6C000E004300680061006E00670065007300}
    object mtCSname: TStringField
      FieldName = 'name'
      Size = 100
    end
    object mtCSsql: TWideMemoField
      FieldName = 'sql'
      BlobType = ftWideMemo
    end
  end
  object dsCS: TDataSource
    DataSet = mtCS
    Left = 336
    Top = 72
  end
end
