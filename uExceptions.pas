unit uExceptions;

interface

uses
  System.SysUtils;

const
  STATUS_SUCCESS = 'SUCCESS';
  STATUS_ERROR = 'ERROR';

type
  TApiException = class(Exception);

implementation

end.

