unit uMemoBox;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxGroupBox, cxTextEdit, cxMemo, cxLabel;

type
  TfmMemoBox = class(TFrame)
    memoMsg: TcxMemo;
    cxGroupBox1: TcxGroupBox;
    btnClose: TcxButton;
    btnShowMore: TcxButton;
    procedure btnShowMoreClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);

  private
    FIdent: string;
    FMessageCount: Integer;
    function CleanupMessage(const AMessage: string): string;

  public
    constructor Create(AOwner: TComponent; const AIdent: string); reintroduce;
    property Ident: string read FIdent;
    procedure AddMessage(const AMessage: string);
  end;

const
  WM_CLOSE_MEMO_BOX = WM_USER + 1;

implementation

uses
  uMemoDlg;

{$R *.dfm}

{ TfmMemoBox }

function TfmMemoBox.CleanupMessage(const AMessage: string): string;
begin
  Result := StringReplace(AMessage, sLineBreak, ' ', [rfReplaceAll]);
  Result := StringReplace(Result, #9, ' ', [rfReplaceAll]);
  while Pos('  ', Result) > 0 do
    Delete(Result, Pos('  ', Result), 1);
end;

procedure TfmMemoBox.AddMessage(const AMessage: string);
begin
  Inc(FMessageCount);
  memoMsg.Lines.Add(IntToStr(FMessageCount) + ' ' + CleanupMessage(AMessage))
end;

procedure TfmMemoBox.btnCloseClick(Sender: TObject);
begin
  PostMessage(TWinControl(Owner).Handle, WM_CLOSE_MEMO_BOX, Cardinal(Self), 0);
end;

procedure TfmMemoBox.btnShowMoreClick(Sender: TObject);
begin
  TfrmMemoDlg.Run(memoMsg.Lines.Text);
end;

constructor TfmMemoBox.Create(AOwner: TComponent; const AIdent: string);
begin
  inherited Create(AOwner);
  FIdent := AIdent;
  memoMsg.Lines.Text := '';
  FMessageCount := 0;
end;

end.
