unit uMemoDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxTextEdit, cxMemo, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TfrmMemoDlg = class(TForm)
    Button1: TButton;
    Panel1: TPanel;
    memoMsg: TcxMemo;
  private
  public
    class procedure Run(const AMessage: string);
  end;

implementation

{$R *.dfm}

{ TfrmMemoDlg }

class procedure TfrmMemoDlg.Run(const AMessage: string);
begin
  with Create(nil) do
  try
    memoMsg.Lines.Text := AMessage;
    ShowModal;
  finally
    Free;
  end;
end;

end.
