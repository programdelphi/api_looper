unit uRestAPIQueueeThread;

interface

uses
  SyncObjs,
  Spring, Spring.Collections, System.Classes,
  System.Generics.Collections,
  uRecords,
  uDataManager, IdThreadSafe, System.Threading;

type
  TCompleteRequestEvent = procedure(const Host: string; const ARestCall: TRestCall) of object;
  TErrorRequestEvent = procedure(const Host: string; const ARestCall: TRestCall; const AErrorMessage: string) of object;

  TRestAPIQueueeThread = class(TThread)
  private
    FWait: IList<TRestCall>;
    FCurrent: IList<TRestCall>;
    FLock: Lock;
    FOnCompleteRequest: TCompleteRequestEvent;
    FOnErrorRequest: TErrorRequestEvent;
    FRetries: Integer;
    FCurrentCount: TIdThreadSafeInteger;
    FTasks: Integer;
    FClearListEvent: TLightweightEvent;
    FPause: TIdThreadSafeBoolean;
    FHost: string;
    function GetListCount: Integer;
    procedure Run(call: TRestCall);
    procedure SetRetries(const Value: Integer);
    function GetPause: Boolean;
    procedure SetPause(const Value: Boolean);

  protected
    procedure Execute; override;

  public
    property Host: string read FHost write FHost;
    property ListCount: Integer read GetListCount;
    property Retries: Integer read FRetries write SetRetries;

    { Methods }
    procedure Add(ARestCall: TRestCall); overload;
    procedure Add(ARestCall: IList<TRestCall>); overload;

    procedure ClearList;
    procedure SetNumTasks(ATasks: Integer);

    { Events }
    property OnCompleteRequest: TCompleteRequestEvent read FOnCompleteRequest write FOnCompleteRequest;
    property OnErrorRequest: TErrorRequestEvent read FOnErrorRequest write FOnErrorRequest;

    property Pause: Boolean read GetPause write SetPause;

    { constructor }
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  uExceptions,
  System.SysUtils,
  System.JSON,
  REST.Client,
  REST.Types, Winapi.Windows, System.Math;

const
  DELAY_BEFORE_RETRY = 4000; // beware calculated as random(1000, n)
  DELAY_NO_WORK      = 100;

procedure ParallelAnononymousFor(const ALow, AHigh: Integer; const ALimit: Integer; ATask: TProc<Integer>;
    ABreakEvent: TLightweightEvent; APause: TIdThreadSafeBoolean);
var
  Running, Current: TIdThreadSafeInteger;
  Thread: TThread;
begin
  Running := TIdThreadSafeInteger.Create;
  Current := TIdThreadSafeInteger.Create;
  try
    Current.Value := ALow;
    Running.Value := 0;
    while  (Current.Value <= AHigh) and not ABreakEvent.IsSet do
    begin
      if not APause.Value and (Running.Value < ALimit) then
      begin
        Running.Increment;
        Thread := TThread.CreateAnonymousThread(
          procedure
          var
            I: Integer;
          begin
            try
              I := Current.Increment;
              // (1) checking if current index is still <= high, because wait locks can go to situation where increment is above limit (thread count above core count)
              if (ALow <= I) and (I <= AHigh) then
                try
                  ATask(I);
                except
                end;
            finally
              Running.Decrement;
            end;
          end);
        Thread.Start;
      end
      else
        Sleep(DELAY_NO_WORK); //also helps with avoid (1) problem
    end;
    while Running.Value > 0 do
      Sleep(DELAY_NO_WORK);
  finally
    Running.Free;
    Current.Free;
  end;
end;

{ TRestAPIQueueeThread }

constructor TRestAPIQueueeThread.Create;
begin
  FWait := TCollections.CreateList<TRestCall>;
  FCurrent := TCollections.CreateList<TRestCall>;
  FCurrentCount := TIdThreadSafeInteger.Create;
  inherited Create(false);
  FRetries := 3;
  FTasks := CPUCount;
  FClearListEvent := TLightweightEvent.Create;
  FPause := TIdThreadSafeBoolean.Create;
end;

destructor TRestAPIQueueeThread.Destroy;
begin
  FWait := nil;
  FCurrent := nil;
  FCurrentCount.Free;
  FClearListEvent.Free;
  FPause.Free;
  inherited;
end;

procedure TRestAPIQueueeThread.Add(ARestCall: TRestCall);
begin
  FLock.Enter;
  try
    FWait.Add(ARestCall);
  finally
    FLock.Leave;
  end;
end;

procedure TRestAPIQueueeThread.Add(ARestCall: IList<TRestCall>);
begin
  FLock.Enter;
  try
    FWait.AddRange(ARestCall);
  finally
    FLock.Leave;
  end;
end;


procedure TRestAPIQueueeThread.Run(call: TRestCall);
var
  mgr: TDataManager;
  I: Integer;
  Success: Boolean;
  LastException: string;
begin
  LastException := '';
  mgr := TDataManager.Create;
  try
    Success := False;
    I := 0;
    while (I < FRetries) and not Success do
    begin
      try
        //raise Exception.Create('test error');
        mgr.RunApi(call);
        Success := True;
      except
        LastException := (ExceptObject as Exception).Message;
        Sleep(RandomRange(1000, DELAY_BEFORE_RETRY));
      end;
      I := I + 1;
    end;

    if Success then
      Queue(
        procedure
        begin
          if Assigned(FOnCompleteRequest) then
            FOnCompleteRequest(Host, call);
        end)
    else
      Queue(
        procedure
        begin
          if Assigned(FOnErrorRequest) then
            FOnErrorRequest(Host, call, LastException);
        end);
  finally
    mgr.Free;
  end;
end;

procedure TRestAPIQueueeThread.SetNumTasks(ATasks: Integer);
begin
  if ATasks > 1 then
    FTasks := ATasks
  else
    FTasks := 1;
end;

procedure TRestAPIQueueeThread.SetPause(const Value: Boolean);
begin
  FPause.Value := Value;
end;

procedure TRestAPIQueueeThread.SetRetries(const Value: Integer);
begin
  if Value < 1 then
    FRetries := 1
  else
    FRetries := Value;
end;

procedure TRestAPIQueueeThread.Execute;
var
  Tmp: TRestCall;
begin
  inherited;
  NameThreadForDebugging('rest_que');

  while not Terminated do
  begin
    FCurrent.Clear;
    FLock.Enter;
    try
      if FClearListEvent.IsSet then
        FWait.Clear;
      while FWait.Count > 0 do
      begin
        Tmp := FWait.Extract(FWait.First);
        FCurrent.Add(Tmp);
      end;
    finally
      FLock.Leave;
    end;

    if FCurrent.Count > 0 then
    begin
      if FClearListEvent.IsSet then
        FWait.Clear;
      FCurrentCount.Value := FCurrent.Count;
      ParallelAnononymousFor(0, FCurrent.Count - 1, FTasks,
          procedure(AIndex: Integer)
          begin
            try
              Run(FCurrent[AIndex]);
            except
            end;
            FCurrentCount.Decrement;
          end,
        FClearListEvent, FPause
        );
      FCurrentCount.Value := 0;
      FCurrent.Clear;
    end
    else
    begin
      FCurrentCount.Value := 0;
      Sleep(DELAY_NO_WORK);
    end
  end;
end;

function TRestAPIQueueeThread.GetListCount: Integer;
begin
  FLock.Enter;
  try
    Result := FWait.Count + FCurrentCount.Value;
  finally
    FLock.Leave; 
  end;
end;

function TRestAPIQueueeThread.GetPause: Boolean;
begin
  Result := FPause.Value;
end;

procedure TRestAPIQueueeThread.ClearList();
begin
  FClearListEvent.SetEvent;
  while GetListCount <> 0 do
    Sleep(100);
  FClearListEvent.ResetEvent;
end;

initialization

end.

