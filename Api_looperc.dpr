program Api_looperc;

{$APPTYPE CONSOLE}

{$R *.res}


uses
  FastMM4,
  System.SysUtils,
  uRestAPIQueueeThread in 'uRestAPIQueueeThread.pas',
  uExceptions in 'uExceptions.pas',
  uApiConnection in 'uApiConnection.pas',
  uDataManager in 'uDataManager.pas',
  uRecords in 'uRecords.pas',
  uLooper in 'uLooper.pas';

procedure PrintHelp;
begin
  writeln('executes api_looper job');
  writeln;
  writeln('api_lopper_c.exe <ini_file_name>');
  writeln;
  writeln('exit code (%errorlevel%):');
  writeln('  0 - ok');
  writeln('  1 - loop error');
  writeln('  2 - api error');
end;

var
  LooperJob: ILooperJob;

begin
  try
    if (ParamCount = 0) or FindCmdLineSwitch('h') or FindCmdLineSwitch('?') then
    begin
      PrintHelp();
      Exit;
    end;

    if ParamCount <> 1 then
      Exit;

    LooperJob := TLooperJob.Create(ParamStr(1));

    if LooperJob.Run then
      ExitCode := 0
    else
      ExitCode := 2;

  except
    on E: Exception do
    begin
      writeln(E.ClassName, ': ', E.Message);
      ExitCode := 1;
    end;
  end;

end.
