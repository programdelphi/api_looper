object fResAndPar: TfResAndPar
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Resources and parameters'
  ClientHeight = 697
  ClientWidth = 940
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object grdDef: TcxGrid
    Left = 0
    Top = 0
    Width = 940
    Height = 660
    Align = alClient
    TabOrder = 0
    object tvEndpoint: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Append.Visible = True
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = dsEnpoint
      DataController.KeyFieldNames = 'id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Appending = True
      OptionsView.CellAutoHeight = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object tvEndpointid: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object tvEndpointendpoint: TcxGridDBColumn
        Caption = 'Endpoint'
        DataBinding.FieldName = 'endpoint'
        Width = 200
      end
      object tvEndpointremark: TcxGridDBColumn
        Caption = 'Remark'
        DataBinding.FieldName = 'remark'
      end
    end
    object tvParam: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Append.Visible = True
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = dsParam
      DataController.DetailKeyFieldNames = 'endpoint_id'
      DataController.KeyFieldNames = 'id'
      DataController.MasterKeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Appending = True
      OptionsView.CellAutoHeight = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object tvParamsel: TcxGridDBColumn
        Caption = 'Select'
        DataBinding.FieldName = 'sel'
        Width = 20
      end
      object tvParamid: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 50
      end
      object tvParamendpoint_id: TcxGridDBColumn
        DataBinding.FieldName = 'endpoint_id'
        Visible = False
        Width = 50
      end
      object tvParamname: TcxGridDBColumn
        Caption = 'Parameter name'
        DataBinding.FieldName = 'name'
        Width = 50
      end
      object tvParamdefault: TcxGridDBColumn
        Caption = 'Default'
        DataBinding.FieldName = 'default'
        Width = 50
      end
      object tvParamdesc: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'desc'
        Width = 50
      end
    end
    object grdDefLevel1: TcxGridLevel
      GridView = tvEndpoint
      object grdDefLevel2: TcxGridLevel
        GridView = tvParam
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 660
    Width = 940
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      940
      37)
    object btnUseAll: TButton
      Left = 653
      Top = 6
      Width = 200
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Use resource and all params'
      Default = True
      TabOrder = 0
      OnClick = btnUseAllClick
    end
    object Button2: TButton
      Left = 859
      Top = 6
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
    object btnUseSel: TButton
      Left = 447
      Top = 6
      Width = 200
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Use resource and selected params'
      Default = True
      TabOrder = 2
      OnClick = btnUseSelClick
    end
  end
  object mtEndpoint: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'id'
        DataType = ftAutoInc
      end
      item
        Name = 'endpoint'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'remark'
        DataType = ftMemo
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 616
    Top = 344
    Content = {
      414442530F001236BE010000FF00010001FF02FF030400140000006D00740045
      006E00640070006F0069006E00740005000A0000005400610062006C00650006
      0000000000070000080032000000090000FF0AFF0B0400040000006900640005
      0004000000690064000C00010000000E000D000F000110000111000112000113
      000114000115000116000117000400000069006400FEFF0B0400100000006500
      6E00640070006F0069006E00740005001000000065006E00640070006F006900
      6E0074000C00020000000E0018001900C80000000F0001100001120001130001
      14000115000117001000000065006E00640070006F0069006E0074001A00C800
      0000FEFF0B04000C000000720065006D00610072006B0005000C000000720065
      006D00610072006B000C00030000000E001B000F00011000011C000112000113
      000114000115000117000C000000720065006D00610072006B00FEFEFF1DFEFF
      1EFEFF1FFF20210000000000FF2200000100000001000D000000557064617461
      50726F64756374FEFEFF20210001000000FF2200000200000001000A00000041
      646450726F64756374FEFEFEFEFEFF23FEFF24250003000000FF26FEFEFE0E00
      4D0061006E0061006700650072001E0055007000640061007400650073005200
      650067006900730074007200790012005400610062006C0065004C0069007300
      74000A005400610062006C00650008004E0061006D006500140053006F007500
      7200630065004E0061006D0065000A0054006100620049004400240045006E00
      66006F0072006300650043006F006E00730074007200610069006E0074007300
      1E004D0069006E0069006D0075006D0043006100700061006300690074007900
      180043006800650063006B004E006F0074004E0075006C006C00140043006F00
      6C0075006D006E004C006900730074000C0043006F006C0075006D006E001000
      53006F007500720063006500490044000E006400740049006E00740033003200
      1000440061007400610054007900700065001400530065006100720063006800
      610062006C006500120041006C006C006F0077004E0075006C006C000E004100
      750074006F0049006E0063000800420061007300650014004F0041006C006C00
      6F0077004E0075006C006C0012004F0049006E00550070006400610074006500
      10004F0049006E005700680065007200650020004F0041006600740065007200
      49006E0073004300680061006E006700650064001A004F007200690067006900
      6E0043006F006C004E0061006D00650018006400740041006E00730069005300
      7400720069006E0067000800530069007A006500140053006F00750072006300
      6500530069007A0065000C00640074004D0065006D006F00100042006C006F00
      620044006100740061001C0043006F006E00730074007200610069006E007400
      4C00690073007400100056006900650077004C006900730074000E0052006F00
      77004C00690073007400060052006F0077000A0052006F007700490044001000
      4F0072006900670069006E0061006C001800520065006C006100740069006F00
      6E004C006900730074001C0055007000640061007400650073004A006F007500
      72006E0061006C001200530061007600650050006F0069006E0074000E004300
      680061006E00670065007300}
    object mtEndpointid: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'id'
    end
    object mtEndpointendpoint: TStringField
      DisplayWidth = 50
      FieldName = 'endpoint'
      Size = 200
    end
    object mtEndpointremark: TMemoField
      DisplayWidth = 50
      FieldName = 'remark'
      BlobType = ftMemo
    end
  end
  object mtParam: TFDMemTable
    Active = True
    AfterInsert = mtParamAfterInsert
    FieldDefs = <
      item
        Name = 'endpoint_id'
        DataType = ftInteger
      end
      item
        Name = 'sel'
        DataType = ftBoolean
      end
      item
        Name = 'name'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'desc'
        DataType = ftMemo
      end
      item
        Name = 'default'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'id'
        DataType = ftAutoInc
      end>
    IndexDefs = <>
    IndexFieldNames = 'endpoint_id'
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 696
    Top = 344
    Content = {
      414442530F0012366D020000FF00010001FF02FF0304000E0000006D00740050
      006100720061006D0005000A0000005400610062006C00650006000000000007
      0000080032000000090000FF0AFF0B04001600000065006E00640070006F0069
      006E0074005F006900640005001600000065006E00640070006F0069006E0074
      005F00690064000C00010000000E000D000F0001100001110001120001130001
      14000115001600000065006E00640070006F0069006E0074005F0069006400FE
      FF0B040006000000730065006C00050006000000730065006C000C0002000000
      0E0016000F000110000111000112000113000114000115000600000073006500
      6C00FEFF0B0400080000006E0061006D0065000500080000006E0061006D0065
      000C00030000000E0017001800640000000F0001100001110001120001130001
      1400011500080000006E0061006D006500190064000000FEFF0B040008000000
      640065007300630005000800000064006500730063000C00040000000E001A00
      0F00011000011B00011100011200011300011400011500080000006400650073
      006300FEFF0B04000E000000640065006600610075006C00740005000E000000
      640065006600610075006C0074000C00050000000E0017001800C80000000F00
      0110000111000112000113000114000115000E00000064006500660061007500
      6C0074001900C8000000FEFF0B04000400000069006400050004000000690064
      000C00060000000E000D000F00011000011C0001110001120001130001140001
      1D00011E000115000400000069006400FEFEFF1FFEFF20FEFF21FEFEFEFF22FE
      FF23240006000000FF25FEFEFE0E004D0061006E0061006700650072001E0055
      0070006400610074006500730052006500670069007300740072007900120054
      00610062006C0065004C006900730074000A005400610062006C00650008004E
      0061006D006500140053006F0075007200630065004E0061006D0065000A0054
      006100620049004400240045006E0066006F0072006300650043006F006E0073
      0074007200610069006E00740073001E004D0069006E0069006D0075006D0043
      006100700061006300690074007900180043006800650063006B004E006F0074
      004E0075006C006C00140043006F006C0075006D006E004C006900730074000C
      0043006F006C0075006D006E00100053006F007500720063006500490044000E
      006400740049006E007400330032001000440061007400610054007900700065
      001400530065006100720063006800610062006C006500120041006C006C006F
      0077004E0075006C006C000800420061007300650014004F0041006C006C006F
      0077004E0075006C006C0012004F0049006E0055007000640061007400650010
      004F0049006E00570068006500720065001A004F0072006900670069006E0043
      006F006C004E0061006D00650012006400740042006F006F006C00650061006E
      0018006400740041006E007300690053007400720069006E0067000800530069
      007A006500140053006F007500720063006500530069007A0065000C00640074
      004D0065006D006F00100042006C006F00620044006100740061000E00410075
      0074006F0049006E0063000C004F0049006E004B006500790020004F00410066
      0074006500720049006E0073004300680061006E006700650064001C0043006F
      006E00730074007200610069006E0074004C0069007300740010005600690065
      0077004C006900730074000E0052006F0077004C006900730074001800520065
      006C006100740069006F006E004C006900730074001C00550070006400610074
      00650073004A006F00750072006E0061006C001200530061007600650050006F
      0069006E0074000E004300680061006E00670065007300}
    object mtParamendpoint_id: TIntegerField
      FieldName = 'endpoint_id'
    end
    object mtParamsel: TBooleanField
      FieldName = 'sel'
    end
    object mtParamname: TStringField
      FieldName = 'name'
      Size = 100
    end
    object mtParamdesc: TMemoField
      FieldName = 'desc'
      BlobType = ftMemo
    end
    object mtParamdefault: TStringField
      FieldName = 'default'
      Size = 200
    end
    object mtParamid: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
  end
  object dsEnpoint: TDataSource
    DataSet = mtEndpoint
    Left = 616
    Top = 392
  end
  object dsParam: TDataSource
    DataSet = mtParam
    Left = 696
    Top = 392
  end
end
