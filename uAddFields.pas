unit uAddFields;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxCheckBox, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxGroupBox, cxCustomListBox, cxCheckListBox,
  System.Types, Spring, Spring.Collections, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, cxTextEdit, cxGridCustomTableView, cxGridTableView, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid;

type
  TfAddFields = class(TForm)
    cxGroupBox1: TcxGroupBox;
    btnUnselect: TcxButton;
    btnSelect: TcxButton;
    btnReverse: TcxButton;
    cxGroupBox2: TcxGroupBox;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    grdFieldsLevel1: TcxGridLevel;
    grdFields: TcxGrid;
    viewFields: TcxGridTableView;
    colCheck: TcxGridColumn;
    colName: TcxGridColumn;
    procedure btnSelectClick(Sender: TObject);
    procedure btnUnselectClick(Sender: TObject);
    procedure btnReverseClick(Sender: TObject);
  public
    class function Run(AFieldsChecks: IList<Tuple<string, Boolean>>): Boolean;
  end;

implementation

{$R *.dfm}

{ TfAddFields }

procedure TfAddFields.btnReverseClick(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to viewFields.DataController.RecordCount - 1 do
    viewFields.DataController.Values[I, colCheck.Index] := not viewFields.DataController.Values[I, colCheck.Index]
end;

procedure TfAddFields.btnSelectClick(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to viewFields.DataController.RecordCount - 1 do
    viewFields.DataController.Values[I, colCheck.Index] := True
end;

procedure TfAddFields.btnUnselectClick(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to viewFields.DataController.RecordCount - 1 do
    viewFields.DataController.Values[I, colCheck.Index] := False
end;

class function TfAddFields.Run(AFieldsChecks: IList<Tuple<string, Boolean>>): Boolean;
var
  Form: TfAddFields;
  I: Integer;
begin
  Form := TfAddFields.Create(nil);
  try
    Form.viewFields.DataController.RecordCount := AFieldsChecks.Count;
    for I := 0 to AFieldsChecks.Count - 1 do
    begin
      Form.viewFields.DataController.Values[I, Form.colCheck.Index] := AFieldsChecks[I].Value2;
      Form.viewFields.DataController.Values[I, Form.colName.Index] := AFieldsChecks[I].Value1;
    end;
    Result := Form.ShowModal = mrOk;
    if Result then
      for I := 0 to AFieldsChecks.Count - 1 do
        AFieldsChecks[I] := Tuple.Create<string, Boolean>(Form.viewFields.DataController.Values[I,
          Form.colName.Index], Form.viewFields.DataController.Values[I, Form.colCheck.Index]);
  finally
    Form.Free;
  end;
end;

end.
