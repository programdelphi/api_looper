program Api_looper_tests;

{$IFNDEF TESTINSIGHT}
{$APPTYPE CONSOLE}
{$ENDIF}
{$STRONGLINKTYPES ON}
uses
  FastMM4,
  {$IFDEF TESTINSIGHT}
  TestInsight.DUnitX,
  {$ENDIF }
  System.SysUtils,
  System.Classes,
  DUnitX.TestFramework,
  DUnitX.Loggers.Console,
  DUnitX.Loggers.Xml.NUnit,
  DunitX.Filters,
  DUnitX.FilterBuilder,
  uMain in 'uMain.pas' {fMain},
  uRestAPIQueueeThread in 'uRestAPIQueueeThread.pas',
  uExceptions in 'uExceptions.pas',
  uApiConnection in 'uApiConnection.pas',
  uDataManager in 'uDataManager.pas',
  uRecords in 'uRecords.pas',
  uScriptEdit in 'uScriptEdit.pas' {frmScriptEdit},
  uProgress in 'uProgress.pas' {frmProgress},
  uAddFields in 'uAddFields.pas' {fAddFields},
  uSentry in 'uSentry.pas',
  uTestSentry in 'uTestSentry.pas',
  TestDataManager in 'TestDataManager.pas',
  uTestRecords in 'uTestRecords.pas';

{$R *.res}

var
  runner : ITestRunner;
  results : IRunResults;
  logger : ITestLogger;
  nunitLogger : ITestLogger;
begin
{$IFDEF TESTINSIGHT}
  TestInsight.DUnitX.RunRegisteredTests;
  exit;
{$ENDIF}
  try
    TDUnitX.Options.HideBanner := True;
    TDUnitX.CheckCommandLine;
    runner := TDUnitX.CreateRunner;
    runner.UseRTTI := True;
    logger := TDUnitXConsoleLogger.Create(False);
    runner.AddLogger(logger);
    nunitLogger := TDUnitXXMLNUnitFileLogger.Create(TDUnitX.Options.XMLOutputFile);
    runner.AddLogger(nunitLogger);
    runner.FailsOnNoAsserts := False; //When true, Assertions must be made during tests;

    //Run tests
    results := runner.Execute;
    if not results.AllPassed then
      System.ExitCode := EXIT_ERRORS;

    {$IFNDEF CI}
    //We don't want this happening when running under CI.
    if TDUnitX.Options.ExitBehavior = TDUnitXExitBehavior.Pause then
    begin
      System.Write('Done.. press <Enter> key to quit.');
      System.Readln;
    end;
    {$ENDIF}
  except
    on E: Exception do
      System.Writeln(E.ClassName, ': ', E.Message);
  end;
end.
