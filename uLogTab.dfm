object fmLogTab: TfmLogTab
  Left = 0
  Top = 0
  Width = 885
  Height = 487
  TabOrder = 0
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 257
    Width = 885
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salTop
  end
  object sbPass: TcxScrollBox
    Left = 0
    Top = 33
    Width = 885
    Height = 224
    Align = alTop
    TabOrder = 1
  end
  object sbFail: TcxScrollBox
    Left = 0
    Top = 265
    Width = 885
    Height = 222
    Align = alClient
    TabOrder = 2
  end
  object gbNav: TcxGroupBox
    Left = 0
    Top = 0
    Align = alTop
    PanelStyle.Active = True
    TabOrder = 3
    Height = 33
    Width = 885
    object btnClearAll: TcxButton
      AlignWithMargins = True
      Left = 74
      Top = 5
      Width = 134
      Height = 23
      Margins.Left = 16
      Align = alLeft
      Caption = 'ClearAll'
      TabOrder = 0
      OnClick = btnClearAllClick
    end
    object lblHost: TcxLabel
      AlignWithMargins = True
      Left = 26
      Top = 5
      Margins.Left = 24
      Align = alLeft
      Caption = 'Host'
      Transparent = True
    end
  end
end
