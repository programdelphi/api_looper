object fParamGroups: TfParamGroups
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Param groups'
  ClientHeight = 697
  ClientWidth = 940
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object grdDef: TcxGrid
    Left = 0
    Top = 0
    Width = 940
    Height = 660
    Align = alClient
    TabOrder = 0
    object tvGroup: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Append.Visible = True
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = dsGroup
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.Visible = True
      OptionsData.Appending = True
      OptionsView.CellAutoHeight = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object tvGroupid: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object tvGroupGroup: TcxGridDBColumn
        Caption = 'Group'
        DataBinding.FieldName = 'group'
        Width = 200
      end
      object tvGroupRemark: TcxGridDBColumn
        Caption = 'Remark'
        DataBinding.FieldName = 'remark'
      end
    end
    object tvParam: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Append.Visible = True
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = dsParam
      DataController.DetailKeyFieldNames = 'group_id'
      DataController.KeyFieldNames = 'id'
      DataController.MasterKeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Appending = True
      OptionsView.CellAutoHeight = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object tvParamid: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 50
      end
      object tvParamGroupID: TcxGridDBColumn
        DataBinding.FieldName = 'group_id'
        Visible = False
        Width = 50
      end
      object tvParamName: TcxGridDBColumn
        Caption = 'Name'
        DataBinding.FieldName = 'name'
        Width = 50
      end
      object tvParamValue: TcxGridDBColumn
        Caption = 'Value'
        DataBinding.FieldName = 'value'
        Width = 50
      end
    end
    object grdDefLevel1: TcxGridLevel
      GridView = tvGroup
      object grdDefLevel2: TcxGridLevel
        GridView = tvParam
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 660
    Width = 940
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      940
      37)
    object btnUse: TButton
      Left = 453
      Top = 6
      Width = 200
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Add params'
      Default = True
      TabOrder = 0
      OnClick = btnUseClick
    end
    object Button2: TButton
      Left = 859
      Top = 6
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
    object btnAddFields: TButton
      Left = 7
      Top = 6
      Width = 200
      Height = 25
      Caption = 'Add &fields'
      Default = True
      TabOrder = 2
      OnClick = btnAddFieldsClick
    end
    object btnRemove: TButton
      Left = 653
      Top = 6
      Width = 200
      Height = 25
      Hint = 'Remove params from this group'
      Anchors = [akTop, akRight]
      Caption = '&Remove params'
      Default = True
      TabOrder = 3
      OnClick = btnRemoveClick
    end
  end
  object mtGroup: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 616
    Top = 344
    object mtGroupID: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'id'
    end
    object mtGroupGroup: TStringField
      DisplayWidth = 50
      FieldName = 'group'
      Size = 200
    end
    object mtGroupRemark: TMemoField
      DisplayWidth = 50
      FieldName = 'remark'
      BlobType = ftMemo
    end
  end
  object mtParam: TFDMemTable
    AfterInsert = mtParamAfterInsert
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'group_id'
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 696
    Top = 344
    object mtParamid: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object mtParamegroup_id: TIntegerField
      FieldName = 'group_id'
    end
    object mtParamName: TStringField
      FieldName = 'name'
      Size = 100
    end
    object mtParamvalue: TStringField
      FieldName = 'value'
      Size = 250
    end
  end
  object dsGroup: TDataSource
    DataSet = mtGroup
    Left = 616
    Top = 392
  end
  object dsParam: TDataSource
    DataSet = mtParam
    Left = 696
    Top = 392
  end
end
