program Api_looper;

uses
  FastMM4,
  System.Classes,
  Vcl.Forms,
  FireDAC.Phys.CDataExcelDef,
  FireDAC.Phys.ADS,
  uMain in 'uMain.pas' {fMain},
  uRestAPIQueueeThread in 'uRestAPIQueueeThread.pas',
  uExceptions in 'uExceptions.pas',
  uApiConnection in 'uApiConnection.pas',
  uDataManager in 'uDataManager.pas',
  uRecords in 'uRecords.pas',
  uScriptEdit in 'uScriptEdit.pas' {frmScriptEdit},
  uProgress in 'uProgress.pas' {frmProgress},
  uAddFields in 'uAddFields.pas' {fAddFields},
  uSentry in 'uSentry.pas',
  uLooper in 'uLooper.pas',
  uResAndPar in 'uResAndPar.pas' {fResAndPar},
  uHosts in 'uHosts.pas' {frmHosts},
  uCS in 'uCS.pas' {fCS},
  uParamGroups in 'uParamGroups.pas' {fParamGroups},
  uMemoBox in 'uMemoBox.pas' {fmMemoBox: TFrame},
  uMemoDlg in 'uMemoDlg.pas' {frmMemoDlg},
  uLogTab in 'uLogTab.pas' {fmLogTab: TFrame},
  uTableHelper in 'uTableHelper.pas';

{$SetPEFlags $20}
{$R *.res}
begin
  TThread.NameThreadForDebugging('main');
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfMain, fMain);
  Application.Run;
end.
