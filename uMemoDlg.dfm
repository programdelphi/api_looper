object frmMemoDlg: TfrmMemoDlg
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Details'
  ClientHeight = 421
  ClientWidth = 862
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 384
    Width = 862
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      862
      37)
    object Button1: TButton
      Left = 782
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
  end
  object memoMsg: TcxMemo
    Left = 0
    Top = 0
    Align = alClient
    TabOrder = 1
    Height = 384
    Width = 862
  end
end
