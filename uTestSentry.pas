unit uTestSentry;

interface

uses
  DUnitX.TestFramework;

type

  [TestFixture]
  TTestSentry = class(TObject)
  private
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [TestCase]
    procedure TestEx1;
  end;

implementation

uses
  VCL.TMSLogging, TMSLoggingUtils, uSentry, Winapi.Windows, TMSLoggingCore;

{ TTestSentry }
procedure TTestSentry.Setup;
begin
end;

procedure TTestSentry.TearDown;
begin
end;

procedure TTestSentry.TestEx1;
begin
  TMSLogger.Info('test information message');
  Sleep(2000);
  Assert.AreEqual(200, TSentryOutputHandler.LastResponseCode);
  TMSLogger.Exception('test error message');
  Sleep(2000);
  Assert.AreEqual(200, TSentryOutputHandler.LastResponseCode);
end;

var
  Handler: TTMSLoggerBaseOutputHandler;

initialization

TDUnitX.RegisterTestFixture(TTestSentry);
// TMSLogger.LogCurrentDateTime('yyyy-mm-dd hh:nn:ss.zzz');
TMSLogger.OutputFormats.ValueFormat := '{%s}';
// TMSLogger.OutputFormats.LogLevelFormat := '{%s}';
// TMSLogger.OutputFormats.TimeStampFormat := '{%s}';
TMSLogger.Active := True;
TMSLogger.Outputs := [loTimeStamp, loLogLevel, loValue];

Handler := TMSLogger.RegisterOutputHandlerClass(TSentryOutputHandler, []);
TSentryOutputHandler(Handler).DSN := 'https://b0d409382dfe47b4bae215919b950bf2@sentry.io/1872178';
TSentryOutputHandler(Handler).Tags := 'organization,test,instance,test-instance';

finalization

TMSLogger.OutputHandlers.Clear;

end.
