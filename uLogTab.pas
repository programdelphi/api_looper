unit uLogTab;
interface
uses
  uMemoBox,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxScrollBox, cxSplitter, System.RegularExpressions, cxContainer, cxEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  cxGroupBox, dxSkinsCore, dxSkinBasic, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkroom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinOffice2019Black,
  dxSkinOffice2019Colorful, dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinTheBezier, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, cxLabel;
type
  TfmLogTab = class(TFrame)
    cxSplitter1: TcxSplitter;
    sbFail: TcxScrollBox;
    sbPass: TcxScrollBox;
    gbNav: TcxGroupBox;
    btnClearAll: TcxButton;
    lblHost: TcxLabel;
    procedure btnClearAllClick(Sender: TObject);
  private
    FHost: string;
    FRClean: TRegEx;
    procedure AddUnique(AParent: TcxScrollBox; const AMessage: string);
    function PrepareMessage(const AMessage: string): string;
    procedure SetHost(const Value: string);
  protected
    procedure WMCloseMemoBox(var Msg: TMessage); message WM_CLOSE_MEMO_BOX;
  public
    property Host: string read FHost write SetHost;
    constructor Create(AOwner: TComponent); override;
    procedure AddFail(const AMessage: string);
    procedure AddPass(const AMessage: string);
  end;

implementation
{$R *.dfm}
{ TfmLogTab }
constructor TfmLogTab.Create(AOwner: TComponent);
begin
  inherited;
  //FRClean := TRegEx.Create('((?:^| |"|'')\s*\d+\s*(?: |"|''))');
   //FRClean := TRegEx.Create('((?:^| |"|'')\s*\w*\d\w*\s*(?: |"|''))');   //From Chat GPT To clean and words miked with digits
   FRClean := TRegEx.Create('((?:^|\s|["''])\s*(\b[A-Z]+\b|\w+\d\w*)\s*(?=\s|["'']|$))');   //From Chat GPT To clean uppercase words and words miked with digits

end;
procedure TfmLogTab.AddFail(const AMessage: string);
begin
  AddUnique(sbFail, AMessage);
end;
procedure TfmLogTab.AddPass(const AMessage: string);
begin
  AddUnique(sbPass, AMessage);
end;
procedure TfmLogTab.AddUnique(AParent: TcxScrollBox; const AMessage: string);
var
  um: string;
  i: Integer;
  memo: TfmMemoBox;
  splt: TcxSplitter;
begin
  um := PrepareMessage(AMessage);
  i := 0;
  while i < AParent.ControlCount do
    if AParent.Controls[i].InheritsFrom(TfmMemoBox) and SameText(um, TfmMemoBox(AParent.Controls[i]).Ident) then
      Break
    else
      Inc(i);
  if i < AParent.ControlCount then
  begin
    TfmMemoBox(AParent.Controls[i]).AddMessage(AMessage);
    Exit;
  end;
  memo := TfmMemoBox.Create(Self, um);
  memo.Name := '';
  memo.Align := alTop;
  memo.Top := AParent.BoundsRect.Bottom + 1;
  memo.Parent := AParent;
  splt := TcxSplitter.Create(memo);
  splt.Top := memo.Top + 1;
  splt.Align := alTop;
  splt.Control := memo;
  splt.Top := AParent.BoundsRect.Bottom + 1;
  splt.Parent := AParent;
  splt.HotZoneStyleClass := TcxSimpleStyle;
  memo.AddMessage(AMessage);
end;
procedure TfmLogTab.btnClearAllClick(Sender: TObject);
begin
  while sbFail.ControlCount > 0 do
    sbFail.Controls[0].Free;
  while sbPass.ControlCount > 0 do
    sbPass.Controls[0].Free;
end;
function TfmLogTab.PrepareMessage(const AMessage: string): string;
begin
  Result := FRClean.Replace(AMessage, ' ');
  Result := StringReplace(Result, sLineBreak, ' ', [rfReplaceAll]);
  Result := StringReplace(Result, #9, ' ', [rfReplaceAll]);
  while Pos('  ', Result) > 0 do
    Delete(Result, Pos('  ', Result), 1);
end;
procedure TfmLogTab.SetHost(const Value: string);
begin
  FHost := Value;
  lblHost.Caption := FHost;
end;

procedure TfmLogTab.WMCloseMemoBox(var Msg: TMessage);
var
  m : TfmMemoBox;
  i: Integer;
begin
  m := TfmMemoBox(Msg.WParam);
  i := 0;
  while i < sbFail.ControlCount do
    if sbFail.Controls[i] = m then
    begin
      sbFail.Controls[i].Free;
      Exit;
    end
    else
      Inc(i);
  i := 0;
  while i < sbPass.ControlCount do
    if sbPass.Controls[i] = m then
    begin
      sbPass.Controls[i].Free;
      Exit;
    end
    else
      Inc(i);
end;
end.
