unit uCS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations, Data.DB,
  cxDBData, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.StorageBin;

type
  TfCS = class(TForm)
    Panel1: TPanel;
    btnCancel: TButton;
    grdCS: TcxGrid;
    dbtvCS: TcxGridDBTableView;
    grdCSLevel1: TcxGridLevel;
    mtCS: TFDMemTable;
    dsCS: TDataSource;
    mtCSname: TStringField;
    mtCSsql: TWideMemoField;
    dbtvCSname: TcxGridDBColumn;
    dbtvCSsql: TcxGridDBColumn;
    btnOK: TButton;
    procedure dbtvCSCellDblClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo; AButton:
        TMouseButton; AShift: TShiftState; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  public
    class function Select(out name, sql: string): Boolean;
  end;

implementation

{$R *.dfm}

uses
  System.IOUtils, System.JSON, uRecords;

const
  CS_FILE_NAME = 'cs.json';

procedure TfCS.dbtvCSCellDblClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo; AButton:
    TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
    ModalResult := mrOk;

end;

//procedure TfCS.dbtvCSDblClick(Sender: TObject);
//var
//  AHitTest: TcxCustomGridHitTest;
//  APoint: TPoint;
//  AView: TcxGridDBTableView;
//begin
//  AView := TcxGridSite(Sender).GridView as TcxGridDBTableView;
//  APoint := TcxGridSite(Sender).ScreenToClient(Mouse.CursorPos);
//  AHitTest := AView.ViewInfo.GetHitTest(APoint);
//
//  if AHitTest is TcxGridRecordCellHitTest then
//end;

  { TfCS }

procedure TfCS.FormCreate(Sender: TObject);
var
  js: TJSONObject;
begin
  if TFile.Exists(CS_FILE_NAME) then
    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(CS_FILE_NAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtCS);
      finally
        js.Free;
      end;
    except
    end;
  mtCS.Active := True;
end;

procedure TfCS.FormDestroy(Sender: TObject);
var
  js: TJSONObject;
begin
  js := MT2JS(mtCS);
  try
    TFile.WriteAllText(CS_FILE_NAME, js.ToString);
  finally
    js.Free;
  end;
end;

class function TfCS.Select(out name, sql: string): Boolean;
var
  form: TfCS;
begin
  form := TfCS.Create(nil);
  try
    Result := form.ShowModal() = mrOk;
    if Result then
    begin
      name := form.mtCSname.AsString;
      sql := form.mtCSsql.AsString;
    end;
  finally
    form.Free;
  end;
end;

end.
