unit TestDataManager;

interface

uses
  DUnitX.TestFramework, uDataManager;

type

  [TestFixture]
  TTestDataManager = class(TObject)
  private
    dm: TDataManager;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure TestA();
  end;

implementation

uses
  uRecords, IdURI, System.SysUtils;

{ TTestDataManagery }

procedure TTestDataManager.TestA;
var
  rc: TRestCall;
  r: Boolean;
begin
  rc.BaseURL := 'http://127.0.0.1:8080/api/v1/TServerMethods1/';
  rc.Resource := 'UpdateProduct';

  rc.Params := 'Code= 64420941490&ccustomn2=' + EncodeUrl('$&+,:;=?@');
  // rc.Params := 'Code=64420941490&tax=False';
  Status(rc.Params);

  r := dm.RunApi(rc);
  Assert.IsTrue(r);
end;

procedure TTestDataManager.Setup;
begin
  dm := TDataManager.Create;
end;

procedure TTestDataManager.TearDown;
begin
  dm.Free;
end;

initialization

TDUnitX.RegisterTestFixture(TTestDataManager);

end.
