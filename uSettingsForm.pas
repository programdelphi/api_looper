unit uSettingsForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.ValEdit, Vcl.StdCtrls,
  Vcl.CheckLst, Vcl.ExtCtrls, Vcl.ComCtrls, Data.DB, VCL.DBGrids, FireDac.Comp.Client,
  TMS.MQTT.Client, uTableHelper, uMQTTClasses;

type
  TfrmSettings = class(TForm)
    edtHost: TLabeledEdit;
    edtPort: TLabeledEdit;
    edtNewFieldName: TLabeledEdit;
    cbNewFieldType: TComboBox;
    btnAddNewField: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    Label1: TLabel;
    lwFields: TListView;
    edtSubscribe: TLabeledEdit;
    edtPublish: TLabeledEdit;
    edtPrimaryKeyName: TLabeledEdit;
    lboxSubscribed: TListBox;
    btnUnsubscribe: TButton;
    btnAddSubscribe: TButton;
    procedure btnAddNewFieldClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnAddSubscribeClick(Sender: TObject);
    procedure btnUnsubscribeClick(Sender: TObject);
    procedure lboxSubscribedClick(Sender: TObject);
  private
    FDBGrid: TDBGrid;
    FTable: TFDMemTable;
    FMQTT: TTMSMQTTClient;

    procedure ApplySettings;
    procedure UpdateGridColumn;

    procedure SettingsToUI;
  public
    procedure AfterMemTableAddField(Sender: TObject; Field: TField);

    function CustomShowModal(AGrid: TDBGrid; ATable: TFDMemTable; AMQTT: TTMSMQTTClient): Integer;

    procedure CheckField(const AFieldName: String; Payload: Variant);
  end;

var
  frmSettings: TfrmSettings;

implementation

uses
  TypInfo;

const
  cPassword = '999';

{$R *.dfm}

procedure TfrmSettings.AfterMemTableAddField(Sender: TObject; Field: TField);
begin
  UpdateGridColumn;
end;

procedure TfrmSettings.ApplySettings;
var
  i: integer;
  FieldType: TFieldType;
  Length: Integer;
begin
  Settings.TableSettingsList.Clear;
  FDBGrid.Columns.BeginUpdate;
  try
    for i:=0 to lwFields.Items.Count - 1 do begin
      Settings.TableSettingsList.Add(TTableSettings.Create(lwFields.Items[i].Checked, lwFields.Items[i].SubItems[0], lwFields.Items[i].SubItems[1]));
      if FTable.FieldDefs.IndexOf(lwFields.Items[i].SubItems[0]) < 0 then begin
        FieldType := TFieldType(TypInfo.GetEnumValue(System.TypeInfo(TFieldType), lwFields.Items[i].SubItems[1]));
        Length := 0;
        if FieldType = TFieldType.ftString then
          Length := 255;
        FTable.AddNewField(lwFields.Items[i].SubItems[0], FieldType, Length);
      end;
    end;
  finally
    FDBGrid.Columns.EndUpdate;
  end;

  if Settings.PrimaryKey <> edtPrimaryKeyName.Text then begin
    if FTable.IsEmpty then begin
      FTable.DeleteField(Settings.PrimaryKey);
      FTable.AddNewField(edtPrimaryKeyName.Text, TFieldType.ftString, 50);
      Settings.PrimaryKey := edtPrimaryKeyName.Text;
    end else begin
      ShowMessage('Can''t change PrimaryKey in non empty table');
    end;
  end;

  UpdateGridColumn;

  if (edtHost.Text <> Settings.Host) or
     (edtPort.Text <> Settings.Port) then begin
     FMQTT.Disconnect;
     Settings.Host := edtHost.Text;
     Settings.Port := edtPort.Text;
     FMQTT.BrokerHostName := Settings.Host;
     FMQTT.BrokerPort := StrToIntDef(Settings.Port, defPortInt);
     FMQTT.Connect;
  end;

  //need subscribe to topic that not in saved FSubscribed
  for i:=0 to lboxSubscribed.Items.Count - 1 do
    if Settings.SubscribedList.IndexOf(lboxSubscribed.Items[i]) < 0 then
      FMQTT.Subscribe(lboxSubscribed.Items[i]);

  //need unsubscribed from topic that not in listbox and in saved FSubscribed
  for i:=0 to Settings.SubscribedList.Count - 1 do
    if lboxSubscribed.Items.IndexOf(Settings.SubscribedList[i]) < 0 then
      FMQTT.Unsubscribe(Settings.SubscribedList[i]);

  Settings.SubscribedList.Clear;
  Settings.SubscribedList.Assign(lboxSubscribed.Items);

  if edtPublish.Text <> Settings.PublishURL then begin
    Settings.PublishURL := edtPublish.Text;
    if not Settings.PublishURL.EndsWith('/') then
      Settings.PublishURL := Settings.PublishURL + '/';
  end;

  Settings.Save;
end;

procedure TfrmSettings.btnAddNewFieldClick(Sender: TObject);
var
  Item: TListItem;
begin
  Item := lwFields.Items.Add;
  Item.Checked := True;
  Item.SubItems.Add(edtNewFieldName.Text);
  Item.SubItems.Add(cbNewFieldType.Items[cbNewFieldType.ItemIndex]);
  edtNewFieldName.Text := '';
end;

procedure TfrmSettings.btnAddSubscribeClick(Sender: TObject);
begin
  if lboxSubscribed.Items.IndexOf(edtSubscribe.Text) < 0 then
    lboxSubscribed.Items.Add(edtSubscribe.Text);

  edtSubscribe.Text := '';
end;

procedure TfrmSettings.btnCancelClick(Sender: TObject);
begin
  SettingsToUI;
  Self.Close;
end;

procedure TfrmSettings.btnOkClick(Sender: TObject);
begin
  ApplySettings;
  Self.Close;
end;

procedure TfrmSettings.btnUnsubscribeClick(Sender: TObject);
begin
  if lboxSubscribed.ItemIndex >= 0 then
    lboxSubscribed.Items.Delete(lboxSubscribed.ItemIndex);
end;

procedure TfrmSettings.CheckField(const AFieldName: String; Payload: Variant);
var
  i: integer;
begin
  for i:=0 to Settings.TableSettingsList.Count - 1 do
    if Settings.TableSettingsList.Items[i].FieldName = AFieldName then
      Exit;

  Settings.TableSettingsList.Add(TTableSettings.Create(True, AFieldName, TypInfo.GetEnumName(System.TypeInfo(TFieldType), Ord(GetFieldTypeByVariantType(TryAnotherType(Payload))))));
  Settings.Save;
end;

function TfrmSettings.CustomShowModal(AGrid: TDBGrid; ATable: TFDMemTable; AMQTT: TTMSMQTTClient): Integer;
var
  Pass: String;
  Bool: Boolean;
begin
  FDBGrid := AGrid;
  FTable := ATable;
  FTable.AfterFieldAdded := Self.AfterMemTableAddField;
  FMQTT := AMQTT;
  Bool := False;
  Result := 0;
  while not Bool do begin
    if not InputQuery('Enter password', 'Password', Pass) then
      Exit;

    Bool := Pass.Equals(cPassword);
    if Bool then
      Result := Self.ShowModal
    else
      ShowMessage('Incorrect password');
  end;
end;

procedure TfrmSettings.FormShow(Sender: TObject);
begin
  SettingsToUI;
end;

procedure TfrmSettings.lboxSubscribedClick(Sender: TObject);
begin
  if lboxSubscribed.ItemIndex >= 0 then
    edtSubscribe.Text := lboxSubscribed.Items[lboxSubscribed.ItemIndex];
end;

procedure TfrmSettings.SettingsToUI;
var
  i: integer;
  Item: TListItem;
begin
  edtHost.Text := Settings.Host;
  edtPort.Text := Settings.Port;
  edtPublish.Text := Settings.PublishURL;
  edtPrimaryKeyName.Text := Settings.PrimaryKey;
  lboxSubscribed.Items.Assign(Settings.SubscribedList);
  lwFields.Items.BeginUpdate;
  try
    lwFields.Items.Clear;
    for i := 0 to Settings.TableSettingsList.Count - 1 do begin
      Item := lwFields.Items.Add;
      Item.SubItems.Add(Settings.TableSettingsList[i].FieldName);
      Item.SubItems.Add(Settings.TableSettingsList[i].FieldType);
      Item.Checked := Settings.TableSettingsList[i].IsActive;
    end;
  finally
    lwFields.Items.EndUpdate;
  end;
end;

procedure TfrmSettings.UpdateGridColumn;
var
  i: integer;
  Column: TColumn;
begin
  FDBGrid.Columns.BeginUpdate;
  try
    for i:=0 to lwFields.Items.Count - 1 do begin
      Column := GetColumnByFieldName(FDBGrid, lwFields.Items[i].SubItems[0]);
      if Assigned(Column) then
        Column.Visible := lwFields.Items[i].Checked;
    end;
    FTable.AfterPost(FTable);
  finally
    FDBGrid.Columns.EndUpdate;
  end;
end;

end.
