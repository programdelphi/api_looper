unit uScriptEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ScrMemo, ScrMps, dxBarBuiltInMenu, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxPC;

type
  TfrmScriptEdit = class(TForm)
    ScrPascalMemoStyler1: TScrPascalMemoStyler;
    pcScriptEdit: TcxPageControl;
    tsSrc: TcxTabSheet;
    tsExamples: TcxTabSheet;
    memEx: TScrMemo;
    Script: TScrMemo;
  private
    { Private declarations }
  public
    class procedure Run(ASourceCode: TStrings);
  end;

implementation

{$R *.dfm}

{ TfrmScriptEdit }

class procedure TfrmScriptEdit.Run(ASourceCode: TStrings);
var
  frm:TfrmScriptEdit;
begin
  frm := Create(nil);
  try
    frm.Script.Lines.Text := ASourceCode.Text;
    frm.pcScriptEdit.ActivePageIndex := 0;
    frm.ShowModal;
    ASourceCode.Text := frm.Script.Lines.Text;
  finally
    frm.Free;
  end;
end;

end.
