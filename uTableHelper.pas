unit uTableHelper;

interface

uses
  System.SysUtils, System.Variants, System.Classes,
  FireDac.Comp.Client,
  Data.DB,
  VCL.DBGrids;

type
  TFieldAddEvent = procedure(Sender: TObject; Field: TField) of object;

  TTableHelper = class helper for TFDMemTable
  private class var
    FAfterFieldAdded: TFieldAddEvent;
  private
    function ReadAfterFieldAdded: TFieldAddEvent;
    procedure SetAfterFieldAdded(AValue: TFieldAddEvent);

    procedure CheckedGetText(Sender: TField; var Text: string; DisplayText: Boolean);
  public
    procedure AddNewField(const AFieldName: String; AFieldType: TFieldType; ALength: Integer = 0);
    procedure SetField(const AIDField: String; AIDValue: Variant; const AFieldName: String; AFieldValue: Variant);
    procedure DeleteField(const AName: String);

    property AfterFieldAdded: TFieldAddEvent read ReadAfterFieldAdded write SetAfterFieldAdded;
  end;

function GetFieldTypeByVariantType(Input: Variant): TFieldType;
function GetColumnByFieldName(ADBGrid: TDBGrid; const AFieldName: String): TColumn;

implementation

function GetColumnByFieldName(ADBGrid: TDBGrid; const AFieldName: String): TColumn;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to ADBGrid.Columns.Count - 1 do
    if ADBGrid.Columns[i].FieldName = AFieldName then
      Exit(ADBGrid.Columns[i]);
end;

function GetFieldTypeByVariantType(Input: Variant): TFieldType;
var
  basicType: TVarType;
begin
  basicType := VarType(Input) and VarTypeMask;
  case basicType of
    varNull:
      Result := TFieldType.ftString;
    varSmallInt:
      Result := TFieldType.ftInteger;
    varInteger:
      Result := TFieldType.ftInteger;
    varSingle:
      Result := TFieldType.ftSingle;
    varDouble:
      Result := TFieldType.ftFloat;
    varCurrency:
      Result := TFieldType.ftCurrency;
    varDate:
      Result := TFieldType.ftDateTime;
    varByte:
      Result := TFieldType.ftInteger;
    varBoolean:
      Result := TFieldType.ftBoolean;
    varVariant:
      Result := TFieldType.ftVariant;
    varInt64:
      Result := TFieldType.ftLargeint;
    varString:
      Result := TFieldType.ftString;
  else
    Result := TFieldType.ftString;
  end;
end;

{ TTableHelper }

procedure TTableHelper.AddNewField(const AFieldName: String; AFieldType: TFieldType; ALength: Integer = 0);
var
  TmpTable: TFDMemTable;
begin
  if AFieldName.IsEmpty then
    Exit;

  TmpTable := TFDMemTable.Create(nil);
  try
    if Self.FieldDefs.Count > 0 then begin
      TmpTable.FieldDefs := Self.FieldDefs;
      TmpTable.CreateDataSet;
      TmpTable.Open;
      TmpTable.CopyDataSet(Self);
    end;

    Self.Close;
    Self.FieldDefs.Add(AFieldName, AFieldType, ALength);
    Self.CreateDataSet;
    Self.Open;
    if TmpTable.Active then
      Self.CopyDataSet(TmpTable);

    if Assigned(FAfterFieldAdded) then
      FAfterFieldAdded(Self, Self.FieldByName(AFieldName));
  finally
    TmpTable.Free;
  end;
end;

procedure TTableHelper.CheckedGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.Name = 'Checked' then
    Text := '';
end;

procedure TTableHelper.DeleteField(const AName: String);
begin
  if AName.IsEmpty then
    Exit;

  if Self.FieldDefs.IndexOf(AName) < 0 then
    Exit;

  Self.Close;
  Self.FieldDefs.Delete(Self.FieldDefs.IndexOf(AName));
  Self.CreateDataSet;
  Self.Fields[0].OnGetText := CheckedGetText;
  Self.Open;
end;

function TTableHelper.ReadAfterFieldAdded: TFieldAddEvent;
begin
  Result := FAfterFieldAdded;
end;

procedure TTableHelper.SetAfterFieldAdded(AValue: TFieldAddEvent);
begin
  FAfterFieldAdded := AValue;
end;

procedure TTableHelper.SetField(const AIDField: String; AIDValue: Variant;
  const AFieldName: String; AFieldValue: Variant);
var
  FieldType: TFieldType;
  Length: Integer;
begin
  if AIDField.IsEmpty or AFieldName.IsEmpty or VarToStr(AIDValue).IsEmpty then
    Exit;

  if Self.FieldList.IndexOf(AFieldName) < 0 then begin
    FieldType := GetFieldTypeByVariantType(AFieldValue);
    Length := 0;
    if FieldType = TFieldType.ftString then
      Length := 255;

    Self.AddNewField(AFieldName, FieldType, Length);
  end;

  if Self.Locate(AIDField, AIDValue, []) then begin
    Self.Edit;
    Self.FieldValues[AFieldName] := AFieldValue;
    Self.Post;
  end else begin
    Self.Append;
    Self.FieldValues['Checked'] := False;
    Self.FieldValues[AIDField] := AIDValue;
    Self.FieldValues[AFieldName] := AFieldValue;
    Self.Post;
  end;
end;

end.
