unit uMain;

interface

uses
  Spring, Spring.Collections, VCL.TMSLogging, TMSLoggingUtils, Winapi.Windows, Winapi.Messages, System.SysUtils,
  System.Variants, VCL.Graphics, VCL.Controls, VCL.Forms, VCL.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.ADS, FireDAC.Phys.ADSDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, Data.DB, TMSLoggingTextOutputHandler, VCL.StdCtrls, VCL.Grids, VCL.DBGrids, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.Generics.Collections, VCL.ValEdit, VCL.ExtCtrls, REST.Types, Data.Bind.Components,
  Data.Bind.ObjectScope, REST.Client, System.IniFiles, VCL.ComCtrls, System.Classes, TMSLoggingCSVOutputHandler,
  VCL.Samples.Spin, StrUtils, FireDAC.Stan.StorageBin, FireDAC.Stan.StorageJSON, uRecords, uDataManager, uRestAPIQueueeThread,
  TextEditor, SqlTextEditor, acAST, acAdvantageSynProvider, acQBBase, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, dxBarBuiltInMenu, cxGridCustomPopupMenu, cxGridPopupMenu, cxContainer, cxLabel, atScript,
  atPascal, FormScript, ScrmPS, VCL.ScripterInit, cxTextEdit, System.Types, dxStatusBar, uSentry, uLooper,
  dxLayoutcxEditAdapters, dxLayoutContainer, cxMemo, dxLayoutControl, cxGroupBox, cxPC, cxMaskEdit, cxDropDownEdit,
  cxImageComboBox, VCL.Menus, cxButtons, System.Actions, VCL.ActnList, cxButtonEdit, uLogTab, cxGridInplaceEditForm,
  cxGridViewLayoutContainer, cxGridRowLayout, dxToggleSwitch, cxCheckBox, FireDAC.Phys.CDataExcel, FireDAC.Phys.CDataExcelDef,
  cxSplitter, cxImage, dxScreenTip, dxCustomHint, cxHint, cxRadioGroup, cxSpinEdit, cxDBEdit, cxDBLabel;

type
  EClientError = class(Exception);

  TfMain = class(TForm)
    connData: TFDConnection;
    qData: TFDQuery;
    dsData: TDataSource;
    tmrEndApp: TTimer;
    acAdvantageSyntaxProvider1: TacAdvantageSyntaxProvider;
    mtData: TFDMemTable;
    tmrAPIStatusUpd: TTimer;
    gpmData: TcxGridPopupMenu;
    Scripter: TatPascalScripter;
    StatusBar: TdxStatusBar;
    pcApp: TcxPageControl;
    tsData: TcxTabSheet;
    tsConfig: TcxTabSheet;
    pnlSQL: TcxGroupBox;
    btnTestSQL: TButton;
    edtSql: TacSqlTextEditor;
    cxGroupBox1: TcxGroupBox;
    pnlBaseURL: TcxGroupBox;
    btnTestURL: TButton;
    acQueryBuilder1: TacQueryBuilder;
    cxGroupBox2: TcxGroupBox;
    lblSQL: TcxLabel;
    cxGroupBox3: TcxGroupBox;
    edtParameters: TcxMemo;
    Settings: TcxGroupBox;
    lblRetries: TcxLabel;
    cxLabel1: TcxLabel;
    btnEditScript: TButton;
    edSentryDSN: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    edSentryTags: TcxTextEdit;
    cxLabel6: TcxLabel;
    cxLabel2: TcxLabel;
    cbExitOnDone: TcxComboBox;
    cbRunMode: TcxComboBox;
    cxLabel7: TcxLabel;
    btnSelectResAndPar: TButton;
    gbProfile: TcxGroupBox;
    cxLabel5: TcxLabel;
    btnLoadIni: TButton;
    btnSaveIni: TButton;
    btnAddAll: TButton;
    cxLabel8: TcxLabel;
    btnAPIHostEdit: TButton;
    lblBaseUrl: TcxLabel;
    btnCanned: TcxButton;
    btnAddGroups: TButton;
    dlgSave: TSaveDialog;
    AL: TActionList;
    aSaveIni: TAction;
    aLoadIni: TAction;
    dlgOpen: TOpenDialog;
    edtIniFileName: TcxButtonEdit;
    Button1: TButton;
    aSaveIniAs: TAction;
    edPrimaryKey: TcxTextEdit;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    grdHosts: TcxGrid;
    dbtvHosts: TcxGridDBTableView;
    dbtvHostsname: TcxGridDBColumn;
    dbtvHostshost: TcxGridDBColumn;
    dbtvHostsenterprise: TcxGridDBColumn;
    grdHostsLevel1: TcxGridLevel;
    dsHosts: TDataSource;
    mtHosts: TFDMemTable;
    mtHostsname: TStringField;
    mtHostshost: TStringField;
    mtHostsenterprise: TStringField;
    cbEnterprise: TcxImageComboBox;
    cxLabel11: TcxLabel;
    dsHostQueues: TDataSource;
    mtHostQueues: TFDMemTable;
    mtHostQueuesListCount: TIntegerField;
    mtHostQueuesname: TStringField;
    mtHostQueuesLenterprise: TStringField;
    mtHostQueueshost: TStringField;
    mtHostQueuesPaused: TBooleanField;
    cxLabel12: TcxLabel;
    mtAPIStatus: TFDMemTable;
    dsAPIStatus: TDataSource;
    mtHostsFullName: TStringField;
    HintStyleControllerHostQuees: TcxHintStyleController;
    tmrHostQueuesHint: TTimer;
    mtHostQueuesRunAll: TBooleanField;
    mtHostQueuesRunSelected: TBooleanField;
    mtHostQueuesClearQueue: TBooleanField;
    mtHostQueuesCFullLocationName: TStringField;
    lblURLPath: TcxLabel;
    mtHostQueuesStartedAmount: TIntegerField;
    mtHostQueuesSuccessAmount: TIntegerField;
    mtHostQueuesErrorAmount: TIntegerField;
    mtHostQueuesLastMessage: TWideStringField;
    mtHostQueuesCDoneAmount: TIntegerField;
    cxSplitterpnlMemo: TcxSplitter;
    cxSplitterpnlBtm: TcxSplitter;
    pnlData: TPanel;
    pnlAPIStatus: TPanel;
    grdAPIStatus: TcxGrid;
    dbtvAPIStatus: TcxGridDBTableView;
    glAPIStatus: TcxGridLevel;
    pnlAPIStatusBottom: TPanel;
    cbFilterAPIStatusByDataRec: TcxCheckBox;
    grdData: TcxGrid;
    dbtvData: TcxGridDBTableView;
    glData: TcxGridLevel;
    pnlMemo: TPanel;
    mmoTransl: TMemo;
    mmoSuccess: TMemo;
    mmoError: TMemo;
    cxSplitter1: TcxSplitter;
    cxSplitter2: TcxSplitter;
    pnlButtons: TPanel;
    btnOpen: TButton;
    btnAPITrans: TButton;
    btnAPIAll: TButton;
    btnAPIselect: TButton;
    seRecno: TSpinEdit;
    btnRunTests: TButton;
    btnClearLog: TButton;
    cxSplitterpnlAPIStatus: TcxSplitter;
    cbFilterAPIStatusByLocation: TcxCheckBox;
    rbRunForAllLocations: TcxRadioButton;
    rbRunForFocusedLocation: TcxRadioButton;
    btnAPISelectedStatRecs: TButton;
    chkLogging: TcxCheckBox;
    chkSeparateLoggingFiles: TcxCheckBox;
    chkRecno: TcxCheckBox;
    edtUrlPath: TcxTextEdit;
    edtResource: TcxTextEdit;
    edSrcURI: TcxTextEdit;
    seRetries: TcxSpinEdit;
    seTasks: TcxSpinEdit;
    seSleep: TcxSpinEdit;
    pgcQueues: TcxPageControl;
    tsHostQueues: TcxTabSheet;
    btnClearAllQue: TButton;
    btnPauseAll: TButton;
    btnResumeAll: TButton;
    grdHostQueues: TcxGrid;
    dbtvHostQueues: TcxGridDBTableView;
    dbtvHostQueuesname: TcxGridDBColumn;
    dbtvHostQueuesenterprise: TcxGridDBColumn;
    dbtvHostQueuesListCount: TcxGridDBColumn;
    dbtvHostQueuesRunAll: TcxGridDBColumn;
    dbtvHostQueuesRunSelected: TcxGridDBColumn;
    dbtvHostQueuesPaused: TcxGridDBColumn;
    dbtvHostQueuesClearQueue: TcxGridDBColumn;
    dbtvHostQueuesStartedCount: TcxGridDBColumn;
    dbtvHostQueuesCDoneCount: TcxGridDBColumn;
    dbtvHostQueuesSuccessCount: TcxGridDBColumn;
    dbtvHostQueuesErrorCount: TcxGridDBColumn;
    dbtvHostQueuesLastMessage: TcxGridDBColumn;
    glHostQueues: TcxGridLevel;
    tsHostQueue: TcxTabSheet;
    dcbHQPaused: TcxDBCheckBox;
    lblStartedCount: TcxLabel;
    lblLocation: TcxLabel;
    lblListCount: TcxLabel;
    lblSuccessCount: TcxLabel;
    lblErrorCount: TcxLabel;
    lblDoneCount: TcxLabel;
    Label7: TcxLabel;
    cxDBTextEdit2: TcxMemo;
    btnClearHostQueue: TcxButton;
    btnPauseHostQueue: TcxButton;
    btnResumeHostQueue: TcxButton;
    dlbHQListCount: TcxDBLabel;
    dlbHQStartedCount: TcxDBLabel;
    dlbHQSuccessCount: TcxDBLabel;
    dlbHQErrorCount: TcxDBLabel;
    dlbHQDoneCount: TcxDBLabel;
    dlbHQLocation: TcxDBLabel;
    procedure aLoadIniExecute(Sender: TObject);
    procedure aSaveIniAsExecute(Sender: TObject);
    procedure aSaveIniExecute(Sender: TObject);
    procedure btnAddAllClick(Sender: TObject);
    procedure btnAddGroupsClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnAPIAllClick(Sender: TObject);
    procedure btnAPIHostEditClick(Sender: TObject);
    procedure btnAPIselectClick(Sender: TObject);
    procedure btnAPITransClick(Sender: TObject);
    procedure btnClearAllQueClick(Sender: TObject);
    procedure btnDictCrerateClick(Sender: TObject);
    procedure btnEditScriptClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure btnRunTestsClick(Sender: TObject);
    procedure btnSelectResAndParClick(Sender: TObject);
    procedure btnTestSQLClick(Sender: TObject);
    procedure btnTestURLClick(Sender: TObject);
    procedure chkLoggingClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure seRetriesChange(Sender: TObject);
    procedure seTasksChange(Sender: TObject);
    procedure tmrAPIStatusUpdTimer(Sender: TObject);
    procedure tmrEndAppTimer(Sender: TObject);
    procedure cbUrlHostPropertiesEditValueChanged(Sender: TObject);
    procedure edtUrlPathChange(Sender: TObject);
    procedure btnCannedClick(Sender: TObject);
    procedure btnClearLogClick(Sender: TObject);
    procedure btnPauseAllClick(Sender: TObject);
    procedure edSentryDSNPropertiesChange(Sender: TObject);
    procedure edtSqlDocumentChanged(Sender: TObject);
    procedure cbUrlHostPropertiesChange(Sender: TObject);
    procedure edtIniFileNameChange(Sender: TObject);
    procedure edtResourceChange(Sender: TObject);
    procedure edtParametersPropertiesChange(Sender: TObject);
    procedure cbRunModePropertiesChange(Sender: TObject);
    procedure cbExitOnDonePropertiesChange(Sender: TObject);
    procedure edSrcURIChange(Sender: TObject);
    procedure edtIniFileNamePropertiesChange(Sender: TObject);
    procedure pcAppPageChanging(Sender: TObject; NewPage: TcxTabSheet; var AllowChange: Boolean);
    procedure cbEnterprisePropertiesEditValueChanged(Sender: TObject);
    procedure cbEnterprisePropertiesChange(Sender: TObject);
    procedure dbtvHostsSelectionChanged(Sender: TcxCustomGridTableView);
    procedure btnResumeAllClick(Sender: TObject);
    procedure mtHostsAfterScroll(DataSet: TDataSet);
    procedure chkSeparateLoggingFilesClick(Sender: TObject);
    procedure mtHostsCalcFields(DataSet: TDataSet);
    procedure cbFilterAPIStatusByDataRecClick(Sender: TObject);
    procedure dbtvHostQueuesCellClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
      AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
    procedure dbtvHostQueuesMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure tmrHostQueuesHintTimer(Sender: TObject);
    procedure dbtvHostQueuesMouseLeave(Sender: TObject);
    procedure mtHostQueuesCalcFields(DataSet: TDataSet);
    procedure edPrimaryKeyPropertiesChange(Sender: TObject);
    procedure edPrimaryKeyPropertiesValidate(Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure pcAppChange(Sender: TObject);
    procedure dbtvAPIStatusFocusedRecordChanged(Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
    procedure dbtvAPIStatusDataControllerFilterChanged(Sender: TObject);
    procedure cbFilterAPIStatusByLocationClick(Sender: TObject);
    procedure dbtvDataDataControllerFilterChanged(Sender: TObject);
    procedure dbtvDataFocusedRecordChanged(Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
    procedure btnAPISelectedStatRecsClick(Sender: TObject);
    procedure seSleepPropertiesChange(Sender: TObject);
    procedure btnClearHostQueueClick(Sender: TObject);
    procedure btnPauseHostQueueClick(Sender: TObject);
    procedure btnResumeHostQueueClick(Sender: TObject);
    procedure mtHostQueuesAfterPost(DataSet: TDataSet);
    procedure mtHostQueuesAfterDelete(DataSet: TDataSet);
    procedure mtHostQueuesAfterOpen(DataSet: TDataSet);

  private
    FGUI: Boolean;
    FRestAPIQueueeThreadList: TDictionary<string, TRestAPIQueueeThread>;
    //FRestAPIQueueeThread: TRestAPIQueueeThread;
    FResponseQueList: TDictionary<string, IDictionary<string, IDictionaryStringString>>;
    //FResponseQue: IDictionary<string, IDictionaryStringString>;
    FPrimaryKey: string;
    FLoadingData: Boolean;
    FDict: IDictionaryStringString;
    FAPIQueuing: Boolean;
    FSentryDSN: string;
    FSentryTags: string;
    FSentryHandler: TSentryOutputHandler;
    FErrorCount: Integer;
    FIniFileName: string;
    FConfigPassword: string;
    fmLogTabList: TDictionary<string, TfmLogTab>;
    //fmLogTab: TfmLogTab;
    FLastDataGridUpdate: TDateTime;
    FEnterprise: string;
    FHostURLs: Tarray<System.string>;
    FRunningQueues: Integer;
    FLoggingFileSWList: TDictionary<string, TStreamWriter>;
    FIniLoaded: Boolean;
    FAPIStatusDataMasterDS, FAPIStatusLocationMasterDS: TDataSource;
    FHintDisplayed: Boolean;
    FHostQueuesGridRecordIndex: integer;
    FHostQueuesItem: TcxCustomGridTableItem;
    FPrimayKeyChanged: Boolean;
    FCreatingResCallsForSelectedStatuses: Boolean;
    procedure TransAll(Host:string = '');
    procedure SaveIni(const AIniFileName: string);
    procedure LoadIni(const AIniFileName: string);
    procedure CloseApp;
    procedure CompleteRequestAPI(const Host: string; const ARestCall: TRestCall);
    procedure ErrorRequestAPI(const Host: string; const ARestCall: TRestCall; const AErrorMessage: string);
    procedure SendToRest(const AKey, ATrans: string);
    procedure ShowQue(Host: string);
    procedure ShowAllQue;
    procedure SendURLtest;
    procedure AddLog(const Host: string; ALevel: TTMSLoggerLogLevel; const AMessage: string);
    procedure OpenSQL(const AProcessMessages: Boolean);
    procedure LoadHosts;
    //procedure ReloadHosts;
    function GetBaseUrl: string;
    function TrySelectEnterprise(const n: string): Boolean;
    //function TrySelectHost(const n: string): Boolean;
    function TrySelectHosts(const n: array of string): Boolean;
    procedure CfgUpd();
    function GetCurrentParams: IDictionaryStringString;
    procedure AddNewFields(AFields: ISet<string>);
    procedure HandleResponseQue;
    function GetSelectedHostURLs: TArray<System.string>;
    procedure LoadHostQueues(HostList: Tarray<System.string>);
    function GetTotalQueListCount: integer;
    procedure AddHostLogginStreamWriter;
    procedure CheckAndUpdateHostLogigingSWList;
    function GetFullHostName(const Host: string): string;
    procedure PauseHostQueue;
    procedure ResumeHostQueue;
    procedure ClearHostQueue;
    procedure HostQueueHideHint;
    procedure AddRestCallToHostQueThread(DataKey: variant);
    procedure APISelected(Host:string = '');
    procedure SynchronizeAPIStatusWithData;
    procedure SynchronizeDataWithAPISatus;
    procedure AlterAPIStatusMasterDetailFilter;
    procedure FilterAPIStatusByDataRecord;
    procedure RefreshPanelQueueCount;
  protected
    property PrimaryKey: string read FPrimaryKey;
    property BaseUrl: string read GetBaseUrl;

    procedure Loaded; override;
    function GetHostBasUrl(const HostURL: string): string;

  public
    function GetNewScripter(const ASource: string): TatPascalScripter;
    function GetPrimaryKeyValue(AData: TDataSet; const APrimaryKey: string): string;
    procedure SetupConnection(const AConnectionString: string; const AConnection: TFDConnection);
    property Dict: IDictionaryStringString read FDict;

  end;

const
  HOSTS_FILE_NAME = 'api_looper.hosts';

var
  fMain: TfMain;

implementation

uses
  ap_Classes, ap_Consts, ap_DB, ap_DBConsts, ap_DBClient,
  ap_System, ap_StrUtils, ap_DateUtils, ap_SysUtils, ap_Types,
  ap_Windows,
  ap_Variants, ap_VarUtils,
  JvVersionInfo,
  JSON,
  System.Threading, uScriptEdit, uProgress, uAddFields, IdURI,
  System.IOUtils, uResAndPar, uHosts, System.Generics.Defaults, uCS, JclStrings, uParamGroups, uTableHelper,
  System.DateUtils, FireDAC.Stan.Consts, Spring.Reflection, System.Rtti;

const
  SParamRUNMIN = 'RUNMIN';
  SParamRUNSHOW = 'RUNSHOW';
  SParamRUNHIDE = 'RUNHIDE';
  SParamRUN = 'RUN';
  SParamDESIGN = 'DESIGN';

  SEPrimaryKeyNotFound = 'Primary key not found';
  SEInternalLocate = 'Internal locate error';
  SEFmtSQLTask = 'SQL task error : %s';

  SSendQueueIsNotEmptyCannotClose = 'Send queue is not empty, cannot close';
  SNoPrimaryKeyWarning =
    'Loaded data has not field marked as key (primary key), function like "API Selected" or "API status update" will not work';
  sURLPathEmpty = 'URL Path is empty';
  SResume = 'Resume';
  SPause = 'Pause';
  SMoshe = 'Moshe';
  SFmtIniNotFound = 'File %s not found';
  SLoadDataAlready = 'Load data already in progress, please wait to complete and try again';
  SLoadAPIAlready = 'API queuing already in progres, please wait to complete and try again';
  SPauseAllQueueAlready = 'All queues are already paused';
  SPauseQueueAlready = 'The queue is already paused';
  SResumeQueueAllNotPaused = 'All queues are not paused before';
  SResumeQueueNotPaused = 'The queue is not paused before';
  SNoHostQueueSelected = 'No location queue is selected';
  SHostNotSelected = 'The focused location is not selected';
  SNoLocationConfiguredToBeRun = 'There is no location which is configured to be run!';
  SLoadData = 'Load data in progress, please wait to complete and try again';
  SLoadAPI = 'API queuing in progres, please wait to complete and try again';
  SQueuedTaskExistTabCannotBeChanged = 'At least one queued task exists, active tab can not be changed!';
  SFmtVersion = 'Version : %s - %s';
  SFmtQue = 'Que: %d';
  SFmtRows = 'Rows: %d';
  SFmtPrimaryKey = 'PrimaryKey=%s';
  SFmtSuccess = 'Success %s';
  SFmtError = 'Error %s';

  SLogAppStart = 'start %s';
  SLogAppStop = 'stop';
  SLAPIAllStart = 'start "API All"';
  SLAPISelectStart = 'start "API Select"';
  SLAPIHostAllStart = 'start "API All" for location %s';
  SLAPIHostSelectStart = 'start "API Select" for location %s';
  SLAPIStatusSelectStart = 'start "API Selected Statuses"';


  SExportDataJson = 'ExportData.json';

  STestMethod = 'EchoString';
  STestResource = 'URL Reached';

{$R *.dfm}


procedure TfMain.AddLog(const Host: string; ALevel: TTMSLoggerLogLevel; const AMessage: string);
var
  strLogLevel, LMessage, FullHostName: string;
  Memo: TMemo;
  LoggingFileSW: TStreamWriter;
begin
  LMessage := StringReplace(AMessage, sLineBreak, ' ', [rfReplaceAll]);

  if (ALevel = TTMSLoggerLogLevel.Exception) or (ALevel = TTMSLoggerLogLevel.Error) then
    Memo := mmoError
  else
    Memo := mmoSuccess;

  FullHostName := GetFullHostName(Host);
  Memo.Lines.BeginUpdate;
  try
    if Memo.Lines.Count > 1000 then
      Memo.Lines.Clear;
    Memo.Lines.Add(FullHostName + ' - ' + LMessage);
  finally
    Memo.Lines.EndUpdate;
  end;

  SendMessage(Memo.Handle, WM_VSCROLL, SB_BOTTOM, 0);

  if not chkSeparateLoggingFiles.Checked then
    LMessage := FullHostName + ' - ' + LMessage;

  case ALevel of
    TTMSLoggerLogLevel.Trace:
    begin
      if not chkSeparateLoggingFiles.Checked then
        TMSLogger.Trace(LMessage);
      strLogLevel := 'Trace';
    end;
    TTMSLoggerLogLevel.Debug:
    begin
      if not chkSeparateLoggingFiles.Checked then
        TMSLogger.Debug(LMessage);
      strLogLevel := 'Debug';
    end;
    TTMSLoggerLogLevel.Info:
    begin
      if not chkSeparateLoggingFiles.Checked then
        TMSLogger.Info(LMessage);
      strLogLevel := 'Info';
    end;
    TTMSLoggerLogLevel.Warning:
    begin
      if not chkSeparateLoggingFiles.Checked then
        TMSLogger.Warning(LMessage);
      strLogLevel := 'Warning';
    end;
    TTMSLoggerLogLevel.Error:
    begin
      if not chkSeparateLoggingFiles.Checked then
        TMSLogger.Error(LMessage);
      strLogLevel := 'Error';
    end;
    TTMSLoggerLogLevel.Exception:
    begin
      if not chkSeparateLoggingFiles.Checked then
        TMSLogger.Exception(LMessage);
      strLogLevel := 'Exception';
    end;
    TTMSLoggerLogLevel.All:
    begin
      if not chkSeparateLoggingFiles.Checked then
        TMSLogger.Custom(LMessage);
      strLogLevel := 'All';
    end;
    TTMSLoggerLogLevel.Custom:
    begin
      if not chkSeparateLoggingFiles.Checked then
        TMSLogger.Custom(LMessage);
      strLogLevel := 'Custom';
    end;
  end;

  if chkSeparateLoggingFiles.Checked then
  begin
    LoggingFileSW := FLoggingFileSWList.Items[Host];
    LoggingFileSW.WriteLine(FormatDateTime('yyyy-mm-dd hh:nn:ss.zz', Now())+ #9 + strLogLevel + #9  +LMessage);
  end;

end;

procedure TfMain.aLoadIniExecute(Sender: TObject);
begin
  if dlgOpen.Execute() then
  begin
    FIniFileName := dlgOpen.FileName;
    LoadIni(FIniFileName);
    aSaveIni.Enabled := False;
  end;
end;

procedure TfMain.aSaveIniAsExecute(Sender: TObject);
begin
  if dlgSave.Execute() then
  begin
    FIniFileName := dlgSave.FileName;
    SaveIni(FIniFileName);
  end;
end;

procedure TfMain.aSaveIniExecute(Sender: TObject);
begin
  if FIniFileName = '' then
    aSaveIniAs.Execute
  else
    SaveIni(FIniFileName);
end;

procedure TfMain.FormCreate(Sender: TObject);
var
  AVer: TJvVersionInfo;
begin
  edtSql.Anchors := [akTop, akLeft, akRight];
  FRestAPIQueueeThreadList := TDictionary<string,TRestAPIQueueeThread>.Create;
  //FRestAPIQueueeThread := TRestAPIQueueeThread.Create;
  //FRestAPIQueueeThread.OnCompleteRequest := CompleteRequestAPI;
  //FRestAPIQueueeThread.OnErrorRequest := ErrorRequestAPI;
  Visible := False;
  FDict := CreateDictionaryStringString;
  FResponseQueList := TDictionary<string, IDictionary<string, IDictionaryStringString>>.Create;
  //FResponseQue := TCollections.CreateDictionary<string, IDictionaryStringString>;
  AVer := TJvVersionInfo.Create(ParamStr(0));
  try
    StatusBar.Panels[4].Text := Format(SFmtVersion, [LongVersionToString(AVer.FileLongVersion),
      DateTimeToStr(AVer.VerFileDate)]);
  finally
    AVer.Free;
  end;

  Scripter.LibOptions.UseScriptFiles := True;
  Scripter.LibOptions.SourceFileExt := SPasExt;

  //ReloadHosts;
  LoadHosts;

  FIniFileName := '';
  fmLogTabList := TDictionary<string, TfmLogTab>.Create;

//  fmLogTab := TfmLogTab.Create(Self);
//  fmLogTab.Parent := tsLog;
//  fmLogTab.Align := alClient;
  tmrHostQueuesHint.Interval := HintStyleControllerHostQuees.HintHidePause;
  FHostQueuesGridRecordIndex := -1;
end;

procedure TfMain.FormDestroy(Sender: TObject);
var
  item: TPair<string,TRestAPIQueueeThread>;
  fmlogTabListItemm: TPair<string,TfmLogTab>;
  FResponseQueListItem: TPair<string, IDictionary<string, IDictionaryStringString>>;
  FLoggingFileSWListItem: TPair<string, TStreamWriter>;
  RestAPIQueueeThread: TRestAPIQueueeThread;
  LoggingFileSW: TStreamWriter;
  ResponseQue: IDictionary<string, IDictionaryStringString>;
begin
  //FRestAPIQueueeThread.Terminate;
  //FRestAPIQueueeThread.WaitFor;
  //FRestAPIQueueeThread.Free;

  for item in FRestAPIQueueeThreadList do
    item.Value.Terminate;

  for item in FRestAPIQueueeThreadList do
  begin
    RestAPIQueueeThread := item.Value;
    RestAPIQueueeThread.WaitFor;
    FreeAndNil(RestAPIQueueeThread);
  end;

  FRestAPIQueueeThreadList.Free;

  for fmlogTabListItemm in fmlogTabList do
    fmlogTabListItemm.Value.Free;

  fmlogTabList.Free;

  if Assigned(FLoggingFileSWList) then
  begin
    for FLoggingFileSWListItem in FLoggingFileSWList do
    begin
      LoggingFileSW := FLoggingFileSWListItem.Value;;
      if Assigned(LoggingFileSW) then
      begin
        LoggingFileSW.Close;
        if LoggingFileSW.BaseStream <> nil then
          LoggingFileSW.BaseStream.Free;
        LoggingFileSW.Free;
      end;
    end;
    FLoggingFileSWList.Free;
  end;

  //FResponseQue := nil;
  for FResponseQueListItem in FResponseQueList do
  begin
    ResponseQue := FResponseQueListItem.Value;
    ResponseQue := nil;
  end;

  FResponseQueList.Free;

  FDict := nil;
  TMSLogger.Info(SLogAppStop);
end;

procedure TfMain.FormShow(Sender: TObject);
var
  AllParams: string;
  RunMode: string;
begin
  try
    FGUI := True;

    AllParams := TLooperJob.AllParams();
    TMSLogger.Info(Format(SLogAppStart, [AllParams]));

    if ParamCount >= 1 then
      FIniFileName := ParamStr(1)
    else
      FIniFileName := SIniDefaultFileName;

    if not FileExists(FIniFileName) then
    begin
      StatusBar.Panels[3].Text := Format(SFmtIniNotFound, [FIniFileName]);
      TMSLogger.Error(StatusBar.Panels[3].Text);
      edtIniFileName.Text := FIniFileName;
      aSaveIni.Enabled := True;
      Exit;
    end;

    LoadIni(FIniFileName);

    if FSentryDSN <> '' then
    begin
      FSentryHandler := TMSLogger.RegisterOutputHandlerClass(TSentryOutputHandler, []) as TSentryOutputHandler;
      FSentryHandler.DSN := FSentryDSN;
      FSentryHandler.Tags := 'job,' + ParamStr(1) + ',' + FSentryTags;
      FSentryHandler.LogLevelFilters := FSentryHandler.LogLevelFilters -
        [TTMSLoggerLogLevel.Debug, TTMSLoggerLogLevel.Trace];
      //
      GTextHandler.Active := False;
      TMSLogger.Info(Format(SLogAppStart, [AllParams]));
      GTextHandler.Active := True;
    end;

    //FRestAPIQueueeThread.Retries := seRetries.Value;

    if edtSql.Text <> '' then
      pcApp.ActivePage := tsData
    else
      pcApp.ActivePage := tsConfig;

    lblSQL.Caption := edtSql.Text;

    OpenSQL(False);
    TLooperJob.UpdateDict(mtData, Dict);

    ShowAllQue;

    RunMode := cbRunMode.Text;
    if ParamStr(2) <> '' then
      RunMode := UpperCase(ParamStr(2));

    if RunMode = SParamRUNMIN then
    begin
      WindowState := wsMinimized;
      Visible := True;
      TransAll();
    end
    else if RunMode = SParamRUNSHOW then
    begin
      WindowState := wsNormal;
      Visible := True;
      TransAll();
    end
    else if RunMode = SParamRUNHIDE then
    begin
      WindowState := wsNormal;
      TransAll();
    end
    else if RunMode = SParamRUN then
    begin
      TransAll();
      cbExitOnDone.ItemIndex := Ord(eodAlways);
    end;

  finally
    tmrAPIStatusUpd.Enabled := True;
  end;
end;

procedure TfMain.btnAPIAllClick(Sender: TObject);
begin
  if FLoadingData then
  begin
    ShowMessage(SLoadDataAlready);
    Exit;
  end;

  if FAPIQueuing then
  begin
    ShowMessage(SLoadAPIAlready);
    Exit;
  end;

  if (mtHostQueues.RecordCount = 0) then
  begin
    ShowMessage(SNoLocationConfiguredToBeRun);
    Exit;
  end;

  FAPIQueuing := True;
  TMSLogger.Info(SLAPIAllStart);
  try
    TransAll(IfThen(rbRunForFocusedLocation.Checked,mtHostQueueshost.AsString,''));
  except
    FAPIQueuing := False;
  end;
end;

procedure TfMain.btnAPIselectClick(Sender: TObject);
begin
  APISelected(IfThen(rbRunForFocusedLocation.Checked,mtHostQueueshost.AsString,''));
end;

procedure TfMain.btnAPISelectedStatRecsClick(Sender: TObject);
var
  List: Spring.Collections.IList<TRestCall>;
  KeyCol: TcxGridDBColumn;
  bmk: TArray<System.Byte>;
  I: Integer;
  ARowIndex: Integer;
  ARowInfo: TcxRowInfo;
  Key: Variant;
  HostQRecNo: Integer;

begin
  if (mtHostQueues.RecordCount = 0) then
  begin
    ShowMessage(SNoLocationConfiguredToBeRun);
    Exit;
  end;

  if FLoadingData then
  begin
    ShowMessage(SLoadDataAlready);
    Exit;
  end;

  List := TCollections.CreateList<TRestCall>;
  TMSLogger.Info(SLAPIStatusSelectStart);
  if PrimaryKey = '' then
    raise EClientError.Create(SEPrimaryKeyNotFound);
  KeyCol := dbtvAPIStatus.GetColumnByFieldName(PrimaryKey);

  dbtvHostQueues.BeginUpdate;
  mtHostQueues.DisableControls;
  HostQRecNo := mtHostQueues.RecNo;

  mtData.DisableControls;

  bmk := mtAPIStatus.Bookmark;
  try
    FCreatingResCallsForSelectedStatuses := True;
    for I := 0 to dbtvAPIStatus.DataController.GetSelectedCount - 1 do
    begin
      ARowIndex := dbtvAPIStatus.DataController.GetSelectedRowIndex(I);
      ARowInfo := dbtvAPIStatus.DataController.GetRowInfo(ARowIndex);

      if ARowInfo.Level < dbtvAPIStatus.DataController.Groups.GroupingItemCount then
        Continue
      else
        Key := dbtvAPIStatus.DataController.Values[ARowInfo.RecordIndex, KeyCol.Index];

      dbtvAPIStatus.DataController.FocusedRowIndex := ARowIndex;

      if mtData.Locate(PrimaryKey, Key, []) then
      begin
        if (mtHostQueueshost.AsVariant <> mtAPIStatus.FieldByName('LocationHost').AsVariant) and
           not mtHostQueues.Locate('host',mtAPIStatus.FieldByName('LocationHost').AsVariant,[]) then
          raise Exception.Create(SEInternalLocate)
        else
          AddRestCallToHostQueThread(Key);
      end
      else
        raise Exception.Create(SEInternalLocate);
    end;

    ShowAllQue;
  finally
    FCreatingResCallsForSelectedStatuses := False;
    mtHostQueues.RecNo := HostQRecNo;
    mtHostQueues.EnableControls;
    dbtvHostQueues.EndUpdate;
    mtData.EnableControls;
    mtAPIStatus.Bookmark := bmk;
  end;
end;

procedure TfMain.btnAPITransClick(Sender: TObject);
var
  s: string;
  LScripter: TatPascalScripter;
  HostQRecNo: Integer;
begin
  if (mtHostQueues.RecordCount = 0) then
  begin
    ShowMessage(SNoLocationConfiguredToBeRun);
    Exit;
  end;

  LScripter := GetNewScripter(Scripter.SourceCode.Text);
  try
    TLooperJob.UpdateDict(mtData, Dict);
    s := TLooperJob.DictTrans(edtParameters.Text, mtData, Dict, LScripter);

    HostQRecNo := mtHostQueues.RecNo;
    try
      mtHostQueues.First;
      while not mtHostQueues.Eof do
      begin
        mmoTransl.Lines.Add(GetHostBasUrl(mtHostQueueshost.AsString) + edtResource.Text + '?' + s);
        mtHostQueues.Next;
      end;

    finally
      if mtHostQueues.RecNo <> HostQRecNo then
        mtHostQueues.RecNo := HostQRecNo;
    end;

  finally
    LScripter.Free;
  end;
end;

procedure TfMain.btnCannedClick(Sender: TObject);
var
  name, sql: string;
begin
  if TfCS.Select(name, sql) then
  begin
    edtSql.Text := sql;
  end;
end;

procedure TfMain.btnClearAllQueClick(Sender: TObject);
var
  OldRecNo: integer;
  //item: TPair<string, TRestAPIQueueeThread>;
  RestAPIQueueeThread: TRestAPIQueueeThread;
begin
  OldRecNo := mtHostQueues.RecNo;

  try
    //for item in FRestAPIQueueeThreadList do
    //begin
    // RestAPIQueueeThread := item.Value;
    mtHostQueues.First;
    while not mtHostQueues.Eof do
    begin
      RestAPIQueueeThread := FRestAPIQueueeThreadList.Items[mtHostQueueshost.AsString];
      TTask.Run(
        procedure
        begin
          RestAPIQueueeThread.ClearList;
            TThread.Synchronize(nil,
              procedure
              begin
                if RestAPIQueueeThread.Pause then
                begin
                  RestAPIQueueeThread.Pause := False;

                  mtHostQueues.DisableControls;
                  try
                    if mtHostQueues.Locate('host',RestAPIQueueeThread.Host,[]) and mtHostQueuesPaused.Value then
                    begin
                      mtHostQueues.Edit;
                      mtHostQueuesPaused.Value := False;
                      mtHostQueues.Post;
                    end;
                  finally
                    mtHostQueues.EnableControls;
                  end;
                end;
              end);
        end);
      mtHostQueues.Next;
    end;
  finally
    if mtHostQueues.RecNo <> OldRecNo then
      mtHostQueues.RecNo := OldRecNo;
  end;

  ShowAllQue;
end;

procedure TfMain.btnClearHostQueueClick(Sender: TObject);
begin
  ClearHostQueue;
end;

procedure TfMain.btnDictCrerateClick(Sender: TObject);
begin
  TLooperJob.UpdateDict(mtData, Dict);
end;

procedure TfMain.TransAll(Host:string = '');
var
  LData, LHostQueues: TFDMemTable;
  LDict: IDictionaryStringString;
  LScripter: TatPascalScripter;
  LPrimaryKey, LResource, LParameters: string;
begin
  LData := TFDMemTable.Create(nil);
  LData.Data := mtData.Data;
  LData.First;
  LHostQueues := TFDMemTable.Create(nil);
  LHostQueues.Data :=  mtHostQueues.Data;
  if chkRecno.Checked then
    LData.MoveBy(seRecno.Value);

  LDict := TCollections.CreateDictionary<string, string>();
  LDict.AddRange(Dict);

  LScripter := GetNewScripter(Scripter.SourceCode.Text);

  LPrimaryKey := PrimaryKey;
  //LBaseURL := BaseUrl;
  LResource := edtResource.Text;
  LParameters := edtParameters.Text;
  FErrorCount := 0;
  FRunningQueues := LHostQueues.RecordCount;
  TTask.Run(
    procedure
    var
      LBaseURL : string;
    procedure AddRestCallToHostQueThread;
    begin
      LBaseURL := GetHostBasUrl(LHostQueues.FieldByName('host').AsString);
      FRestAPIQueueeThreadList.Items[LHostQueues.FieldByName('host').AsString].Add(TLooperJob.GetRestCall(LData, LDict, LScripter,
        GetPrimaryKeyValue(LData, LPrimaryKey), LBaseURL, LResource, LParameters));
      TThread.Synchronize(FRestAPIQueueeThreadList.Items[LHostQueues.FieldByName('host').AsString],
        procedure
        var
          OldRecNo: Integer;
        begin
          dbtvHostQueues.BeginUpdate;
          mtHostQueues.DisableControls;
          OldRecNo := mtHostQueues.RecNo;
          try
            if mtHostQueues.Locate('host',LHostQueues.FieldByName('host').AsString, []) then
            begin
              mtHostQueues.Edit;
              mtHostQueuesStartedAmount.Value := mtHostQueuesStartedAmount.AsInteger + 1;
              mtHostQueues.Post;
            end;
          finally
            mtHostQueues.RecNo := OldRecNo;
            mtHostQueues.EnableControls;
            dbtvHostQueues.EndUpdate;
          end;
        end);
    end;
    procedure UpdateRunninQueriesAndtmrEndApp;
    begin
      TThread.Synchronize(FRestAPIQueueeThreadList.Items[LHostQueues.FieldByName('host').AsString],
        procedure
        begin
          FRunningQueues := FRunningQueues  - 1;
          if (FRunningQueues = 0) and (cbExitOnDone.ItemIndex in [Ord(eodAlways), Ord(eodSuccess)]) then
            tmrEndApp.Enabled := True;
        end);
    end;
    begin
      try
        while not LData.Eof do
        begin
//          FRestAPIQueueeThread.Add(TLooperJob.GetRestCall(LData, LDict, LScripter, GetPrimaryKeyValue(LData,
//            LPrimaryKey),
//            LBaseURL, LResource, LParameters));
          if Host = '' then
          begin
            LHostQueues.First;
            while not LHostQueues.Eof do
            begin
              AddRestCallToHostQueThread;
              LHostQueues.Next;
            end;
          end else
          begin
            if not LHostQueues.Locate('Host',Host,[]) then
              raise Exception.Create(SEInternalLocate)
            else
              AddRestCallToHostQueThread;
          end;
          LData.Next;
        end;

//        TThread.Queue(FRestAPIQueueeThreadList.Items[LHostQueues.FieldByName('host').AsString],
//          procedure
//          begin
//            FRunningQueues := FRunningQueues  - 1;
//            if (FRunningQueues = 0) and (cbExitOnDone.ItemIndex in [Ord(eodAlways), Ord(eodSuccess)]) then
//              tmrEndApp.Enabled := True;
//          end);

        if Host = '' then
        begin
          LHostQueues.First;
          while not LHostQueues.Eof do
          begin
            UpdateRunninQueriesAndtmrEndApp;
            LHostQueues.Next;
          end;
        end else
        begin
          if not LHostQueues.Locate('Host',Host,[]) then
            raise Exception.Create(SEInternalLocate)
          else
            UpdateRunninQueriesAndtmrEndApp;
        end
      finally
        LData.Free;
        LHostQueues.Free;
        LScripter.Free;
        LDict := nil;
        FAPIQueuing := False;
      end;
    end);
end;

procedure TfMain.btnOpenClick(Sender: TObject);
begin
  OpenSQL(True);
  TLooperJob.UpdateDict(mtData, Dict);
end;

procedure TfMain.btnResumeAllClick(Sender: TObject);
var
  item: TPair<string, TRestAPIQueueeThread>;
  Resumed: Boolean;
  OldRecNo: Integer;
begin
  Resumed := False;
  for item in FRestAPIQueueeThreadList do
    if item.Value.Pause then
    begin
      item.Value.Pause := False;
      if not Resumed then
        Resumed := True;

      OldRecNo := mtHostQueues.RecNo;
      mtHostQueues.DisableControls;
      try
        if mtHostQueues.Locate('host',item.Key,[]) and mtHostQueuesPaused.Value then
        begin
          mtHostQueues.Edit;
          mtHostQueuesPaused.Value := False;
          mtHostQueues.Post;
        end;
      finally
        mtHostQueues.RecNo := OldRecNo;
        mtHostQueues.EnableControls;
      end;

    end;

  if not Resumed then
    ShowMessage(SResumeQueueAllNotPaused)
  else
    ShowAllQue;
end;

procedure TfMain.btnResumeHostQueueClick(Sender: TObject);
begin
  ResumeHostQueue;
end;

procedure TfMain.btnRunTestsClick(Sender: TObject);
var
  I: Integer;
  AfmLogTab: TfmLogTab;
begin
  if (dbtvHostQueues.Controller.FocusedRow = nil) or not (dbtvHostQueues.Controller.FocusedRow is TcxGridDataRow) then
  begin
    ShowMessage(SNoHostQueueSelected);
    exit;
  end;

  for I := 1 to seRecno.Value do
  begin
    SendToRest('', SMoshe);
    btnRunTests.Caption := I.ToString;
    ShowQue(mtHostQueueshost.AsString);
    Sleep(seSleep.Value);
  end;
  AfmLogTab := fmLogTablist.Items[mtHostQueueshost.AsString];
  AfmLogTab.AddPass('pass 1');
  AfmLogTab.AddPass('pass 2');
  AfmLogTab.AddPass('success 1');
  AfmLogTab.AddPass('success 2');
  AfmLogTab.AddFail('error 1');
  AfmLogTab.AddFail('error 2');
  AfmLogTab.AddFail('exception 1');
  AfmLogTab.AddFail('exception 2');
end;

procedure TfMain.btnTestSQLClick(Sender: TObject);
begin
  OpenSQL(True);
  TLooperJob.UpdateDict(mtData, Dict);
end;

procedure TfMain.btnTestURLClick(Sender: TObject);
begin
  if (dbtvHosts.Controller.FocusedRow = nil) or not (dbtvHosts.Controller.FocusedRow is TcxGridDataRow) then
  begin
    ShowMessage(SNoHostQueueSelected);
    exit;
  end;
  if not dbtvHosts.Controller.FocusedRow.Selected then
  begin
    ShowMessage(SHostNotSelected);
    exit;
  end;

  pcApp.ActivePage := tsData;
  if not mtHostQueues.Locate('host',mtHostshost.AsString, []) then
    exit;

  while dbtvHostQueues.Controller.FocusedRow is TcxGridGroupRow do
     dbtvHostQueues.DataController.GotoNext;

  SendURLtest;
end;

procedure TfMain.cbEnterprisePropertiesChange(Sender: TObject);
begin
  CfgUpd;
end;

procedure TfMain.cbEnterprisePropertiesEditValueChanged(Sender: TObject);
var
  HostsFilter: String;
begin
  if cbEnterprise.EditValue <> FEnterprise then
  begin
    if cbEnterprise.EditValue = null then
      FEnterprise := ''
    else
      FEnterprise := cbEnterprise.EditValue;
    if FEnterprise = '' then
      HostsFilter := ''
    else
      HostsFilter := 'enterprise = '+QuotedStr(Fenterprise);

    if mtHosts.Filter <> HostsFilter then
      mtHosts.Filter := HostsFilter;
    mtHosts.Filtered := mtHosts.Filter <> '';
  end;
end;

procedure TfMain.cbExitOnDonePropertiesChange(Sender: TObject);
begin
  CfgUpd();
end;

procedure TfMain.cbFilterAPIStatusByDataRecClick(Sender: TObject);
begin
  AlterAPIStatusMasterDetailFilter;
end;

procedure TfMain.cbFilterAPIStatusByLocationClick(Sender: TObject);
begin
  AlterAPIStatusMasterDetailFilter;
end;

procedure TfMain.cbRunModePropertiesChange(Sender: TObject);
begin
  CfgUpd();
end;

procedure TfMain.cbUrlHostPropertiesChange(Sender: TObject);
begin
  //CfgUpd();
end;

procedure TfMain.cbUrlHostPropertiesEditValueChanged(Sender: TObject);
begin
  //lblBaseUrl.Caption := BaseUrl;
end;

procedure TfMain.chkLoggingClick(Sender: TObject);
begin
  TMSLogger.Active := chkLogging.Checked;

  CfgUpd();

  chkSeparateLoggingFiles.Enabled := chkLogging.Checked;

  if FIniLoaded and chkLogging.Checked and chkSeparateLoggingFiles.Checked then
    CheckAndUpdateHostLogigingSWList;
end;

procedure TfMain.chkSeparateLoggingFilesClick(Sender: TObject);
begin
  CfgUpd();

  if FIniLoaded and chkLogging.Checked and chkSeparateLoggingFiles.Checked then
    CheckAndUpdateHostLogigingSWList;
end;

procedure TfMain.btnAddAllClick(Sender: TObject);
var
  FieldsChecks: IList<Tuple<string, Boolean>>;
  Fields: IListString;
  Params: IListString;
  fld: TField;
  ns: string;
begin
  Params := TCollections.CreateList<string>(SplitString(edtParameters.Text, SParamSeparator));
  Params.RemoveAll(
    function(const e: string): Boolean
    begin
      Result := e = '';
    end);

  Fields := TCollections.CreateList<string>();
  for fld in mtData.Fields do
    if fld.FieldName <> SFieldAPIStatus then
      Fields.Add(fld.FieldName);

  FieldsChecks := TCollections.CreateList<Tuple<string, Boolean>>();
  Fields.ForEach(
    procedure(const f: string)
    begin
      FieldsChecks.Add(Tuple.Create<string, Boolean>(f, Params.Any(
        function(const p: string): Boolean
        begin
          Result := Pos(STokenBegin + UpperCase(f) + STokenEnd, p) <> 0;
        end)));
    end);

  if not TfAddFields.Run(FieldsChecks) then
    Exit;

  ns := '';
  // all other params first
  Params.Where(
    function(const p: string): Boolean
    begin
      Result := Fields.All(
        function(const f: string): Boolean
        begin
          Result := Pos(STokenBegin + UpperCase(f) + STokenEnd, p) = 0;
        end)
    end).ForEach(
    procedure(const p: string)
    begin
      ns := ns + SParamSeparator + p;
    end);

  // from checks
  FieldsChecks.Where(
    function(const T: Tuple<string, Boolean>): Boolean
    begin
      Result := T.Value2;
    end).ForEach(
    procedure(const T: Tuple<string, Boolean>)
    begin
      ns := ns + SParamSeparator + PascalCase(T.Value1) + '=' + STokenBegin + UpperCase(T.Value1) + STokenEnd;
    end);

  if ns <> '' then
    Delete(ns, 1, Length(SParamSeparator));

  edtParameters.Text := ns;
end;

procedure TfMain.btnAddGroupsClick(Sender: TObject);
var
  AddParams: IListString;
  RemoveParams: IListString;
  Current: IListString;
  AllFields: IListString;
  fld: TField;
  np: string;
  s: string;
begin
  AllFields := TCollections.CreateList<string>();
  for fld in mtData.Fields do
    if fld.FieldName <> SFieldAPIStatus then
      AllFields.Add(fld.FieldName);

  Current := TCollections.CreateList<string>(SplitString(edtParameters.Text, SParamSeparator));
  AddParams := TCollections.CreateList<string>;
  RemoveParams := TCollections.CreateList<string>;

  if TfParamGroups.Run(AddParams, RemoveParams, AllFields) then
  begin

    // add params from current if not exist
    if AddParams.Count > 0 then
    begin
      Current.RemoveAll(
        function(const c: string): Boolean
        begin
          Result := AddParams.Any(
            function(const p: string): Boolean
            begin
              Result := ParamToPair(c).Key = ParamToPair(p).Key;
            end);
        end);
      Current.AddRange(AddParams);
    end;

    // remove params
    if RemoveParams.Count > 0 then
    begin
      Current.RemoveAll(
        function(const c: string): Boolean
        begin
          Result := RemoveParams.Any(
            function(const p: string): Boolean
            begin
              Result := ParamToPair(c).Key = ParamToPair(p).Key;
            end)
        end);
    end;

    np := '';
    for s in Current do
      np := np + SParamSeparator + s;
    if np <> '' then
      Delete(np, 1, Length(SParamSeparator));

    edtParameters.Text := np;
  end;
end;

function TfMain.TrySelectEnterprise(const n: string): Boolean;
var
  i, NewItemIndex: Integer;
begin
  Result := False;
  NewItemIndex := -1;
  for i := 0 to cbEnterprise.Properties.Items.Count - 1 do
    if VarToStr(cbEnterprise.Properties.Items[i].Value) = n then
    begin
      NewItemIndex := i;
      Result := True;
      break;
    end;

  if cbEnterprise.ItemIndex <> NewItemIndex then
    cbEnterprise.ItemIndex := NewItemIndex;
end;

{function TfMain.TrySelectHost(const n: string): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to cbUrlHost.Properties.Items.Count - 1 do
    if VarToStr(cbUrlHost.Properties.Items[I].Value) = n then
    begin
      cbUrlHost.ItemIndex := I;
      Exit(True);
    end;
end;
}

function TfMain.TrySelectHosts(const n: array of string): Boolean;
var
  SelectedHostCount, LastSelectedRecNo: Integer;
begin
  Result := False;
  SelectedHostCount := 0;
  LastSelectedRecNo := -1;

  mtHosts.First;
  while not mtHosts.Eof do
  begin
    if IndexStr(VarToStr(mtHostshost.AsString), n) <> -1 then
    begin
      if dbtvHosts.Controller.FocusedRecord is TcxGridGroupRow then
      begin
        //dbtvHosts.Controller.FocusedRowIndex := dbtvHosts.Controller.FocusedRowIndex + 1;
        dbtvHosts.Controller.FocusedRecord.Expand(True);
        dbtvHosts.Controller.FocusedRecordIndex := dbtvHosts.Controller.FocusedRecordIndex +
          dbtvHosts.GroupedItemCount - dbtvHosts.Controller.FocusedRecord.Level;
      end;
      dbtvHosts.Controller.FocusedRecord.Selected := True;
      SelectedHostCount := SelectedHostCount + 1;
      LastSelectedRecNo := mtHosts.RecNo;
      if SelectedHostCount = Length(n) then
      begin
        Exit(True);
      end;
    end;
    mtHosts.Next;
  end;

  if mtHosts.RecNo <> LastSelectedRecNo then
    mtHosts.RecNo := LastSelectedRecNo;
end;

{procedure TfMain.ReloadHosts();
var
  T: Tuple<string, string>;
  item: TcxImageComboBoxItem;
  host: string;
begin
  cbUrlHost.Properties.Items.Clear;

  host := VarToStr(cbUrlHost.EditValue);

  for T in TfrmHosts.List do
  begin
    item := cbUrlHost.Properties.Items.Add;
    item.Description := T.Value1;
    item.Value := T.Value2;
  end;

  TrySelectHost(host);
end;
}

procedure TfMain.btnAPIHostEditClick(Sender: TObject);
begin
  TfrmHosts.Edit;
  //ReloadHosts;
  LoadHosts;
end;

procedure TfMain.btnClearLogClick(Sender: TObject);
var
 SavedRecNo: Integer;
begin
  mmoSuccess.Lines.Clear;
  mmoError.Lines.Clear;
  SavedRecNo := mtHostQueues.RecNo;
  try
    mtHostQueues.First;
    while not mtHostQueues.Eof do
    begin
      mtHostQueues.Edit;
      mtHostQueuesListCount.Value := 0;
      mtHostQueuesStartedAmount.Value := 0;
      mtHostQueuesSuccessAmount.Value := 0;
      mtHostQueuesErrorAmount.Value := 0;
      mtHostQueues.Post;
      mtHostQueues.Next;
    end;
  finally
    if mtHostQueues.RecNo <> SavedRecNo then
      mtHostQueues.RecNo := SavedRecNo;
  end;
end;

procedure TfMain.btnEditScriptClick(Sender: TObject);
var
  LScripter: TatPascalScripter;
begin
  TfrmScriptEdit.Run(Scripter.SourceCode);
  LScripter := GetNewScripter(Scripter.SourceCode.Text);
  try
    // only check if compilation is ok
  finally
    LScripter.Free;
  end;
  CfgUpd();
end;

procedure TfMain.btnPauseAllClick(Sender: TObject);
var
  item: TPair<string, TRestAPIQueueeThread>;
  Paused: Boolean;
  OldRecNo: integer;
begin
  Paused := False;
  for item in FRestAPIQueueeThreadList do
    if not item.Value.Pause then
    begin
      item.Value.Pause := True;
      if not Paused then
        Paused := True;

      OldRecNo := mtHostQueues.RecNo;
      mtHostQueues.DisableControls;
      try
        if mtHostQueues.Locate('host',item.Key,[]) and not mtHostQueuesPaused.Value then
        begin
          mtHostQueues.Edit;
          mtHostQueuesPaused.Value := True;
          mtHostQueues.Post;
        end;
      finally
        mtHostQueues.RecNo := OldRecNo;
        mtHostQueues.EnableControls;
      end;

    end;

  if not Paused then
    ShowMessage(SPauseAllQueueAlready)
  else
    ShowAllQue;
end;

procedure TfMain.btnPauseHostQueueClick(Sender: TObject);
begin
  PauseHostQueue;
end;

function TfMain.GetCurrentParams: IDictionaryStringString;
var
  r: IDictionaryStringString;
begin
  r := TCollections.CreateDictionary<string, string>();
  TCollections.CreateList<string>(SplitString(edtParameters.Text, SParamSeparator)).ForEach(
    procedure(const n: string)
    var
      p: Integer;
    begin
      p := Pos('=', n);
      if p > 0 then
        r[Copy(n, 1, p - 1)] := Copy(n, p + 1);
    end
    );
  Result := r;
end;

function TfMain.GetHostBasUrl(const HostURL: string): string;
begin
  Result := HostURL + edtUrlPath.Text;
end;

procedure TfMain.btnSelectResAndParClick(Sender: TObject);
var
  Resource: string;
  Params: IListString;
  Current: IDictionaryStringString;
  s: string;
  pair: TPair<string, string>;
begin
  Params := TCollections.CreateList<string>;
  if TfResAndPar.List(edtParameters.Text, Resource, Params) then
  begin
    edtResource.Text := Resource;

    Current := GetCurrentParams();
    Params.ForEach(
      procedure(const n: string)
      var
        p: Integer;
      begin
        p := Pos('=', n);
        if p < 0 then
          Exit;
        Current[Copy(n, 1, p - 1)] := Copy(n, p + 1);
      end);

    s := '';
    for pair in Current do
      s := s + pair.Key + '=' + pair.Value + '&';

    while RightStr(s, 1) = '&' do
      Delete(s, Length(s), 1);

    edtParameters.Text := s;
  end;
end;

procedure TfMain.SaveIni(const AIniFileName: string);
var
  IniFile: TIniFile;
  PasFileName: string;
  HostURLs: string;
begin
  edtIniFileName.Text := AIniFileName;
  IniFile := TIniFile.Create(TPath.Combine(TDirectory.GetCurrentDirectory, AIniFileName));
  try
    IniFile.WriteString(SIniSectionSQL, SIniKeyURI, StrStringToEscaped(edSrcURI.Text));
    IniFile.WriteString(SIniSectionSQL, SIniKeySQL, StrStringToEscaped(edtSql.Text));
    IniFile.WriteString(SIniSectionAPI, SIniKeyEnterprise, FEnterprise);
    //IniFile.WriteString(SIniSectionAPI, SIniKeyURLHost, VarToStr(cbUrlHost.EditValue));
    HostURLs := HostURLs.Join(',', GetSelectedHostURLs);
    IniFile.WriteString(SIniSectionAPI, SIniKeyURLHosts, HostURLs);

    IniFile.WriteString(SIniSectionAPI, SIniKeyURLPath, edtUrlPath.Text);
    IniFile.WriteString(SIniSectionAPI, SIniKeyResource, edtResource.Text);
    IniFile.WriteString(SIniSectionAPI, SIniKeyParameters, edtParameters.Text);
    IniFile.WriteInteger(SIniSectionSETTINGS, SIniKeyEXITONDONE, cbExitOnDone.ItemIndex);
    IniFile.WriteBool(SIniSectionSETTINGS, SIniKeyLOGGING, chkLogging.Checked);
    IniFile.WriteBool(SIniSectionSETTINGS, SIniKeySeparateLoggingFile, chkSeparateLoggingFiles.Checked);
    IniFile.WriteString(SIniSectionSETTINGS, SIniKeySENTRYDSN, edSentryDSN.Text);
    IniFile.WriteString(SIniSectionSETTINGS, SIniKeySENTRYTAGS, edSentryTags.Text);
    IniFile.WriteInteger(SIniSectionSETTINGS, SIniKeySLEEP, seSleep.Value);
    IniFile.WriteInteger(SIniSectionSETTINGS, SIniKeyRETRIES, seRetries.Value);
    IniFile.WriteInteger(SIniSectionSETTINGS, SIniKeyTASKS, seTasks.Value);
    IniFile.WriteString(SIniSectionSQL, SIniKeyRunMode, cbRunMode.Text);
    IniFile.WriteString(SIniSectionSQL, SIniKeyPrimaryKey, edPrimaryKey.Text);
  finally
    IniFile.Free;
  end;
  PasFileName := TPath.Combine(TDirectory.GetCurrentDirectory, TPath.GetFileName(AIniFileName) + SPasExt);
  if Trim(Scripter.SourceCode.Text) <> '' then
    Scripter.SourceCode.SaveToFile(PasFileName, TEncoding.UTF8);
  aSaveIni.Enabled := False;
end;

procedure TfMain.Loaded;
begin
  inherited;
  pcApp.ActivePage := tsData;
end;

procedure TfMain.LoadHosts;
var
  js: TJSONObject;
  enterprise, AEnterprise: string;
  EnterpriseList: TList<string>;
  i: Integer;
  item: TcxImageComboBoxItem;
begin
  if TFile.Exists(HOSTS_FILE_NAME) then
    try
      js := TJSONObject.ParseJSONValue(TFile.ReadAllText(HOSTS_FILE_NAME, TEncoding.UTF8)) as TJSONObject;
      try
        JS2MT(js, mtHosts);
      finally
        js.Free;
      end;
    except
    end;
  mtHosts.Active := True;

  enterprise := VarToStr(cbEnterprise.EditValue);
  cbEnterprise.Properties.Items.Clear;

  EnterpriseList := TList<string>.Create;
  try
    if not mtHosts.Eof then
    begin
      AEnterprise := mtHostsenterprise.AsString;
      EnterpriseList.Add(AEnterprise);
      mtHosts.Next;
    end;

    while not mtHosts.Eof do
    begin
      if mtHostsenterprise.AsString <> AEnterprise then
      begin
        AEnterprise := mtHostsenterprise.AsString;
        if not EnterpriseList.Contains(AEnterprise) then
          EnterpriseList.Add(AEnterprise);
      end;
      mtHosts.Next;
    end;

    for i := 0 to EnterpriseList.Count - 1 do
    begin
      item := cbEnterprise.Properties.Items.Add;
      item.Value := EnterpriseList.Items[i];
      item.Description := EnterpriseList.Items[i];
    end;
  finally
    EnterpriseList.Free;
  end;

  if TrySelectEnterprise(enterprise) then
    FEnterprise := enterprise;

  LoadHostQueues(FHostURLs);
end;

procedure TfMain.LoadIni(const AIniFileName: string);
var
  IniFile: TIniFile;
  PasFileName: string;
  Enterprise: string;
  sql: string;
begin
  edtIniFileName.Text := AIniFileName;
  IniFile := TIniFile.Create(TPath.Combine(TDirectory.GetCurrentDirectory, AIniFileName));
  try
    edSrcURI.Text := StrEscapedToString(IniFile.ReadString(SIniSectionSQL, SIniKeyURI, SIniDefaultSQL));

    sql := StrEscapedToString(IniFile.ReadString(SIniSectionSQL, SIniKeySQL, SIniDefaultSQL));
    edtSql.Text := sql;

    chkLogging.Checked := IniFile.ReadBool(SIniSectionSETTINGS, SIniKeyLOGGING, False);
    chkSeparateLoggingFiles.Checked := IniFile.ReadBool(SIniSectionSETTINGS, SIniKeySeparateLoggingFile, False);

    Enterprise := IniFile.ReadString(SIniSectionAPI, SIniKeyEnterprise, '');
    TrySelectEnterprise(Enterprise);

    //host := IniFile.ReadString(SIniSectionAPI, SIniKeyURLHost, '');
    //TrySelectHost(host);

    FHostURLs := iniFile.ReadString(SIniSectionAPI, SIniKeyURLHosts, '').Split([',',';']);
    TrySelectHosts(FHostURLs);

    LoadHostQueues(FHostURLs);

    edtUrlPath.Text := IniFile.ReadString(SIniSectionAPI, SIniKeyURLPath, SIniDefaultAPIURLPath);

    edtResource.Text := IniFile.ReadString(SIniSectionAPI, SIniKeyResource, '');
    edtParameters.Text := IniFile.ReadString(SIniSectionAPI, SIniKeyParameters, '');
    cbExitOnDone.ItemIndex := IniFile.ReadInteger(SIniSectionSETTINGS, SIniKeyEXITONDONE, Ord(eodNever));
    seSleep.Value := IniFile.ReadInteger(SIniSectionSETTINGS, SIniKeySLEEP, 200);
    seRetries.Value := IniFile.ReadInteger(SIniSectionSETTINGS, SIniKeyRETRIES, 3);
    seTasks.Value := IniFile.ReadInteger(SIniSectionSETTINGS, SIniKeyTASKS, 0);
    edSentryDSN.Text := IniFile.ReadString(SIniSectionSETTINGS, SIniKeySENTRYDSN, '');
    FSentryDSN := edSentryDSN.Text;
    edSentryTags.Text := IniFile.ReadString(SIniSectionSETTINGS, SIniKeySENTRYTAGS, '');
    FSentryTags := edSentryTags.Text;
    cbRunMode.ItemIndex := cbRunMode.Properties.Items.IndexOf(IniFile.ReadString(SIniSectionSQL, SIniKeyRunMode,
      SParamDESIGN));
    edPrimaryKey.Text := IniFile.ReadString(SIniSectionSQL, SIniKeyPrimaryKey, '');
  finally
    IniFile.Free;
  end;
  PasFileName := TPath.Combine(TDirectory.GetCurrentDirectory, TPath.GetFileName(AIniFileName) + SPasExt);
  if FileExists(PasFileName) then
    Scripter.SourceCode.LoadFromFile(PasFileName);
  aSaveIni.Enabled := False;
  aSaveIniAs.Enabled := True;
  FIniLoaded := True;
end;

procedure TfMain.mtHostQueuesAfterDelete(DataSet: TDataSet);
begin
  RefreshPanelQueueCount;
end;

procedure TfMain.mtHostQueuesAfterOpen(DataSet: TDataSet);
begin
  RefreshPanelQueueCount;
end;

procedure TfMain.mtHostQueuesAfterPost(DataSet: TDataSet);
begin
  RefreshPanelQueueCount;
end;

procedure TfMain.mtHostQueuesCalcFields(DataSet: TDataSet);
begin
  if mtHostQueuesCFullLocationName.AsString = '' then
    mtHostQueuesCFullLocationName.AsString := mtHostQueuesLenterprise.AsString + ' ' + mtHostQueuesname.AsString;
  if mtHostQueuesCDoneAmount.IsNull or
    (mtHostQueuesCDoneAmount.AsInteger <> mtHostQueuesSuccessAmount.AsInteger + mtHostQueuesErrorAmount.AsInteger) then
    mtHostQueuesCDoneAmount.AsInteger := mtHostQueuesSuccessAmount.AsInteger + mtHostQueuesErrorAmount.AsInteger;
end;

procedure TfMain.mtHostsAfterScroll(DataSet: TDataSet);
begin
  lblBaseUrl.Caption := BaseUrl;
end;

procedure TfMain.mtHostsCalcFields(DataSet: TDataSet);
begin
  if mtHostsFullName.AsString = '' then
    mtHostsFullName.AsString := mtHostsenterprise.AsString + ' ' + mtHostsname.AsString;
end;

procedure TfMain.CfgUpd;
begin
  if FIniLoaded then
  begin
    aSaveIni.Enabled := True;
    aSaveIniAs.Enabled := True;
  end;
end;

procedure TfMain.OpenSQL(const AProcessMessages: Boolean);
var
  sql: string;
  Data: TFDMemTable;
  Error: string;
  QD: IFuture<Boolean>;
  Progress: Boolean;
  uri: string;
  //strfld : TStringField;
begin
  if FLoadingData then
  begin
    ShowMessage(SLoadDataAlready);
    Exit;
  end;

  if (Trim(edtSql.Text) = '') or (Trim(edtSql.Text) = SIniDefaultSQL) then
  begin
    if FGUI then
      pcApp.ActivePage := tsConfig;
    Exit;
  end;

  Progress := FGUI and not AProcessMessages; // show progress when not in blocking mode and GUI

  FLoadingData := True;

  FPrimayKeyChanged:= False;

  if FGUI then
    (StatusBar.Panels[2].PanelStyle as TdxStatusBarStateIndicatorPanelStyle).Indicators.Items[0].IndicatorType
      := sitRed;
  try
    if Progress then
      TfrmProgress.Start(0, 5);

  // capture for task
    uri := edSrcURI.Text;
    sql := edtSql.Text;

    Data := TFDMemTable.Create(nil);
    try
      Error := '';
      QD := TTask.Future<Boolean>(

        function(): Boolean
        var
          Connection: TFDConnection;
          Query: TFDQuery;
          I: Integer;
        begin
          try
            Connection := TFDConnection.Create(nil);
            try
              Connection.Assign(connData);
              SetupConnection(uri, Connection);
              Query := TFDQuery.Create(Connection);
              Query.Connection := Connection;
              Query.FetchOptions.CursorKind := ckForwardOnly;
              Query.FetchOptions.Unidirectional := True;
              Query.FetchOptions.RowsetSize := 1000;
              Query.SQL.Text := sql;
              Query.Open();

              if (edPrimaryKey.Text <> '') and (Query.FindField(edPrimaryKey.Text) <> nil) then
                FPrimaryKey := Query.FieldByName(edPrimaryKey.Text).FieldName
              else
              begin
                FPrimaryKey := '';
                I := 0;
                while (I < Query.Fields.Count) and (FPrimaryKey = '') do
                begin
                  if pfInKey in Query.Fields[I].ProviderFlags then
                    FPrimaryKey := Query.Fields[I].FieldName;
                  Inc(I);
                end;
              end;

              // backup
              if (FPrimaryKey = '') then
              if (Query.FindField(SFieldCode) <> nil) then
                FPrimaryKey := SFieldCode
              else if (Query.FindField(SFieldCode1) <> nil) then
                FPrimaryKey := SFieldCode1
              else if (Query.FindField(SFieldCode2) <> nil) then
                FPrimaryKey := SFieldCode2
              else if Query.Fields.Count > 0 then
                FPrimaryKey := Query.Fields[0].FieldName;

              Data.FieldDefs.Assign(Query.FieldDefs);
              //Data.FieldDefs.Add(SFieldAPIStatus, TFieldType.ftString, 200);
              Data.CreateDataSet;
              for i := 0 to Data.Fields.Count - 1 do
                Data.Fields[i].ReadOnly := False;
              Data.CopyDataSet(Query, [coRestart, coAppend]);
              if FPrimaryKey <> '' then
                Data.IndexFieldNames := FPrimaryKey;
              Result := True;
            finally
              Connection.Free;
            end;
          except
            Result := False;
            Error := Exception(ExceptObject()).Message;
          end;
        end);

      if Progress then
        TfrmProgress.Step;

      while QD.Status <> TTaskStatus.Completed do
      begin
        if AProcessMessages then
          Application.ProcessMessages;
        Sleep(1);
      end;

      if Progress then
        TfrmProgress.Step();

      if QD.Value then
      begin
        if FGUI then
        begin
          dbtvData.BeginUpdate();
          dbtvAPIStatus.BeginUpdate();
        end;
        try
          mtData.Active := False;
          mtData.Data := Data.Data;
          mtData.IndexFieldNames := Data.IndexFieldNames;
          mtData.First;
          if FGUI then
          begin
            dbtvData.ClearItems;
            dbtvData.DataController.CreateAllItems();
            dbtvData.DataController.KeyFieldNames := PrimaryKey;
            // dbtvData.DataController.DataModeController.SmartRefresh := True;
          end;

          //mtAPIStatus.Active := False;
          if not mtAPIStatus.Active and (mtAPIStatus.FieldDefs.Count = 0) then
          begin
            mtAPIStatus.FieldDefs.Add('LocationHost', TFieldType.ftString, 200);
            mtAPIStatus.FieldDefs.Add('LocationName', TFieldType.ftString, 101);
            mtAPIStatus.FieldDefs.Add(PrimaryKey, Data.FieldByName(PrimaryKey).DataType, Data.FieldByName(PrimaryKey).Size);
            mtAPIStatus.FieldDefs.Add(SFieldAPIStatus, TFieldType.ftString, 200);
            mtAPIStatus.CreateDataSet;
          end;
          if not mtAPIStatus.Active then
            mtAPIStatus.Active := True;

          if FAPIStatusDataMasterDS = nil then
          begin
            FAPIStatusDataMasterDS := TDataSource.Create(self);
            FAPIStatusDataMasterDS.DataSet := mtData;
          end;
          if FAPIStatusLocationMasterDS = nil then
          begin
            FAPIStatusLocationMasterDS := TDataSource.Create(self);
            FAPIStatusLocationMasterDS.DataSet := mtHostQueues;
          end;

          AlterAPIStatusMasterDetailFilter;

          if FGUI then
          begin
            dbtvAPIStatus.ClearItems;
            dbtvAPIStatus.DataController.CreateAllItems();
            dbtvAPIStatus.Columns[0].Visible := False;
            dbtvAPIStatus.Columns[1].Caption := 'Location';
            dbtvAPIStatus.Columns[1].Width := 150;
            //dbtvAPIStatus.Columns[2].Width := 100; // primary key
            dbtvAPIStatus.Columns[3].Width := 150; // API Status
            dbtvAPIStatus.DataController.KeyFieldNames := 'LocationHost;'+PrimaryKey;
            if mtHostQueues.Active and (mtHostQueues.RecordCount <= 1) then
            begin
              dbtvAPIStatus.Columns[1].Visible := False;
              dbtvAPIStatus.Columns[2].Visible := False;
            end;
          end;

        finally
          if FGUI then
          begin
            dbtvData.EndUpdate;
            dbtvAPIStatus.EndUpdate;
            StatusBar.Panels[0].Text := Format(SFmtRows, [mtData.RecordCount]);
          end;
        end;

        if Progress then
          TfrmProgress.Step();

        if FGUI then
        begin
          dbtvData.ApplyBestFit();
          lblSQL.Caption := sql;
          pcApp.ActivePage := tsData;
        end;

      end
      else
        raise EClientError.CreateFmt(SEFmtSQLTask, [Error]);

      if Progress then
        TfrmProgress.Step();

    finally
      Data.Free;
    end;

  finally
    if Progress then
      TfrmProgress.Stop;

    FLoadingData := False;
    if FGUI then
      (StatusBar.Panels[2].PanelStyle as TdxStatusBarStateIndicatorPanelStyle).Indicators.Items[0].IndicatorType :=
        sitYellow;
  end;

  StatusBar.Panels[3].Text := Format(SFmtPrimaryKey, [PrimaryKey]) + DupeString(' ', 8);
  if not tmrAPIStatusUpd.Enabled then
    tmrAPIStatusUpd.Enabled := True;
end;

procedure TfMain.SendToRest(const AKey, ATrans: string);
var
  LCurrRestCall: TRestCall;
begin
  if mtHostQueues.RecordCount = 0 then
    Exit;

  LCurrRestCall.PrimaryKeyValue := AKey;
  //LCurrRestCall.BaseUrl := BaseUrl;
  LCurrRestCall.BaseUrl := GetHostBasUrl(mtHostQueueshost.AsString);
  LCurrRestCall.Resource := edtResource.Text;
  LCurrRestCall.Params := ATrans;
  FRestAPIQueueeThreadList.Items[mtHostQueueshost.AsString].Add(LCurrRestCall);
end;

procedure TfMain.CloseApp;
var
  item: TPair<string, TRestAPIQueueeThread>;
begin
  //FRestAPIQueueeThread.ClearList;
  for item in FRestAPIQueueeThreadList do
    item.Value.ClearList;

  Application.Terminate;
end;

procedure TfMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseApp;
end;

procedure TfMain.edPrimaryKeyPropertiesChange(Sender: TObject);
begin
  CfgUpd();
end;

procedure TfMain.edPrimaryKeyPropertiesValidate(Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  FPrimayKeyChanged := True;
end;

procedure TfMain.edSentryDSNPropertiesChange(Sender: TObject);
begin
  FSentryDSN := edSentryDSN.Text;
  FSentryTags := edSentryTags.Text;
  if FSentryHandler <> nil then
  begin
    FSentryHandler.DSN := FSentryDSN;
    FSentryHandler.Tags := FSentryTags;
  end;
  CfgUpd();
end;

procedure TfMain.edtIniFileNameChange(Sender: TObject);
begin
  CfgUpd();
end;

procedure TfMain.edtIniFileNamePropertiesChange(Sender: TObject);
begin
  CfgUpd();
end;

procedure TfMain.edtParametersPropertiesChange(Sender: TObject);
begin
  CfgUpd();
end;

procedure TfMain.edtResourceChange(Sender: TObject);
begin
  CfgUpd();
end;

procedure TfMain.edtSqlDocumentChanged(Sender: TObject);
begin
  CfgUpd();
end;

procedure TfMain.edtUrlPathChange(Sender: TObject);
begin
  lblBaseUrl.Caption := BaseUrl;
  CfgUpd();

  if FIniLoaded then
  begin
    if (edtUrlPath.Text = '') then
    begin
      ShowMessage(sURLPathEmpty);
      StatusBar.Panels[3].Text := sURLPathEmpty;
    end
    else
    if StatusBar.Panels[3].Text = sURLPathEmpty then
      StatusBar.Panels[3].Text := '';
  end else
  begin
    if (edtUrlPath.Text = '') then
      StatusBar.Panels[3].Text := sURLPathEmpty;
  end;
end;

procedure TfMain.CompleteRequestAPI(const Host: string; const ARestCall: TRestCall);
var
  response: IDictionary<string, string>;
begin
  AddLog(Host, TTMSLoggerLogLevel.Trace, Format(SFmtSuccess, [ARestCall.ResponseText]));
  if FGUI then
  begin
    ShowQue(Host);
    fmLogTablist.Items[Host].AddPass(ARestCall.ResponseText);
  end;

  if ARestCall.PrimaryKeyValue = '' then
  begin
    AddLog(Host, TTMSLoggerLogLevel.Error, Format(SFmtError, ['Primary key value stored on rest call is empty.']));
    Exit;
  end;

  response := ARestCall.FlatKV;
  if (response <> nil) and (response.Count = 0) then
    AddLog(Host, TTMSLoggerLogLevel.Error, Format(SFmtError, ['Count of response fields is zero.']));

  //if (response <> nil) and (response.Count > 0) then
  if (response <> nil) then
    FResponseQueList.Items[Host].Items[ARestCall.PrimaryKeyValue] := response;
end;

procedure TfMain.dbtvAPIStatusDataControllerFilterChanged(Sender: TObject);
begin
  SynchronizeAPIStatusWithData;
end;

procedure TfMain.dbtvAPIStatusFocusedRecordChanged(Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  if AFocusedRecord = nil then
    Exit;
  SynchronizeAPIStatusWithData;
end;

procedure TfMain.dbtvDataDataControllerFilterChanged(Sender: TObject);
begin
  FilterAPIStatusByDataRecord;

  SynchronizeDataWithAPISatus;
end;

procedure TfMain.dbtvDataFocusedRecordChanged(Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  FilterAPIStatusByDataRecord;

  if AFocusedRecord = nil then
    Exit;

  SynchronizeDataWithAPISatus;
end;

procedure TfMain.dbtvHostQueuesCellClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  if ACellViewInfo.Item = dbtvHostQueuesPaused then
  begin
    AHandled := True;
    if not mtHostQueuesPaused.AsBoolean then
      PauseHostQueue
    else
      ResumeHostQueue;
  end
  else if ACellViewInfo.Item = dbtvHostQueuesRunAll then
  begin
    AHandled := True;
    if FLoadingData then
    begin
      ShowMessage(SLoadDataAlready);
      Exit;
    end;

    if FAPIQueuing then
    begin
      ShowMessage(SLoadAPIAlready);
      Exit;
    end;

    try
      FAPIQueuing := True;
      mtHostQueues.DisableControls;
      try
        mtHostQueues.Edit;
        mtHostQueuesRunSelected.Value := True;
        mtHostQueues.Post;
      finally
        mtHostQueues.EnableControls;
      end;
      Application.ProcessMessages;

      TMSLogger.Info(Format(SLAPIHostAllStart,[mtHostQueuesCFullLocationName.AsString]));
      try
        TransAll(mtHostQueueshost.AsString);
      except
        FAPIQueuing := False;
      end;

    finally
      try
        mtHostQueues.Edit;
        mtHostQueuesRunSelected.Value := False;
        mtHostQueues.Post;
      finally
        mtHostQueues.EnableControls;
      end;
      Application.ProcessMessages;
    end
  end
  else if ACellViewInfo.Item = dbtvHostQueuesRunSelected then
  begin
    AHandled := True;
    try
      mtHostQueues.DisableControls;
      try
        mtHostQueues.Edit;
        mtHostQueuesRunSelected.Value := True;
        mtHostQueues.Post;
      finally
        mtHostQueues.EnableControls;
      end;
      Application.ProcessMessages;

      APISelected(mtHostQueueshost.AsString);
    finally
      try
        mtHostQueues.Edit;
        mtHostQueuesRunSelected.Value := False;
        mtHostQueues.Post;
      finally
        mtHostQueues.EnableControls;
      end;
      Application.ProcessMessages;
    end
  end
  else if ACellViewInfo.Item = dbtvHostQueuesClearQueue then
  begin
    AHandled := True;
    ClearHostQueue;
  end;
end;

procedure TfMain.dbtvHostsSelectionChanged(Sender: TcxCustomGridTableView);
begin
  CfgUpd;
end;

procedure TfMain.ErrorRequestAPI(const Host: string; const ARestCall: TRestCall; const AErrorMessage: string);
begin
  AddLog(Host, TTMSLoggerLogLevel.Error, Format(SFmtError, [AErrorMessage]));

  if ARestCall.PrimaryKeyValue <> '' then
  begin
    //FResponseQue.AddOrSetValue(ARestCall.PrimaryKeyValue, CreateDictionaryStringString(SFieldAPIStatus, AErrorMessage));
    FResponseQueList.Items[Host].AddOrSetValue(ARestCall.PrimaryKeyValue,
      CreateDictionaryStringString(SFieldAPIStatus, AErrorMessage));
  end;
  if FGUI then
  begin
    ShowQue(Host);
    fmLogTablist.Items[Host].AddFail(AErrorMessage);
  end;

  Inc(FErrorCount);
end;

procedure TfMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  TotalQueListCount: Integer;
  MessageResult: Integer;
begin
  TotalQueListCount := GetTotalQueListCount;
  CanClose := TotalQueListCount = 0;

  if not CanClose then
    Exit;

  if aSaveIni.Enabled then
  begin
    MessageResult := MessageBox(0, PChar('Save changes?'), PChar('Close'), MB_ICONWARNING or MB_YESNOCANCEL);

    case MessageResult of
      IDYES:
        begin
          aSaveIniExecute(nil);
          CanClose := CanClose and not aSaveIni.Enabled;
        end;
      IDNO:
        begin
          // Do nothing
        end;
      IDCANCEL:
        begin
          CanClose := False;
        end;
    end;
  end else
  begin
    MessageResult := MessageBox(0, PChar('Do you approve to close the form?'), PChar('Close'), MB_ICONWARNING or MB_YESNO);

    if MessageResult = IDYES then
      CanClose := True
    else
      CanClose := False;
  end;
end;

function TfMain.GetBaseUrl: string;
begin
  //Result := VarToStr(cbUrlHost.EditValue) + edtUrlPath.Text;
  Result := mtHostshost.AsString + edtUrlPath.Text;
end;

function TfMain.GetNewScripter(const ASource: string): TatPascalScripter;
begin
  Result := TatPascalScripter.Create(nil);
  try
    Result.SourceCode.Text := Scripter.SourceCode.Text;
    Result.LibOptions.UseScriptFiles := True;
    Result.LibOptions.SourceFileExt := SPasExt;
    Result.Compile;
  except
    Result.Free;
    raise;
  end;
end;

function TfMain.GetPrimaryKeyValue(AData: TDataSet; const APrimaryKey: string): string;
var
  Field: TField;
begin
  Result := '';
  if (APrimaryKey = '') then
    Exit;
  Field := AData.FindField(APrimaryKey);
  if Field = nil then
    Exit;
  Result := Field.AsString;
end;

function TfMain.GetSelectedHostURLs: TArray<System.string>;
var
  i, SavedFocusedRowIndex: Integer;
begin
  SavedFocusedRowIndex := dbtvHosts.Controller.FocusedRowIndex;

  try
    SetLength(Result, 0);
    for i := 0 to dbtvHosts.Controller.SelectedRowCount - 1 do
    begin
      // Expanding collapsed groups
      if dbtvHosts.Controller.SelectedRows[i] = nil then
        continue;
      dbtvHosts.Controller.FocusedRowIndex := dbtvHosts.Controller.SelectedRows[i].Index;
      if (dbtvHosts.Controller.FocusedRow is TcxGridGroupRow) then
      begin
        if not (TcxGridGroupRow(dbtvHosts.Controller.FocusedRow).Expanded) then
          TcxGridGroupRow(dbtvHosts.Controller.FocusedRow).Expand(False);
      end;
    end;

    for i := 0 to dbtvHosts.Controller.SelectedRowCount - 1 do
    begin
      dbtvHosts.Controller.FocusedRowIndex := dbtvHosts.Controller.SelectedRows[i].Index;
      if (dbtvHosts.Controller.FocusedRow is TcxGridGroupRow) then
        continue;
      SetLength(Result, Length(Result)+1);
      Result[Length(Result) - 1] := mtHostshost.AsString;
    end;
  finally
    if dbtvHosts.Controller.FocusedRowIndex <> SavedFocusedRowIndex then
      dbtvHosts.Controller.FocusedRowIndex := SavedFocusedRowIndex;
  end;
end;

procedure TfMain.pcAppChange(Sender: TObject);
begin
  if (pcApp.ActivePage = tsData) and FPrimayKeyChanged then
    btnOpenClick(Self);
end;

procedure TfMain.pcAppPageChanging(Sender: TObject; NewPage: TcxTabSheet; var AllowChange: Boolean);
var
  Password: string;
begin
  if (NewPage = tsConfig) then
  begin
    if FAPIQueuing then
    begin
      ShowMessage(SLoadAPI);
      AllowChange := False;
      Exit;
    end;

    if mtHostQueues.Active and mtHostQueues.AggregatesActive and mtHostQueues.Aggregates.Items[0].Active and
       (mtHostQueues.Aggregates.Items[0].Value <> null) and (mtHostQueues.Aggregates.Items[0].Value > 0) then
    begin
      ShowMessage(SQueuedTaskExistTabCannotBeChanged);
      AllowChange := False;
      Exit;
    end;

    if FConfigPassword = '' then
    begin
      Password := '';
      AllowChange := InputQuery('Password', #31'Prompt', Password);
      AllowChange := AllowChange and (Password = '9999');
      if AllowChange then
        FConfigPassword := Password;
    end;
  end;

  if pcApp.ActivePage = tsConfig then
    LoadHostQueues(GetSelectedHostURLs);
end;

procedure TfMain.SendURLtest();
var
  LCurrRestCall: TRestCall;
begin
  //LCurrRestCall.BaseUrl := BaseUrl + STestMethod;
  LCurrRestCall.BaseUrl := GetHostBasUrl(mtHostQueueshost.AsString) + STestMethod;
  LCurrRestCall.Resource := STestResource;
  FRestAPIQueueeThreadList.Items[mtHostQueueshost.AsString].Add(LCurrRestCall);
end;

procedure TfMain.seRetriesChange(Sender: TObject);
var
  item: TPair<string, TRestAPIQueueeThread>;
begin
  CfgUpd();
  for item in FRestAPIQueueeThreadList do
    item.Value.Retries := seRetries.Value;
end;

procedure TfMain.seSleepPropertiesChange(Sender: TObject);
begin
  CfgUpd();
end;

procedure TfMain.seTasksChange(Sender: TObject);
var
  item: TPair<string, TRestAPIQueueeThread>;
begin
  CfgUpd();
  for item in FRestAPIQueueeThreadList do
    item.Value.SetNumTasks(seTasks.Value);
end;

procedure TfMain.ShowAllQue;
var
  item: TPair<string, TRestAPIQueueeThread>;
  OldRecNo : Integer;
begin
  dbtvHostQueues.BeginUpdate;
  mtHostQueues.DisableControls;
  OldRecNo := mtHostQueues.RecNo;
  try
    for item in FRestAPIQueueeThreadList do
    begin
      if mtHostQueues.Locate('host',item.Key) and (mtHostQueuesListCount.AsVariant <> item.Value.ListCount) then
      begin
        mtHostQueues.Edit;
        mtHostQueuesListCount.Value := item.Value.ListCount;
        mtHostQueues.Post;
      end;
    end;
  finally
    mtHostQueues.RecNo := OldRecNo;
    mtHostQueues.EnableControls;
    dbtvHostQueues.EndUpdate;
  end;
end;

procedure TfMain.ShowQue(Host: string);
var
  queListCount, OldRecNo: integer;
begin
  dbtvHostQueues.BeginUpdate;
  mtHostQueues.DisableControls;
  OldRecNo := mtHostQueues.RecNo;
  try
    if mtHostQueues.Locate('host', Host) then
    begin
      queListCount := FRestAPIQueueeThreadList.Items[Host].ListCount;
      if mtHostQueuesListCount.Asvariant <> queListCount then
      begin
        mtHostQueues.Edit;
        mtHostQueuesListCount.Value := queListCount;
        mtHostQueues.Post;
      end;
    end;
  finally
    mtHostQueues.RecNo := OldRecNo;
    mtHostQueues.EnableControls;
    dbtvHostQueues.EndUpdate;
  end;
end;

const
  SResponseFieldNameFmt = 'response_%s';

procedure TfMain.AddNewFields(AFields: ISet<string>);
var
  Field: string;
  col: TcxGridDBColumn;
begin
  for Field in AFields do
  begin
    //mtData.AddNewField(Format(SResponseFieldNameFmt, [LowerCase(Field)]), ftWideString, 200);
    mtAPIStatus.AddNewField(Format(SResponseFieldNameFmt, [LowerCase(Field)]), ftWideString, 200);
  end;

  for Field in AFields do
  begin
//    col := dbtvData.CreateColumn;
//    col.Caption := Field;
//    col.DataBinding.FieldName := Format(SResponseFieldNameFmt, [LowerCase(Field)]);
//    col.Width := 100;

    col := dbtvAPIStatus.CreateColumn;
    col.Caption := Field;
    col.DataBinding.FieldName := Format(SResponseFieldNameFmt, [LowerCase(Field)]);
    col.Width := 100;
  end;
end;

procedure TfMain.AddRestCallToHostQueThread(DataKey: variant);
var
  j: Integer;
begin
  if ((mtAPIStatus.FieldByName(PrimaryKey).AsVariant = DataKey) and
      (mtAPIStatus.FieldByName('LocationHost').AsString = mtHostQueueshost.AsString)) or
     mtAPIStatus.Locate('LocationHost;' + PrimaryKey, vararrayof([mtHostQueueshost.AsString, DataKey]), []) then
  begin
    mtAPIStatus.Edit;
    mtAPIStatus[SFieldAPIStatus] := Null;
    for j := mtAPIStatus.FieldByName(SFieldAPIStatus).Index to mtAPIStatus.FieldCount - 1 do
      mtAPIStatus.Fields[j].Value := Null;
    mtAPIStatus.Post;
  end;
  FRestAPIQueueeThreadList.Items[mtHostQueueshost.AsString].Add(TLooperJob.GetRestCall(mtData, Dict, Scripter,
    GetPrimaryKeyValue(mtData, PrimaryKey), GetHostBasUrl(mtHostQueueshost.AsString), edtResource.Text, edtParameters.Text));

  mtHostQueues.Edit;
  mtHostQueuesStartedAmount.Value := mtHostQueuesStartedAmount.AsInteger + 1;
  mtHostQueues.Post;
end;

procedure TfMain.edSrcURIChange(Sender: TObject);
begin
  CfgUpd();
end;

procedure TfMain.HandleResponseQue;
var
  ResponseQueListItem: TPair<string, IDictionary<string, IDictionaryStringString>>;
  ResponseQue: IDictionary<string, IDictionaryStringString>;
  NewFields: ISet<string>;
  R: TPair<string, IDictionaryStringString>;
  F: TPair<string, string>;
  fld: TField;
  SavedRecNo, SavedAPIStatusRecNo, SavedHostQueueRecNo: Integer;
  SavedAPIStatusMasterSource: TDataSource;
  FullHostName: string;
  LastMessage: string;
begin
  dbtvData.BeginUpdate();
  mtData.DisableControls;
  dbtvAPIStatus.BeginUpdate();
  mtAPIStatus.DisableControls;

  SavedRecNo := mtData.RecNo;
  SavedAPIStatusRecNo := mtAPIStatus.RecNo;
  SavedAPIStatusMasterSource := mtAPIStatus.MasterSource;
  mtAPIStatus.MasterSource := nil;
  try
    for ResponseQueListItem in FResponseQueList do
    begin
      ResponseQue := ResponseQueListItem.Value;
      // identify new fields in response
      NewFields := TCollections.CreateSet<string>();
      //for R in FResponseQue do
      for R in ResponseQue do
        for F in R.Value do
          if not SameText(F.Key, SFieldAPIStatus) then
            NewFields.Add(F.Key);

      NewFields.RemoveAll(
        function(const F: string):Boolean
        begin
          //Result := mtData.FindField(Format(SResponseFieldNameFmt, [F])) <> nil;
          Result := mtAPIStatus.FindField(Format(SResponseFieldNameFmt, [F])) <> nil;
        end);

      // add them all if any
      if not NewFields.IsEmpty then
        AddNewFields(NewFields);

      //for R in FResponseQue do
      for R in ResponseQue do
      begin
        if (R.Key <> '') and mtData.Locate(PrimaryKey, R.Key, []) then
        begin
//          mtData.Edit;
//          if R.Value.ContainsKey(SFieldAPIStatus) then
//            mtData.FieldByName(SFieldAPIStatus).AsString := R.Value[SFieldAPIStatus];
//
//          for F in R.Value do
//          begin
//            fld := mtData.FindField(Format(SResponseFieldNameFmt, [LowerCase(F.Key)]));
//            if Assigned(fld) then
//              fld.AsString := F.Value;
//          end;
//
//          mtData.Post;
          dbtvHostQueues.BeginUpdate;
          mtHostQueues.DisableControls;
          SavedHostQueueRecNo := mtHostQueues.RecNo;
          try
            if mtHostQueues.Locate('host', ResponseQueListItem.Key, []) then
            begin
              if R.Value.ContainsKey(SFieldAPIStatus) then
                LastMessage := R.Value[SFieldAPIStatus]
              else
                LastMessage := '';

              mtHostQueues.Edit;
              if LastMessage.ToLowerInvariant = 'success' then
                mtHostQueuesSuccessAmount.Value := mtHostQueuesSuccessAmount.AsInteger + 1
              else
                mtHostQueuesErrorAmount.Value := mtHostQueuesErrorAmount.AsInteger + 1;
              mtHostQueuesLastMessage.Value := LastMessage;
              mtHostQueues.Post;
            end
            else
            begin
              AddLog(ResponseQueListItem.Key, TTMSLoggerLogLevel.Error, Format(SFmtError, ['Location qeue record related to response message could not be located.']));
              Exit;
            end;

            FullHostName := GetFullHostName(ResponseQueListItem.Key);
            if (mtAPIStatus.RecordCount = 0) or
               (((mtAPIStatus.FieldByName('LocationHost').AsString <> mtHostQueueshost.AsString) or
                 (mtAPIStatus.FieldByName(PrimaryKey).AsVariant <> R.Key)) and
               not mtAPIStatus.Locate('LocationHost;'+PrimaryKey, vararrayof([ResponseQueListItem.Key, R.Key]), [])) then
            begin
              mtAPIStatus.Append;
              mtAPIStatus.FieldByName('LocationHost').AsVariant := ResponseQueListItem.Key;
              mtAPIStatus.FieldByName('LocationName').AsVariant := FullHostName;
              mtAPIStatus.FieldByName(PrimaryKey).AsVariant := R.Key;
            end
            else
              mtAPIStatus.Edit;

            if R.Value.ContainsKey(SFieldAPIStatus) then
              mtAPIStatus.FieldByName(SFieldAPIStatus).AsString := R.Value[SFieldAPIStatus];

            for F in R.Value do
            begin
              fld := mtAPIStatus.FindField(Format(SResponseFieldNameFmt, [LowerCase(F.Key)]));
              if Assigned(fld) then
                fld.AsString := F.Value;
            end;

            mtAPIStatus.Post;
          finally
            mtHostQueues.RecNo := SavedHostQueueRecNo;
            mtHostQueues.EnableControls;
            dbtvHostQueues.EndUpdate;
          end;
        end
        else
        begin
          AddLog(ResponseQueListItem.Key, TTMSLoggerLogLevel.Error, Format(SFmtError, ['Data record related to response message could not be located.']));
          Exit;
        end;
      end;

      //FResponseQue.Clear;
      ResponseQue.Clear;
    end;
  finally
    //mtData.Bookmark := bmk;

    mtData.RecNo := SavedRecNo;

    mtData.EnableControls;
    dbtvData.EndUpdate;

    if mtAPIStatus.MasterSource <> SavedAPIStatusMasterSource then
      mtAPIStatus.MasterSource := SavedAPIStatusMasterSource;
    mtAPIStatus.RecNo := SavedAPIStatusRecNo;

    mtAPIStatus.EnableControls;
    dbtvAPIStatus.EndUpdate;
  end;
end;

procedure TfMain.tmrAPIStatusUpdTimer(Sender: TObject);
var
  ResponseQueListItem: TPair<string, IDictionary<string, IDictionaryStringString>>;
  ResponseQue: IDictionary<string, IDictionaryStringString>;
begin
//  if (FResponseQue <> nil) and (FResponseQue.Count > 0) and (PrimaryKey <> '') and
//     (SecondsBetween(FLastDataGridUpdate, Now()) > 2) then
//  begin
//    HandleResponseQue;
//    FLastDataGridUpdate := Now();
//  end;

  if (PrimaryKey <> '') and (SecondsBetween(FLastDataGridUpdate, Now()) > 2) then
  begin
    for ResponseQueListItem in FResponseQueList do
    begin
      ResponseQue := ResponseQueListItem.Value;
      if (ResponseQue <> nil) and (ResponseQue.Count > 0) then
      begin
        HandleResponseQue;
        FLastDataGridUpdate := Now();
      end;
    end;
  end;

  ShowAllQue;
end;

procedure TfMain.tmrEndAppTimer(Sender: TObject);
var
  TotalQueListCount: integer;
begin
  TotalQueListCount := GetTotalQueListCount;
  if (TotalQueListCount = 0) and
    ((cbExitOnDone.ItemIndex = Ord(eodAlways)) or
    ((cbExitOnDone.ItemIndex = Ord(eodSuccess)) and (FErrorCount = 0))
    ) then
  begin
    Sleep(5000);
    CloseApp;
  end;
end;

procedure TfMain.tmrHostQueuesHintTimer(Sender: TObject);
begin
  tmrHostQueuesHint.Enabled := False;
  HintStyleControllerHostQuees.HideHint;
  Application.ProcessMessages;
  //FHintDisplayed := False;
end;

procedure TfMain.SetupConnection(const AConnectionString: string; const AConnection: TFDConnection);
var
  rttiType: TRttiType;
  rttiProperty: TRttiProperty;
begin
  if AConnectionString = '' then
  begin
    AConnection.Params.Clear;
    AConnection.DriverName := 'ADS';
    AConnection.Params.Add('Alias=TisWin3DD');
    AConnection.Params.Add('Password=lezaM123$51');
    AConnection.Params.Add('User_Name=User1');
  end
  else
  begin
    AConnection.Params.Clear;
    AConnection.ConnectionString := AConnectionString;
    if (AConnection.ActualDriverID = 'CDataExcel') then
    if TType.TryGetType(AConnection.Params.ClassInfo, rttiType) then
    begin
      try
        if not rttiType.TryGetProperty('AccessKey', rttiProperty) then
        begin
          // CData Version is less than 22.0
          AConnection.Params.Add(
            'RTK=5258464741423541473656424E47585942344159534D000000000000000000000000000000000000535852575254415300004E59553946544D473737564A0000');
        end
        else
          rttiProperty.Free;
      finally
        rttiType.Free;
      end;
    end;

{      AConnection.DriverName := uri.Protocol;
    for kv in SplitString(uri.URLDecode(uri.Params), '&') do
      AConnection.Params.Add(kv);
    if (uri.Host <> '')
        and TType.TryGetType(AConnection.Params.ClassInfo, rttiType)
        and rttiType.TryGetProperty('Server', rttiProperty)
        and rttiProperty.PropertyType.IsString
    then
      rttiProperty.SetValue(AConnection.Params, uri.Host);

    if (uri.Port <> '')
        and TType.TryGetType(AConnection.Params.ClassInfo, rttiType)
        and rttiType.TryGetProperty('Port', rttiProperty)
        and (rttiProperty.PropertyType.TypeKind = System.TTypeKind.tkInteger)
    then
      rttiProperty.SetValue(AConnection.Params, StrToIntDef(uri.Port, 0));}

  end;
end;

procedure TfMain.RefreshPanelQueueCount;
begin
  if not mtHostQueues.Aggregates.Items[0].Active then
    mtHostQueues.Aggregates.Items[0].Active := True;
  if mtHostQueues.Aggregates.Items[0].Value <> null then
    StatusBar.Panels[1].Text := Format(SFmtQue, [VarToInteger(mtHostQueues.Aggregates.Items[0].Value)])
  else
    StatusBar.Panels[1].Text := Format(SFmtQue, [0]);
end;

procedure TfMain.SynchronizeDataWithAPISatus;
begin
  if FCreatingResCallsForSelectedStatuses then
    Exit;

  if (mtData.RecordCount = 0) or (mtAPIStatus.RecordCount = 0) then
    Exit;

  if mtAPIStatus.FieldByName(PrimaryKey).AsVariant <> mtData.FieldByName(PrimaryKey).AsVariant then
    mtAPIStatus.Locate(PrimaryKey, mtData.FieldByName(PrimaryKey).AsVariant, []);
end;

procedure TfMain.FilterAPIStatusByDataRecord;
var
  FltrValueStr: string;
  NewFilter: string;
begin
  if cbFilterAPIStatusByLocation.Checked and cbFilterAPIStatusByDataRec.Checked then
  begin
    if mtData.Active and (mtData.RecordCount > 0) then
    begin
      if mtData.FieldByName(PrimaryKey).DataType in [Data.DB.ftString, Data.DB.ftWideString, Data.DB.ftMemo, Data.DB.ftWideMemo] then
        FltrValueStr := QuotedStr(mtData.FieldByName(PrimaryKey).AsString)
      else
        FltrValueStr := mtData.FieldByName(PrimaryKey).AsString;
      NewFilter := PrimaryKey + ' = ' + FltrValueStr;
    end
    else
      NewFilter := '';
    if mtAPIStatus.Filter <> NewFilter then
      mtAPIStatus.Filter := NewFilter;
    mtAPIStatus.Filtered := mtAPIStatus.Filter <> '';
  end;
end;

procedure TfMain.AlterAPIStatusMasterDetailFilter;
begin
  if cbFilterAPIStatusByDataRec.Checked and not cbFilterAPIStatusByLocation.Checked then
  begin
    if mtAPIStatus.MasterSource <> nil then
      mtAPIStatus.MasterSource := nil;
    mtAPIStatus.MasterFields := PrimaryKey;
    mtAPIStatus.IndexFieldNames := PrimaryKey;
    mtAPIStatus.MasterSource := FAPIStatusDataMasterDS;
  end
  else if cbFilterAPIStatusByLocation.Checked then
  begin
    if mtAPIStatus.MasterSource <> nil then
      mtAPIStatus.MasterSource := nil;
    mtAPIStatus.MasterFields := mtHostQueueshost.FieldName;
    mtAPIStatus.IndexFieldNames := 'LocationHost';
    mtAPIStatus.MasterSource := FAPIStatusLocationMasterDS;
    if not cbFilterAPIStatusByDataRec.Checked then
    begin
      if mtAPIStatus.Filtered then
        mtAPIStatus.Filtered := False;
    end
    else
     FilterAPIStatusByDataRecord;
  end
  else
  begin
    if not cbFilterAPIStatusByDataRec.Checked then
      if mtAPIStatus.Filtered then
        mtAPIStatus.Filtered := False;

    if mtAPIStatus.MasterSource <> nil then
      mtAPIStatus.MasterSource := nil;
  end;
end;

procedure TfMain.SynchronizeAPIStatusWithData;
begin
  if (mtData.RecordCount = 0) or (mtAPIStatus.RecordCount = 0) then
    Exit;

  if mtData.FieldByName(PrimaryKey).AsVariant <> mtAPIStatus.FieldByName(PrimaryKey).AsVariant then
    mtData.Locate(PrimaryKey, mtAPIStatus.FieldByName(PrimaryKey).AsVariant, []);
end;

procedure TfMain.APISelected(Host:string = '');
var
  List: Spring.Collections.IList<TRestCall>;
  KeyCol: TcxGridDBColumn;
  bmk: TArray<System.Byte>;
  I: Integer;
  ARowIndex: Integer;
  ARowInfo: TcxRowInfo;
  Key: Variant;
  HostQRecNo: Integer;

//  procedure AddRestCallToHostQueThread;
//  var
//    j: Integer;
//  begin
//    if ((mtAPIStatus.FieldByName(PrimaryKey).AsVariant = DataKey) and
//        (mtAPIStatus.FieldByName('LocationHost').AsString = mtHostQueueshost.AsString)) or
//       mtAPIStatus.Locate('LocationHost;' + PrimaryKey, vararrayof([mtHostQueueshost.AsString, DataKey]), []) then
//    begin
//      mtAPIStatus.Edit;
//      mtAPIStatus[SFieldAPIStatus] := Null;
//      for j := mtAPIStatus.FieldByName(SFieldAPIStatus).Index to mtAPIStatus.FieldCount - 1 do
//        mtAPIStatus.Fields[j].Value := Null;
//      mtAPIStatus.Post;
//    end;
//    FRestAPIQueueeThreadList.Items[mtHostQueueshost.AsString].Add(TLooperJob.GetRestCall(mtData, Dict, Scripter, GetPrimaryKeyValue(mtData, PrimaryKey), GetHostBasUrl(mtHostQueueshost.AsString), edtResource.Text, edtParameters.Text));
//
//    mtHostQueues.Edit;
//    mtHostQueuesStartedAmount.Value := mtHostQueuesStartedAmount.AsInteger + 1;
//    mtHostQueues.Post;
//  end;

begin
  if FLoadingData then
  begin
    ShowMessage(SLoadDataAlready);
    Exit;
  end;

  if FAPIQueuing then
  begin
    ShowMessage(SLoadAPIAlready);
    Exit;
  end;

  if (mtHostQueues.RecordCount = 0) then
  begin
    ShowMessage(SNoLocationConfiguredToBeRun);
    Exit;
  end;

  List := TCollections.CreateList<TRestCall>;
  if Host <> '' then
    TMSLogger.Info(SLAPISelectStart)
  else
    TMSLogger.Info(SLAPISelectStart);
  if PrimaryKey = '' then
    raise EClientError.Create(SEPrimaryKeyNotFound);
  KeyCol := dbtvData.GetColumnByFieldName(PrimaryKey);
  bmk := mtData.Bookmark;
  mtData.DisableControls;
  try
    for I := 0 to dbtvData.DataController.GetSelectedCount - 1 do
    begin
      ARowIndex := dbtvData.DataController.GetSelectedRowIndex(I);
      ARowInfo := dbtvData.DataController.GetRowInfo(ARowIndex);
      if ARowInfo.Level < dbtvData.DataController.Groups.GroupingItemCount then
        Continue
      else
        Key := dbtvData.DataController.Values[ARowInfo.RecordIndex, KeyCol.Index];
      if mtData.Locate(PrimaryKey, Key, []) then
      begin
        //mtData.Edit;
        //mtData[SFieldAPIStatus] := Null;
        //mtData.Post;
        //        List.Add(TLooperJob.GetRestCall(mtData, Dict, Scripter, GetPrimaryKeyValue(mtData, PrimaryKey), BaseUrl,
        //          edtResource.Text, edtParameters.Text));
        dbtvHostQueues.BeginUpdate;
        mtHostQueues.DisableControls;
        HostQRecNo := mtHostQueues.RecNo;
        try
          if Host = '' then
          begin
            mtHostQueues.First;
            while not mtHostQueues.Eof do
            begin
              AddRestCallToHostQueThread(Key);
              mtHostQueues.Next;
            end;
          end else
          begin
            if not mtHostQueues.Locate('host',Host,[]) then
              raise Exception.Create(SEInternalLocate)
            else
              AddRestCallToHostQueThread(Key);
          end;
        finally
          mtHostQueues.RecNo := HostQRecNo;
          mtHostQueues.EnableControls;
          dbtvHostQueues.EndUpdate;
        end;
      end
      else
        raise Exception.Create(SEInternalLocate);
    end;
    //FRestAPIQueueeThread.Add(List);
    ShowAllQue;
  finally
    mtData.Bookmark := bmk;
    mtData.EnableControls;
  end;
end;

procedure TfMain.HostQueueHideHint;
begin
  tmrHostQueuesHint.Enabled := False;
  HintStyleControllerHostQuees.HideHint;
  FHintDisplayed := False;
  Application.ProcessMessages;
end;

procedure TfMain.ClearHostQueue;
var
  RestAPIQueueeThread: TRestAPIQueueeThread;
begin
  if (dbtvHostQueues.Controller.FocusedRow = nil) or not (dbtvHostQueues.Controller.FocusedRow is TcxGridDataRow) then
  begin
    ShowMessage(SNoHostQueueSelected);
    exit;
  end;

  mtHostQueues.DisableControls;
  try
    mtHostQueues.Edit;
    mtHostQueuesClearQueue.Value := True;
    mtHostQueues.Post;
  finally
    mtHostQueues.EnableControls;
  end;
  Application.ProcessMessages;

  RestAPIQueueeThread := FRestAPIQueueeThreadList.Items[mtHostQueueshost.AsString];
  TTask.Run(
    procedure
    begin
      RestAPIQueueeThread.ClearList;
      TThread.Synchronize(nil,
      procedure
      begin
        if RestAPIQueueeThread.Pause then
          RestAPIQueueeThread.Pause := False;
        mtHostQueues.DisableControls;
        try
          mtHostQueues.Edit;
          if mtHostQueuesPaused.AsBoolean then
            mtHostQueuesPaused.Value := False;
          mtHostQueuesClearQueue.Value := False;
          mtHostQueues.Post;
        finally
          mtHostQueues.EnableControls;
        end;
        ShowQue(mtHostQueueshost.AsString);
      end);
    end);
end;

procedure TfMain.ResumeHostQueue;
var
  RestAPIQueueeThread: TRestAPIQueueeThread;
begin
  if (dbtvHostQueues.Controller.FocusedRow = nil) or not (dbtvHostQueues.Controller.FocusedRow is TcxGridDataRow) then
  begin
    ShowMessage(SNoHostQueueSelected);
    exit;
  end;

  RestAPIQueueeThread := FRestAPIQueueeThreadList.Items[mtHostQueueshost.AsString];
  if RestAPIQueueeThread.Pause then
  begin
    RestAPIQueueeThread.Pause := False;
    mtHostQueues.DisableControls;
    try
      mtHostQueues.Edit;
      mtHostQueuesPaused.Value := False;
      mtHostQueues.Post;
    finally
      mtHostQueues.EnableControls;
    end;
    HostQueueHideHint;
    ShowQue(mtHostQueueshost.AsString);
  end
  else
    ShowMessage(SResumeQueueNotPaused);
end;

procedure TfMain.PauseHostQueue;
var
  RestAPIQueueeThread: TRestAPIQueueeThread;
begin
  if (dbtvHostQueues.Controller.FocusedRow = nil) or not (dbtvHostQueues.Controller.FocusedRow is TcxGridDataRow) then
  begin
    ShowMessage(SNoHostQueueSelected);
    exit;
  end;

  RestAPIQueueeThread := FRestAPIQueueeThreadList.Items[mtHostQueueshost.AsString];
  if not RestAPIQueueeThread.Pause then
  begin
    RestAPIQueueeThread.Pause := True;
    mtHostQueues.DisableControls;
    try
      mtHostQueues.Edit;
      mtHostQueuesPaused.Value := True;
      mtHostQueues.Post;
    finally
      mtHostQueues.EnableControls;
    end;
    HostQueueHideHint;
    ShowQue(mtHostQueueshost.AsString);
  end
  else
    ShowMessage(SPauseQueueAlready);
end;



function TfMain.GetFullHostName(const Host: string): string;
var
  HostLookupValue: Variant;
begin
  Result := '';
  HostLookupValue := mtHosts.Lookup('host', Host, 'enterprise;name');
  if not VarIsNull(HostLookupValue) then
    Result := VarToStr(HostLookupValue[0]) + ' ' + VarToStr(HostLookupValue[1]);
end;


procedure TfMain.CheckAndUpdateHostLogigingSWList;
var
  SavedRecNo: Integer;
begin
  if not Assigned(FLoggingFileSWList) then
    FLoggingFileSWList := TDictionary<string, TStreamWriter>.Create;

  SavedRecNo := mtHostQueues.RecNo;
  mtHostQueues.DisableControls;
  try
    mtHostQueues.First;
    while not mtHostQueues.Eof do
    begin
      if not FLoggingFileSWList.ContainsKey(mtHostQueueshost.AsString) then
        AddHostLogginStreamWriter;
      mtHostQueues.Next;
    end;
  finally
    mtHostQueues.RecNo := SavedRecNo;
    mtHostQueues.EnableControls;
  end;
end;
procedure TfMain.AddHostLogginStreamWriter;
var
  LoggingFileStream: TFileStream;
  LoggingFileSW: TStreamWriter;
begin
  if chkLogging.Checked and chkSeparateLoggingFiles.Checked then
  begin
    if not Assigned(FLoggingFileSWList) then
      FLoggingFileSWList := TDictionary<string, TStreamWriter>.Create;
    LoggingFileStream := TFileStream.Create(SLogDir + mtHostQueuesLenterprise.AsString.Trim + ' ' +
      mtHostQueuesname.AsString.Trim + SLogExt, fmCreate or fmOpenReadWrite or fmShareDenyNone);

    LoggingFileSW := TStreamWriter.Create(LoggingFileStream, TEncoding.UTF8);
    FLoggingFileSWList.Add(mtHostQueueshost.AsString, LoggingFileSW);
  end;
end;
function TfMain.GetTotalQueListCount: Integer;
var
  item: System.Generics.Collections.TPair<string, TRestAPIQueueeThread>;
begin
  Result := 0;
  for item in FRestAPIQueueeThreadList do
    Result := Result + item.Value.ListCount;
end;

procedure TfMain.LoadHostQueues(HostList: Tarray<System.string>);
var
  i: Integer;
  OldHostQueuesRecordCount: Integer;
  HostNameV: Variant;
  RestAPIQueueeThread: TRestAPIQueueeThread;
  tsHostLog: TcxTabSheet;
  AfmLogTab: TfmLogTab;
  LoggingFileSW: TStreamWriter;
  AResponseQue: IDictionary<string, IDictionaryStringString>;
begin
  if not mtHostQueues.Active then
    mtHostQueues.Active := True;
  // remove unselected hosts from host queues dataset
  mtHostQueues.First;
  while not mtHostQueues.Eof do
  begin
    if IndexStr(mtHostQueueshost.AsString, HostList) = -1 then
    begin
      RestAPIQueueeThread := FRestAPIQueueeThreadList.Items[mtHostQueueshost.AsString];
      if RestAPIQueueeThread.ListCount = 0 then
      begin
        RestAPIQueueeThread.Terminate;
        RestAPIQueueeThread.WaitFor;
        FreeAndNil(RestAPIQueueeThread);
        FRestAPIQueueeThreadList.Remove(mtHostQueueshost.AsString);

        AfmLogTab := fmLogTabList.Items[mtHostQueueshost.AsString];
        if AfmLogTab.Parent <> nil then
          tsHostLog := TcxTabSheet(AfmLogTab.Parent)
        else
          tsHostLog := nil;
        AfmLogTab.Free;
        if tsHostLog <> nil then
        tsHostLog.Free;
        fmLogTabList.Remove(mtHostQueueshost.AsString);

        if Assigned(FLoggingFileSWList) then
        begin
          LoggingFileSW := FLoggingFileSWList.Items[mtHostQueueshost.AsString];
          if Assigned(LoggingFileSW) then
          begin
            LoggingFileSW.Close;
            if LoggingFileSW.BaseStream <> nil then
              LoggingFileSW.BaseStream.Free;
            LoggingFileSW.Free;
          end;
          FLoggingFileSWList.Remove(mtHostQueueshost.AsString);

        end;

        AResponseQue := FResponseQueList.Items[mtHostQueueshost.AsString];
        if AResponseQue.Count = 0 then
        begin
          AResponseQue := nil;
          FResponseQueList.Remove(mtHostQueueshost.AsString);
        end;

        mtHostQueues.Delete;
      end;
    end
    else
      mtHostQueues.Next;
  end;
  OldHostQueuesRecordCount := mtHostQueues.RecordCount;
  // add newly selected hosts to host queues dataset
  for i := 0 to Length(HostList) - 1 do
  begin
    HostNameV := mtHosts.Lookup('host', HostList[i], 'name');
    if HostNameV <> null then
      if (OldHostQueuesRecordCount = 0) or not mtHostQueues.Locate('host', HostList[i], []) then
      begin
        mtHostQueues.Append;
        mtHostQueueshost.AsString := HostList[i];
        mtHostQueuesname.AsString := HostNameV;
        mtHostQueues.Post;

        if not FRestAPIQueueeThreadList.ContainsKey(mtHostQueueshost.AsString) then
        begin
          RestAPIQueueeThread := TRestAPIQueueeThread.Create;
          FRestAPIQueueeThreadList.Add(mtHostQueueshost.AsString, RestAPIQueueeThread);
          RestAPIQueueeThread.Host := HostList[i];
          RestAPIQueueeThread.OnCompleteRequest := CompleteRequestAPI;
          RestAPIQueueeThread.OnErrorRequest := ErrorRequestAPI;
          RestAPIQueueeThread.Retries := seRetries.Value;
        end;

        if not fmLogTabList.ContainsKey(mtHostQueueshost.AsString) then
        begin
          AfmLogTab := TfmLogTab.Create(self);
          AfmLogTab.Name := 'fmLogTab'+stringReplace(mtHostQueuesname.AsString + '_' + mtHostQueuesLenterprise.AsString, ' ', '_', [rfReplaceAll]);
          fmLogTabList.Add(mtHostQueueshost.AsString, AfmLogTab);
          tsHostLog := tcxTabSheet.Create(self);
          tsHostLog.PageControl := pcApp;
          tsHostLog.Caption := mtHostQueuesname.AsString + ' Log';
          AfmLogTab.Parent := tsHostLog;
          AfmLogTab.Align := alClient;
          AfmLogTab.Host := mtHostQueueshost.AsString;
        end;

        if chkLogging.Checked and chkSeparateLoggingFiles.Checked then
          AddHostLogginStreamWriter;

        if not FResponseQueList.ContainsKey(mtHostQueueshost.AsString) then
          FResponseQueList.Add(mtHostQueueshost.AsString, TCollections.CreateDictionary<string, IDictionaryStringString>);
      end;
  end;
  mtHostQueues.First;

  if mtHostQueues.RecordCount > 1 then
  begin
    pgcQueues.ActivePage := tsHostQueues;
    rbRunForAllLocations.Visible := True;
    rbRunForFocusedLocation.Visible := True;
    cbFilterAPIStatusByLocation.Visible := True;
    pnlAPIStatusBottom.Height := cbFilterAPIStatusByLocation.Top + cbFilterAPIStatusByLocation.Height + 15;
  end
  else
  begin
    pgcQueues.ActivePage := tsHostQueue;
    rbRunForAllLocations.Visible := False;
    rbRunForFocusedLocation.Visible := False;
    cbFilterAPIStatusByLocation.Visible := False;
    pnlAPIStatusBottom.Height := cbFilterAPIStatusByDataRec.Top + cbFilterAPIStatusByDataRec.Height + 15;
  end;
end;

procedure TfMain.dbtvHostQueuesMouseLeave(Sender: TObject);
begin
  inherited;
  if FHintDisplayed then
    HostQueueHideHint;
end;

procedure TfMain.dbtvHostQueuesMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  AHitTest: TcxCustomGridHitTest;
  AHint: string;
begin
  inherited;

  if not (Sender is TcxGridSite) then
    Exit;

  AHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(X, Y);
  if (AHitTest.HitTestCode <> htCell) or (X > (TcxGridSite(Sender).Width - 18)) then
  begin
    HostQueueHideHint;
    Exit;
  end;

  ///check the current record and column over which the mouse is placed
  if (FHostQueuesGridRecordIndex <> TcxGridRecordCellHitTest(AHitTest).GridRecord.RecordIndex) or not FHintDisplayed then
  begin
    //redisplay hint window is mouse has been moved to a new cell
    HintStyleControllerHostQuees.HideHint;
    tmrHostQueuesHint.Enabled := False;
    //store the current record and column
    FHostQueuesItem := TcxGridRecordCellHitTest(AHitTest).Item;
    FHostQueuesGridRecordIndex := TcxGridRecordCellHitTest(AHitTest).GridRecord.RecordIndex;
    //obtain the current cell value
    if dbtvHostQueues.DataController.Values[TcxGridRecordCellHitTest(AHitTest).GridRecord.RecordIndex, dbtvHostQueuesPaused.Index] = true then
      AHint := 'Paused'
    else
      AHint := 'Unpaused';

    with dbtvHostQueues.Site.ClientToScreen(Point(X, Y)) do
    begin
      // show hint
      FHintDisplayed := True;
      HintStyleControllerHostQuees.ShowHint(X, Y, '', AHint);
    end;
    //start the hide hint timer
    tmrHostQueuesHint.Enabled := True;
  end;
end;

initialization

finalization

TMSLogger.OutputHandlers.Clear;

end.
