object frmHosts: TfrmHosts
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Locations'
  ClientHeight = 421
  ClientWidth = 529
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 384
    Width = 529
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      529
      37)
    object Button1: TButton
      Left = 441
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Close'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
  end
  object grdHosts: TcxGrid
    Left = 0
    Top = 0
    Width = 529
    Height = 384
    Align = alClient
    TabOrder = 1
    object dbtvHosts: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Append.Visible = True
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = dsHosts
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <
        item
          Links = <
            item
              Column = dbtvHostsenterprise
            end>
          SummaryItems = <
            item
              Kind = skCount
              Column = dbtvHostsname
            end>
        end>
      EditForm.UseDefaultLayout = False
      OptionsBehavior.EditMode = emInplaceEditFormHideCurrentRow
      OptionsData.Appending = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object dbtvHostsname: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        LayoutItem = dbtvHostsLayoutItem1.Owner
        SortIndex = 1
        SortOrder = soAscending
        Width = 139
      end
      object dbtvHostshost: TcxGridDBColumn
        Caption = 'host'
        DataBinding.FieldName = 'host'
        LayoutItem = dbtvHostsLayoutItem2.Owner
        Width = 297
      end
      object dbtvHostsenterprise: TcxGridDBColumn
        DataBinding.FieldName = 'enterprise'
        Visible = False
        VisibleForExpressionEditor = bTrue
        GroupIndex = 0
        LayoutItem = dbtvHostsLayoutItem3.Owner
        SortIndex = 0
        SortOrder = soDescending
        VisibleForEditForm = bTrue
        VisibleForRowLayout = bTrue
        Width = 139
      end
      object dbtvHostsRootGroup: TcxGridInplaceEditFormGroup
        AlignHorz = ahLeft
        AlignVert = avTop
        CaptionOptions.Text = 'Template Layout'
        ButtonOptions.Buttons = <>
        Hidden = True
        ShowBorder = False
        Index = -1
      end
      object dbtvHostsLayoutItem1: TcxGridInplaceEditFormLayoutItem
        Parent = dbtvHostsAutoCreatedGroup1.Owner
        AlignHorz = ahLeft
        AlignVert = avTop
        Index = 0
      end
      object dbtvHostsLayoutItem2: TcxGridInplaceEditFormLayoutItem
        Parent = dbtvHostsRootGroup
        Index = 1
      end
      object dbtvHostsLayoutItem3: TcxGridInplaceEditFormLayoutItem
        Parent = dbtvHostsAutoCreatedGroup1.Owner
        AlignVert = avClient
        Index = 2
      end
      object dbtvHostsAutoCreatedGroup1: TdxLayoutAutoCreatedGroup
        Parent = dbtvHostsRootGroup
        AlignVert = avTop
        LayoutDirection = ldHorizontal
        Index = 0
      end
      object dbtvHostsSpaceItem1: TdxLayoutEmptySpaceItem
        Parent = dbtvHostsAutoCreatedGroup1.Owner
        SizeOptions.Height = 10
        SizeOptions.Width = 10
        CaptionOptions.Text = 'Empty Space Item'
        Index = 1
      end
    end
    object grdHostsLevel1: TcxGridLevel
      GridView = dbtvHosts
    end
  end
  object mtHosts: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 336
    Top = 24
    object mtHostsname: TStringField
      DisplayLabel = 'Name'
      FieldName = 'name'
      Required = True
      Size = 50
    end
    object mtHostshost: TStringField
      DisplayLabel = 'URL Host'
      FieldName = 'host'
      Required = True
      Size = 200
    end
    object mtHostsenterprise: TStringField
      DisplayLabel = 'Enterprise'
      FieldName = 'enterprise'
      Required = True
      Size = 50
    end
  end
  object dsHosts: TDataSource
    DataSet = mtHosts
    Left = 336
    Top = 72
  end
end
